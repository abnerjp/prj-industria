
CREATE SEQUENCE public.perda_seq;

CREATE TABLE public.perda (
                per_codigo INTEGER NOT NULL DEFAULT nextval('public.perda_seq'),
                per_nome VARCHAR(50) NOT NULL,
                per_sigla VARCHAR(6),
                per_ativo BOOLEAN NOT NULL,
                per_observacao VARCHAR(200),
                CONSTRAINT perda_pk PRIMARY KEY (per_codigo)
);


ALTER SEQUENCE public.perda_seq OWNED BY public.perda.per_codigo;

CREATE SEQUENCE public.area_seq;

CREATE TABLE public.area (
                are_codigo INTEGER NOT NULL DEFAULT nextval('public.area_seq'),
                are_nome VARCHAR(50) NOT NULL,
                are_descricao VARCHAR(100),
                are_ativo BOOLEAN NOT NULL,
                are_observacao VARCHAR(200),
                CONSTRAINT area_pk PRIMARY KEY (are_codigo)
);


ALTER SEQUENCE public.area_seq OWNED BY public.area.are_codigo;

CREATE SEQUENCE public.acesso_seq;

CREATE TABLE public.acesso (
                ace_codigo INTEGER NOT NULL DEFAULT nextval('public.acesso_seq'),
                ace_data DATE NOT NULL,
                ace_ativo VARCHAR NOT NULL,
                ace_observacao VARCHAR(200),
                CONSTRAINT acesso_pk PRIMARY KEY (ace_codigo)
);


ALTER SEQUENCE public.acesso_seq OWNED BY public.acesso.ace_codigo;

CREATE SEQUENCE public.pais_seq;

CREATE TABLE public.pais (
                pai_codigo SMALLINT NOT NULL DEFAULT nextval('public.pais_seq'),
                pai_sigla VARCHAR(10) NOT NULL,
                pai_nome VARCHAR(100) NOT NULL,
                pai_ativo BOOLEAN NOT NULL,
                pai_observacao VARCHAR(200),
                CONSTRAINT pai_pk PRIMARY KEY (pai_codigo)
);


ALTER SEQUENCE public.pais_seq OWNED BY public.pais.pai_codigo;

CREATE SEQUENCE public.estado_seq;

CREATE TABLE public.estado (
                est_codigo INTEGER NOT NULL DEFAULT nextval('public.estado_seq'),
                est_nome VARCHAR(100) NOT NULL,
                est_sigla VARCHAR(10) NOT NULL,
                est_ativo BOOLEAN NOT NULL,
                est_observacao VARCHAR(200),
                pai_codigo SMALLINT NOT NULL,
                CONSTRAINT estado_pk PRIMARY KEY (est_codigo)
);


ALTER SEQUENCE public.estado_seq OWNED BY public.estado.est_codigo;

CREATE SEQUENCE public.cidade_seq;

CREATE TABLE public.cidade (
                cid_codigo INTEGER NOT NULL DEFAULT nextval('public.cidade_seq'),
                cid_nome VARCHAR(70) NOT NULL,
                cid_ativo BOOLEAN NOT NULL,
                cid_observacao VARCHAR(200),
                est_codigo INTEGER NOT NULL,
                CONSTRAINT cidade_pk PRIMARY KEY (cid_codigo)
);


ALTER SEQUENCE public.cidade_seq OWNED BY public.cidade.cid_codigo;

CREATE SEQUENCE public.endereco_seq;

CREATE TABLE public.endereco (
                end_codigo INTEGER NOT NULL DEFAULT nextval('public.endereco_seq'),
                end_logradouro VARCHAR(100) NOT NULL,
                end_cep VARCHAR(8),
                end_bairro VARCHAR(50) NOT NULL,
                end_observacao VARCHAR(200),
                cid_codigo INTEGER NOT NULL,
                CONSTRAINT endereco_pk PRIMARY KEY (end_codigo)
);


ALTER SEQUENCE public.endereco_seq OWNED BY public.endereco.end_codigo;

CREATE SEQUENCE public.usuario_seq;

CREATE TABLE public.usuario (
                usu_codigo INTEGER NOT NULL DEFAULT nextval('public.usuario_seq'),
                usu_nome VARCHAR(30) NOT NULL,
                usu_sexo VARCHAR(1) NOT NULL,
                usu_login VARCHAR(20) NOT NULL,
                usu_senha VARCHAR(100) NOT NULL,
                usu_email VARCHAR(70),
                usu_imagem VARCHAR,
                usu_ativo BOOLEAN NOT NULL,
                usu_observacao VARCHAR(200),
                CONSTRAINT usu_codigo_pk PRIMARY KEY (usu_codigo)
);
COMMENT ON COLUMN public.usuario.usu_imagem IS 'salva o endereco da imagem';


ALTER SEQUENCE public.usuario_seq OWNED BY public.usuario.usu_codigo;

CREATE TABLE public.endereco_usuario (
                usu_codigo INTEGER NOT NULL,
                end_codigo INTEGER NOT NULL,
                endxusu_data DATE NOT NULL,
                endxusu_numero INTEGER,
                endxusu_complemento VARCHAR(30),
                endxusu_observacao VARCHAR(200),
                CONSTRAINT endereco_usuario_pk PRIMARY KEY (usu_codigo, end_codigo, endxusu_data)
);


CREATE SEQUENCE public.unidade_seq;

CREATE TABLE public.unidade (
                uni_codigo INTEGER NOT NULL DEFAULT nextval('public.unidade_seq'),
                uni_razaosocial VARCHAR(150) NOT NULL,
                uni_nomefantasia VARCHAR(50) NOT NULL,
                uni_cnpj VARCHAR(14) NOT NULL,
                uni_telefonefixo VARCHAR(10),
                uni_telefonemovel VARCHAR(11),
                uni_email VARCHAR(70),
                uni_inscricaoestadual VARCHAR(20),
                uni_referencia VARCHAR(10),
                uni_senha VARCHAR(100),
                uni_ativo BOOLEAN NOT NULL,
                uni_observacao VARCHAR(200),
                CONSTRAINT uni_codigo_pk PRIMARY KEY (uni_codigo)
);


ALTER SEQUENCE public.unidade_seq OWNED BY public.unidade.uni_codigo;

CREATE SEQUENCE public.area_unidade_seq;

CREATE TABLE public.area_unidade (
                arxun_codigo INTEGER NOT NULL DEFAULT nextval('public.area_unidade_seq'),
                arxun_ativo BOOLEAN NOT NULL,
                arxun_data DATE NOT NULL,
                arxun_observacao VARCHAR(200),
                uni_codigo INTEGER NOT NULL,
                are_codigo INTEGER NOT NULL,
                CONSTRAINT area_unidade_pk PRIMARY KEY (arxun_codigo)
);


ALTER SEQUENCE public.area_unidade_seq OWNED BY public.area_unidade.arxun_codigo;

CREATE SEQUENCE public.perda_unidade_seq;

CREATE TABLE public.perda_unidade (
                pexun_codigo INTEGER NOT NULL DEFAULT nextval('public.perda_unidade_seq'),
                pexun_ativo BOOLEAN NOT NULL,
                pexun_data DATE NOT NULL,
                pexun_observacao VARCHAR(200),
                per_codigo INTEGER NOT NULL,
                uni_codigo INTEGER NOT NULL,
                CONSTRAINT perda_unidade_pk PRIMARY KEY (pexun_codigo)
);


ALTER SEQUENCE public.perda_unidade_seq OWNED BY public.perda_unidade.pexun_codigo;

CREATE TABLE public.endereco_unidade (
                uni_codigo INTEGER NOT NULL,
                end_codigo INTEGER NOT NULL,
                endxuni_data DATE NOT NULL,
                endxuni_numero INTEGER,
                endxuni_complemento VARCHAR(30),
                endxuni_observacao VARCHAR(200),
                CONSTRAINT endereco_unidade_pk PRIMARY KEY (uni_codigo, end_codigo, endxuni_data)
);


CREATE SEQUENCE public.valida_seq;

CREATE TABLE public.valida (
                val_codigo INTEGER NOT NULL DEFAULT nextval('public.valida_seq'),
                val_qtde_usuario INTEGER NOT NULL,
                val_limite VARCHAR(20) NOT NULL,
                val_criptolim VARCHAR(100) NOT NULL,
                val_status VARCHAR(100) NOT NULL,
                val_pass VARCHAR(100) NOT NULL,
                val_observacao VARCHAR(200),
                uni_codigo INTEGER NOT NULL,
                CONSTRAINT val_codigo_pk PRIMARY KEY (val_codigo)
);


ALTER SEQUENCE public.valida_seq OWNED BY public.valida.val_codigo;

CREATE SEQUENCE public.usuario_unidade_seq;

CREATE TABLE public.usuario_unidade (
                usxun_codigo INTEGER NOT NULL DEFAULT nextval('public.usuario_unidade_seq'),
                usuxuni_data DATE NOT NULL,
                usxun_movimento BOOLEAN NOT NULL,
                usxun_ativo BOOLEAN NOT NULL,
                usuxuni_observacao VARCHAR(200),
                usu_codigo INTEGER NOT NULL,
                uni_codigo INTEGER NOT NULL,
                CONSTRAINT usu_uni_pk PRIMARY KEY (usxun_codigo)
);
COMMENT ON COLUMN public.usuario_unidade.usxun_movimento IS 'usuario tem permissao para lancar paradas';


ALTER SEQUENCE public.usuario_unidade_seq OWNED BY public.usuario_unidade.usxun_codigo;

CREATE TABLE public.acesso_usuario (
                ace_codigo INTEGER NOT NULL,
                usxun_codigo INTEGER NOT NULL,
                usuxace_data DATE NOT NULL,
                usuxace_observacao VARCHAR(200),
                CONSTRAINT acesso_usuario_pk PRIMARY KEY (ace_codigo, usxun_codigo)
);


CREATE SEQUENCE public.tipoparada_seq;

CREATE TABLE public.tipoparada (
                tpa_codigo INTEGER NOT NULL DEFAULT nextval('public.tipoparada_seq'),
                tpa_nome VARCHAR(30) NOT NULL,
                tpa_sigla VARCHAR(6) NOT NULL,
                tpa_ativo BOOLEAN NOT NULL,
                tpa_observacao VARCHAR(200),
                CONSTRAINT tpa_codigo_pk PRIMARY KEY (tpa_codigo)
);


ALTER SEQUENCE public.tipoparada_seq OWNED BY public.tipoparada.tpa_codigo;

CREATE SEQUENCE public.tparada_unidade_seq;

CREATE TABLE public.tparada_unidade (
                tpxun_codigo INTEGER NOT NULL DEFAULT nextval('public.tparada_unidade_seq'),
                tpxun_ativo BOOLEAN NOT NULL,
                tpxun_data DATE NOT NULL,
                tpxun_observacao VARCHAR(200),
                uni_codigo INTEGER NOT NULL,
                tpa_codigo INTEGER NOT NULL,
                CONSTRAINT tpa_uni_pk PRIMARY KEY (tpxun_codigo)
);


ALTER SEQUENCE public.tparada_unidade_seq OWNED BY public.tparada_unidade.tpxun_codigo;

CREATE SEQUENCE public.submotivo_seq;

CREATE TABLE public.submotivo (
                sub_codigo INTEGER NOT NULL DEFAULT nextval('public.submotivo_seq'),
                sub_nome VARCHAR(50) NOT NULL,
                sub_sigla VARCHAR(6) NOT NULL,
                sub_ativo BOOLEAN NOT NULL,
                sub_observacao VARCHAR(200),
                CONSTRAINT sub_codigo_pk PRIMARY KEY (sub_codigo)
);


ALTER SEQUENCE public.submotivo_seq OWNED BY public.submotivo.sub_codigo;

CREATE SEQUENCE public.producao_seq;

CREATE TABLE public.producao (
                pro_codigo INTEGER NOT NULL DEFAULT nextval('public.producao_seq'),
                pro_dataproducao DATE NOT NULL,
                pro_qtdeproduzida INTEGER DEFAULT 0,
                pro_aprovada BOOLEAN NOT NULL,
                pro_observacao_aprovador VARCHAR(200),
                usxun_codigo INTEGER NOT NULL,
                CONSTRAINT pro_codigo_pk PRIMARY KEY (pro_codigo)
);
COMMENT ON TABLE public.producao IS 'abre produ��o do dia';
COMMENT ON COLUMN public.producao.usxun_codigo IS 'usu�rio que ir� abrir a produ��o';


ALTER SEQUENCE public.producao_seq OWNED BY public.producao.pro_codigo;

CREATE SEQUENCE public.motivoparada_seq;

CREATE TABLE public.motivoparada (
                mpa_codigo INTEGER NOT NULL DEFAULT nextval('public.motivoparada_seq'),
                mpa_nome VARCHAR(50) NOT NULL,
                mpa_sigla VARCHAR(6) NOT NULL,
                mpa_ativo BOOLEAN NOT NULL,
                mpa_observacao VARCHAR(200),
                CONSTRAINT mpa_codig_pk PRIMARY KEY (mpa_codigo)
);


ALTER SEQUENCE public.motivoparada_seq OWNED BY public.motivoparada.mpa_codigo;

CREATE SEQUENCE public.mparada_unidade_seq;

CREATE TABLE public.mparada_unidade (
                mpxun_codigo INTEGER NOT NULL DEFAULT nextval('public.mparada_unidade_seq'),
                mpxun_ativo BOOLEAN NOT NULL,
                mpxun_data DATE NOT NULL,
                mpxun_observacao VARCHAR(200),
                uni_codigo INTEGER NOT NULL,
                mpa_codigo INTEGER NOT NULL,
                CONSTRAINT mpa_uni_pk PRIMARY KEY (mpxun_codigo)
);


ALTER SEQUENCE public.mparada_unidade_seq OWNED BY public.mparada_unidade.mpxun_codigo;

CREATE SEQUENCE public.tpxmp_unidade_seq;

CREATE TABLE public.tpxmp_unidade (
                tpxmp_codigo INTEGER NOT NULL DEFAULT nextval('public.tpxmp_unidade_seq'),
                tpxmp_ativo BOOLEAN NOT NULL,
                tpxmp_data DATE NOT NULL,
                tpxmp_observacao VARCHAR(200),
                tpxun_codigo INTEGER NOT NULL,
                mpxun_codigo INTEGER NOT NULL,
                CONSTRAINT tpxmp_pk PRIMARY KEY (tpxmp_codigo)
);
COMMENT ON TABLE public.tpxmp_unidade IS 'motivo de parada para cada unidade';


ALTER SEQUENCE public.tpxmp_unidade_seq OWNED BY public.tpxmp_unidade.tpxmp_codigo;

CREATE SEQUENCE public.medida_seq;

CREATE TABLE public.medida (
                med_codigo INTEGER NOT NULL DEFAULT nextval('public.medida_seq'),
                med_nome VARCHAR(30) NOT NULL,
                med_sigla VARCHAR(6) NOT NULL,
                med_ativo BOOLEAN NOT NULL,
                med_observacao VARCHAR(200),
                CONSTRAINT med_codigo_pk PRIMARY KEY (med_codigo)
);


ALTER SEQUENCE public.medida_seq OWNED BY public.medida.med_codigo;

CREATE SEQUENCE public.equipamento_seq;

CREATE TABLE public.equipamento (
                equ_codigo INTEGER NOT NULL DEFAULT nextval('public.equipamento_seq'),
                equ_nome VARCHAR(50) NOT NULL,
                equ_modelo VARCHAR(30),
                equ_ativo BOOLEAN NOT NULL,
                equ_observacao VARCHAR(200),
                CONSTRAINT equ_codigo_pk PRIMARY KEY (equ_codigo)
);


ALTER SEQUENCE public.equipamento_seq OWNED BY public.equipamento.equ_codigo;

CREATE SEQUENCE public.equipamento_unidade_seq;

CREATE TABLE public.equipamento_unidade (
                eqxun_codigo INTEGER NOT NULL DEFAULT nextval('public.equipamento_unidade_seq'),
                eqxun_ativo BOOLEAN NOT NULL,
                eqxun_data DATE NOT NULL,
                eqxun_observacao VARCHAR(200),
                uni_codigo INTEGER NOT NULL,
                equ_codigo INTEGER NOT NULL,
                CONSTRAINT equipamento_unidade_pk PRIMARY KEY (eqxun_codigo)
);
COMMENT ON TABLE public.equipamento_unidade IS 'equipamento de cada unidade';


ALTER SEQUENCE public.equipamento_unidade_seq OWNED BY public.equipamento_unidade.eqxun_codigo;

CREATE SEQUENCE public.equipamento_area_seq;

CREATE TABLE public.equipamento_area (
                eqxar_codigo INTEGER NOT NULL DEFAULT nextval('public.equipamento_area_seq'),
                eqxar_ativo BOOLEAN NOT NULL,
                eqxar_referencia VARCHAR(6) NOT NULL,
                eqxar_data DATE NOT NULL,
                eqxar_observacao VARCHAR(200),
                arxun_codigo INTEGER NOT NULL,
                eqxun_codigo INTEGER NOT NULL,
                CONSTRAINT equipamento_area_pk PRIMARY KEY (eqxar_codigo)
);


ALTER SEQUENCE public.equipamento_area_seq OWNED BY public.equipamento_area.eqxar_codigo;

CREATE SEQUENCE public.sub_mpa_equipamento_seq;

CREATE TABLE public.sub_mpa_equipamento (
                sme_codigo INTEGER NOT NULL DEFAULT nextval('public.sub_mpa_equipamento_seq'),
                sme_ativo BOOLEAN NOT NULL,
                sme_observacao VARCHAR(200),
                eqxar_codigo INTEGER NOT NULL,
                tpxmp_codigo INTEGER NOT NULL,
                sub_codigo INTEGER NOT NULL,
                CONSTRAINT sub_mpa_equipamento_pk PRIMARY KEY (sme_codigo)
);
COMMENT ON TABLE public.sub_mpa_equipamento IS 'vicular submotivos aos motivos de paradas para cada m�quina';


ALTER SEQUENCE public.sub_mpa_equipamento_seq OWNED BY public.sub_mpa_equipamento.sme_codigo;

CREATE SEQUENCE public.parada_seq;

CREATE TABLE public.parada (
                par_codigo INTEGER NOT NULL DEFAULT nextval('public.parada_seq'),
                par_data DATE NOT NULL,
                par_datareg DATE NOT NULL,
                par_tempoparada SMALLINT,
                par_horaini TIME NOT NULL,
                par_horafim TIME,
                par_aprovada BOOLEAN NOT NULL,
                par_observacao VARCHAR(200),
                eqxar_codigo INTEGER NOT NULL,
                usxun_codigo INTEGER NOT NULL,
                tpxmp_codigo INTEGER NOT NULL,
                sme_codigo INTEGER,
                CONSTRAINT par_codigo_pk PRIMARY KEY (par_codigo)
);
COMMENT ON TABLE public.parada IS 'apontamento de paradas de equipamentos';


ALTER SEQUENCE public.parada_seq OWNED BY public.parada.par_codigo;

CREATE TABLE public.aprovar_producao (
                par_codigo INTEGER NOT NULL,
                pro_codigo INTEGER NOT NULL,
                usxun_codigo INTEGER NOT NULL,
                aprxpro_data_aprovado VARCHAR NOT NULL,
                aprxpro_observacao VARCHAR(200),
                CONSTRAINT aprovar_producao_pk PRIMARY KEY (par_codigo, pro_codigo, usxun_codigo)
);


CREATE SEQUENCE public.item_seq;

CREATE TABLE public.item (
                ite_codigo INTEGER NOT NULL DEFAULT nextval('public.item_seq'),
                ite_nome VARCHAR(50) NOT NULL,
                ite_ativo BOOLEAN NOT NULL,
                ite_observacao VARCHAR(200),
                med_codigo INTEGER NOT NULL,
                CONSTRAINT ite_codigo_pk PRIMARY KEY (ite_codigo)
);


ALTER SEQUENCE public.item_seq OWNED BY public.item.ite_codigo;

CREATE SEQUENCE public.item_unidade_seq;

CREATE TABLE public.item_unidade (
                itxun_codigo INTEGER NOT NULL DEFAULT nextval('public.item_unidade_seq'),
                itxun_ativo BOOLEAN NOT NULL,
                itxun_data DATE NOT NULL,
                itxun_observacao VARCHAR(200),
                uni_codigo INTEGER NOT NULL,
                ite_codigo INTEGER NOT NULL,
                CONSTRAINT ite_uni_codigo_pk PRIMARY KEY (itxun_codigo)
);
COMMENT ON TABLE public.item_unidade IS 'itens que a  unidade ira produzir';


ALTER SEQUENCE public.item_unidade_seq OWNED BY public.item_unidade.itxun_codigo;

CREATE SEQUENCE public.item_equipamento_seq;

CREATE TABLE public.item_equipamento (
                itxeq_codigo INTEGER NOT NULL DEFAULT nextval('public.item_equipamento_seq'),
                itxeq_capacidade INTEGER NOT NULL,
                itxeq_ativo BOOLEAN NOT NULL,
                itxeq_observacao VARCHAR(200),
                eqxar_codigo INTEGER NOT NULL,
                itxun_codigo INTEGER NOT NULL,
                CONSTRAINT item_equipamento_pk PRIMARY KEY (itxeq_codigo)
);


ALTER SEQUENCE public.item_equipamento_seq OWNED BY public.item_equipamento.itxeq_codigo;

CREATE SEQUENCE public.qtde_producao_seq;

CREATE TABLE public.qtde_producao (
                qtxpr_codigo INTEGER NOT NULL DEFAULT nextval('public.qtde_producao_seq'),
                qtxpr_quantidade INTEGER NOT NULL,
                qtxpr_observacao VARCHAR(200) NOT NULL,
                usxun_codigo INTEGER NOT NULL,
                itxeq_codigo INTEGER NOT NULL,
                pro_codigo INTEGER NOT NULL,
                CONSTRAINT pro_qtde_codigo_pk PRIMARY KEY (qtxpr_codigo)
);
COMMENT ON TABLE public.qtde_producao IS 'insere itens produzidos no dia';


ALTER SEQUENCE public.qtde_producao_seq OWNED BY public.qtde_producao.qtxpr_codigo;

CREATE SEQUENCE public.perda_producao_seq;

CREATE TABLE public.perda_producao (
                perxpro_codigo INTEGER NOT NULL DEFAULT nextval('public.perda_producao_seq'),
                perxpro_quantidade INTEGER NOT NULL,
                perxpro_calcular BOOLEAN NOT NULL,
                perxpro_observacao VARCHAR(200),
                itxeq_codigo INTEGER NOT NULL,
                pexun_codigo INTEGER NOT NULL,
                qtxpr_codigo INTEGER NOT NULL,
                usxun_codigo INTEGER NOT NULL,
                CONSTRAINT perda_producao_pk PRIMARY KEY (perxpro_codigo)
);
COMMENT ON TABLE public.perda_producao IS 'perda na producao';
COMMENT ON COLUMN public.perda_producao.perxpro_calcular IS 'se for true, a quantidade apontada ser� descontada da produ��o';
COMMENT ON COLUMN public.perda_producao.itxeq_codigo IS 'item que foi perdido';
COMMENT ON COLUMN public.perda_producao.pexun_codigo IS 'tipo da perda';
COMMENT ON COLUMN public.perda_producao.qtxpr_codigo IS 'onde houve a perda';
COMMENT ON COLUMN public.perda_producao.usxun_codigo IS 'usuario que lancou a perda';


ALTER SEQUENCE public.perda_producao_seq OWNED BY public.perda_producao.perxpro_codigo;

CREATE SEQUENCE public.funcao_seq;

CREATE TABLE public.funcao (
                fun_codigo INTEGER NOT NULL DEFAULT nextval('public.funcao_seq'),
                fun_nome VARCHAR(50) NOT NULL,
                fun_ativo BOOLEAN NOT NULL,
                fun_observacao VARCHAR(200),
                CONSTRAINT fun_codigo_pk PRIMARY KEY (fun_codigo)
);


ALTER SEQUENCE public.funcao_seq OWNED BY public.funcao.fun_codigo;

CREATE SEQUENCE public.funcao_usuario_seq;

CREATE TABLE public.funcao_usuario (
                fuxus_codigo INTEGER NOT NULL DEFAULT nextval('public.funcao_usuario_seq'),
                fuxus_datainicio DATE NOT NULL,
                fuxus_datafim DATE,
                fuxus_observacao VARCHAR(200),
                are_codigo INTEGER NOT NULL,
                fun_codigo INTEGER NOT NULL,
                usxun_codigo INTEGER NOT NULL,
                CONSTRAINT fun_usu_codigo_pk PRIMARY KEY (fuxus_codigo)
);


ALTER SEQUENCE public.funcao_usuario_seq OWNED BY public.funcao_usuario.fuxus_codigo;

ALTER TABLE public.perda_unidade ADD CONSTRAINT perda_perda_unidade_fk
FOREIGN KEY (per_codigo)
REFERENCES public.perda (per_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.funcao_usuario ADD CONSTRAINT area_funcao_usuario_fk
FOREIGN KEY (are_codigo)
REFERENCES public.area (are_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.area_unidade ADD CONSTRAINT area_area_unidade_fk
FOREIGN KEY (are_codigo)
REFERENCES public.area (are_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.acesso_usuario ADD CONSTRAINT acesso_acesso_usuario_fk
FOREIGN KEY (ace_codigo)
REFERENCES public.acesso (ace_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.estado ADD CONSTRAINT pais_estado_fk
FOREIGN KEY (pai_codigo)
REFERENCES public.pais (pai_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cidade ADD CONSTRAINT estado_cidade_fk
FOREIGN KEY (est_codigo)
REFERENCES public.estado (est_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.endereco ADD CONSTRAINT cidade_endereco_fk
FOREIGN KEY (cid_codigo)
REFERENCES public.cidade (cid_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.endereco_usuario ADD CONSTRAINT endereco_endereco_usuario_fk
FOREIGN KEY (end_codigo)
REFERENCES public.endereco (end_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.endereco_unidade ADD CONSTRAINT endereco_endereco_unidade_fk
FOREIGN KEY (end_codigo)
REFERENCES public.endereco (end_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario_unidade ADD CONSTRAINT usuario_usuario_unidade_fk
FOREIGN KEY (usu_codigo)
REFERENCES public.usuario (usu_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.endereco_usuario ADD CONSTRAINT usuario_endereco_usuario_fk
FOREIGN KEY (usu_codigo)
REFERENCES public.usuario (usu_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.item_unidade ADD CONSTRAINT unidade_unidade_item_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario_unidade ADD CONSTRAINT unidade_usuario_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.valida ADD CONSTRAINT unidade_valida_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tparada_unidade ADD CONSTRAINT unidade_tparada_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipamento_unidade ADD CONSTRAINT unidade_maquina_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.endereco_unidade ADD CONSTRAINT unidade_endereco_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.mparada_unidade ADD CONSTRAINT unidade_mparada_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.perda_unidade ADD CONSTRAINT unidade_perda_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.area_unidade ADD CONSTRAINT unidade_area_unidade_fk
FOREIGN KEY (uni_codigo)
REFERENCES public.unidade (uni_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipamento_area ADD CONSTRAINT area_unidade_equipamento_area_fk
FOREIGN KEY (arxun_codigo)
REFERENCES public.area_unidade (arxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.perda_producao ADD CONSTRAINT perda_unidade_perda_producao_fk
FOREIGN KEY (pexun_codigo)
REFERENCES public.perda_unidade (pexun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.funcao_usuario ADD CONSTRAINT usuario_unidade_funcao_usuario_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.producao ADD CONSTRAINT usuario_unidade_producao_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.parada ADD CONSTRAINT usuario_unidade_parada_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.qtde_producao ADD CONSTRAINT usuario_unidade_qtde_producao_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.perda_producao ADD CONSTRAINT usuario_unidade_perda_producao_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.aprovar_producao ADD CONSTRAINT usuario_unidade_aprovar_producao_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.acesso_usuario ADD CONSTRAINT usuario_unidade_acesso_usuario_fk
FOREIGN KEY (usxun_codigo)
REFERENCES public.usuario_unidade (usxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tparada_unidade ADD CONSTRAINT tipoparada_tparada_unidade_fk
FOREIGN KEY (tpa_codigo)
REFERENCES public.tipoparada (tpa_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tpxmp_unidade ADD CONSTRAINT tparada_unidade_parada_unidade_fk
FOREIGN KEY (tpxun_codigo)
REFERENCES public.tparada_unidade (tpxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sub_mpa_equipamento ADD CONSTRAINT submotivo_sub_mpa_maq_fk
FOREIGN KEY (sub_codigo)
REFERENCES public.submotivo (sub_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.qtde_producao ADD CONSTRAINT producao_producao_qtde_fk
FOREIGN KEY (pro_codigo)
REFERENCES public.producao (pro_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.aprovar_producao ADD CONSTRAINT producao_aprovar_parada_fk
FOREIGN KEY (pro_codigo)
REFERENCES public.producao (pro_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.mparada_unidade ADD CONSTRAINT motivoparada_mparada_unidade_fk
FOREIGN KEY (mpa_codigo)
REFERENCES public.motivoparada (mpa_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tpxmp_unidade ADD CONSTRAINT mparada_unidade_tpxmp_unidade_fk
FOREIGN KEY (mpxun_codigo)
REFERENCES public.mparada_unidade (mpxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.parada ADD CONSTRAINT parada_unidade_parada_fk
FOREIGN KEY (tpxmp_codigo)
REFERENCES public.tpxmp_unidade (tpxmp_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sub_mpa_equipamento ADD CONSTRAINT tpxmp_unidade_sub_mpa_maq_fk
FOREIGN KEY (tpxmp_codigo)
REFERENCES public.tpxmp_unidade (tpxmp_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.item ADD CONSTRAINT medida_item_fk
FOREIGN KEY (med_codigo)
REFERENCES public.medida (med_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipamento_unidade ADD CONSTRAINT maquina_maquina_unidade_fk
FOREIGN KEY (equ_codigo)
REFERENCES public.equipamento (equ_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.equipamento_area ADD CONSTRAINT maquina_unidade_equipamento_area_fk
FOREIGN KEY (eqxun_codigo)
REFERENCES public.equipamento_unidade (eqxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.parada ADD CONSTRAINT equipamento_area_parada_fk
FOREIGN KEY (eqxar_codigo)
REFERENCES public.equipamento_area (eqxar_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.sub_mpa_equipamento ADD CONSTRAINT equipamento_area_sub_mpa_maq_fk
FOREIGN KEY (eqxar_codigo)
REFERENCES public.equipamento_area (eqxar_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.item_equipamento ADD CONSTRAINT equipamento_area_item_equipamento_fk
FOREIGN KEY (eqxar_codigo)
REFERENCES public.equipamento_area (eqxar_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.parada ADD CONSTRAINT sub_mpa_maq_parada_fk
FOREIGN KEY (sme_codigo)
REFERENCES public.sub_mpa_equipamento (sme_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.aprovar_producao ADD CONSTRAINT parada_aprovar_parada_fk
FOREIGN KEY (par_codigo)
REFERENCES public.parada (par_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.item_unidade ADD CONSTRAINT item_unidade_item_fk
FOREIGN KEY (ite_codigo)
REFERENCES public.item (ite_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.item_equipamento ADD CONSTRAINT item_unidade_item_maquina_fk
FOREIGN KEY (itxun_codigo)
REFERENCES public.item_unidade (itxun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.qtde_producao ADD CONSTRAINT item_maquina_qtde_producao_fk
FOREIGN KEY (itxeq_codigo)
REFERENCES public.item_equipamento (itxeq_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.perda_producao ADD CONSTRAINT item_equipamento_perda_producao_fk
FOREIGN KEY (itxeq_codigo)
REFERENCES public.item_equipamento (itxeq_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.perda_producao ADD CONSTRAINT qtde_producao_perda_producao_fk
FOREIGN KEY (qtxpr_codigo)
REFERENCES public.qtde_producao (qtxpr_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.funcao_usuario ADD CONSTRAINT funcao_funcao_usuario_fk
FOREIGN KEY (fun_codigo)
REFERENCES public.funcao (fun_codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
