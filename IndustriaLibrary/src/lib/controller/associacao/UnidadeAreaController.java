package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeArea;
import lib.viewmodel.associacao.UnidadeAreaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeAreaController {

    /**
     * Grava no banco de dados os valores de uma nova área para uma unidade
     *
     * @param idUnidade
     * @param vmAreaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadeAreaViewModel vmAreaDaUnidade) {
        if (vmAreaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmAreaDaUnidade.getArea() != null) {
                    return new UnidadeArea(
                            vmAreaDaUnidade.isAtivo(),
                            vmAreaDaUnidade.getData(),
                            vmAreaDaUnidade.getObservacao(),
                            idUnidade,
                            vmAreaDaUnidade.getArea().getId()
                    ).gravar();
                }
                return 2304;
            }
            return 2303;
        }
        return 2307;
    }

    /**
     * Altera no banco de dados os valores de uma área relacionada a uma unidade
     *
     * @param idUnidade
     * @param vmAreaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(int idUnidade, UnidadeAreaViewModel vmAreaDaUnidade) {
        if (vmAreaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmAreaDaUnidade.getArea() != null) {
                    return new UnidadeArea(
                            vmAreaDaUnidade.getId(),
                            vmAreaDaUnidade.isAtivo(),
                            vmAreaDaUnidade.getData(),
                            vmAreaDaUnidade.getObservacao(),
                            idUnidade,
                            vmAreaDaUnidade.getArea().getId()
                    ).alterar();
                }
                return 2304;
            }
            return 2303;
        }
        return 2307;
    }

    /**
     * Excluir uma área relacionada a uma unidade
     *
     * @param idUnidade
     * @param vmAreaDaUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadeAreaViewModel vmAreaDaUnidade) {
        if (vmAreaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmAreaDaUnidade.getArea() != null) {
                    return new UnidadeArea(
                            vmAreaDaUnidade.getId(),
                            vmAreaDaUnidade.isAtivo(),
                            vmAreaDaUnidade.getData(),
                            vmAreaDaUnidade.getObservacao(),
                            idUnidade, vmAreaDaUnidade.getArea().getId()
                    ).excluir();
                }
                return 2304;
            }
            return 2303;
        }
        return 2307;
    }

    /**
     * Busca registro de área relacionado a uma unidade pelo ID informado
     *
     * @param id
     * @return objeto com registro, ou objeto nulo se não for encontrado
     */
    public UnidadeAreaViewModel getRegPorCodigo(int id) {
        try {
            return modelToViewModel(new UnidadeArea().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca todas áreas registradas para uma determinada unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeAreaViewModel> getAreasUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeArea(idUnidade).getAreasPorUnidade());
    }
    
    /**
     * busca área através do código da unidade e área
     *
     * @param idUnidade
     * @param idArea
     * @return objeto com registro, ou objeto nulo se não for encontrado
     */
    public UnidadeAreaViewModel getRegPorUnidadeEArea(int idUnidade, int idArea) {
        try {
            return modelToViewModel(new UnidadeArea().getPorUnidadeEArea(idUnidade, idArea));
        } catch (Exception ex) {
            return null;
        }
    }

    private List<UnidadeAreaViewModel> modelToViewModel(List<UnidadeArea> listaAreaDaUnidade) {
        List<UnidadeAreaViewModel> lista = new ArrayList<>();

        listaAreaDaUnidade.forEach((ua) -> {
            try {
                lista.add(modelToViewModel(ua));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UnidadeAreaViewModel modelToViewModel(UnidadeArea areaDaUnidade) throws Exception {
        try {
            return new UnidadeAreaViewModel(
                    areaDaUnidade.getCodigo(),
                    areaDaUnidade.isAtivo(),
                    areaDaUnidade.getData(),
                    areaDaUnidade.getObservacao(),
                    areaDaUnidade.getCodArea()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo: " + ex.getMessage());
        }
    }
}
