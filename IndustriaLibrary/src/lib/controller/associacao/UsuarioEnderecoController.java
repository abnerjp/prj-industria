package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UsuarioEndereco;
import lib.viewmodel.associacao.UsuarioEnderecoViewModel;

/**
 *
 * @author abnerjp
 */
public class UsuarioEnderecoController {

    /**
     * Grava no banco de dados os valores de um novo endereço para um usuário
     *
     * @param idUsuario
     * @param vmEndUsuario
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUsuario, UsuarioEnderecoViewModel vmEndUsuario) {
        if (vmEndUsuario != null) {
            if (idUsuario > 0) {
                if (vmEndUsuario.getEndereco() != null) {
                    return new UsuarioEndereco(
                            vmEndUsuario.getNumero(),
                            vmEndUsuario.getData(),
                            vmEndUsuario.getComplemento(),
                            vmEndUsuario.getObservacao(),
                            idUsuario,
                            vmEndUsuario.getEndereco().getId()
                    ).gravar();
                } else {
                    return 2104;
                }
            } else {
                return 2103;
            }
        }
        return 2109;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param idUnidade
     * @param vmEndUsuario
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UsuarioEnderecoViewModel vmEndUsuario) {
        if (vmEndUsuario != null) {
            if (idUnidade > 0) {
                if (vmEndUsuario.getEndereco() != null) {
                    return new UsuarioEndereco(
                            vmEndUsuario.getNumero(),
                            vmEndUsuario.getData(),
                            vmEndUsuario.getComplemento(),
                            vmEndUsuario.getObservacao(),
                            idUnidade,
                            vmEndUsuario.getEndereco().getId()
                    ).excluir();
                } else {
                    return 2104;
                }
            } else {
                return 2103;
            }
        }
        return 2109;
    }

    /**
     * Busca o endereço atual de um usuário
     *
     * @param idUsuario
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioEnderecoViewModel getEnderecoAtual(int idUsuario) {
        try {
            return modelToViewModel(new UsuarioEndereco().getEnderecoAtual(idUsuario));
        } catch (Exception ex) {
            return null;
        }

    }

    /**
     * Busca todos endereços já registrados para um usuário
     *
     * @param idUsuario
     * @return retorna uma lista com N registros
     */
    public List<UsuarioEnderecoViewModel> getTodosEnderecos(int idUsuario) {
        return modelToViewModel(new UsuarioEndereco().getTodosEnderecos(idUsuario));
    }

    private List<UsuarioEnderecoViewModel> modelToViewModel(List<UsuarioEndereco> listaEnderecoUsuario) {
        List<UsuarioEnderecoViewModel> lista = new ArrayList();

        listaEnderecoUsuario.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                // aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UsuarioEnderecoViewModel modelToViewModel(UsuarioEndereco eu) throws Exception {
        try {
            return new UsuarioEnderecoViewModel(
                    eu.getNumero(),
                    eu.getData(),
                    eu.getComplemento(),
                    eu.getObservacao(),
                    eu.getCodEndereco());
        } catch (NullPointerException ex) {
            throw new Exception("objeto nulo: " + ex.getMessage());
        }
    }
}
