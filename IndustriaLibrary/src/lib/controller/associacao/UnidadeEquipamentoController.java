package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeEquipamento;
import lib.viewmodel.associacao.UnidadeEquipamentoViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeEquipamentoController {

    /**
     * Grava no banco de dados os valores de um novo equipamento para uma
     * unidade
     *
     * @param idUnidade
     * @param vmEquipamentoDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadeEquipamentoViewModel vmEquipamentoDaUnidade) {
        if (vmEquipamentoDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmEquipamentoDaUnidade.getEquipamento() != null) {
                    return new UnidadeEquipamento(
                            vmEquipamentoDaUnidade.isAtivo(),
                            vmEquipamentoDaUnidade.getData(),
                            vmEquipamentoDaUnidade.getObservacao(),
                            idUnidade,
                            vmEquipamentoDaUnidade.getEquipamento().getId()
                    ).gravar();
                } else {
                    //equipamento nulo
                    return 2204;
                }
            } else {
                //unidade nula
                return 2203;
            }
        }
        //objeto equipamento nulo (vmEquipamentoDaUnidade)
        return 2207;
    }

    /**
     * Altera no banco de dados os valores de um equipamento relacionado a uma
     * unidade
     *
     * @param idUnidade
     * @param vmEquipamentoDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(int idUnidade, UnidadeEquipamentoViewModel vmEquipamentoDaUnidade) {
        if (vmEquipamentoDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmEquipamentoDaUnidade.getEquipamento() != null) {
                    return new UnidadeEquipamento(
                            vmEquipamentoDaUnidade.getId(),
                            vmEquipamentoDaUnidade.isAtivo(),
                            vmEquipamentoDaUnidade.getData(),
                            vmEquipamentoDaUnidade.getObservacao(),
                            idUnidade,
                            vmEquipamentoDaUnidade.getEquipamento().getId()
                    ).alterar();
                } else {
                    //equipamento nulo
                    return 2204;
                }
            } else {
                //unidade nula
                return 2203;
            }
        }
        //objeto equipamento nulo (vmEquipamentoDaUnidade)
        return 2207;
    }

    /**
     * Excluir um equipamento do banco de dados, relacionado a uma unidade
     *
     * @param idUnidade
     * @param vmEquipamentoDaUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadeEquipamentoViewModel vmEquipamentoDaUnidade) {
        if (vmEquipamentoDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmEquipamentoDaUnidade.getEquipamento() != null) {
                    return new UnidadeEquipamento(
                            vmEquipamentoDaUnidade.getId(),
                            vmEquipamentoDaUnidade.isAtivo(),
                            vmEquipamentoDaUnidade.getData(),
                            vmEquipamentoDaUnidade.getObservacao(),
                            idUnidade,
                            vmEquipamentoDaUnidade.getEquipamento().getId()
                    ).excluir();
                } else {
                    //equipamento nulo
                    return 2204;
                }
            } else {
                //unidade nula
                return 2203;
            }
        }
        //objeto equipamento nulo (vmEquipamentoDaUnidade)
        return 2207;
    }

    /**
     * Busca todos equipamentos registrados para uma determinada unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeEquipamentoViewModel> getEquipamentosUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeEquipamento(idUnidade).getEquipamentosPorUnidade());
    }

    /**
     * Busca registro de equpamento, relacionado a uma unidade no banco de dados,
     * igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeEquipamentoViewModel getRegPorCodigo(int id) {
        try {
            return modelToViewModel(new UnidadeEquipamento().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * busca registro através do codigo da unidade e do item
     *
     * @param idUnidade
     * @param idEquipamento 
     * @return objeto com registro, ou objeto nulo se não for encontrado
     */
    public UnidadeEquipamentoViewModel getRegPorUnidadeEEquipamento(int idUnidade, int idEquipamento) {
        try {
            return modelToViewModel(new UnidadeEquipamento().getRegPorUnidadeEEquipamento(idUnidade, idEquipamento));
        } catch (Exception ex) {
            return null;
        }
    }
    
    private UnidadeEquipamentoViewModel modelToViewModel(UnidadeEquipamento equipamentoUnidade) throws Exception {
        try {
            return new UnidadeEquipamentoViewModel(
                    equipamentoUnidade.getCodigo(),
                    equipamentoUnidade.isAtivo(),
                    equipamentoUnidade.getData(),
                    equipamentoUnidade.getObservacao(),
                    equipamentoUnidade.getCodEquipamento()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo");
        }
    }

    private List<UnidadeEquipamentoViewModel> modelToViewModel(List<UnidadeEquipamento> listaEquipamentoUnidade) {
        List<UnidadeEquipamentoViewModel> lista = new ArrayList<>();

        listaEquipamentoUnidade.forEach((ue) -> {
            try {
                lista.add(modelToViewModel(ue));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }
}
