package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadePerda;
import lib.viewmodel.associacao.UnidadePerdaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadePerdaController {

    /**
     * Grava no banco de dados os valores de uma nova perda para uma unidade
     *
     * @param idUnidade
     * @param vmPerdaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadePerdaViewModel vmPerdaDaUnidade) {
        if (vmPerdaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmPerdaDaUnidade.getPerda() != null) {
                    return new UnidadePerda(
                            vmPerdaDaUnidade.isAtivo(),
                            vmPerdaDaUnidade.getData(),
                            vmPerdaDaUnidade.getObservacao(),
                            idUnidade,
                            vmPerdaDaUnidade.getPerda().getId()
                    ).gravar();
                }
                return 1904;
            }
            return 1903;
        }
        return 1907;
    }

    /**
     * Altera no banco de dados os valores de uma perda relacionada a uma
     * unidade
     *
     * @param idUnidade
     * @param vmPerdaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(int idUnidade, UnidadePerdaViewModel vmPerdaDaUnidade) {
        if (vmPerdaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmPerdaDaUnidade.getPerda() != null) {
                    return new UnidadePerda(
                            vmPerdaDaUnidade.getId(),
                            vmPerdaDaUnidade.isAtivo(),
                            vmPerdaDaUnidade.getData(),
                            vmPerdaDaUnidade.getObservacao(),
                            idUnidade,
                            vmPerdaDaUnidade.getPerda().getId()
                    ).alterar();
                }
                return 1904;
            }
            return 1903;
        }
        return 1907;
    }

    /**
     * Excluir uma perda relacionada a uma unidade
     *
     * @param idUnidade
     * @param vmPerdaDaUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadePerdaViewModel vmPerdaDaUnidade) {
        if (vmPerdaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmPerdaDaUnidade.getPerda() != null) {
                    return new UnidadePerda(
                            vmPerdaDaUnidade.getId(),
                            vmPerdaDaUnidade.isAtivo(),
                            vmPerdaDaUnidade.getData(),
                            vmPerdaDaUnidade.getObservacao(),
                            idUnidade,
                            vmPerdaDaUnidade.getPerda().getId()
                    ).excluir();
                }
                return 1904;
            }
            return 1903;
        }
        return 1907;
    }

    /**
     * Busca registro de perda de parada relacionado a uma unidade pelo ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadePerdaViewModel getRegPorCodigo(int id) {
        try {
            return modelToViewModel(new UnidadePerda().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * busca perda através do codigo da unidade e perda
     *
     * @param idUnidade
     * @param idPerda
     * @return objeto com registro, ou objeto nulo se não for encontrado
     */
    public UnidadePerdaViewModel getRegPorUnidadeEPerda(int idUnidade, int idPerda) {
        try {
            return modelToViewModel(new UnidadePerda().getPorUnidadeEPerda(idUnidade, idPerda));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca todas perdas registradas para uma determinada unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadePerdaViewModel> getPerdasUnidade(int idUnidade) {
        return modelToViewModel(new UnidadePerda(idUnidade).getPerdasPorUnidade());
    }

    private List<UnidadePerdaViewModel> modelToViewModel(List<UnidadePerda> listaPerdaDaUnidade) {
        List<UnidadePerdaViewModel> lista = new ArrayList();

        listaPerdaDaUnidade.forEach((up) -> {
            try {
                lista.add(modelToViewModel(up));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UnidadePerdaViewModel modelToViewModel(UnidadePerda perdaDaUnidade) throws Exception {
        try {
            return new UnidadePerdaViewModel(
                    perdaDaUnidade.getCodigo(),
                    perdaDaUnidade.isAtivo(),
                    perdaDaUnidade.getData(),
                    perdaDaUnidade.getObservacao(),
                    perdaDaUnidade.getCodPerda());
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo: " + ex.getMessage());
        }
    }

}
