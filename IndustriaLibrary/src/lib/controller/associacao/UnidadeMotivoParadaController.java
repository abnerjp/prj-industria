package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeMotivoParada;
import lib.viewmodel.associacao.UnidadeMotivoParadaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeMotivoParadaController {

    /**
     * Grava no banco de dados os valores de um novo motivo de parada para uma
     * unidade
     *
     * @param idUnidade
     * @param vmMotivoParadaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadeMotivoParadaViewModel vmMotivoParadaDaUnidade) {
        if (vmMotivoParadaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmMotivoParadaDaUnidade.getMotivoParada() != null) {
                    return new UnidadeMotivoParada(
                            vmMotivoParadaDaUnidade.isAtivo(),
                            vmMotivoParadaDaUnidade.getData(),
                            vmMotivoParadaDaUnidade.getObservacao(),
                            idUnidade,
                            vmMotivoParadaDaUnidade.getMotivoParada().getId()
                    ).gravar();
                }
                return 1504;
            }
            return 1503;
        }
        return 1507;
    }

    /**
     * Altera no banco de dados os valores de um motivo de parada relacionado a
     * uma unidade
     *
     * @param idUnidade
     * @param vmMotivoParadaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(int idUnidade, UnidadeMotivoParadaViewModel vmMotivoParadaDaUnidade) {
        if (vmMotivoParadaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmMotivoParadaDaUnidade.getMotivoParada() != null) {
                    return new UnidadeMotivoParada(
                            vmMotivoParadaDaUnidade.getId(),
                            vmMotivoParadaDaUnidade.isAtivo(),
                            vmMotivoParadaDaUnidade.getData(),
                            vmMotivoParadaDaUnidade.getObservacao(),
                            idUnidade,
                            vmMotivoParadaDaUnidade.getMotivoParada().getId()
                    ).alterar();
                }
                return 1504;
            }
            return 1503;
        }
        return 1507;
    }

    /**
     * Excluir um motivo de parada relacionado a uma unidade
     *
     * @param idUnidade
     * @param vmMotivoParadaDaUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadeMotivoParadaViewModel vmMotivoParadaDaUnidade) {
        if (vmMotivoParadaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmMotivoParadaDaUnidade.getMotivoParada() != null) {
                    return new UnidadeMotivoParada(
                            vmMotivoParadaDaUnidade.getId(),
                            vmMotivoParadaDaUnidade.isAtivo(),
                            vmMotivoParadaDaUnidade.getData(),
                            vmMotivoParadaDaUnidade.getObservacao(),
                            idUnidade,
                            vmMotivoParadaDaUnidade.getMotivoParada().getId()
                    ).excluir();
                } else {
                    return 1504;
                }
            } else {
                return 1503;
            }
        }
        return 1507;
    }

    /**
     * Busca registro de motivo de parada relacionado a uma unidade pelo ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeMotivoParadaViewModel getRegPorCodigo(int id) {
        try {
            return modelToViewModel(new UnidadeMotivoParada().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * busca motivo de parada através do codigo da unidade e motivo de parada
     *
     * @param idMotivoParada
     * @param idUnidade
     * @return objeto com registro, ou objeto nulo se não for encontrado
     */
    public UnidadeMotivoParadaViewModel getRegPorUnidadeEMotivoParada(int idUnidade, int idMotivoParada) {
        try {
            return modelToViewModel(new UnidadeMotivoParada().getPorUnidadeEMotivoParada(idUnidade, idMotivoParada));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca todos motivos de parada registrados para uma determinada unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeMotivoParadaViewModel> getMotivosParadaUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeMotivoParada(idUnidade).getMotivosParadaPorUnidade());
    }

    private List<UnidadeMotivoParadaViewModel> modelToViewModel(List<UnidadeMotivoParada> listaTipoParadaDaUnidade) {
        List<UnidadeMotivoParadaViewModel> lista = new ArrayList();

        listaTipoParadaDaUnidade.forEach((utp) -> {
            try {
                lista.add(modelToViewModel(utp));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UnidadeMotivoParadaViewModel modelToViewModel(UnidadeMotivoParada motivoParadaDaUnidade) throws Exception {
        try {
            return new UnidadeMotivoParadaViewModel(
                    motivoParadaDaUnidade.getCodigo(),
                    motivoParadaDaUnidade.isAtivo(),
                    motivoParadaDaUnidade.getData(),
                    motivoParadaDaUnidade.getObservacao(),
                    motivoParadaDaUnidade.getCodMotivoParada()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo: " + ex.getMessage());
        }
    }

}
