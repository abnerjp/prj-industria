package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeItem;
import lib.viewmodel.associacao.UnidadeItemViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeItemController {

    /**
     * Grava no banco de dados os valores de um novo item para uma unidade
     *
     * @param idUnidade
     * @param vmItemDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadeItemViewModel vmItemDaUnidade) {
        if (vmItemDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmItemDaUnidade.getItem() != null) {
                    return new UnidadeItem(
                            vmItemDaUnidade.isAtivo(),
                            vmItemDaUnidade.getData(),
                            vmItemDaUnidade.getObservacao(),
                            idUnidade, 
                            vmItemDaUnidade.getItem().getId()
                    ).gravar();
                } else {
                    //item nulo
                    return 1304;
                }
            } else {
                //unidade nula
                return 1303;
            }
        }
        //objeto item nulo {vmItemDaUnidade}
        return 1307;
    }

    /**
     * Altera no banco de dados os valores de um item relacionado a uma unidade
     *
     * @param idUnidade
     * @param vmItemDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(int idUnidade, UnidadeItemViewModel vmItemDaUnidade) {
        if (vmItemDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmItemDaUnidade.getItem() != null) {
                    return new UnidadeItem(
                            vmItemDaUnidade.getId(),
                            vmItemDaUnidade.isAtivo(),
                            vmItemDaUnidade.getData(),
                            vmItemDaUnidade.getObservacao(),
                            idUnidade,
                            vmItemDaUnidade.getItem().getId()
                    ).alterar();
                } else {
                    //item nulo
                    return 1304;
                }
            } else {
                //unidade nula
                return 1303;
            }
        }
        //objeto item nulo {vmItemDaUnidade}
        return 1307;
    }

    /**
     * Excluir um item do banco de dados, relacionado a uma unidade
     *
     * @param idUnidade
     * @param vmItemDaUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadeItemViewModel vmItemDaUnidade) {
        if (vmItemDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmItemDaUnidade.getItem() != null) {
                    return new UnidadeItem(
                            vmItemDaUnidade.getId(),
                            vmItemDaUnidade.isAtivo(),
                            vmItemDaUnidade.getData(),
                            vmItemDaUnidade.getObservacao(),
                            idUnidade, 
                            vmItemDaUnidade.getItem().getId()
                    ).excluir();
                }
                return 1304;
            }
            return 1303;
        }
        return 1307;
    }

    /**
     * Busca todos itens registrados para uma determinada unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeItemViewModel> getItensUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeItem(idUnidade).getItensPorUnidade());
    }

    /**
     * Busca registro de item, relacionado a uma unidade no banco de dados,
     * igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeItemViewModel getRegPorCodigo(int id) {
        try {
            return modelToViewModel(new UnidadeItem().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * busca registro através do codigo da unidade e do item
     *
     * @param idUnidade
     * @param idItem
     * @return objeto com registro, ou objeto nulo se não for encontrado
     */
    public UnidadeItemViewModel getRegPorUnidadeEItem(int idUnidade, int idItem) {
        try {
            return modelToViewModel(new UnidadeItem().getRegPorUnidadeEItem(idUnidade, idItem));
        } catch (Exception ex) {
            return null;
        }
    }

    private UnidadeItemViewModel modelToViewModel(UnidadeItem itemUnidade) throws Exception {
        try {
            return new UnidadeItemViewModel(
                    itemUnidade.getCodigo(),
                    itemUnidade.isAtivo(),
                    itemUnidade.getData(),
                    itemUnidade.getObservacao(),
                    itemUnidade.getCodItem()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo");
        }
    }

    private List<UnidadeItemViewModel> modelToViewModel(List<UnidadeItem> listaItemUnidade) {
        List<UnidadeItemViewModel> lista = new ArrayList<>();

        listaItemUnidade.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }
}
