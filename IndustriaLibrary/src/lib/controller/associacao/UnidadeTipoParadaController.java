package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lib.model.associacao.UnidadeTipoParada;
import lib.viewmodel.associacao.UnidadeTipoParadaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeTipoParadaController {

    /**
     * Grava no banco de dados os valores de um novo tipo de parada para uma
     * unidade
     *
     * @param idUnidade
     * @param vmTipoParadaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadeTipoParadaViewModel vmTipoParadaDaUnidade) {
        if (vmTipoParadaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmTipoParadaDaUnidade.getTipoParada() != null) {
                    return new UnidadeTipoParada(
                            vmTipoParadaDaUnidade.isAtivo(),
                            vmTipoParadaDaUnidade.getData(),
                            vmTipoParadaDaUnidade.getObservacao(),
                            idUnidade,
                            vmTipoParadaDaUnidade.getTipoParada().getId()
                    ).gravar();
                } else {
                    return 1204;
                }
            } else {
                return 1203;
            }
        }
        return 1207;
    }

    /**
     * Altera no banco de dados os valores de um tipo de parada relacionado a
     * uma unidade
     *
     * @param idUnidade
     * @param vmTipoParadaDaUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(int idUnidade, UnidadeTipoParadaViewModel vmTipoParadaDaUnidade) {
        if (vmTipoParadaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmTipoParadaDaUnidade.getTipoParada() != null) {
                    return new UnidadeTipoParada(
                            vmTipoParadaDaUnidade.getId(),
                            vmTipoParadaDaUnidade.isAtivo(),
                            vmTipoParadaDaUnidade.getData(),
                            vmTipoParadaDaUnidade.getObservacao(),
                            idUnidade,
                            vmTipoParadaDaUnidade.getTipoParada().getId()
                    ).alterar();
                } else {
                    return 1204;
                }
            } else {
                return 1203;
            }
        }
        return 1207;
    }

    /**
     * Excluir um Tipo de Parada do banco de dados, relacionado a uma unidade
     *
     * @param idUnidade
     * @param vmTipoParadaDaUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadeTipoParadaViewModel vmTipoParadaDaUnidade) {
        if (vmTipoParadaDaUnidade != null) {
            if (idUnidade > 0) {
                if (vmTipoParadaDaUnidade.getTipoParada() != null) {
                    return new UnidadeTipoParada(
                            vmTipoParadaDaUnidade.getId(),
                            vmTipoParadaDaUnidade.isAtivo(),
                            vmTipoParadaDaUnidade.getData(),
                            vmTipoParadaDaUnidade.getObservacao(),
                            idUnidade,
                            vmTipoParadaDaUnidade.getTipoParada().getId()
                    ).excluir();
                } else {
                    return 1204;
                }
            } else {
                return 1203;
            }
        }
        return 1207;
    }

    /**
     * Busca registro de Tipo de Parada, relacionado a uma Unidade no banco de
     * dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeTipoParadaViewModel getRegPorCodigo(int id) {
        try {
            return modelToViewModel(new UnidadeTipoParada().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }

    }

    /**
     * busca tipo de parada através do codigo de unidade e tipo de parada
     *
     * @param idTipoParada
     * @param idUnidade
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeTipoParadaViewModel getRegPorUnidadeETipoParada(int idUnidade, int idTipoParada) {
        try {
            return modelToViewModel(new UnidadeTipoParada().getPorUnidadeETipoParada(idUnidade, idTipoParada));
        } catch (Exception ex) {
            return null;
        }
    }
    
    /**
     * Busca todos tipos de parada registrados para uma determinada unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeTipoParadaViewModel> getTiposParadaUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeTipoParada(idUnidade).getTiposParadaPorUnidade());
    }

    private UnidadeTipoParadaViewModel modelToViewModel(UnidadeTipoParada tipoParadaDaUnidade) throws Exception {
        return new UnidadeTipoParadaViewModel(
                tipoParadaDaUnidade.getCodigo(),
                tipoParadaDaUnidade.isAtivo(),
                tipoParadaDaUnidade.getData(),
                tipoParadaDaUnidade.getObservacao(),
                tipoParadaDaUnidade.getCodTipoParada()
        );
    }

    private List<UnidadeTipoParadaViewModel> modelToViewModel(List<UnidadeTipoParada> listaTipoParadaDaUnidade) {
        List<UnidadeTipoParadaViewModel> lista = new ArrayList();

        listaTipoParadaDaUnidade.forEach((utp) -> {
            try {
                lista.add(modelToViewModel(utp));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }

}
