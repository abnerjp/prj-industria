package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeMotivoComTipoParada;
import lib.viewmodel.associacao.UnidadeMotivoComTipoParadaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeMotivoComTipoParadaController {

    /**
     * Grava no banco novo vínculo de motivo de parada com tipo de parada
     *
     * @param vmMotivoComTipo
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(UnidadeMotivoComTipoParadaViewModel vmMotivoComTipo) {
        if (vmMotivoComTipo != null) {
            if (vmMotivoComTipo.getUnidadeMotivoParada() != null) {
                if (vmMotivoComTipo.getUnidadeTipoParada() != null) {
                    return new UnidadeMotivoComTipoParada(
                            vmMotivoComTipo.isAtivo(),
                            vmMotivoComTipo.getData(),
                            vmMotivoComTipo.getObservacao(),
                            vmMotivoComTipo.getUnidadeTipoParada().getId(),
                            vmMotivoComTipo.getUnidadeMotivoParada().getId()
                    ).gravar();
                }
                return 1603;
            }
            return 1604;
        }
        return 1607;
    }

    /**
     * Grava no banco uma lista de vínculos de motivo de parada com tipo de
     * parada
     *
     * @param listaMotivoComTipo
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado no banco de dados
     */
    public int gravar(List<UnidadeMotivoComTipoParadaViewModel> listaMotivoComTipo) {
        if (listaMotivoComTipo != null && listaMotivoComTipo.size() > 0) {
            List<UnidadeMotivoComTipoParada> lista = new ArrayList<>();
            listaMotivoComTipo.forEach((vmMotivoComTipo) -> {
                lista.add(new UnidadeMotivoComTipoParada(
                        vmMotivoComTipo.isAtivo(),
                        vmMotivoComTipo.getData(),
                        vmMotivoComTipo.getObservacao(),
                        vmMotivoComTipo.getUnidadeTipoParada().getId(),
                        vmMotivoComTipo.getUnidadeMotivoParada().getId())
                );
            });
            return new UnidadeMotivoComTipoParada().gravar(lista);
        }
        return 1607;
    }

    /**
     * Altera no banco novo vínculo de motivo de parada com tipo de parada
     *
     * @param vmMotivoComTipo
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(UnidadeMotivoComTipoParadaViewModel vmMotivoComTipo) {
        if (vmMotivoComTipo != null) {
            if (vmMotivoComTipo.getUnidadeMotivoParada() != null) {
                if (vmMotivoComTipo.getUnidadeTipoParada() != null) {
                    return new UnidadeMotivoComTipoParada(
                            vmMotivoComTipo.getId(),
                            vmMotivoComTipo.isAtivo(),
                            vmMotivoComTipo.getData(),
                            vmMotivoComTipo.getObservacao(),
                            vmMotivoComTipo.getUnidadeTipoParada().getId(),
                            vmMotivoComTipo.getUnidadeMotivoParada().getId()
                    ).alterar();
                }
                return 1603;
            }
            return 1604;
        }
        return 1607;
    }

    /**
     * Excluir um vínculo de motico de parada com tipo de parada do banco de
     * dados
     *
     * @param vmMotivoComTipo
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(UnidadeMotivoComTipoParadaViewModel vmMotivoComTipo) {
        if (vmMotivoComTipo != null) {
            if (vmMotivoComTipo.getUnidadeMotivoParada() != null) {
                if (vmMotivoComTipo.getUnidadeTipoParada() != null) {
                    return new UnidadeMotivoComTipoParada(
                            vmMotivoComTipo.getId(),
                            vmMotivoComTipo.isAtivo(),
                            vmMotivoComTipo.getData(),
                            vmMotivoComTipo.getObservacao(),
                            vmMotivoComTipo.getUnidadeTipoParada().getId(),
                            vmMotivoComTipo.getUnidadeMotivoParada().getId()
                    ).excluir();
                }
                return 1603;
            }
            return 1604;
        }
        return 1607;
    }

    /**
     * Busca vínculo de motivo de parada com tipo de parada no banco de dados,
     * através do id
     *
     * @param idVinculoMotivoComTipoParada
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeMotivoComTipoParadaViewModel getRegPorCodigoDoVinculo(int idVinculoMotivoComTipoParada) {
        try {
            return modelToViewModel(new UnidadeMotivoComTipoParada().getRegPorCodigo(idVinculoMotivoComTipoParada));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca vínculo de motivo de parada com tipo de parada no banco de dados,
     * através do id do motivo de parada e tipo de parada de uma unidade
     *
     * @param idUnidadeTipoParada
     * @param idUnidadeMotivoParada
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeMotivoComTipoParadaViewModel getRegPorTipoParadaEMotivoParada(int idUnidadeTipoParada, int idUnidadeMotivoParada) {
        try {
            return modelToViewModel(new UnidadeMotivoComTipoParada().getRegPorTipoParadaEMotivoParada(idUnidadeTipoParada, idUnidadeMotivoParada));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca todos motivos de parada registrados para uma tipo de parada de uma
     * unidade
     *
     * @param idUnidadeTipoParada
     * @return retorna uma lista com N registros
     */
    public List<UnidadeMotivoComTipoParadaViewModel> getMotivosParadaPorTipoParada(int idUnidadeTipoParada) {
        return modelToViewModel(new UnidadeMotivoComTipoParada(idUnidadeTipoParada).getMotivosParadaPorTipoParada());
    }

    /**
     * Busca todos os vínculos de motivo de parada com tipo de parada de uma
     * unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeMotivoComTipoParadaViewModel> getMotivosETiposPorUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeMotivoComTipoParada().getMotivosETiposPorUnidade(idUnidade));
    }

    private List<UnidadeMotivoComTipoParadaViewModel> modelToViewModel(List<UnidadeMotivoComTipoParada> listaMotivoComParada) {
        List<UnidadeMotivoComTipoParadaViewModel> lista = new ArrayList<>();

        listaMotivoComParada.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UnidadeMotivoComTipoParadaViewModel modelToViewModel(UnidadeMotivoComTipoParada motivoComTipoParada) throws Exception {
        try {
            return new UnidadeMotivoComTipoParadaViewModel(
                    motivoComTipoParada.getCodigo(),
                    motivoComTipoParada.isAtivo(),
                    motivoComTipoParada.getData(),
                    motivoComTipoParada.getObservacao(),
                    motivoComTipoParada.getCodUnidadeTipoParada(),
                    motivoComTipoParada.getCodUnidadeMotivoParada()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo");
        }
    }
}
