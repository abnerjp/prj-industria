package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeEndereco;
import lib.viewmodel.associacao.UnidadeEnderecoViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeEnderecoController {

    /**
     * Grava no banco de dados os valores de um novo endereço para uma unidade
     *
     * @param idUnidade
     * @param vmEndUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(int idUnidade, UnidadeEnderecoViewModel vmEndUnidade) {
        if (vmEndUnidade != null) {
            if (idUnidade > 0) {
                if (vmEndUnidade.getEndereco() != null) {
                    return new UnidadeEndereco(
                            vmEndUnidade.getNumero(),
                            vmEndUnidade.getData(),
                            vmEndUnidade.getComplemento(),
                            vmEndUnidade.getObservacao(),
                            idUnidade,
                            vmEndUnidade.getEndereco().getId()
                    ).gravar();
                } else {
                    //endereço nulo
                    return 1004;
                }
            } else {
                //unidade nula
                return 1003;
            }
        }
        //endereço da unidade nulo {vmEndUnidade)
        return 1009;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param idUnidade
     * @param vmEndUnidade
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int idUnidade, UnidadeEnderecoViewModel vmEndUnidade) {
        if (vmEndUnidade != null) {
            if (idUnidade > 0) {
                if (vmEndUnidade.getEndereco() != null) {
                    return new UnidadeEndereco(
                            vmEndUnidade.getNumero(),
                            vmEndUnidade.getData(),
                            vmEndUnidade.getComplemento(),
                            vmEndUnidade.getObservacao(),
                            idUnidade,
                            vmEndUnidade.getEndereco().getId()
                    ).excluir();
                } else {
                    //endereço nulo
                    return 1004;
                }
            } else {
                //unidade nula
                return 1003;
            }
        }
        //endereço da unidade nulo {vmEndUnidade}
        return 1009;
    }

    /**
     * Busca o endereço atual de uma unidade
     *
     * @param idUnidade
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeEnderecoViewModel getEnderecoAtual(int idUnidade) {
        return modelToViewModel(new UnidadeEndereco().getEnderecoAtual(idUnidade));
    }

    /**
     * Busca todos endereços já registrados para uma unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeEnderecoViewModel> getTodosEnderecos(int idUnidade) {
        return modelToViewModel(new UnidadeEndereco().getTodosEnderecos(idUnidade));
    }

    private UnidadeEnderecoViewModel modelToViewModel(UnidadeEndereco eu) {
        if (eu != null) {
            return new UnidadeEnderecoViewModel(
                    eu.getNumero(),
                    eu.getData(),
                    eu.getComplemento(),
                    eu.getObservacao(),
                    eu.getCodEndereco()
            );
        }
        return null;
    }

    private List<UnidadeEnderecoViewModel> modelToViewModel(List<UnidadeEndereco> listaEnderecoUnidade) {
        List<UnidadeEnderecoViewModel> lista = new ArrayList<>();

        listaEnderecoUnidade.forEach((eu) -> {
            lista.add(modelToViewModel(eu));
        });
        return lista;
    }
}
