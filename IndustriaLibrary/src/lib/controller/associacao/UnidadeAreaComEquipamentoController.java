package lib.controller.associacao;

import java.util.ArrayList;
import java.util.List;
import lib.model.associacao.UnidadeAreaComEquipamento;
import lib.viewmodel.associacao.UnidadeAreaComEquipamentoViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeAreaComEquipamentoController {

    /**
     * Grava no banco novo vínculo de equipamento com área
     *
     * @param vmAreaComEquipamento
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(UnidadeAreaComEquipamentoViewModel vmAreaComEquipamento) {
        if (vmAreaComEquipamento != null) {
            if (vmAreaComEquipamento.getUnidadeEquipamento() != null) {
                if (vmAreaComEquipamento.getUnidadeArea() != null) {
                    System.out.println(vmAreaComEquipamento.getReferencia());
                    return new UnidadeAreaComEquipamento(
                            vmAreaComEquipamento.isAtivo(),
                            vmAreaComEquipamento.getReferencia(),
                            vmAreaComEquipamento.getData(),
                            vmAreaComEquipamento.getObservacao(),
                            vmAreaComEquipamento.getUnidadeArea().getId(),
                            vmAreaComEquipamento.getUnidadeEquipamento().getId()
                    ).gravar();
                }
                return 2403;
            }
            return 2404;
        }
        return 2408;
    }

    /**
     * Grava no banco uma lista de vínculos de área com equipamento
     *
     * @param listaAreaComEquipamento
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado no banco de dados
     */
    public int gravar(List<UnidadeAreaComEquipamentoViewModel> listaAreaComEquipamento) {
        if (listaAreaComEquipamento != null && listaAreaComEquipamento.size() > 0) {
            List<UnidadeAreaComEquipamento> lista = new ArrayList<>();
            listaAreaComEquipamento.forEach((vmAreaComEquipamento) -> {
                lista.add(new UnidadeAreaComEquipamento(
                        vmAreaComEquipamento.isAtivo(),
                        vmAreaComEquipamento.getReferencia(),
                        vmAreaComEquipamento.getData(),
                        vmAreaComEquipamento.getObservacao(),
                        vmAreaComEquipamento.getUnidadeArea().getId(),
                        vmAreaComEquipamento.getUnidadeEquipamento().getId())
                );
            });
            return new UnidadeAreaComEquipamento().gravar(lista);
        }
        return 2408;
    }

    /**
     * Altera no banco vínculo de área com equipamento
     *
     * @param vmAreaComEquipamento
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(UnidadeAreaComEquipamentoViewModel vmAreaComEquipamento) {
        if (vmAreaComEquipamento != null) {
            if (vmAreaComEquipamento.getUnidadeEquipamento() != null) {
                if (vmAreaComEquipamento.getUnidadeArea() != null) {
                    return new UnidadeAreaComEquipamento(
                            vmAreaComEquipamento.getId(),
                            vmAreaComEquipamento.isAtivo(),
                            vmAreaComEquipamento.getReferencia(),
                            vmAreaComEquipamento.getData(),
                            vmAreaComEquipamento.getObservacao(),
                            vmAreaComEquipamento.getUnidadeArea().getId(),
                            vmAreaComEquipamento.getUnidadeEquipamento().getId()
                    ).alterar();
                }
                return 2403;
            }
            return 2404;
        }
        return 2408;
    }

    /**
     * Excluir um vínculo de área com equipamento do banco de dados
     *
     * @param vmAreaComEquipamento
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(UnidadeAreaComEquipamentoViewModel vmAreaComEquipamento) {
        if (vmAreaComEquipamento != null) {
            if (vmAreaComEquipamento.getUnidadeEquipamento() != null) {
                if (vmAreaComEquipamento.getUnidadeArea() != null) {
                    return new UnidadeAreaComEquipamento(
                            vmAreaComEquipamento.getId(),
                            vmAreaComEquipamento.isAtivo(),
                            vmAreaComEquipamento.getReferencia(),
                            vmAreaComEquipamento.getData(),
                            vmAreaComEquipamento.getObservacao(),
                            vmAreaComEquipamento.getUnidadeArea().getId(),
                            vmAreaComEquipamento.getUnidadeEquipamento().getId()
                    ).excluir();
                }
                return 2403;
            }
            return 2404;
        }
        return 2408;
    }

    /**
     * Busca vínculo de equipamento com área no banco de dados, através do id
     *
     * @param idVinculoAreaCoEquipamento
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeAreaComEquipamentoViewModel getRegPorCodigoDoVinculo(int idVinculoAreaCoEquipamento) {
        try {
            return modelToViewModel(new UnidadeAreaComEquipamento().getRegPorCodigo(idVinculoAreaCoEquipamento));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca vínculo de equipamento com área no banco de dados, através do id do
     * equipamento, id da área e referência
     *
     * @param idUnidadeArea
     * @param idUnidadeEquipamento
     * @param referencia
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeAreaComEquipamentoViewModel getRegPorEquipamentoEArea(int idUnidadeArea, int idUnidadeEquipamento, String referencia) {
        try {
            return modelToViewModel(new UnidadeAreaComEquipamento().getRegistro(idUnidadeArea, idUnidadeEquipamento, referencia));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca todos equipamentos registrados para uma área de uma unidade
     *
     * @param idUnidadeArea
     * @return retorna uma lista com N registros
     */
    public List<UnidadeAreaComEquipamentoViewModel> getEquipamentosPorArea(int idUnidadeArea) {
        return modelToViewModel(new UnidadeAreaComEquipamento(idUnidadeArea).getEquipamentosPorArea());
    }

    /**
     * Busca todos os vínculos de equipamentos com área de uma unidade
     *
     * @param idUnidade
     * @return retorna uma lista com N registros
     */
    public List<UnidadeAreaComEquipamentoViewModel> getEquipamentosEAreasPorUnidade(int idUnidade) {
        return modelToViewModel(new UnidadeAreaComEquipamento().getEquipamentosEAreasPorUnidade(idUnidade));
    }

    private List<UnidadeAreaComEquipamentoViewModel> modelToViewModel(List<UnidadeAreaComEquipamento> listaAreaComEquipamento) {
        List<UnidadeAreaComEquipamentoViewModel> lista = new ArrayList<>();
        listaAreaComEquipamento.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                //item não incluido na lista. aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UnidadeAreaComEquipamentoViewModel modelToViewModel(UnidadeAreaComEquipamento areaComEquipamento) throws Exception {
        try {
            return new UnidadeAreaComEquipamentoViewModel(
                    areaComEquipamento.getCodigo(),
                    areaComEquipamento.isAtivo(),
                    areaComEquipamento.getReferencia(),
                    areaComEquipamento.getData(),
                    areaComEquipamento.getObservacao(),
                    areaComEquipamento.getCodUnidadeArea(),
                    areaComEquipamento.getCodUnidadeEquipamento()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo");
        }
    }
}
