package lib.controller.cadastro;

import java.util.ArrayList;
import java.util.List;
import lib.model.cadastro.Area;
import lib.viewmodel.cadastro.AreaViewModel;

/**
 *
 * @author abnerjp
 */
public class AreaController {

    public AreaController() {
    }

    /**
     * Grava no banco de dados os valores de uma nova Área
     *
     * @param vmArea
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(AreaViewModel vmArea) {
        if (vmArea != null) {
            return new Area(
                    vmArea.getNome(),
                    vmArea.getDescricao(),
                    vmArea.isAtivo(),
                    vmArea.getObservacao()
            ).gravar();
        }
        return 1107;
    }

    /**
     * Altera no banco de dados os valores de uma nova Área
     *
     * @param vmArea
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int alterar(AreaViewModel vmArea) {
        if (vmArea != null) {
            return new Area(
                    vmArea.getId(),
                    vmArea.getNome(),
                    vmArea.getDescricao(),
                    vmArea.isAtivo(),
                    vmArea.getObservacao()
            ).alterar();
        }
        return 1107;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Area(id).excluir();
    }

    /**
     * Busca o primeiro registro de Área no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public AreaViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Area().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Área no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public AreaViewModel getUltimoRegistro() {
        return modelToViewModel(new Area().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Área no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public AreaViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Area(id).getProximoRegistro());
    }

    /**
     * Busca registro de Área no banco de dados , anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public AreaViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Area(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Área no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public AreaViewModel getRegPorCodigo(int id) {
        return modelToViewModel(new Area().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeArea
     * @return retorna uma lista com N registros
     */
    public List<AreaViewModel> getListaPorNome(String nomeArea) {
        return modelToViewModel(new Area().getListaPorNome(nomeArea));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeArea
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<AreaViewModel> getListaLimitada(String nomeArea, int qtdeResultado) {
        return modelToViewModel(new Area().getListaLimitada(nomeArea, qtdeResultado));
    }

    /* métodos internos à classe */
    private AreaViewModel modelToViewModel(Area a) {
        if (a != null) {
            return new AreaViewModel(
                    a.getCodigo(),
                    a.getNome(),
                    a.getDescricao(),
                    a.isAtivo(),
                    a.getObservacao()
            );
        }
        return null;
    }

    public List<AreaViewModel> modelToViewModel(List<Area> listaArea) {
        List<AreaViewModel> lista = new ArrayList<>();

        listaArea.forEach((a) -> {
            lista.add(modelToViewModel(a));
        });
        return lista;
    }
}
