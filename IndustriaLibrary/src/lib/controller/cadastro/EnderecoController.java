package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Endereco;
import java.util.List;
import lib.viewmodel.cadastro.EnderecoViewModel;

/**
 *
 * @author abnerjp
 */
public class EnderecoController {

    public EnderecoController() {
    }

    /**
     * Grava no banco de dados os valores de um novo Endereço
     *
     * @param vmEndereco
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(EnderecoViewModel vmEndereco) {
        if (vmEndereco != null) {
            if (vmEndereco.getCidade() != null) {
                if (!vmEndereco.getCidade().isAtivo()) {
                    return 810;
                }
                return new Endereco(
                        vmEndereco.getId(),
                        vmEndereco.getLogradouro(),
                        vmEndereco.getCep().replace("-", ""),
                        vmEndereco.getBairro(),
                        vmEndereco.getObservacao(),
                        vmEndereco.getCidade().getId()
                ).gravar();
            }
            return 808;
        }
        return 811;
    }

    /**
     * Altera no banco de dados os valores de um Endereço
     *
     * @param vmEndereco
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(EnderecoViewModel vmEndereco) {
        if (vmEndereco != null) {
            if (vmEndereco.getCidade() != null) {
                if (!vmEndereco.getCidade().isAtivo()) {
                    EnderecoViewModel enderecoAux = regPorCodigo(vmEndereco.getId());
                    if (enderecoAux != null && enderecoAux.getCidade().getId() != vmEndereco.getCidade().getId()) {
                        return 810;
                    }
                }
                return new Endereco(
                        vmEndereco.getId(),
                        vmEndereco.getLogradouro(),
                        vmEndereco.getCep().replace("-", ""),
                        vmEndereco.getBairro(),
                        vmEndereco.getObservacao(),
                        vmEndereco.getCidade().getId()
                ).alterar();
            }
            return 808;
        }
        return 811;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new Endereco(id).excluir();
    }

    /**
     * Busca o primeiro registro de Endereço no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EnderecoViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Endereco().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Endereço no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EnderecoViewModel getUltimoRegistro() {
        return modelToViewModel(new Endereco().getUltimoRegistro());
    }

    /**
     * Busca registro de Endereço no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EnderecoViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Endereco(id).getProximoRegistro());
    }

    /**
     * Busca registro de Endereço no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EnderecoViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Endereco(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Endereço no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EnderecoViewModel regPorCodigo(int id) {
        return modelToViewModel(new Endereco().getRegPorCodigo(id));
    }

    /**
     * Busca registro de Endereço no banco de dados igual ao CEP informado
     *
     * @param cep
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EnderecoViewModel regPorCep(String cep) {
        return modelToViewModel(new Endereco().getRegPorCep(cep));
    }

    /**
     * Busca registros de acordo com o Logradouro informado
     *
     * @param logradouro
     * @return retorna uma lista com N registros
     */
    public List<EnderecoViewModel> listaPorEndereco(String logradouro) {
        return modelToViewModel(new Endereco().getListaPorEndereco(logradouro));
    }

    /**
     * Busca registros de acordo com o CEP informado
     *
     * @param cepEndereco
     * @return retorna uma lista com N registros
     */
    public List<EnderecoViewModel> listaPorCep(String cepEndereco) {
        return modelToViewModel(new Endereco().getListaPorCep(cepEndereco));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param logradouro
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<EnderecoViewModel> listaLimitada(String logradouro, int qtdeResultado) {
        return modelToViewModel(new Endereco().getListaLimitada(logradouro, qtdeResultado));
    }

    /**
     * Busca registros de acordo com a Cidade informada
     *
     * @param nomeCidade
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<EnderecoViewModel> listaPorCidade(String nomeCidade) {
        return modelToViewModel(new Endereco().getListaPorCidade(nomeCidade));
    }

    private EnderecoViewModel modelToViewModel(Endereco e) {
        if (e != null) {
            return new EnderecoViewModel(
                    e.getCodigo(),
                    e.getLogradouro(),
                    e.getCep(),
                    e.getBairro(),
                    e.getObservacao(),
                    e.getCodigoCidade()
            );
        }
        return null;
    }

    private List<EnderecoViewModel> modelToViewModel(List<Endereco> listaEndereco) {
        List<EnderecoViewModel> lista = new ArrayList<>();

        listaEndereco.forEach((e) -> {
            lista.add(modelToViewModel(e));
        });
        return lista;
    }
}
