package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Medida;
import java.util.List;
import lib.viewmodel.cadastro.MedidaViewModel;

/**
 *
 * @author abnerjp
 */
public class MedidaController {

    public MedidaController() {
    }

    /**
     * Grava no banco de dados os valores de uma nova Medida
     *
     * @param vmMedida
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(MedidaViewModel vmMedida) {
        if (vmMedida != null) {
            return new Medida(
                    vmMedida.getId(),
                    vmMedida.getNome(),
                    vmMedida.getSigla(),
                    vmMedida.isAtivo(),
                    vmMedida.getObservacao()
            ).gravar();
        }
        return 8;
    }

    /**
     * Altera no banco de dados os valores de uma Medida
     *
     * @param vmMedida
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(MedidaViewModel vmMedida) {
        if (vmMedida != null) {
            return new Medida(
                    vmMedida.getId(),
                    vmMedida.getNome(),
                    vmMedida.getSigla(),
                    vmMedida.isAtivo(),
                    vmMedida.getObservacao()
            ).alterar();
        }
        return 8;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Medida(id).excluir();
    }

    /**
     * Busca o primeiro registro de Medida no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MedidaViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Medida().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Medida no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MedidaViewModel getUltimoRegistro() {
        return modelToViewModel(new Medida().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Medida no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MedidaViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Medida(id).getProximoRegistro());
    }

    /**
     * Busca registro de Medida no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MedidaViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Medida(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Medida no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MedidaViewModel regPorCodigo(int id) {
        return modelToViewModel(new Medida().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com a sigla informada
     *
     * @param siglaMedida
     * @return retorna uma lista com N registros
     */
    public List<MedidaViewModel> listaPorSigla(String siglaMedida) {
        return modelToViewModel(new Medida().getListaPorSigla(siglaMedida));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeMedida
     * @return retorna uma lista com N registros
     */
    public List<MedidaViewModel> listaPorNome(String nomeMedida) {
        return modelToViewModel(new Medida().getListaPorNome(nomeMedida));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeMedida
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<MedidaViewModel> listaLimitada(String nomeMedida, int qtdeResultado) {
        return modelToViewModel(new Medida().getListaLimitada(nomeMedida, qtdeResultado));
    }

    /* métodos internos à classe */
    private MedidaViewModel modelToViewModel(Medida m) {
        if (m != null) {
            return new MedidaViewModel(
                    m.getCodigo(),
                    m.getNome(),
                    m.getSigla(),
                    m.isAtivo(),
                    m.getObservacao()
            );
        }
        return null;
    }

    public List<MedidaViewModel> modelToViewModel(List<Medida> listaMedida) {
        List<MedidaViewModel> lista = new ArrayList<>();

        listaMedida.forEach((m) -> {
            lista.add(modelToViewModel(m));
        });
        return lista;
    }
}
