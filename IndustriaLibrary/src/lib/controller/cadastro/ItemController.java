package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Item;
import java.util.List;
import lib.viewmodel.cadastro.ItemViewModel;

/**
 *
 * @author abnerjp
 */
public class ItemController {

    public ItemController() {
    }

    /**
     * Grava no banco de dados os valores de um novo Item
     *
     * @param vmItem
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(ItemViewModel vmItem) {
        if (vmItem != null) {
            if (vmItem.getMedida() != null) {
                if (!vmItem.getMedida().isAtivo()) {
                    return 107;
                }
                return new Item(
                        vmItem.getId(),
                        vmItem.getNome(),
                        vmItem.isAtivo(),
                        vmItem.getObservacao(),
                        vmItem.getMedida().getId()
                ).gravar();
            }
            return 105;
        }
        return 108;
    }

    /**
     * Altera no banco de dados os valores de um Item
     *
     * @param vmItem
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(ItemViewModel vmItem) {
        if (vmItem != null) {
            if (vmItem.getMedida() != null) {
                if (!vmItem.getMedida().isAtivo()) {
                    ItemViewModel itemAux = regPorCodigo(vmItem.getId());
                    if (itemAux != null && itemAux.getMedida().getId() != vmItem.getMedida().getId()) {
                        return 107;
                    }
                }
                return new Item(
                        vmItem.getId(),
                        vmItem.getNome(),
                        vmItem.isAtivo(),
                        vmItem.getObservacao(),
                        vmItem.getMedida().getId()
                ).alterar();
            }
            return 105;
        }
        return 108;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new Item(id).excluir();
    }

    /**
     * Busca o primeiro registro de Item no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public ItemViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Item().getProximoRegistro());
    }

    /**
     * Busca o último registro de Item no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public ItemViewModel getUltimoRegistro() {
        return modelToViewModel(new Item().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Item no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public ItemViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Item(id).getProximoRegistro());
    }

    /**
     * Busca registro de Item no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public ItemViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Item(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Item no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public ItemViewModel regPorCodigo(int id) {
        return modelToViewModel(new Item().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeItem
     * @return retorna uma lista com N registros
     */
    public List<ItemViewModel> listaPorNome(String nomeItem) {
        return modelToViewModel(new Item().getListaPorNome(nomeItem));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeMedida
     * @return retorna uma lista com N registros
     */
    public List<ItemViewModel> listaPorMedida(String nomeMedida) {
        return modelToViewModel(new Item().getListaPorMedida(nomeMedida));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeItem
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<ItemViewModel> listaLimitada(String nomeItem, int qtdeResultado) {
        return modelToViewModel(new Item().getListaLimitada(nomeItem, qtdeResultado));
    }

    private ItemViewModel modelToViewModel(Item i) {
        if (i != null) {
            return new ItemViewModel(
                    i.getCodigo(),
                    i.getNome(),
                    i.isAtivo(),
                    i.getObservacao(),
                    i.getCodigoMedida()
            );
        }
        return null;
    }

    private List<ItemViewModel> modelToViewModel(List<Item> listaItem) {
        List<ItemViewModel> lista = new ArrayList<>();

        listaItem.forEach((i) -> {
            lista.add(modelToViewModel(i));
        });
        return lista;
    }
}
