package lib.controller.cadastro;

import lib.controller.associacao.UnidadeEnderecoController;
import java.util.ArrayList;
import java.util.List;
import lib.model.cadastro.Unidade;
import lib.viewmodel.associacao.UnidadeEnderecoViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abner.jacomo
 */
public class UnidadeController {

    public UnidadeController() {
    }

    /**
     * Grava no banco de dados os valores de uma nova Unidade
     *
     * @param vmUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(UnidadeViewModel vmUnidade) {
        if (vmUnidade != null) {
            return new Unidade(
                    vmUnidade.getId(),
                    vmUnidade.getRazaoSocial(),
                    vmUnidade.getCnpj(),
                    vmUnidade.getInscricaoEstadual(),
                    vmUnidade.getTelefoneFixo(),
                    vmUnidade.getTelefoneCelular(),
                    vmUnidade.getEmail(),
                    vmUnidade.getNomeFantasia(),
                    vmUnidade.getReferencia(),
                    vmUnidade.getSenha(),
                    vmUnidade.isAtivo(),
                    vmUnidade.getObservacao()
            ).gravar();
        }
        return 915;
    }

    /**
     * Altera no banco de dados os valores de uma nova Unidade
     *
     * @param vmUnidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int alterar(UnidadeViewModel vmUnidade) {
        if (vmUnidade != null) {
            return new Unidade(
                    vmUnidade.getId(),
                    vmUnidade.getRazaoSocial(),
                    vmUnidade.getCnpj(),
                    vmUnidade.getInscricaoEstadual(),
                    vmUnidade.getTelefoneFixo(),
                    vmUnidade.getTelefoneCelular(),
                    vmUnidade.getEmail(),
                    vmUnidade.getNomeFantasia(),
                    vmUnidade.getReferencia(),
                    vmUnidade.getSenha(),
                    vmUnidade.isAtivo(),
                    vmUnidade.getObservacao()
            ).alterar();
        }
        return 915;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new Unidade(id).excluir();
    }

    /**
     * Busca o primeiro registro de Unidade no banco de dados
     *
     * @param comEnderecoAtual
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeViewModel getPrimeiroRegistro(boolean comEnderecoAtual) {
        if (comEnderecoAtual) {
            return comEnderecoAtual(modelToViewModel(new Unidade().getPrimeiroRegistro()));
        }
        return modelToViewModel(new Unidade().getPrimeiroRegistro());

    }

    /**
     * Busca o último registro de Unidade no banco de dados
     *
     * @param comEnderecoAtual
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeViewModel getUltimoRegistro(boolean comEnderecoAtual) {
        if (comEnderecoAtual) {
            return comEnderecoAtual(modelToViewModel(new Unidade().getUltimoRegistro()));
        }
        return modelToViewModel(new Unidade().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Unidade no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @param comEnderecoAtual
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeViewModel getProximoRegistro(int id, boolean comEnderecoAtual) {
        if (comEnderecoAtual) {
            return comEnderecoAtual(modelToViewModel(new Unidade(id).getProximoRegistro()));
        }
        return modelToViewModel(new Unidade(id).getProximoRegistro());
    }

    /**
     * Busca registro de Unidade no banco de dados, anterior ao ID informado
     *
     * @param id
     * @param comEnderecoAtual
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeViewModel getAnteriorRegistro(int id, boolean comEnderecoAtual) {
        if (comEnderecoAtual) {
            return comEnderecoAtual(modelToViewModel(new Unidade(id).getAnteriorRegistro()));
        }
        return modelToViewModel(new Unidade(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Unidade no banco de dados igual ao ID informado
     *
     * @param id
     * @param comEnderecoAtual
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeViewModel regPorCodigo(int id, boolean comEnderecoAtual) {
        if (comEnderecoAtual) {
            return comEnderecoAtual(modelToViewModel(new Unidade().getRegPorCodigo(id)));
        }
        return modelToViewModel(new Unidade().getRegPorCodigo(id));
    }

    /**
     * Busca registro de Unidade no banco de dados igual ao referência informada
     *
     * @param referenciaUnidade
     * @param comEnderecoAtual
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UnidadeViewModel regPorReferencia(String referenciaUnidade, boolean comEnderecoAtual) {
        if (comEnderecoAtual) {
            return comEnderecoAtual(modelToViewModel(new Unidade().getRegPorReferencia(referenciaUnidade)));
        }
        return modelToViewModel(new Unidade().getRegPorReferencia(referenciaUnidade));
    }

    /**
     * Busca registros de acordo com a referência informada
     *
     * @param nomeReferencia
     * @return retorna uma lista com N registros
     */
    public List<UnidadeViewModel> listaPorReferencia(String nomeReferencia) {
        return modelToViewModel(new Unidade().getListaPorReferencia(nomeReferencia));
    }

    /**
     * Busca registros de acordo com o nome fantasia informado
     *
     * @param nomeFantasia
     * @return retorna uma lista com N registros
     */
    public List<UnidadeViewModel> listaPorNomeFantasia(String nomeFantasia) {
        return modelToViewModel(new Unidade().getListaPorNomeFantasia(nomeFantasia));
    }

    /**
     * Busca registros de acordo com a razão social informada
     *
     * @param razaoSocial
     * @return retorna uma lista com N registros
     */
    public List<UnidadeViewModel> listaPorRazaoSocial(String razaoSocial) {
        return modelToViewModel(new Unidade().getListaPorRazaoSocial(razaoSocial));
    }

    /**
     * Busca registros de acordo com o CNPJ informado
     *
     * @param cnpj
     * @return retorna uma lista com N registros
     */
    public List<UnidadeViewModel> listaPorCnpj(String cnpj) {
        return modelToViewModel(new Unidade().getListaPorCnpj(cnpj));
    }

    /**
     * Busca histórico de endereço de uma Unidade, de acordo com o Id informado
     *
     * @param id
     * @return retorna uma lista com N registros
     */
    public List<UnidadeEnderecoViewModel> getHistoricoEndereco(int id) {
        return new UnidadeEnderecoController().getTodosEnderecos(id);
    }

    /**
     * Busca registros de Unidade de acordo com o nome fantasia informado
     *
     * @param nomeFantasia
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<UnidadeViewModel> listaLimitada(String nomeFantasia, int qtdeResultado) {
        return modelToViewModel(new Unidade().getListaLimitada(nomeFantasia, qtdeResultado));
    }

    private UnidadeViewModel comEnderecoAtual(UnidadeViewModel vmAux) {
        if (vmAux != null) {
            vmAux.setEnderecoAtual(new UnidadeEnderecoController().getEnderecoAtual(vmAux.getId()));
        }
        return vmAux;
    }

    private UnidadeViewModel modelToViewModel(Unidade u) {
        if (u != null) {
            return new UnidadeViewModel(
                    u.getCodigo(),
                    u.getRazaoSocial(),
                    u.getCnpj(),
                    u.getInscricaoEstadual(),
                    u.getTelefoneFixo(),
                    u.getTelefoneCelular(),
                    u.getEmail(),
                    u.getNomeFantasia(),
                    u.getReferencia(),
                    u.getSenha(),
                    u.isAtivo(),
                    u.getObservacao()
            );
        }
        return null;
    }

    private List<UnidadeViewModel> modelToViewModel(List<Unidade> listaUnidade) {
        List<UnidadeViewModel> lista = new ArrayList<>();

        listaUnidade.forEach((u) -> {
            lista.add(modelToViewModel(u));
        });
        return lista;
    }
}
