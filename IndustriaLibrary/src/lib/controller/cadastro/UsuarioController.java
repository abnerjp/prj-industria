package lib.controller.cadastro;

import java.util.ArrayList;
import java.util.List;
import lib.controller.associacao.UsuarioEnderecoController;
import lib.model.cadastro.Usuario;
import lib.viewmodel.associacao.UsuarioEnderecoViewModel;
import lib.viewmodel.cadastro.UsuarioViewModel;

/**
 *
 * @author abnerjp
 */
public class UsuarioController {

    /**
     * Grava no banco de dados um novo usuário
     *
     * @param vmUsuario
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(UsuarioViewModel vmUsuario) {
        if (vmUsuario != null) {
            return new Usuario(
                    vmUsuario.getNome(),
                    vmUsuario.getSexo(),
                    vmUsuario.getLogin(),
                    vmUsuario.getSenha(),
                    vmUsuario.getEmail(),
                    vmUsuario.getImagem(),
                    vmUsuario.isAtivo(),
                    vmUsuario.getObservacao()
            ).gravar();
        }
        return 2014;
    }

    /**
     * Altera no banco de dados os dados do usuário
     *
     * @param vmUsuario
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(UsuarioViewModel vmUsuario) {
        if (vmUsuario != null) {
            return new Usuario(
                    vmUsuario.getId(),
                    vmUsuario.getNome(),
                    vmUsuario.getSexo(),
                    vmUsuario.getLogin(),
                    vmUsuario.getSenha(),
                    vmUsuario.getEmail(),
                    vmUsuario.getImagem(),
                    vmUsuario.isAtivo(),
                    vmUsuario.getObservacao()
            ).alterar();
        }
        return 2014;
    }

    /**
     * Altera no banco de dados os dados do usuário, com exceção da imagem
     *
     * @param vmUsuario
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterarSomenteDados(UsuarioViewModel vmUsuario) {
        if (vmUsuario != null) {
            return new Usuario(
                    vmUsuario.getId(),
                    vmUsuario.getNome(),
                    vmUsuario.getSexo(),
                    vmUsuario.getLogin(),
                    vmUsuario.getSenha(),
                    vmUsuario.getEmail(),
                    vmUsuario.getImagem(),
                    vmUsuario.isAtivo(),
                    vmUsuario.getObservacao()
            ).alterarDados();
        }
        return 2014;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Usuario(id).excluir();
    }

    /**
     * Busca o primeiro registro de Usuário no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioViewModel getPrimeiroRegistro() {
        try {
            return modelToViewModel(new Usuario().getPrimeiroRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca o último registro de Usuário no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioViewModel getUltimoRegistro() {
        try {
            return modelToViewModel(new Usuario().getUltimoRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca o próximo registro de Usuário no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioViewModel getProximoRegistro(int id) {
        try {
            return modelToViewModel(new Usuario(id).getProximoRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de Usuário no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioViewModel getAnteriorRegistro(int id) {
        try {
            return modelToViewModel(new Usuario(id).getAnteriorRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de Usuário no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioViewModel regPorCodigo(int id) {
        try {
            return modelToViewModel(new Usuario().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de Usuário no banco de dados igual ao Login informado
     *
     * @param login
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public UsuarioViewModel regPorLogin(String login) {
        try {
            return modelToViewModel(new Usuario().getPorLogin(login));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeUsuario
     * @return retorna uma lista com N registros
     */
    public List<UsuarioViewModel> listaPorNome(String nomeUsuario) {
        return modelToViewModel(new Usuario().getListaPorNome(nomeUsuario));
    }

    /**
     * Busca registros de acordo com o login informado
     *
     * @param loginUsuario
     * @return retorna uma lista com N registros
     */
    public List<UsuarioViewModel> listaPorLogin(String loginUsuario) {
        return modelToViewModel(new Usuario().getListaPorLogin(loginUsuario));
    }
    
    /**
     * Busca registros de acordo com o e-mail informado
     *
     * @param emailUsuario
     * @return retorna uma lista com N registros
     */
    public List<UsuarioViewModel> listaPorEmail(String emailUsuario) {
        return modelToViewModel(new Usuario().getListaPorEmail(emailUsuario));
    }

    /**
     * Busca histórico de endereço de um usuário, de acordo com o Id informado
     *
     * @param id
     * @return retorna uma lista com N registros
     */
    public List<UsuarioEnderecoViewModel> getHistoricoEndereco(int id) {
        return new UsuarioEnderecoController().getTodosEnderecos(id);
    }
    
    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeUsuario
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<UsuarioViewModel> listaLimitada(String nomeUsuario, int qtdeResultado) {
        return modelToViewModel(new Usuario().getListaLimitada(nomeUsuario, qtdeResultado));
    }

    private List<UsuarioViewModel> modelToViewModel(List<Usuario> listaUsuario) {
        List<UsuarioViewModel> lista = new ArrayList();

        listaUsuario.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                // aconteceu algo inesperado
            }
        });
        return lista;
    }

    private UsuarioViewModel modelToViewModel(Usuario u) throws Exception {
        try {
            return new UsuarioViewModel(
                    u.getCodigo(),
                    u.getNome(),
                    u.getSexo(),
                    u.getLogin(),
                    u.getSenha(),
                    u.getEmail(),
                    u.getImagem(),
                    u.isAtivo(),
                    u.getObservacao());
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo: " + ex.getMessage());
        }
    }
}
