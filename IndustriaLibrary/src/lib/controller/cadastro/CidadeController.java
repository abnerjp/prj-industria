package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Cidade;
import java.util.List;
import lib.viewmodel.cadastro.CidadeViewModel;

/**
 *
 * @author abnerjp
 */
public class CidadeController {

    public CidadeController() {
    }

    /**
     * Grava no banco de dados os valores de uma nova Cidade
     *
     * @param vmCidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(CidadeViewModel vmCidade) {
        if (vmCidade != null) {
            if (vmCidade.getEstado() != null) {
                if (!vmCidade.getEstado().isAtivo()) {
                    return 707;
                }
                return new Cidade(
                        vmCidade.getId(),
                        vmCidade.getNome(),
                        vmCidade.isAtivo(),
                        vmCidade.getObservacao(),
                        vmCidade.getEstado().getId()
                ).gravar();
            }
            return 705;
        }
        return 708;
    }

    /**
     * Altera no banco de dados os valores de uma Cidade
     *
     * @param vmCidade
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(CidadeViewModel vmCidade) {
        if (vmCidade != null) {
            if (vmCidade.getEstado() != null) {
                if (!vmCidade.getEstado().isAtivo()) {
                    CidadeViewModel cidadeAux = regPorCodigo(vmCidade.getId());
                    if (cidadeAux != null && cidadeAux.getEstado().getId() != vmCidade.getEstado().getId()) {
                        return 707;
                    }
                }
                return new Cidade(
                        vmCidade.getId(),
                        vmCidade.getNome(),
                        vmCidade.isAtivo(),
                        vmCidade.getObservacao(),
                        vmCidade.getEstado().getId()
                ).alterar();
            }
            return 705;
        }
        return 708;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new Cidade(id).excluir();
    }

    /**
     * Busca o primeiro registro de Cidade no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public CidadeViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Cidade().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Cidade no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public CidadeViewModel getUltimoRegistro() {
        return modelToViewModel(new Cidade().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Cidade no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public CidadeViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Cidade(id).getProximoRegistro());
    }

    /**
     * Busca registro de Cidade no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public CidadeViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Cidade(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Cidade no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public CidadeViewModel regPorCodigo(int id) {
        return modelToViewModel(new Cidade().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeCidade
     * @return retorna uma lista com N registros
     */
    public List<CidadeViewModel> listaPorNome(String nomeCidade) {
        return modelToViewModel(new Cidade().getListaPorNome(nomeCidade));
    }

    /**
     * Busca registro de acordo com o nome informado
     *
     * @param nomeCidade
     * @return retorna uma lista com N registros
     */
    public CidadeViewModel cidadePorNome(String nomeCidade) {
        List<CidadeViewModel> lista = modelToViewModel(new Cidade().getListaPorNome(nomeCidade));
        return lista.size() > 0 ? lista.get(0) : null;
    }

    /**
     * Busca registros de acordo com o Estado informado
     *
     * @param nomeEstado
     * @return retorna uma lista com N registros
     */
    public List<CidadeViewModel> listaPorEstado(String nomeEstado) {
        return modelToViewModel(new Cidade().getListaPorEstado(nomeEstado));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeCidade
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<CidadeViewModel> listaLimitada(String nomeCidade, int qtdeResultado) {
        return modelToViewModel(new Cidade().getListaLimitada(nomeCidade, qtdeResultado));
    }

    private CidadeViewModel modelToViewModel(Cidade c) {
        if (c != null) {
            return new CidadeViewModel(
                    c.getCodigo(),
                    c.getNome(),
                    c.isAtivo(),
                    c.getObservacao(),
                    c.getCodigoEstado()
            );
        }
        return null;
    }

    private List<CidadeViewModel> modelToViewModel(List<Cidade> listaCidade) {
        List<CidadeViewModel> lista = new ArrayList<>();

        listaCidade.forEach((c) -> {
            lista.add(modelToViewModel(c));
        });
        return lista;
    }
}
