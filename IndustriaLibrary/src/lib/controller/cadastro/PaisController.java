package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Pais;
import java.util.List;
import lib.viewmodel.cadastro.PaisViewModel;

/**
 *
 * @author abnerjp
 */
public class PaisController {

    public PaisController() {
    }

    /**
     * Grava no banco de dados os valores de um novo País
     *
     * @param vmPais
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(PaisViewModel vmPais) {
        if (vmPais != null) {
            return new Pais(
                    vmPais.getId(),
                    vmPais.getNome(),
                    vmPais.getSigla(),
                    vmPais.isAtivo(),
                    vmPais.getObservacao()
            ).gravar();
        }
        return 208;
    }

    /**
     * Altera no banco de dados os valores de um novo País
     *
     * @param vmPais
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(PaisViewModel vmPais) {
        if (vmPais != null) {
            return new Pais(
                    vmPais.getId(),
                    vmPais.getNome(),
                    vmPais.getSigla(),
                    vmPais.isAtivo(),
                    vmPais.getObservacao()
            ).alterar();
        }
        return 208;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new Pais(id).excluir();
    }

    /**
     * Busca o primeiro registro de País no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PaisViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Pais().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de País no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PaisViewModel getUltimoRegistro() {
        return modelToViewModel(new Pais().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de País no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PaisViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Pais(id).getProximoRegistro());
    }

    /**
     * Busca registro de País no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PaisViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Pais(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de País no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PaisViewModel regPorCodigo(int id) {
        return modelToViewModel(new Pais().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com a sigla informada
     *
     * @param siglaPais
     * @return retorna uma lista com N registros
     */
    public List<PaisViewModel> listaPorSigla(String siglaPais) {
        return modelToViewModel(new Pais().getListaPorSigla(siglaPais));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomePais
     * @return retorna uma lista com N registros
     */
    public List<PaisViewModel> listaPorNome(String nomePais) {
        return modelToViewModel(new Pais().getListaPorNome(nomePais));
    }

    /**
     * Busca registros de acordo com o País informado
     *
     * @param nomePais
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<PaisViewModel> listaLimitada(String nomePais, int qtdeResultado) {
        return modelToViewModel(new Pais().getListaLimitada(nomePais, qtdeResultado));
    }

    private PaisViewModel modelToViewModel(Pais p) {
        if (p != null) {
            return new PaisViewModel(
                    p.getCodigo(),
                    p.getNome(),
                    p.getSigla(),
                    p.isAtivo(),
                    p.getObservacao()
            );
        }
        return null;
    }

    private List<PaisViewModel> modelToViewModel(List<Pais> listaPais) {
        List<PaisViewModel> lista = new ArrayList<>();

        listaPais.forEach((p) -> {
            lista.add(modelToViewModel(p));
        });
        return lista;
    }
}
