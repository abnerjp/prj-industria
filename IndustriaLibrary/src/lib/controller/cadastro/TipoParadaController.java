package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.TipoParada;
import java.util.List;
import lib.viewmodel.cadastro.TipoParadaViewModel;

/**
 *
 * @author abnerjp
 */
public class TipoParadaController {

    public TipoParadaController() {
    }

    /**
     * Grava no banco de dados os valores de um novo Tipo de Parada
     *
     * @param vmTipoParada
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(TipoParadaViewModel vmTipoParada) {
        if (vmTipoParada != null) {
            return new TipoParada(
                    vmTipoParada.getId(),
                    vmTipoParada.getNome(),
                    vmTipoParada.getSigla(),
                    vmTipoParada.isAtivo(),
                    vmTipoParada.getObservacao()
            ).gravar();
        }
        return 208;
    }

    /**
     * Altera no banco de dados os valores de um novo Tipo de Parada
     *
     * @param vmTipoParada
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(TipoParadaViewModel vmTipoParada) {
        if (vmTipoParada != null) {
            return new TipoParada(
                    vmTipoParada.getId(),
                    vmTipoParada.getNome(),
                    vmTipoParada.getSigla(),
                    vmTipoParada.isAtivo(),
                    vmTipoParada.getObservacao()
            ).alterar();
        }
        return 208;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new TipoParada(id).excluir();
    }

    /**
     * Busca o primeiro registro de Tipo de Parada no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public TipoParadaViewModel getPrimeiroRegistro() {
        return modelToViewModel(new TipoParada().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Tipo de Parada no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public TipoParadaViewModel getUltimoRegistro() {
        return modelToViewModel(new TipoParada().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Tipo de Parada no banco de dados a partir do
     * ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public TipoParadaViewModel getProximoRegistro(int id) {
        return modelToViewModel(new TipoParada(id).getProximoRegistro());
    }

    /**
     * Busca registro de Tipo de Parada no banco de dados, anterior ao ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public TipoParadaViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new TipoParada(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Tipo de Parada no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public TipoParadaViewModel regPorCodigo(int id) {
        return modelToViewModel(new TipoParada().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com a sigla informada
     *
     * @param siglaTipoParada
     * @return retorna uma lista com N registros
     */
    public List<TipoParadaViewModel> listaPorSigla(String siglaTipoParada) {
        return modelToViewModel(new TipoParada().getListaPorSigla(siglaTipoParada));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeTipoParda
     * @return retorna uma lista com N registros
     */
    public List<TipoParadaViewModel> listaPorNome(String nomeTipoParda) {
        return modelToViewModel(new TipoParada().getListaPorNome(nomeTipoParda));
    }

    /**
     * Busca registros de acordo com o nome do Tipo de Parada informado
     *
     * @param nomeTipoParda
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<TipoParadaViewModel> listaLimitada(String nomeTipoParda, int qtdeResultado) {
        return modelToViewModel(new TipoParada().getListaLimitada(nomeTipoParda, qtdeResultado));
    }

    private TipoParadaViewModel modelToViewModel(TipoParada tp) {
        if (tp != null) {
            return new TipoParadaViewModel(
                    tp.getCodigo(),
                    tp.getNome(),
                    tp.getSigla(),
                    tp.isAtivo(),
                    tp.getObservacao()
            );
        }
        return null;
    }

    private List<TipoParadaViewModel> modelToViewModel(List<TipoParada> listaTipoParada) {
        List<TipoParadaViewModel> lista = new ArrayList<>();

        listaTipoParada.forEach((tp) -> {
            lista.add(modelToViewModel(tp));
        });

        return lista;
    }
}
