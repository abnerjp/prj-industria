package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Submotivo;
import java.util.List;
import lib.viewmodel.cadastro.SubmotivoViewModel;

/**
 *
 * @author abnerjp
 */
public class SubmotivoController {

    public SubmotivoController() {
    }

    /**
     * Grava no banco de dados os valores de um novo Submotivo
     *
     * @param vmSubmotivo
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(SubmotivoViewModel vmSubmotivo) {
        if (vmSubmotivo != null) {
            return new Submotivo(
                    vmSubmotivo.getId(),
                    vmSubmotivo.getNome(),
                    vmSubmotivo.getSigla(),
                    vmSubmotivo.isAtivo(),
                    vmSubmotivo.getObservacao()
            ).gravar();
        }
        return 408;
    }

    /**
     * Altera no banco de dados os valores de um Submotivo
     *
     * @param vmSubmotivo
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(SubmotivoViewModel vmSubmotivo) {
        if (vmSubmotivo != null) {
            return new Submotivo(
                    vmSubmotivo.getId(),
                    vmSubmotivo.getNome(),
                    vmSubmotivo.getSigla(),
                    vmSubmotivo.isAtivo(),
                    vmSubmotivo.getObservacao()
            ).alterar();
        }
        return 408;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Submotivo(id).excluir();
    }

    /**
     * Busca o primeiro registro de Submotivo no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public SubmotivoViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Submotivo().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Submotivo no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public SubmotivoViewModel getUltimoRegistro() {
        return modelToViewModel(new Submotivo().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Submotivo no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public SubmotivoViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Submotivo(id).getProximoRegistro());
    }

    /**
     * Busca registro de Submotivo no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public SubmotivoViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Submotivo(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Submotivo no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public SubmotivoViewModel regPorCodigo(int id) {
        return modelToViewModel(new Submotivo().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com a sigla informada
     *
     * @param siglaSubmotivo
     * @return retorna uma lista com N registros
     */
    public List<SubmotivoViewModel> listaPorSigla(String siglaSubmotivo) {
        return modelToViewModel(new Submotivo().getListaPorSigla(siglaSubmotivo));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeSubmotivo
     * @return retorna uma lista com N registros
     */
    public List<SubmotivoViewModel> listaPorNome(String nomeSubmotivo) {
        return modelToViewModel(new Submotivo().getListaPorNome(nomeSubmotivo));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeSubmotivo
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<SubmotivoViewModel> listaLimitada(String nomeSubmotivo, int qtdeResultado) {
        return modelToViewModel(new Submotivo().getListaLimitada(nomeSubmotivo, qtdeResultado));
    }

    private SubmotivoViewModel modelToViewModel(Submotivo s) {
        if (s != null) {
            return new SubmotivoViewModel(
                    s.getCodigo(),
                    s.getNome(),
                    s.getSigla(),
                    s.isAtivo(),
                    s.getObservacao()
            );
        }
        return null;
    }

    private List<SubmotivoViewModel> modelToViewModel(List<Submotivo> listaSubMotivo) {
        List<SubmotivoViewModel> lista = new ArrayList<>();

        listaSubMotivo.forEach((s) -> {
            lista.add(modelToViewModel(s));
        });
        return lista;
    }
}
