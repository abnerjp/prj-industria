package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.MotivoParada;
import java.util.List;
import lib.viewmodel.cadastro.MotivoParadaViewModel;

/**
 *
 * @author abnerjp
 */
public class MotivoParadaController {

    public MotivoParadaController() {
    }

    /**
     * Grava no banco de dados os valores de um novo Motivo de Parada
     *
     * @param vmMotivoParada
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(MotivoParadaViewModel vmMotivoParada) {
        if (vmMotivoParada != null) {
            return new MotivoParada(
                    vmMotivoParada.getId(),
                    vmMotivoParada.getNome(),
                    vmMotivoParada.getSigla(),
                    vmMotivoParada.isAtivo(),
                    vmMotivoParada.getObservacao()
            ).gravar();
        }
        return 608;
    }

    /**
     * Grava no banco de dados os valores de um novo Motivo de Parada
     *
     * @param vmMotivoParada
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int alterar(MotivoParadaViewModel vmMotivoParada) {
        if (vmMotivoParada != null) {
            return new MotivoParada(
                    vmMotivoParada.getId(),
                    vmMotivoParada.getNome(),
                    vmMotivoParada.getSigla(),
                    vmMotivoParada.isAtivo(),
                    vmMotivoParada.getObservacao()
            ).alterar();
        }
        return 608;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new MotivoParada(id).excluir();
    }

    /**
     * Busca o primeiro registro de Motivo de Parada no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MotivoParadaViewModel getPrimeiroRegistro() {
        return modelToViewModel(new MotivoParada().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Motivo de Parada no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MotivoParadaViewModel getUltimoRegistro() {
        return modelToViewModel(new MotivoParada().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Motivo de Parada no banco de dados a partir
     * do ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MotivoParadaViewModel getProximoRegistro(int id) {
        return modelToViewModel(new MotivoParada(id).getProximoRegistro());
    }

    /**
     * Busca registro de Motivo de Parada no banco de dados, anterior ao ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MotivoParadaViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new MotivoParada(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Motivo de Parada no banco de dados igual ao ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public MotivoParadaViewModel regPorCodigo(int id) {
        return modelToViewModel(new MotivoParada().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com a sigla informada
     *
     * @param siglaMotivoParada
     * @return retorna uma lista com N registros
     */
    public List<MotivoParadaViewModel> listaPorSigla(String siglaMotivoParada) {
        return modelToViewModel(new MotivoParada().getListaPorSigla(siglaMotivoParada));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeMotivoParada
     * @return retorna uma lista com N registros
     */
    public List<MotivoParadaViewModel> listaPorNome(String nomeMotivoParada) {
        return modelToViewModel(new MotivoParada().getListaPorNome(nomeMotivoParada));
    }

    /**
     * Busca registros de acordo com o Motivo de Parada informado
     *
     * @param nomeMotivoParada
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<MotivoParadaViewModel> listaLimitada(String nomeMotivoParada, int qtdeResultado) {
        return modelToViewModel(new MotivoParada().getListaLimitada(nomeMotivoParada, qtdeResultado));
    }

    private MotivoParadaViewModel modelToViewModel(MotivoParada mp) {
        if (mp != null) {
            return new MotivoParadaViewModel(
                    mp.getCodigo(),
                    mp.getNome(),
                    mp.getSigla(),
                    mp.isAtivo(),
                    mp.getObservacao()
            );
        }
        return null;
    }

    private List<MotivoParadaViewModel> modelToViewModel(List<MotivoParada> listaMotivoParada) {
        List<MotivoParadaViewModel> lista = new ArrayList<>();

        listaMotivoParada.forEach((mp) -> {
            lista.add(modelToViewModel(mp));
        });
        return lista;
    }
}
