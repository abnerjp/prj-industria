package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Estado;
import java.util.List;
import lib.viewmodel.cadastro.EstadoViewModel;

/**
 *
 * @author abnerjp
 */
public class EstadoController {

    public EstadoController() {
    }

    /**
     * Grava no banco de dados os valores de um novo Estado
     *
     * @param vmEstado
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(EstadoViewModel vmEstado) {
        if (vmEstado != null) {
            if (vmEstado.getPais() != null) {
                if (!vmEstado.getPais().isAtivo()) {
                    return 309;
                }
                return new Estado(
                        vmEstado.getId(),
                        vmEstado.getNome(),
                        vmEstado.getSigla(),
                        vmEstado.isAtivo(),
                        vmEstado.getObservacao(),
                        vmEstado.getPais().getId()
                ).gravar();
            }
            return 307;
        }
        return 310;
    }

    /**
     * Altera no banco de dados os valores de um novo Estado
     *
     * @param vmEstado
     * @return inteiro 0 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(EstadoViewModel vmEstado) {
        if (vmEstado != null) {
            if (vmEstado.getPais() != null) {
                if (!vmEstado.getPais().isAtivo()) {
                    EstadoViewModel estadoAux = regPorCodigo(vmEstado.getId());
                    if (estadoAux != null && estadoAux.getPais().getId() != vmEstado.getPais().getId()) {
                        return 309;
                    }
                }
                return new Estado(
                        vmEstado.getId(),
                        vmEstado.getNome(),
                        vmEstado.getSigla(),
                        vmEstado.isAtivo(),
                        vmEstado.getObservacao(),
                        vmEstado.getPais().getId()
                ).alterar();
            }
            return 307;
        }
        return 310;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 0 para excluído e 1 para não excluído
     */
    public int excluir(int id) {
        return new Estado(id).excluir();
    }

    /**
     * Busca o primeiro registro de Estado no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EstadoViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Estado().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de Estado no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EstadoViewModel getUltimoRegistro() {
        return modelToViewModel(new Estado().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de Estado no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EstadoViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Estado(id).getProximoRegistro());
    }

    /**
     * Busca registro de Estado no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EstadoViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Estado(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de Estado no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EstadoViewModel regPorCodigo(int id) {
        return modelToViewModel(new Estado().getRegPorCodigo(id));
    }

    /**
     * Busca uma lista de registros de acordo com o nome informado
     *
     * @param nomeEstado
     * @return retorna uma lista com N registros
     */
    public List<EstadoViewModel> listaPorNome(String nomeEstado) {
        return modelToViewModel(new Estado().getListaPorNome(nomeEstado));
    }

    /**
     * Busca registro de acordo com a sigla informada
     *
     * @param siglaEstado
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EstadoViewModel regPorSigla(String siglaEstado) {
        return modelToViewModel(new Estado().getRegPorSigla(siglaEstado));
    }

    /**
     * Busca uma lista de registros de acordo com a sigla do estado informada
     *
     * @param siglaEstado
     * @return retorna uma lista com N registros
     */
    public List<EstadoViewModel> listaPorSigla(String siglaEstado) {
        return modelToViewModel(new Estado().getListaPorSigla(siglaEstado));
    }

    /**
     * Busca uma lista de registros de acordo com o país informado
     *
     * @param nomePais
     * @return retorna uma lista com N registros
     */
    public List<EstadoViewModel> listaPorPais(String nomePais) {
        return modelToViewModel(new Estado().getListaPorPais(nomePais));
    }

    /**
     * Busca uma lista de registros de acordo com o nome informado
     *
     * @param nomeEstado
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<EstadoViewModel> listaLimitada(String nomeEstado, int qtdeResultado) {
        return modelToViewModel(new Estado().getListaLimitada(nomeEstado, qtdeResultado));
    }

    private EstadoViewModel modelToViewModel(Estado e) {
        if (e != null) {
            return new EstadoViewModel(
                    e.getCodigo(),
                    e.getNome(),
                    e.getSigla(),
                    e.isAtivo(),
                    e.getObservacao(),
                    e.getCodigoPais()
            );
        }
        return null;
    }

    private List<EstadoViewModel> modelToViewModel(List<Estado> listaEstado) {
        List<EstadoViewModel> lista = new ArrayList<>();

        listaEstado.forEach((e) -> {
            lista.add(modelToViewModel(e));
        });
        return lista;
    }
}
