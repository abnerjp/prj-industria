package lib.controller.cadastro;

import java.util.ArrayList;
import lib.model.cadastro.Funcao;
import java.util.List;
import lib.viewmodel.cadastro.FuncaoViewModel;

/**
 *
 * @author abnerjp
 */
public class FuncaoController {

    /**
     * Grava no banco de dados os valores de uma nova função
     *
     * @param vmFuncao
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(FuncaoViewModel vmFuncao) {
        if (vmFuncao != null) {
            return new Funcao(
                    vmFuncao.getId(),
                    vmFuncao.getNome(),
                    vmFuncao.isAtivo(),
                    vmFuncao.getObservacao()
            ).gravar();
        }
        return 1806;
    }

    /**
     * Altera no banco de dados os valores de uma função
     *
     * @param vmFuncao
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(FuncaoViewModel vmFuncao) {
        if (vmFuncao != null) {
            return new Funcao(
                    vmFuncao.getId(),
                    vmFuncao.getNome(),
                    vmFuncao.isAtivo(),
                    vmFuncao.getObservacao()
            ).alterar();
        }
        return 1806;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Funcao(id).excluir();
    }

    /**
     * Busca o primeiro registro de função no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public FuncaoViewModel getPrimeiroRegistro() {
        try {
            return modelToViewModel(new Funcao().getPrimeiroRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca o último registro de função no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public FuncaoViewModel getUltimoRegistro() {
        try {
            return modelToViewModel(new Funcao().getUltimoRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca o próximo registro de função no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public FuncaoViewModel getProximoRegistro(int id) {
        try {
            return modelToViewModel(new Funcao(id).getProximoRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de função no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public FuncaoViewModel getAnteriorRegistro(int id) {
        try {
            return modelToViewModel(new Funcao(id).getAnteriorRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de função no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public FuncaoViewModel regPorCodigo(int id) {
        try {
            return modelToViewModel(new Funcao().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomePerda
     * @return retorna uma lista com N registros
     */
    public List<FuncaoViewModel> listaPorNome(String nomePerda) {
        return modelToViewModel(new Funcao().getListaPorNome(nomePerda));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeFuncao
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<FuncaoViewModel> listaLimitada(String nomeFuncao, int qtdeResultado) {
        return modelToViewModel(new Funcao().getListaLimitada(nomeFuncao, qtdeResultado));
    }

    private List<FuncaoViewModel> modelToViewModel(List<Funcao> listaFuncao) {
        List<FuncaoViewModel> lista = new ArrayList();

        listaFuncao.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                //aconteceu algo inesperado
            }
        });
        return lista;
    }

    private FuncaoViewModel modelToViewModel(Funcao f) throws Exception {
        try {
            return new FuncaoViewModel(
                    f.getCodigo(),
                    f.getNome(),
                    f.isAtivo(),
                    f.getObservacao()
            );
        } catch (Exception ex) {
            throw new Exception("objeto nulo: " + ex.getMessage());
        }
    }

}
