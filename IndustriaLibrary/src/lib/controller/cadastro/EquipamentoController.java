package lib.controller.cadastro;

import java.util.ArrayList;
import java.util.List;
import lib.model.cadastro.Equipamento;
import lib.viewmodel.cadastro.EquipamentoViewModel;

/**
 *
 * @author abnerjp
 */
public class EquipamentoController {

    public EquipamentoController() {
    }

    /**
     * Grava no banco de dados um novo equipamento
     *
     * @param vmEquipamento
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(EquipamentoViewModel vmEquipamento) {
        if (vmEquipamento != null) {
            return new Equipamento(
                    vmEquipamento.getNome(),
                    vmEquipamento.getModelo(),
                    vmEquipamento.isAtivo(),
                    vmEquipamento.getObservacao()
            ).gravar();
        }
        return 1407;
    }

    /**
     * Altera no banco de dados um equipamento
     *
     * @param vmEquipamento
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int alterar(EquipamentoViewModel vmEquipamento) {
        if (vmEquipamento != null) {
            return new Equipamento(
                    vmEquipamento.getId(),
                    vmEquipamento.getNome(),
                    vmEquipamento.getModelo(),
                    vmEquipamento.isAtivo(),
                    vmEquipamento.getObservacao()
            ).alterar();
        }
        return 1407;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Equipamento(id).excluir();
    }

    /**
     * Busca o primeiro registro de equipamento no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EquipamentoViewModel getPrimeiroRegistro() {
        return modelToViewModel(new Equipamento().getPrimeiroRegistro());
    }

    /**
     * Busca o último registro de equipamento no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EquipamentoViewModel getUltimoRegistro() {
        return modelToViewModel(new Equipamento().getUltimoRegistro());
    }

    /**
     * Busca o próximo registro de equipamento no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EquipamentoViewModel getProximoRegistro(int id) {
        return modelToViewModel(new Equipamento(id).getProximoRegistro());
    }

    /**
     * Busca registro de equipamento no banco de dados , anterior ao ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EquipamentoViewModel getAnteriorRegistro(int id) {
        return modelToViewModel(new Equipamento(id).getAnteriorRegistro());
    }

    /**
     * Busca registro de equipamento no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public EquipamentoViewModel getRegPorCodigo(int id) {
        return modelToViewModel(new Equipamento().getRegPorCodigo(id));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeEquipamento
     * @return retorna uma lista com N registros
     */
    public List<EquipamentoViewModel> getListaPorNome(String nomeEquipamento) {
        return modelToViewModel(new Equipamento().getListaPorNome(nomeEquipamento));
    }

    /**
     * Busca registros de acordo com o modelo informado
     *
     * @param modeloEquipamento
     * @return retorna uma lista com N registros
     */
    public List<EquipamentoViewModel> getListaPorModelo(String modeloEquipamento) {
        return modelToViewModel(new Equipamento().getListaPorModelo(modeloEquipamento));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomeEquipamento
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<EquipamentoViewModel> getListaLimitada(String nomeEquipamento, int qtdeResultado) {
        return modelToViewModel(new Equipamento().getListaLimitada(nomeEquipamento, qtdeResultado));
    }

    /* métodos internos à classe */
    private EquipamentoViewModel modelToViewModel(Equipamento e) {
        if (e != null) {
            return new EquipamentoViewModel(
                    e.getCodigo(),
                    e.getNome(),
                    e.getModelo(),
                    e.isAtivo(),
                    e.getObservacao()
            );
        }
        return null;
    }

    public List<EquipamentoViewModel> modelToViewModel(List<Equipamento> listaEquipamento) {
        List<EquipamentoViewModel> lista = new ArrayList<>();
        listaEquipamento.forEach((e) -> {
            lista.add(modelToViewModel(e));
        });
        return lista;
    }

}
