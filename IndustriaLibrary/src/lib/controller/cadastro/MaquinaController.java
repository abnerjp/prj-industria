package lib.controller.cadastro;

import lib.dao.cadastro.MaquinaDAO;
import lib.model.cadastro.Maquina;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public class MaquinaController {
    private final MaquinaDAO maqDAO;
    
    public MaquinaController() {
        maqDAO = new MaquinaDAO();
    }
    
    public boolean gravar(Maquina m) {
        return maqDAO.gravar(m);
    }
    
    public boolean alterar(Maquina m) {
        return maqDAO.alterar(m);
    }
    
    public boolean excluir(int codigo) {
        return maqDAO.excluir(codigo);
    }
    
    public Maquina getPrimeiroRegistro() {
        return maqDAO.getPrimeiroRegistro();
    }
    
    public Maquina getUltimoRegistro() {
        return maqDAO.getUltimoRegistro();
    }
    
    public Maquina getProximoRegistro(int codigo) {
        return maqDAO.getProximoRegistro(codigo);
    }
    
    public Maquina getAnteriorRegistro(int codigo) {
        return maqDAO.getAnteriorRegistro(codigo);
    }
    
    public Maquina regPorCodigo(int codigo) {
        return maqDAO.getRegistroPorCodigo(codigo);
    }
    
    public List<Maquina> listaPorModelo(String modelo) {
        return maqDAO.getListaPorModelo(modelo);
    }
    
    public List<Maquina> listaLimitada(String modelo, int qtdeResultado) {
        return maqDAO.getListaLimitada(modelo, qtdeResultado);
    }
}
