package lib.controller.cadastro;

import java.util.ArrayList;
import java.util.List;
import lib.model.cadastro.Perda;
import lib.viewmodel.cadastro.PerdaViewModel;

/**
 *
 * @author abnerjp
 */
public class PerdaController {

    /**
     * Grava no banco de dados os valores de uma nova perda
     *
     * @param vmPerda
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for gravado
     */
    public int gravar(PerdaViewModel vmPerda) {
        if (vmPerda != null) {
            return new Perda(
                    vmPerda.getNome(),
                    vmPerda.getSigla(),
                    vmPerda.isAtivo(),
                    vmPerda.getObservacao()
            ).gravar();
        }
        return 1708;
    }

    /**
     * Altera no banco de dados os valores de uma perda
     *
     * @param vmPerda
     * @return inteiro 1 para quando for gravado e outros valores para quando
     * não for alterado
     */
    public int alterar(PerdaViewModel vmPerda) {
        if (vmPerda != null) {
            return new Perda(
                    vmPerda.getId(),
                    vmPerda.getNome(),
                    vmPerda.getSigla(),
                    vmPerda.isAtivo(),
                    vmPerda.getObservacao()
            ).alterar();
        }
        return 1708;
    }

    /**
     * Excluir um registro do banco de dados
     *
     * @param id
     * @return 1 para excluído e 0 para não excluído
     */
    public int excluir(int id) {
        return new Perda(id).excluir();
    }

    /**
     * Busca o primeiro registro de Perda no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PerdaViewModel getPrimeiroRegistro() {
        try {
            return modelToViewModel(new Perda().getPrimeiroRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca o último registro de Perda no banco de dados
     *
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PerdaViewModel getUltimoRegistro() {
        try {
            return modelToViewModel(new Perda().getUltimoRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca o próximo registro de Perda no banco de dados a partir do ID
     * informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PerdaViewModel getProximoRegistro(int id) {
        try {
            return modelToViewModel(new Perda(id).getProximoRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de Perda no banco de dados, anterior ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PerdaViewModel getAnteriorRegistro(int id) {
        try {
            return modelToViewModel(new Perda(id).getAnteriorRegistro());
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registro de Perda no banco de dados igual ao ID informado
     *
     * @param id
     * @return objeto com registro ou objeto nulo, se não for encontrado
     */
    public PerdaViewModel regPorCodigo(int id) {
        try {
            return modelToViewModel(new Perda().getRegPorCodigo(id));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Busca registros de acordo com a sigla informada
     *
     * @param siglaPerda
     * @return retorna uma lista com N registros
     */
    public List<PerdaViewModel> listaPorSigla(String siglaPerda) {
        return modelToViewModel(new Perda().getListaPorSigla(siglaPerda));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomePerda
     * @return retorna uma lista com N registros
     */
    public List<PerdaViewModel> listaPorNome(String nomePerda) {
        return modelToViewModel(new Perda().getListaPorNome(nomePerda));
    }

    /**
     * Busca registros de acordo com o nome informado
     *
     * @param nomePerda
     * @param qtdeResultado
     * @return retorna uma lista com no máximo N registros. Sendo N a quantidade
     * informada.
     */
    public List<PerdaViewModel> listaLimitada(String nomePerda, int qtdeResultado) {
        return modelToViewModel(new Perda().getListaLimitada(nomePerda, qtdeResultado));
    }

    private List<PerdaViewModel> modelToViewModel(List<Perda> listaPerda) {
        List<PerdaViewModel> lista = new ArrayList();

        listaPerda.forEach((ui) -> {
            try {
                lista.add(modelToViewModel(ui));
            } catch (Exception ex) {
                // aconteceu algo inesperado
            }
        });
        return lista;
    }

    private PerdaViewModel modelToViewModel(Perda p) throws Exception {
        try {
            return new PerdaViewModel(
                    p.getCodigo(),
                    p.getNome(),
                    p.getSigla(),
                    p.isAtivo(),
                    p.getObservacao()
            );
        } catch (NullPointerException ex) {
            throw new Exception("Objeto nulo: " + ex.getMessage());
        }
    }

}
