package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeMotivoParada;

/**
 * @author abnerjp
 */
public abstract class UnidadeMotivoParadaDAO {

    protected boolean gravar(UnidadeMotivoParada ump) {
        String sql = "INSERT INTO mparada_unidade (mpxun_codigo, mpxun_ativo, mpxun_data, mpxun_observacao, uni_codigo, mpa_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ump.isAtivo());
                    ps.setDate(2, Date.valueOf(ump.getData()));
                    ps.setString(3, ump.getObservacao());
                    ps.setInt(4, ump.getCodUnidade());
                    ps.setInt(5, ump.getCodMotivoParada());
                    return ps.executeUpdate() == 1;

                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeMotivoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(UnidadeMotivoParada ump) {
        String sql = "UPDATE mparada_unidade "
                + "SET mpxun_ativo = ?, mpxun_observacao =? "
                + "WHERE mpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ump.isAtivo());
                    ps.setString(2, ump.getObservacao());
                    ps.setInt(3, ump.getCodigo());
                    return ps.executeUpdate() == 1;

                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeMotivoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM mparada_unidade "
                + "WHERE mpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeMotivoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeMotivoParada regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM mparada_unidade "
                + "WHERE mpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadeMotivoParada: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeMotivoParada regPorUnidadeEMotivoParada(int codUnidade, int codMotivoParada) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM mparada_unidade "
                + "WHERE uni_codigo = ? AND mpa_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codMotivoParada);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorUnidadeEMotivoParada-UnidadeMotivoParada: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeMotivoParada> motivosParadaPorUnidade(int codUnidade) {
        List<UnidadeMotivoParada> lista = new ArrayList();
        String sql = "SELECT mpu.mpxun_codigo, mpu.mpxun_ativo, mpu.mpxun_data, mpu.mpxun_observacao, mpu.uni_codigo, mpu.mpa_codigo "
                + "FROM mparada_unidade as mpu "
                + "INNER JOIN motivoparada as mpa ON mpa.mpa_codigo = mpu.mpa_codigo "
                + "WHERE mpu.uni_codigo = ? "
                + "ORDER BY mpa.mpa_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("motivosParadaPorUnidade-UnidadeMotivoParada: " + ex.getMessage());
        }
        return lista;
    }

    private String todosCampos() {
        return "mpxun_codigo, mpxun_ativo, mpxun_data, mpxun_observacao, uni_codigo, mpa_codigo";
    }

    private UnidadeMotivoParada cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeMotivoParada(
                rs.getInt("mpxun_codigo"),
                rs.getBoolean("mpxun_ativo"),
                rs.getDate("mpxun_data").toLocalDate(),
                rs.getString("mpxun_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("mpa_codigo")
        );
    }
}
