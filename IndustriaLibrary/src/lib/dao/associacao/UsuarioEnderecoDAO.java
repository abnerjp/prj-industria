package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UsuarioEndereco;

/**
 *
 * @author abnerjp
 */
public class UsuarioEnderecoDAO {

    public boolean gravar(UsuarioEndereco ue) {
        String sql = "INSERT INTO endereco_usuario (" + todosCampos() + ") "
                + "VALUES (?, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, ue.getCodUsuario());
                    ps.setInt(2, ue.getCodEndereco());
                    ps.setDate(3, Date.valueOf(ue.getData()));
                    ps.setInt(4, ue.getNumero());
                    ps.setString(5, ue.getComplemento());
                    ps.setString(6, ue.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UsuarioEnderecoDAO: " + ex.getMessage());
        }
        return false;
    }

    public boolean excluir(int codUnidade, int codEndereco, LocalDate data) {
        String sql = "DELETE FROM endereco_usuario "
                + "WHERE usu_codigo = ? AND end_codigo = ? AND endxusu_data = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codEndereco);
                    ps.setDate(3, Date.valueOf(data));
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UsuarioEnderecoDAO: " + ex.getMessage());
        }
        return false;
    }

    public UsuarioEndereco regAtual(int codUsuario) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM endereco_usuario "
                + "WHERE usu_codigo = ? "
                + "ORDER BY endxusu_data DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUsuario);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regAtual-UsuarioEnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    public List<UsuarioEndereco> enderecosUsuario(int codUnidade) {
        List<UsuarioEndereco> lista = new ArrayList<>();
        String sql = "SELECT "+todosCampos()+" "
                + "FROM endereco_usuario "
                + "WHERE usu_codigo = ? "
                + "ORDER BY endxusu_data ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("enderecosUsuario-UsuarioEnderecoDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    private String todosCampos() {
        return "usu_codigo, "
                + "end_codigo, "
                + "endxusu_data, "
                + "endxusu_numero, "
                + "endxusu_complemento, "
                + "endxusu_observacao";
    }
    
    private UsuarioEndereco cargaObjeto(ResultSet rs) throws SQLException {
        return new UsuarioEndereco(
                rs.getInt("endxusu_numero"),
                rs.getDate("endxusu_data").toLocalDate(),
                rs.getString("endxusu_complemento"),
                rs.getString("endxusu_observacao"),
                rs.getInt("usu_codigo"),
                rs.getInt("end_codigo")
        );
    }
}
