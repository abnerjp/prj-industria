package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeTipoParada;

/**
 * @author abnerjp
 */
public abstract class UnidadeTipoParadaDAO {

    public UnidadeTipoParadaDAO() {
    }

    protected boolean gravar(UnidadeTipoParada utp) {
        String sql = "INSERT INTO tparada_unidade (tpxun_codigo, tpxun_ativo, tpxun_data, tpxun_observacao, uni_codigo, tpa_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, utp.isAtivo());
                    ps.setDate(2, Date.valueOf(utp.getData()));
                    ps.setString(3, utp.getObservacao());
                    ps.setInt(4, utp.getCodUnidade());
                    ps.setInt(5, utp.getCodTipoParada());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeTipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(UnidadeTipoParada utp) {
        String sql = "UPDATE tparada_unidade "
                + "SET tpxun_ativo = ?, tpxun_observacao = ? "
                + "WHERE tpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, utp.isAtivo());
                    ps.setString(2, utp.getObservacao());
                    ps.setInt(3, utp.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeTipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM tparada_unidade "
                + "WHERE tpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeTipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeTipoParada regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM tparada_unidade "
                + "WHERE tpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadeTipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeTipoParada regPorUnidadeETipoParada(int codUnidade, int codTipoParada) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM tparada_unidade "
                + "WHERE uni_codigo = ? AND tpa_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codTipoParada);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorUnidadeTipoParada-UnidadeTipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeTipoParada> tiposParadaPorUnidade(int codUnidade) {
        List<UnidadeTipoParada> lista = new ArrayList();
        String sql = "SELECT tpu.tpxun_codigo, tpu.tpxun_ativo, tpu.tpxun_data, tpu.tpxun_observacao, tpu.uni_codigo, tpu.tpa_codigo "
                + "FROM tparada_unidade as tpu "
                + "INNER JOIN tipoparada AS tpa ON tpa.tpa_codigo = tpu.tpa_codigo "
                + "WHERE tpu.uni_codigo = ? "
                + "ORDER BY tpa.tpa_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("tiposParadasPorUnidade-UnidadeTipoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private String todosCampos() {
        return "tpxun_codigo, tpxun_ativo, tpxun_data, tpxun_observacao, uni_codigo, tpa_codigo";
    }

    private UnidadeTipoParada cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeTipoParada(
                rs.getInt("tpxun_codigo"),
                rs.getBoolean("tpxun_ativo"),
                rs.getDate("tpxun_data").toLocalDate(),
                rs.getString("tpxun_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("tpa_codigo")
        );
    }
}
