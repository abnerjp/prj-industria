package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadePerda;

/**
 *
 * @author abnerjp
 */
public abstract class UnidadePerdaDAO {

    protected boolean gravar(UnidadePerda up) {
        String sql = "INSERT INTO perda_unidade (" + todosCampos() + ") "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, up.isAtivo());
                    ps.setDate(2, Date.valueOf(up.getData()));
                    ps.setString(3, up.getObservacao());
                    ps.setInt(4, up.getCodUnidade());
                    ps.setInt(5, up.getCodPerda());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadePerdaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(UnidadePerda up) {
        String sql = "UPDATE perda_unidade "
                + "SET pexun_ativo = ?, pexun_data = ?, pexun_observacao = ? "
                + "WHERE pexun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, up.isAtivo());
                    ps.setDate(2, Date.valueOf(up.getData()));
                    ps.setString(3, up.getObservacao());
                    ps.setInt(4, up.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadePerdaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM perda_unidade "
                + "WHERE pexun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadePerdaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadePerda regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda_unidade "
                + "WHERE pexun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadePerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadePerda regPorUnidadeEPerda(int codUnidade, int codPerda) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda_unidade "
                + "WHERE uni_codigo = ? AND per_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codPerda);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorUnidadeEPerda-UnidadePerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadePerda> perdasPorUnidade(int codUnidade) {
        List<UnidadePerda> lista = new ArrayList();
        String sql = "SELECT pu.pexun_codigo, pu.pexun_ativo, pu.pexun_data, pu.pexun_observacao, pu.uni_codigo, pu.per_codigo "
                + "FROM perda_unidade as pu "
                + "INNER JOIN perda as p ON p.per_codigo = pu.per_codigo "
                + "WHERE pu.uni_codigo = ? "
                + "ORDER BY p.per_nome;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("perdasPorUnidade-UnidadePerdaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private UnidadePerda cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadePerda(
                rs.getInt("pexun_codigo"),
                rs.getBoolean("pexun_ativo"),
                rs.getDate("pexun_data").toLocalDate(),
                rs.getString("pexun_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("per_codigo")
        );
    }

    private String todosCampos() {
        return "pexun_codigo, pexun_ativo, pexun_data, pexun_observacao, uni_codigo, per_codigo";
    }
}
