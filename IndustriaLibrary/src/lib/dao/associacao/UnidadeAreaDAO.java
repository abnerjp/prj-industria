package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeArea;

/**
 *
 * @author abnerjp
 */
public abstract class UnidadeAreaDAO {

    protected boolean gravar(UnidadeArea ua) {
        String sql = "INSERT INTO area_unidade (" + todosCampos() + ") "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ua.isAtivo());
                    ps.setDate(2, Date.valueOf(ua.getData()));
                    ps.setString(3, ua.getObservacao());
                    ps.setInt(4, ua.getCodUnidade());
                    ps.setInt(5, ua.getCodArea());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeAreaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(UnidadeArea ua) {
        String sql = "UPDATE area_unidade "
                + "SET arxun_ativo = ?, arxun_data = ?, arxun_observacao = ? "
                + "WHERE arxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ua.isAtivo());
                    ps.setDate(2, Date.valueOf(ua.getData()));
                    ps.setString(3, ua.getObservacao());
                    ps.setInt(4, ua.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeAreaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM area_unidade "
                + "WHERE arxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeAreaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeArea regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area_unidade "
                + "WHERE arxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("refPorCodigo-UnidadeAreaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeArea regPorunidadeEArea(int codUnidade, int codArea) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area_unidade "
                + "WHERE uni_codigo = ? AND are_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codArea);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorunidadeEArea-UnidadeAreaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeArea> areasPorUnidade(int codUnidade) {
        List<UnidadeArea> lista = new ArrayList<>();

        String sql = "SELECT au.arxun_codigo, au.arxun_ativo, au.arxun_data, au.arxun_observacao, au.uni_codigo, au.are_codigo "
                + "FROM area_unidade as au "
                + "INNER JOIN area as a ON a.are_codigo = au.are_codigo "
                + "WHERE au.uni_codigo = ? "
                + "ORDER BY a.are_nome;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("areasPorUnidade-UnidadeAreaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private UnidadeArea cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeArea(
                rs.getInt("arxun_codigo"),
                rs.getBoolean("arxun_ativo"),
                rs.getDate("arxun_data").toLocalDate(),
                rs.getString("arxun_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("are_codigo")
        );
    }

    private String todosCampos() {
        return "arxun_codigo, arxun_ativo, arxun_data, arxun_observacao, uni_codigo, are_codigo";
    }
}
