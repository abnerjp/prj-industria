package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeEndereco;

/**
 *
 * @author abnerjp
 */
public abstract class UnidadeEnderecoDAO {

    protected boolean gravar(UnidadeEndereco eu) {
        String sql = "INSERT INTO endereco_unidade (uni_codigo, end_codigo, endxuni_data, endxuni_numero, endxuni_complemento, endxuni_observacao) "
                + "VALUES (?, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, eu.getCodUnidade());
                    ps.setInt(2, eu.getCodEndereco());
                    ps.setDate(3, Date.valueOf(eu.getData()));
                    ps.setInt(4, eu.getNumero());
                    ps.setString(5, eu.getComplemento());
                    ps.setString(6, eu.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-EnderecoUnidadeDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codUnidade, int codEndereco, LocalDate data) {
        String sql = "DELETE FROM endereco_unidade "
                + "WHERE uni_codigo = ? AND end_codigo = ? AND endxuni_data = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codEndereco);
                    ps.setDate(3, Date.valueOf(data));
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-EnderecoUnidadeDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Endereço atual de uma unidade
     *
     * @param codUnidade
     * @return objeto com endereço ou nulo, se não for encontrado
     */
    protected UnidadeEndereco regAtual(int codUnidade) {
        String sql = "SELECT uni_codigo, end_codigo, endxuni_data, endxuni_numero, endxuni_complemento, endxuni_observacao "
                + "FROM endereco_unidade "
                + "WHERE uni_codigo = ? "
                + "ORDER BY endxuni_data DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regAtual-EnderecoUnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Busca todos endereços ja registrados para uma unidade
     *
     * @param codUnidade
     * @return uma lista de enderecos com N registros
     */
    protected List<UnidadeEndereco> enderecosUnidade(int codUnidade) {
        List<UnidadeEndereco> lista = new ArrayList<>();
        String sql = "SELECT uni_codigo, end_codigo, endxuni_data, endxuni_numero, endxuni_complemento, endxuni_observacao "
                + "FROM endereco_unidade "
                + "WHERE uni_codigo = ? "
                + "ORDER BY endxuni_data ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("enderecosUnidade-EnderecoUnidadeDAO: " + ex.getMessage());
        }
        return lista;
    }

    /* Carregar objeto com resultado de resultset */
    private UnidadeEndereco cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeEndereco(
                rs.getInt("endxuni_numero"),
                rs.getDate("endxuni_data").toLocalDate(),
                rs.getString("endxuni_complemento"),
                rs.getString("endxuni_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("end_codigo")
        );
    }
}
