package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeAreaComEquipamento;

/**
 *
 * @author abnerjp
 */
public abstract class UnidadeAreaComEquipamentoDAO {

    protected boolean gravar(UnidadeAreaComEquipamento uae) {
        String sql = "INSERT INTO equipamento_area (eqxar_codigo, eqxar_ativo, eqxar_referencia, eqxar_data, eqxar_observacao, arxun_codigo, eqxun_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, uae.isAtivo());
                    ps.setString(2, uae.getReferencia());
                    ps.setDate(3, Date.valueOf(uae.getData()));
                    ps.setString(4, uae.getObservacao());
                    ps.setInt(5, uae.getCodUnidadeArea());
                    ps.setInt(6, uae.getCodUnidadeEquipamento());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean gravarLista(List<UnidadeAreaComEquipamento> uae) {
        boolean gravou = false;
        String sql = "INSERT INTO equipamento_area (eqxar_codigo, eqxar_ativo, eqxar_referencia, eqxar_data, eqxar_observacao, arxun_codigo, eqxun_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                conn.setAutoCommit(false);
                gravou = true;
                for (int i = 0; i < uae.size() && gravou; i++) {
                    try (PreparedStatement ps = conn.prepareStatement(sql)) {
                        ps.setBoolean(1, uae.get(i).isAtivo());
                        ps.setString(2, uae.get(i).getReferencia());
                        ps.setDate(3, Date.valueOf(uae.get(i).getData()));
                        ps.setString(4, uae.get(i).getObservacao());
                        ps.setInt(5, uae.get(i).getCodUnidadeArea());
                        ps.setInt(6, uae.get(i).getCodUnidadeEquipamento());
                        
                        gravou = ps.executeUpdate() == 1;
                    } catch (IndexOutOfBoundsException | NullPointerException ex) {
                        gravou = false;
                        throw new SQLException("fora do intervalo e/ou objeto nulo: " + ex.getMessage());
                    }
                }
                if (gravou) {
                    conn.commit();
                } else {
                    conn.rollback();
                }
                conn.setAutoCommit(true);
            }
        } catch (SQLException ex) {
            System.out.println("gravarLista-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return gravou;
    }

    protected boolean alterar(UnidadeAreaComEquipamento uae) {
        String sql = "UPDATE equipamento_area "
                + "SET eqxar_ativo = ?, eqxar_data = ?, eqxar_observacao = ? "
                + "WHERE eqxar_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, uae.isAtivo());
                    ps.setDate(2, Date.valueOf(uae.getData()));
                    ps.setString(3, uae.getObservacao());
                    ps.setInt(4, uae.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM equipamento_area "
                + "WHERE eqxar_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeAreaComEquipamento regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento_area "
                + "WHERE eqxar_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeAreaComEquipamento getRegistroUnico(int codUnidadeArea, int codUnidadeEquipamento, String referencia) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento_area "
                + "WHERE arxun_codigo = ? AND eqxun_codigo = ? AND eqxar_referencia = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidadeArea);
                    ps.setInt(2, codUnidadeEquipamento);
                    ps.setString(3, referencia);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroUnico-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeAreaComEquipamento> getEquipamentosPorArea(int codAreaUnidade) {
        List<UnidadeAreaComEquipamento> lista = new ArrayList<>();
        String sql = "SELECT uea.eqxar_codigo, uea.eqxar_ativo, uea.eqxar_referencia, uea.eqxar_data, uea.eqxar_uea.observacao, uea.arxun_codigo, uea.eqxun_codigo "
                + "FROM equipamento_area as uea "
                + "INNER JOIN equipamento_unidade AS eu ON eu.eqxun_codigo = uea.eqxun_codigo "
                + "INNER JOIN equipamento AS e ON e.equ_codigo = eu.equ_codigo "
                + "WHERE uea.arxun_codigo = ? "
                + "ORDER BY e.equ_nome;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codAreaUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getEquipamentosPorArea-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<UnidadeAreaComEquipamento> getEquipamentosPorUnidade(int codUnidade) {
        List<UnidadeAreaComEquipamento> lista = new ArrayList<>();
        String sql = "SELECT uea.eqxar_codigo, uea.eqxar_ativo, uea.eqxar_referencia, uea.eqxar_data, uea.eqxar_observacao, uea.arxun_codigo, uea.eqxun_codigo "
                + "FROM equipamento_area as uea "
                + "INNER JOIN equipamento_unidade AS eu ON eu.eqxun_codigo = uea.eqxun_codigo "
                + "INNER JOIN equipamento AS e ON e.equ_codigo = eu.equ_codigo "
                + "INNER JOIN area_unidade AS au ON au.arxun_codigo = uea.arxun_codigo "
                + "INNER JOIN area AS a ON a.are_codigo = au.are_codigo "
                + "INNER JOIN unidade AS u ON u.uni_codigo = au.uni_codigo "
                + "WHERE u.uni_codigo = ? "
                + "ORDER BY a.are_nome, e.equ_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getEquipamentosPorUnidade-UnidadeAreaComEquipamentoDAO: " + ex.getMessage());
        }
        return lista;
    }

    private String todosCampos() {
        return "eqxar_codigo, eqxar_ativo, eqxar_referencia, eqxar_data, eqxar_observacao, arxun_codigo, eqxun_codigo";
    }

    private UnidadeAreaComEquipamento cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeAreaComEquipamento(
                rs.getInt("eqxar_codigo"),
                rs.getBoolean("eqxar_ativo"),
                rs.getString("eqxar_referencia"),
                rs.getDate("eqxar_data").toLocalDate(),
                rs.getString("eqxar_observacao"),
                rs.getInt("arxun_codigo"),
                rs.getInt("eqxun_codigo")
        );
    }
}
