package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeEquipamento;

/**
 *
 * @author abner.jacomo
 */
public abstract class UnidadeEquipamentoDAO {

    public UnidadeEquipamentoDAO() {
    }

    protected boolean gravar(UnidadeEquipamento ue) {
        String sql = "INSERT INTO equipamento_unidade (eqxun_codigo, eqxun_ativo, eqxun_data, eqxun_observacao, uni_codigo, equ_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ue.isAtivo());
                    ps.setDate(2, Date.valueOf(ue.getData()));
                    ps.setString(3, ue.getObservacao());
                    ps.setInt(4, ue.getCodUnidade());
                    ps.setInt(5, ue.getCodEquipamento());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeEquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(UnidadeEquipamento ue) {
        String sql = "UPDATE equipamento_unidade "
                + "SET eqxun_ativo = ?, eqxun_observacao = ? "
                + "WHERE eqxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ue.isAtivo());
                    ps.setString(2, ue.getObservacao());
                    ps.setInt(3, ue.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeEquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM equipamento_unidade "
                + "WHERE eqxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeEquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeEquipamento regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento_unidade "
                + "WHERE eqxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadeEquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeEquipamento regPorUnidadeEEquipamento(int codUnidade, int codEquipamento) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento_unidade "
                + "WHERE uni_codigo = ? AND equ_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codEquipamento);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorUnidadeEEquipamento-UnidadeEquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeEquipamento> equipamentosPorUnidade(int codUnidade) {
        List<UnidadeEquipamento> lista = new ArrayList<>();
        String sql = "SELECT eu.eqxun_codigo, eu.eqxun_data, eu.eqxun_ativo, eu.eqxun_observacao, eu.uni_codigo, eu.equ_codigo "
                + "FROM equipamento_unidade as eu "
                + "INNER JOIN equipamento as e ON e.equ_codigo = eu.equ_codigo "
                + "WHERE eu.uni_codigo = ? "
                + "ORDER BY e.equ_nome ASC;";
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("equipamentosPorUnidade-UnidadeEquipamentoDAO: " + ex.getMessage());
        }
        return lista;
    }

    /* métodos internos à classe */
    private String todosCampos() {
        return "eqxun_codigo, eqxun_data, eqxun_ativo, eqxun_observacao, uni_codigo, equ_codigo";
    }

    //Carregar objeto com resultado de resultset 
    private UnidadeEquipamento cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeEquipamento(
                rs.getInt("eqxun_codigo"),
                rs.getBoolean("eqxun_ativo"),
                rs.getDate("eqxun_data").toLocalDate(),
                rs.getString("eqxun_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("equ_codigo")
        );
    }
}
