package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeItem;

/**
 *
 * @author abnerjp
 */
public abstract class UnidadeItemDAO {

    public UnidadeItemDAO() {
    }

    protected boolean gravar(UnidadeItem ui) {
        String sql = "INSERT INTO item_unidade (itxun_codigo, itxun_ativo, itxun_data, itxun_observacao, uni_codigo, ite_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ui.isAtivo());
                    ps.setDate(2, Date.valueOf(ui.getData()));
                    ps.setString(3, ui.getObservacao());
                    ps.setInt(4, ui.getCodUnidade());
                    ps.setInt(5, ui.getCodItem());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeItemDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(UnidadeItem ui) {
        String sql = "UPDATE item_unidade "
                + "SET itxun_ativo = ?, itxun_observacao = ? "
                + "WHERE itxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ui.isAtivo());
                    ps.setString(2, ui.getObservacao());
                    ps.setInt(3, ui.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeItemDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM item_unidade "
                + "WHERE itxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeItemDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeItem regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM item_unidade "
                + "WHERE itxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadeItemDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeItem regPorUnidadeEItem(int codUnidade, int codItem) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM item_unidade "
                + "WHERE uni_codigo = ? AND ite_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    ps.setInt(2, codItem);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorUnidadeItem-UnidadeItemDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeItem> itensPorUnidade(int codUnidade) {
        List<UnidadeItem> lista = new ArrayList<>();
        String sql = "SELECT iu.itxun_codigo, iu.itxun_ativo, iu.itxun_data, iu.itxun_observacao, iu.uni_codigo, iu.ite_codigo "
                + "FROM item_unidade as iu "
                + "INNER JOIN item as i ON i.ite_codigo = iu.ite_codigo "
                + "WHERE iu.uni_codigo = ? "
                + "ORDER BY i.ite_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("itensPorUnidade-UnidadeItemDAO: " + ex.getMessage());
        }
        return lista;
    }

    /* métodos internos à classe */
    private String todosCampos() {
        return "itxun_codigo, itxun_ativo, itxun_data, itxun_observacao, uni_codigo, ite_codigo";
    }

    //Carregar objeto com resultado de resultset 
    private UnidadeItem cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeItem(
                rs.getInt("itxun_codigo"),
                rs.getBoolean("itxun_ativo"),
                rs.getDate("itxun_data").toLocalDate(),
                rs.getString("itxun_observacao"),
                rs.getInt("uni_codigo"),
                rs.getInt("ite_codigo")
        );
    }
}
