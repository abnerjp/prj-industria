package lib.dao.associacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.associacao.UnidadeMotivoComTipoParada;

/**
 *
 * @author abnerjp
 */
public abstract class UnidadeMotivoComTipoParadaDAO {

    protected boolean gravar(UnidadeMotivoComTipoParada ump) {
        String sql = "INSERT INTO tpxmp_unidade (tpxmp_codigo, tpxmp_ativo, tpxmp_data, tpxmp_observacao, mpxun_codigo, tpxun_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ump.isAtivo());
                    ps.setDate(2, Date.valueOf(ump.getData()));
                    ps.setString(3, ump.getObservacao());
                    ps.setInt(4, ump.getCodUnidadeMotivoParada());
                    ps.setInt(5, ump.getCodUnidadeTipoParada());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean gravarLista(List<UnidadeMotivoComTipoParada> ump) {
        boolean gravou = false;
        String sql = "INSERT INTO tpxmp_unidade (tpxmp_codigo, tpxmp_ativo, tpxmp_data, tpxmp_observacao, mpxun_codigo, tpxun_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                conn.setAutoCommit(false);
                gravou = true;
                for (int i = 0; i < ump.size() && gravou; i++) {
                    gravou = false;
                    try (PreparedStatement ps = conn.prepareStatement(sql)) {
                        ps.setBoolean(1, ump.get(i).isAtivo());
                        ps.setDate(2, Date.valueOf(ump.get(i).getData()));
                        ps.setString(3, ump.get(i).getObservacao());
                        ps.setInt(4, ump.get(i).getCodUnidadeMotivoParada());
                        ps.setInt(5, ump.get(i).getCodUnidadeTipoParada());
                        gravou = ps.executeUpdate() == 1;
                    } catch (IndexOutOfBoundsException | NullPointerException ex) {
                        throw new SQLException("fora do intevalo e/ou objeto nulo: " + ex.getMessage());
                    }
                }
                if (gravou) {
                    conn.commit();
                } else {
                    conn.rollback();
                }
                conn.setAutoCommit(true);
            }
        } catch (SQLException ex) {
            System.out.println("gravarLista-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return gravou;
    }

    protected boolean alterar(UnidadeMotivoComTipoParada ump) {
        String sql = "UPDATE tpxmp_unidade "
                + "SET tpxmp_ativo = ?, tpxmp_data = ?, tpxmp_observacao = ? "
                + "WHERE tpxmp_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setBoolean(1, ump.isAtivo());
                    ps.setDate(2, Date.valueOf(ump.getData()));
                    ps.setString(3, ump.getObservacao());
                    ps.setInt(4, ump.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM tpxmp_unidade "
                + "WHERE tpxmp_codigo = ?";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected UnidadeMotivoComTipoParada regPorCodigo(int codigoVinculo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM tpxmp_unidade "
                + "WHERE tpxmp_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigoVinculo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorCodigo-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected UnidadeMotivoComTipoParada regPorTipoParadaEMotivoParada(int codUndiadeTipoParada, int codUnidadeMotivoParada) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM tpxmp_unidade "
                + "WHERE tpxun_codigo = ? AND mpxun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUndiadeTipoParada);
                    ps.setInt(2, codUnidadeMotivoParada);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("regPorTipoParadaEMotivoParada-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<UnidadeMotivoComTipoParada> getMotivosParadasPorTipoParada(int codTpParadaUnidade) {
        List<UnidadeMotivoComTipoParada> lista = new ArrayList<>();
        String sql = "SELECT tmp.tpxmp_codigo, tmp.tpxmp_ativo, tmp.tpxmp_data, tmp.tpxmp_observacao, tmp.mpxun_codigo, tmp.tpxun_codigo "
                + "FROM tpxmp_unidade as tmp "
                + "INNER JOIN mparada_unidade AS mpu ON mpu.mpxun_codigo = tpm.mpxun_codigo "
                + "INNER JOIN motivoparada AS mpa ON mpa.mpa_codigo = mpu.mpa_codigo "
                + "WHERE tmp.tpxun_codigo = ? "
                + "ORDER BY mpa.mpa_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codTpParadaUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getMotivosParadasPorTipoParada-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<UnidadeMotivoComTipoParada> getMotivosTiposPorUnidade(int codUnidade) {
        List<UnidadeMotivoComTipoParada> lista = new ArrayList<>();
        String sql = "SELECT tmp.tpxmp_codigo, tmp.tpxmp_ativo, tmp.tpxmp_data, tmp.tpxmp_observacao, tmp.mpxun_codigo, tmp.tpxun_codigo "
                + "FROM tpxmp_unidade as tmp "
                + "INNER JOIN mparada_unidade AS mpu ON mpu.mpxun_codigo = tmp.mpxun_codigo "
                + "INNER JOIN motivoparada AS mpa ON mpa.mpa_codigo = mpu.mpa_codigo "
                + "INNER JOIN tparada_unidade AS tpu ON tpu.tpxun_codigo = tmp.tpxun_codigo "
                + "INNER JOIN tipoparada AS tp ON tp.tpa_codigo = tpu.tpa_codigo "
                + "INNER JOIN unidade AS u ON u.uni_codigo = tpu.uni_codigo "
                + "WHERE u.uni_codigo = ? "
                + "ORDER BY tp.tpa_nome, mpa.mpa_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codUnidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getMotivosTiposPorUnidade-UnidadeMotivoComTipoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /* métodos internos à classe */
    private String todosCampos() {
        return "tpxmp_codigo, tpxmp_ativo, tpxmp_data, tpxmp_observacao, mpxun_codigo, tpxun_codigo";
    }

    //Carregar objeto com resultado de resultset 
    private UnidadeMotivoComTipoParada cargaObjeto(ResultSet rs) throws SQLException {
        return new UnidadeMotivoComTipoParada(
                rs.getInt("tpxmp_codigo"),
                rs.getBoolean("tpxmp_ativo"),
                rs.getDate("tpxmp_data").toLocalDate(),
                rs.getString("tpxmp_observacao"),
                rs.getInt("tpxun_codigo"),
                rs.getInt("mpxun_codigo")
        );
    }
}
