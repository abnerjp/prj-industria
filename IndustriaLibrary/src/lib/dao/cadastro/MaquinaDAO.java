package lib.dao.cadastro;

import lib.model.cadastro.Maquina;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public class MaquinaDAO {

    public MaquinaDAO() {
    }

    public boolean gravar(Maquina m) {
        String sql = "INSERT INTO maquina (maq_codigo, maq_modelo, maq_ativo, maq_observacao) "
                + "VALUES (default, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, m.getModelo());
                    ps.setBoolean(2, m.isAtivo());
                    ps.setString(3, m.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-MaquinaDAO: " + ex.getMessage());
        }
        return false;
    }

    public boolean alterar(Maquina m) {
        String sql = "UPDATE maquina "
                + "SET maq_modelo = ?, maq_ativo = ?, maq_observacao = ? "
                + "WHERE maq_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, m.getModelo());
                    ps.setBoolean(2, m.isAtivo());
                    ps.setString(3, m.getObservacao());
                    ps.setInt(4, m.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-MaquinaDAO: " + ex.getMessage());
        }
        return false;
    }

    public boolean excluir(int codigo) {
        String sql = "DELETE FROM maquina "
                + "WHERE maq_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-MaquinaDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    public Maquina getUltimoRegistro() {
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "ORDER BY maq_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-MaquinaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    public Maquina getPrimeiroRegistro() {
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "ORDER BY maq_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-MaquinaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    public Maquina getProximoRegistro(int codigo) {
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "WHERE maq_codigo > ? "
                + "ORDER BY maq_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-MaquinaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    public Maquina getAnteriorRegistro(int codigo) {
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "WHERE maq_codigo < ? "
                + "ORDER BY maq_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-MaquinaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    public Maquina getRegistroPorCodigo(int codigo) {
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "WHERE maq_codigo = ? "
                + "ORDER BY maq_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorCodigo-MaquinaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param modelo
     * @return
     */
    public List<Maquina> getListaPorModelo(String modelo) {
        List<Maquina> lista = new ArrayList<>();
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "WHERE maq_modelo ILIKE ? "
                + "ORDER BY maq_modelo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, modelo);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorModelo-MaquinaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param modelo
     * @param limite
     * @return
     */
    public List<Maquina> getListaLimitada(String modelo, int limite) {
        List<Maquina> lista = new ArrayList<>();
        String sql = "SELECT maq_codigo, maq_modelo, maq_ativo, maq_observacao "
                + " FROM maquina "
                + "WHERE maq_modelo ILIKE ? "
                + "ORDER BY maq_modelo ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, modelo);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-MaquinaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Maquina cargaObjeto(ResultSet rs) throws SQLException {
        return new Maquina(
                rs.getInt("maq_codigo"),
                rs.getString("maq_modelo"),
                rs.getBoolean("maq_ativo"),
                rs.getString("maq_observacao")
        );
    }
}
