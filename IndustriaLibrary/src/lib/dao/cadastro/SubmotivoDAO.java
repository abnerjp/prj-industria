package lib.dao.cadastro;

import lib.model.cadastro.Submotivo;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class SubmotivoDAO {

    public SubmotivoDAO() {
    }

    protected boolean gravar(Submotivo sm) {
        String sql = "INSERT INTO submotivo (sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao) "
                + "VALUES "
                + "(default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sm.getNome());
                    ps.setString(2, sm.getSigla());
                    ps.setBoolean(3, sm.isAtivo());
                    ps.setString(4, sm.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-SubmotivoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Submotivo sm) {
        String sql = "UPDATE submotivo "
                + "SET sub_nome = ?, sub_sigla = ?, sub_ativo = ?, sub_observacao = ? "
                + "WHERE sub_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sm.getNome());
                    ps.setString(2, sm.getSigla());
                    ps.setBoolean(3, sm.isAtivo());
                    ps.setString(4, sm.getObservacao());
                    ps.setInt(5, sm.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-SubmotivoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM submotivo "
                + "WHERE sub_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("exlcuir-SubmotivoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected Submotivo ultimoRegistro() {
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "ORDER BY sub_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-SubmotivoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Submotivo primeiroRegistro() {
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "ORDER BY sub_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("primeiroRegistro-SubmotivoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Submotivo proximoRegistro(int codigo) {
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "WHERE sub_codigo > ? "
                + "ORDER BY sub_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("proximoRegistro-SubmotivoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Submotivo anteriorRegistro(int codigo) {
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "WHERE sub_codigo < ? "
                + "ORDER BY sub_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("anteriorRegistro-SubmotivoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Submotivo registroPorCodigo(int codigo) {
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "WHERE sub_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-SubmotivoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<Submotivo> listaPorSigla(String sigla) {
        List<Submotivo> lista = new ArrayList<>();
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "WHERE sub_sigla ILIKE ? "
                + "ORDER BY sub_sigla ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorSigla-SubmotivoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Submotivo> listaPorNome(String nome) {
        List<Submotivo> lista = new ArrayList<>();
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "WHERE sub_nome ILIKE ? "
                + "ORDER BY sub_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-SubmotivoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Submotivo> listaLimitada(String nome, int limite) {
        List<Submotivo> lista = new ArrayList<>();
        String sql = "SELECT sub_codigo, sub_nome, sub_sigla, sub_ativo, sub_observacao "
                + "FROM submotivo "
                + "WHERE sub_nome ILIKE ? "
                + "ORDER BY sub_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaLimitada-SubmotivoDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Submotivo cargaObjeto(ResultSet rs) throws SQLException {
        return new Submotivo(
                rs.getInt("sub_codigo"),
                rs.getString("sub_nome"),
                rs.getString("sub_sigla"),
                rs.getBoolean("sub_ativo"),
                rs.getString("sub_observacao")
        );
    }
}
