package lib.dao.cadastro;

import lib.model.cadastro.Item;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class ItemDAO {

    public ItemDAO() {
    }

    protected boolean gravar(Item i) {
        String sql = "INSERT INTO item (ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo) "
                + "VALUES (default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, i.getNome());
                    ps.setBoolean(2, i.isAtivo());
                    ps.setString(3, i.getObservacao());
                    ps.setInt(4, i.getCodigoMedida());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-ItemDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Item i) {
        String sql = "UPDATE item "
                + "SET ite_nome = ?, ite_ativo = ?, ite_observacao = ?, med_codigo = ? "
                + "WHERE ite_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, i.getNome());
                    ps.setBoolean(2, i.isAtivo());
                    ps.setString(3, i.getObservacao());
                    ps.setInt(4, i.getCodigoMedida());
                    ps.setInt(5, i.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-ItemDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM item "
                + "WHERE ite_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-ItemDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Item ultimoRegistro() {
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "ORDER BY ite_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-ItemDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Item primeiroRegistro() {
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "ORDER BY ite_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-ItemDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Item proximoRegistro(int codigo) {
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "WHERE ite_codigo > ? "
                + "ORDER BY ite_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-ItemDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Item anteriorRegistro(int codigo) {
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "WHERE ite_codigo < ? "
                + "ORDER BY ite_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Item registroPorCodigo(int codigo) {
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "WHERE ite_codigo = ? "
                + "ORDER BY ite_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<Item> listaPorNome(String nome) {
        List<Item> lista = new ArrayList<>();
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "WHERE ite_nome ILIKE ? "
                + "ORDER BY ite_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-ItemDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param nome
     * @param limite
     * @return
     */
    protected List<Item> listaLimitada(String nome, int limite) {
        List<Item> lista = new ArrayList<>();
        String sql = "SELECT ite_codigo, ite_nome, ite_ativo, ite_observacao, med_codigo "
                + "FROM item "
                + "WHERE ite_nome ILIKE ? "
                + "ORDER BY ite_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-ItemDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com a medida informada
     *
     * @param nomeMedida
     * @return
     */
    protected List<Item> listaPorMedida(String nomeMedida) {
        List<Item> lista = new ArrayList<>();
        String sql = "SELECT ite.ite_codigo, ite.ite_nome, ite.ite_ativo, ite.ite_observacao, ite.med_codigo "
                + "FROM item AS ite "
                + "INNER JOIN medida as med on ite.ite_codigo = med.med_codigo "
                + "WHERE med.med_nome ILIKE ? "
                + "ORDER BY ite.ite_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nomeMedida);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorMedida-ItemDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Item cargaObjeto(ResultSet rs) throws SQLException {
        return new Item(
                rs.getInt("ite_codigo"),
                rs.getString("ite_nome"),
                rs.getBoolean("ite_ativo"),
                rs.getString("ite_observacao"),
                rs.getInt("med_codigo")
        );
    }

}
