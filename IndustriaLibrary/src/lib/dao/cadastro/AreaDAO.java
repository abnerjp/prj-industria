package lib.dao.cadastro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.cadastro.Area;

/**
 *
 * @author abnerjp
 */
public abstract class AreaDAO {

    public AreaDAO() {
    }

    protected boolean gravar(Area a) {
        String sql = "INSERT INTO area (are_codigo, are_nome, are_descricao, are_ativo, are_observacao) "
                + "VALUES (default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, a.getNome());
                    ps.setString(2, a.getDescricao());
                    ps.setBoolean(3, a.isAtivo());
                    ps.setString(4, a.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-AreaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Area a) {
        String sql = "UPDATE area "
                + "SET are_nome = ?, are_descricao = ?, are_ativo = ?, are_observacao = ? "
                + "WHERE are_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, a.getNome());
                    ps.setString(2, a.getDescricao());
                    ps.setBoolean(3, a.isAtivo());
                    ps.setString(4, a.getObservacao());
                    ps.setInt(5, a.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-AreaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM area "
                + "WHERE are_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-AreaDAO: " + ex.getMessage());
        }
        return false;
    }

    //retorna o ultimo registro da tabela
    protected Area ultimoRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "ORDER BY are_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("ultimoRegistro-AreaDAO: " + ex.getMessage());
        }
        return null;
    }

    //retorna o primeiro registro da tabela
    protected Area primeiroRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "ORDER BY are_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("primeiroRegistro-AreaDAO: " + ex.getMessage());
        }
        return null;
    }

    //retorna o próximo registro da tabela, a partir do código informado
    protected Area proximoRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "WHERE are_codigo > ? "
                + "ORDER BY are_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("proximoRegistro-AreaDAO: " + ex.getMessage());
        }
        return null;
    }

    //retorna o registro anterior da tabela, a partir do código informado
    protected Area anteriorRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "WHERE are_codigo < ? "
                + "ORDER BY are_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("anteriorRegistro-AreaDAO: " + ex.getMessage());
        }
        return null;
    }

    //retorna um registro de acordo com o codigo informado
    protected Area registroPorCodigo(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "WHERE are_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-AreaDAO: " + ex.getMessage());
        }
        return null;
    }

    //retorna uma lista de registros de acordo com o filtro informado
    protected List<Area> listaPorNome(String nome) {
        List<Area> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "WHERE are_nome ILIKE ? "
                + "ORDER BY are_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorNome-AreaDAO: " + ex.getMessage());
        }
        return lista;
    }

    //retorna uma lista de registro de acordo com o filtro informado, e com resultado limitado a quantidade informada
    protected List<Area> listaLimitada(String nome, int limite) {
        List<Area> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM area "
                + "WHERE are_nome ILIKE ? "
                + "ORDER BY are_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaLimitada-AreaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /* métodos internos à classe */
    private String todosCampos() {
        return "are_codigo, are_nome, are_descricao, are_ativo, are_observacao";
    }

    //Carregar objeto com resultado de resultset 
    private Area cargaObjeto(ResultSet rs) throws SQLException {
        return new Area(
                rs.getInt("are_codigo"),
                rs.getString("are_nome"),
                rs.getString("are_descricao"),
                rs.getBoolean("are_ativo"),
                rs.getString("are_observacao")
        );
    }
}
