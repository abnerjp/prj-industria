package lib.dao.cadastro;

import lib.model.cadastro.TipoParada;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class TipoParadaDAO {

    public TipoParadaDAO() {
    }

    protected boolean gravar(TipoParada tp) {
        String sql = "INSERT INTO tipoparada (tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao) "
                + "VALUES "
                + "(default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, tp.getNome());
                    ps.setString(2, tp.getSigla());
                    ps.setBoolean(3, tp.isAtivo());
                    ps.setString(4, tp.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-TipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(TipoParada tp) {
        String sql = "UPDATE tipoparada "
                + "SET tpa_nome = ?, tpa_sigla = ?, tpa_ativo = ?, tpa_observacao = ? "
                + "WHERE tpa_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, tp.getNome());
                    ps.setString(2, tp.getSigla());
                    ps.setBoolean(3, tp.isAtivo());
                    ps.setString(4, tp.getObservacao());
                    ps.setInt(5, tp.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-TipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM tipoparada "
                + "WHERE tpa_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("exlcuir-TipoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected TipoParada ultimoRegistro() {
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "ORDER BY tpa_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-TipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected TipoParada primeiroRegistro() {
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "ORDER BY tpa_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-TipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected TipoParada proximoRegistro(int codigo) {
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "WHERE tpa_codigo > ? "
                + "ORDER BY tpa_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-TipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected TipoParada anteriorRegistro(int codigo) {
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "WHERE tpa_codigo < ? "
                + "ORDER BY tpa_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-TipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected TipoParada registroPorCodigo(int codigo) {
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "WHERE tpa_codigo = ? "
                + "ORDER BY tpa_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-TipoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param sigla
     * @return
     */
    protected List<TipoParada> listaPorSigla(String sigla) {
        List<TipoParada> lista = new ArrayList<>();
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "WHERE tpa_sigla ILIKE ? "
                + "ORDER BY tpa_sigla ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorSigla-TipoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<TipoParada> listaPorNome(String nome) {
        List<TipoParada> lista = new ArrayList<>();
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "WHERE tpa_nome ILIKE ? "
                + "ORDER BY tpa_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-TipoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param nome
     * @param limite
     * @return
     */
    protected List<TipoParada> listaLimitada(String nome, int limite) {
        List<TipoParada> lista = new ArrayList<>();
        String sql = "SELECT tpa_codigo, tpa_codigo, tpa_nome, tpa_sigla, tpa_ativo, tpa_observacao "
                + "FROM tipoparada "
                + "WHERE tpa_nome ILIKE ? "
                + "ORDER BY tpa_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-TipoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private TipoParada cargaObjeto(ResultSet rs) throws SQLException {
        return new TipoParada(
                rs.getInt("tpa_codigo"),
                rs.getString("tpa_nome"),
                rs.getString("tpa_sigla"),
                rs.getBoolean("tpa_ativo"),
                rs.getString("tpa_observacao")
        );
    }
}
