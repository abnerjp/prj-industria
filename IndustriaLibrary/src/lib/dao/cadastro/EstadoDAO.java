package lib.dao.cadastro;

import lib.model.cadastro.Estado;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class EstadoDAO {

    public EstadoDAO() {
    }

    protected boolean gravar(Estado e) {
        String sql = "INSERT INTO estado (est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, e.getNome());
                    ps.setString(2, e.getSigla());
                    ps.setBoolean(3, e.isAtivo());
                    ps.setString(4, e.getObservacao());
                    ps.setInt(5, e.getCodigoPais());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-EstadoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Estado e) {
        String sql = "UPDATE estado "
                + "SET est_nome = ?, est_sigla = ?, est_ativo = ?, est_observacao = ?, pai_codigo = ? "
                + "WHERE est_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, e.getNome());
                    ps.setString(2, e.getSigla());
                    ps.setBoolean(3, e.isAtivo());
                    ps.setString(4, e.getObservacao());
                    ps.setInt(5, e.getCodigoPais());
                    ps.setInt(6, e.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-EstadoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM estado "
                + "WHERE est_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-EstadoDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Estado ultimoRegistro() {
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "ORDER BY est_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-EstadoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Estado primeiroRegistro() {
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "ORDER BY est_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-EstadoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Estado proximoRegistro(int codigo) {
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_codigo > ? "
                + "ORDER BY est_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-EstadoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Estado anteriorRegistro(int codigo) {
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_codigo < ? "
                + "ORDER BY est_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-EstadoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Estado registroPorCodigo(int codigo) {
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_codigo = ? "
                + "ORDER BY est_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-EstadoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com a sigla informada
     *
     * @param sigla
     * @return
     */
    protected Estado registroPorSigla(String sigla) {
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_sigla LIKE ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorSigla-EstadoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<Estado> listaPorNome(String nome) {
        List<Estado> lista = new ArrayList<>();
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_nome ILIKE ? "
                + "ORDER BY est_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-EstadoDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param sigla
     * @return
     */
    protected List<Estado> listaPorSigla(String sigla) {
        List<Estado> lista = new ArrayList<>();
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_sigla ILIKE ? "
                + "ORDER BY est_codigo ASC ";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorSigla-EstadoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Estado> listaLimitada(String nome, int limite) {
        List<Estado> lista = new ArrayList<>();
        String sql = "SELECT est_codigo, est_nome, est_sigla, est_ativo, est_observacao, pai_codigo "
                + "FROM estado "
                + "WHERE est_nome ILIKE ? "
                + "ORDER BY est_codigo ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-EstadoDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o país informado
     *
     * @param nomePais
     * @return
     */
    protected List<Estado> listaPorPais(String nomePais) {
        List<Estado> lista = new ArrayList<>();
        String sql = "SELECT est.est_codigo, est.est_nome, est.est_sigla, est.est_ativo, est.est_observacao, est.pai_codigo "
                + "FROM estado AS est "
                + "INNER JOIN pais as pai on est.est_codigo = pai.pai_codigo "
                + "WHERE pai.pai_nome ILIKE ? "
                + "ORDER BY est.est_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nomePais);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorPais-EstadoDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Estado cargaObjeto(ResultSet rs) throws SQLException {
        return new Estado(
                rs.getInt("est_codigo"),
                rs.getString("est_nome"),
                rs.getString("est_sigla"),
                rs.getBoolean("est_ativo"),
                rs.getString("est_observacao"),
                rs.getInt("pai_codigo")
        );
    }
}
