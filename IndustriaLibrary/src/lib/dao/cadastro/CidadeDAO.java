package lib.dao.cadastro;

import lib.model.cadastro.Cidade;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class CidadeDAO {

    public CidadeDAO() {
    }

    protected boolean gravar(Cidade c) {
        String sql = "INSERT INTO cidade (cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo) "
                + "VALUES (default, ?, ?, ?, ?);";
        
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, c.getNome());
                    ps.setBoolean(2, c.isAtivo());
                    ps.setString(3, c.getObservacao());
                    ps.setInt(4, c.getCodigoEstado());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-CidadeDAO: " + ex.getMessage());
        }
        return false;
    }
    
    protected boolean alterar(Cidade c) {
        String sql = "UPDATE cidade "
                + "SET cid_nome = ?, cid_ativo = ?, cid_observacao = ?, est_codigo = ? "
                + "WHERE cid_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, c.getNome());
                    ps.setBoolean(2, c.isAtivo());
                    ps.setString(3, c.getObservacao());
                    ps.setInt(4, c.getCodigoEstado());
                    ps.setInt(5, c.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-CidadeDAO: " + ex.getMessage());
        }
        return false;
    }
    
    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM cidade "
                + "WHERE cid_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-CidadeDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Cidade ultimoRegistro() {
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "ORDER BY cid_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-CidadeDAO: " + ex.getMessage());
        }
        return null;
    }
    
     /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Cidade primeiroRegistro() {
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "ORDER BY cid_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-CidadeDAO: " + ex.getMessage());
        }
        return null;
    }
    
    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Cidade proximoRegistro(int codigo) {
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "WHERE cid_codigo > ? "
                + "ORDER BY cid_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-CidadeDAO: " + ex.getMessage());
        }
        return null;
    }
    
    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Cidade anteriorRegistro(int codigo) {
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "WHERE cid_codigo < ? "
                + "ORDER BY cid_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-CidadeDAO: " + ex.getMessage());
        }
        return null;
    }
    
    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Cidade registroPorCodigo(int codigo) {
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "WHERE cid_codigo = ? "
                + "ORDER BY cid_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-CidadeDAO: " + ex.getMessage());
        }
        return null;
    }
    
    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<Cidade> listaPorNome(String nome) {
        List<Cidade> lista = new ArrayList<>();
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "WHERE cid_nome ILIKE ? "
                + "ORDER BY cid_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-CidadeDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    protected List<Cidade> listaLimitada(String nome, int limite) {
        List<Cidade> lista = new ArrayList<>();
        String sql = "SELECT cid_codigo, cid_nome, cid_ativo, cid_observacao, est_codigo "
                + "FROM cidade "
                + "WHERE cid_nome ILIKE ? "
                + "ORDER BY cid_codigo ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-CidadeDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    /**
     * Retorna uma lista de registro de acordo com o estado informado
     *
     * @param nomeEstado
     * @return
     */
    protected List<Cidade> listaPorEstado(String nomeEstado) {
        List<Cidade> lista = new ArrayList<>();
        String sql = "SELECT cid.cid_codigo, cid.cid_nome, cid.cid_ativo, cid.cid_observacao, cid.est_codigo "
                + "FROM cidade AS cid "
                + "INNER JOIN estado as est on cid.est_codigo = est.est_codigo "
                + "WHERE est.est_nome ILIKE ? "
                + "ORDER BY cid.cid_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nomeEstado);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorEstado-CidadeDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    private Cidade cargaObjeto(ResultSet rs) throws SQLException {
        return new Cidade(
                rs.getInt("cid_codigo"),
                rs.getString("cid_nome"),
                rs.getBoolean("cid_ativo"),
                rs.getString("cid_observacao"),
                rs.getInt("est_codigo")
        );
    }
}
