package lib.dao.cadastro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.cadastro.Equipamento;

/**
 *
 * @author abnerjp
 */
public abstract class EquipamentoDAO {

    public EquipamentoDAO() {
    }

    protected boolean gravar(Equipamento e) {
        String sql = "INSERT INTO equipamento (equ_codigo, equ_nome, equ_modelo, equ_ativo, equ_observacao) "
                + "VALUES (default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, e.getNome());
                    ps.setString(2, e.getModelo());
                    ps.setBoolean(3, e.isAtivo());
                    ps.setString(4, e.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-EquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Equipamento e) {
        String sql = "UPDATE equipamento "
                + "SET equ_nome = ?, equ_modelo = ?, equ_ativo = ?, equ_observacao = ? "
                + "WHERE equ_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, e.getNome());
                    ps.setString(2, e.getModelo());
                    ps.setBoolean(3, e.isAtivo());
                    ps.setString(4, e.getObservacao());
                    ps.setInt(5, e.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-EquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM equipamento "
                + "WHERE equ_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-EquipamentoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected Equipamento ultimoRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "ORDER BY equ_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("ultimoRegistro-EquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Equipamento primeiroRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "ORDER BY equ_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("primeiroRegistro-EquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Equipamento proximoRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "WHERE equ_codigo > ? "
                + "ORDER BY equ_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("proximoRegistro-EquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Equipamento anteriorRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "WHERE equ_codigo < ? "
                + "ORDER BY equ_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("anteriorRegistro-EquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Equipamento registroPorCodigo(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "WHERE equ_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-EquipamentoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<Equipamento> listaPorNome(String nome) {
        List<Equipamento> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "WHERE equ_nome ILIKE ? "
                + "ORDER BY equ_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorNome-EquipamentoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Equipamento> listaPorModelo(String modelo) {
        List<Equipamento> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "WHERE equ_modelo ILIKE ? "
                + "ORDER BY equ_modelo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, modelo);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorModelo-EquipamentoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Equipamento> listaLimitada(String nome, int limite) {
        List<Equipamento> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM equipamento "
                + "WHERE equ_nome ILIKE ? "
                + "ORDER BY equ_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaLimitada-EquipamentoDAO: " + ex.getMessage());
        }
        return lista;
    }

    /* métodos internos à classe */
    private String todosCampos() {
        return "equ_codigo, equ_nome, equ_modelo, equ_ativo, equ_observacao";
    }

    //Carregar objeto com resultado de resultset 
    private Equipamento cargaObjeto(ResultSet rs) throws SQLException {
        return new Equipamento(
                rs.getInt("equ_codigo"),
                rs.getString("equ_nome"),
                rs.getString("equ_modelo"),
                rs.getBoolean("equ_ativo"),
                rs.getString("equ_observacao")
        );
    }
}
