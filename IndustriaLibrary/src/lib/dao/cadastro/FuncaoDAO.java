package lib.dao.cadastro;

import lib.model.cadastro.Funcao;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class FuncaoDAO {

    protected boolean gravar(Funcao f) {
        String sql = "INSERT INTO funcao (" + todosCampos() + ") "
                + "VALUES "
                + "(default,?,?,?);";
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, f.getNome());
                    ps.setBoolean(2, f.isAtivo());
                    ps.setString(3, f.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-FuncaoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Funcao f) {
        String sql = "UPDATE funcao "
                + "SET fun_nome = ?, fun_ativo = ?, fun_observacao = ? "
                + "WHERE fun_codigo = ?;";
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, f.getNome());
                    ps.setBoolean(2, f.isAtivo());
                    ps.setString(3, f.getObservacao());
                    ps.setInt(4, f.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-FuncaoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM funcao "
                + "WHERE fun_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-FuncaoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected Funcao ultimoRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "ORDER BY fun_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("ultimoRegistro-FuncaoDAO: " + ex.getMessage());
        }
        return null;
    }

    public Funcao primeiroRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "ORDER BY fun_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("primeiroRegistro-FuncaoDAO: " + ex.getMessage());
        }
        return null;
    }

    public Funcao proximoRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "WHERE fun_codigo > ? "
                + "ORDER BY fun_codigo ASC "
                + "LIMIT 1;";
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("proximoRegistro-FuncaoDAO: " + ex.getMessage());
        }
        return null;
    }

    public Funcao anteriorRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "WHERE fun_codigo < ? "
                + "ORDER BY fun_codigo DESC "
                + "LIMIT 1;";
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("anteriorRegistro-FuncaoDAO: " + ex.getMessage());
        }
        return null;
    }

    public Funcao registroPorCodigo(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "WHERE fun_codigo = ? "
                + "ORDER BY fun_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-FuncaoDAO: " + ex.getMessage());
        }
        return null;
    }

    public List<Funcao> listaPorNome(String nome) {
        List<Funcao> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "WHERE fun_nome ILIKE ? "
                + "ORDER BY fun_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorNome-FuncaoDAO: " + ex.getMessage());
        }
        return lista;
    }

    public List<Funcao> listaLimitada(String nome, int limite) {
        List<Funcao> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM funcao "
                + "WHERE fun_nome ILIKE ? "
                + "ORDER BY fun_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaLimitada-FuncaoDAO: " + ex.getMessage());
        }
        return lista;
    }

    private String todosCampos() {
        return "fun_codigo, fun_nome, fun_ativo, fun_observacao";
    }

    private Funcao cargaObjeto(ResultSet rs) throws SQLException {
        return new Funcao(
                rs.getInt("fun_codigo"),
                rs.getString("fun_nome"),
                rs.getBoolean("fun_ativo"),
                rs.getString("fun_observacao")
        );
    }
}
