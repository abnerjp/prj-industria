package lib.dao.cadastro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.cadastro.Usuario;

/**
 *
 * @author abnerjp
 */
public abstract class UsuarioDAO {

    protected boolean gravar(Usuario u) {
        String sql = "INSERT INTO usuario (" + todosCampos() + ") "
                + "VALUES "
                + "(default, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, u.getNome());
                    ps.setString(2, u.getSexo());
                    ps.setString(3, u.getLogin());
                    ps.setString(4, u.getSenha());
                    ps.setString(5, u.getEmail());
                    ps.setBoolean(6, u.isAtivo());
                    ps.setString(7, u.getImagem());
                    ps.setString(8, u.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UsuarioDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Usuario u) {
        String sql = "UPDATE usuario "
                + "SET usu_nome = ?, usu_sexo = ?, usu_login = ?, usu_senha = ?, usu_email = ?, usu_ativo = ?, usu_imagem = ?, usu_observacao = ? "
                + "WHERE usu_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, u.getNome());
                    ps.setString(2, u.getSexo());
                    ps.setString(3, u.getLogin());
                    ps.setString(4, u.getSenha());
                    ps.setString(5, u.getEmail());
                    ps.setBoolean(6, u.isAtivo());
                    ps.setString(7, u.getImagem());
                    ps.setString(8, u.getObservacao());
                    ps.setInt(9, u.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UsuarioDAO: " + ex.getMessage());
        }
        return false;
    }
    
    //não altera a imagem
    protected boolean alterarDados(Usuario u) {
        String sql = "UPDATE usuario "
                + "SET usu_nome = ?, usu_sexo = ?, usu_login = ?, usu_senha = ?, usu_email = ?, usu_ativo = ?, usu_observacao = ? "
                + "WHERE usu_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, u.getNome());
                    ps.setString(2, u.getSexo());
                    ps.setString(3, u.getLogin());
                    ps.setString(4, u.getSenha());
                    ps.setString(5, u.getEmail());
                    ps.setBoolean(6, u.isAtivo());
                    ps.setString(7, u.getObservacao());
                    ps.setInt(8, u.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterarDados-UsuarioDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM usuario "
                + "WHERE usu_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("exlcuir-UsuarioDAO: " + ex.getMessage());
        }
        return false;
    }

    protected Usuario ultimoRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "ORDER BY usu_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("ultimoRegistro-UsuarioDAO: " + ex.getMessage());
        }
        return null;
    }
    
    protected Usuario primeiroRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "ORDER BY usu_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("primeiroRegistro-UsuarioDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Usuario proximoRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_codigo > ? "
                + "ORDER BY usu_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("proximoRegistro-UsuarioDAO: " + ex.getMessage());
        }
        return null;
    }
    
    protected Usuario anteriorRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_codigo < ? "
                + "ORDER BY usu_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("anteriorRegistro-UsuarioDAO: " + ex.getMessage());
        }
        return null;
    }
    
    protected Usuario registroPorCodigo(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-UsuarioDAO: " + ex.getMessage());
        }
        return null;
    }
    
    protected Usuario registroPorLogin(String login) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_login ILIKE ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, login);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-UsuarioDAO: " + ex.getMessage());
        }
        return null;
    }
    
    protected List<Usuario> listaPorLogin(String login) {
        List<Usuario> lista = new ArrayList();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_login ILIKE ? "
                + "ORDER BY usu_login ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, login);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorLogin-UsuarioDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    protected List<Usuario> listaPorEmail(String email) {
        List<Usuario> lista = new ArrayList();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_email ILIKE ? "
                + "ORDER BY usu_email ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, email);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorEmail-UsuarioDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    protected List<Usuario> listaPorNome(String nome) {
        List<Usuario> lista = new ArrayList();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_nome ILIKE ? "
                + "ORDER BY usu_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorNome-UsuarioDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    protected List<Usuario> listaLimitada(String nome, int limite) {
        List<Usuario> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM usuario "
                + "WHERE usu_nome ILIKE ? "
                + "ORDER BY usu_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaLimitada-UsuarioDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    private String todosCampos() {
        return "usu_codigo, usu_nome, usu_sexo, usu_login, usu_senha, usu_email, usu_ativo, usu_imagem, usu_observacao";
    }

    private Usuario cargaObjeto(ResultSet rs) throws SQLException {
        return new Usuario(
                rs.getInt("usu_codigo"),
                rs.getString("usu_nome"),
                rs.getString("usu_sexo"),
                rs.getString("usu_login"),
                rs.getString("usu_senha"),
                rs.getString("usu_email"),
                rs.getString("usu_imagem"),
                rs.getBoolean("usu_ativo"),
                rs.getString("usu_observacao")
        );
    }
}
