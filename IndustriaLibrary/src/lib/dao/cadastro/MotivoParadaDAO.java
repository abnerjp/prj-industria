package lib.dao.cadastro;

import lib.model.cadastro.MotivoParada;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class MotivoParadaDAO {

    public MotivoParadaDAO() {
    }

    protected boolean gravar(MotivoParada mp) {
        String sql = "INSERT INTO motivoparada (mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao) "
                + "VALUES "
                + "(default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, mp.getNome());
                    ps.setString(2, mp.getSigla());
                    ps.setBoolean(3, mp.isAtivo());
                    ps.setString(4, mp.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-MotivoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(MotivoParada mp) {
        String sql = "UPDATE motivoparada "
                + "SET mpa_nome = ?, mpa_sigla = ?, mpa_ativo = ?, mpa_observacao = ? "
                + "WHERE mpa_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, mp.getNome());
                    ps.setString(2, mp.getSigla());
                    ps.setBoolean(3, mp.isAtivo());
                    ps.setString(4, mp.getObservacao());
                    ps.setInt(5, mp.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-MotivoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM motivoparada "
                + "WHERE mpa_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("exlcuir-MotivoParadaDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected MotivoParada ultimoRegistro() {
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "ORDER BY mpa_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-MotivoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected MotivoParada primeiroRegistro() {
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "ORDER BY mpa_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-MotivoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected MotivoParada proximoRegistro(int codigo) {
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "WHERE mpa_codigo > ? "
                + "ORDER BY mpa_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-MotivoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected MotivoParada anteriorRegistro(int codigo) {
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "WHERE mpa_codigo < ? "
                + "ORDER BY mpa_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-MotivoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected MotivoParada registroPorCodigo(int codigo) {
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "WHERE mpa_codigo = ? "
                + "ORDER BY mpa_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-MotivoParadaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param sigla
     * @return
     */
    protected List<MotivoParada> listaPorSigla(String sigla) {
        List<MotivoParada> lista = new ArrayList<>();
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "WHERE mpa_sigla ILIKE ? "
                + "ORDER BY mpa_sigla ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorSigla-MotivoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<MotivoParada> listaPorNome(String nome) {
        List<MotivoParada> lista = new ArrayList<>();
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "WHERE mpa_nome ILIKE ? "
                + "ORDER BY mpa_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-MotivoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param nome
     * @param limite
     * @return
     */
    protected List<MotivoParada> listaLimitada(String nome, int limite) {
        List<MotivoParada> lista = new ArrayList<>();
        String sql = "SELECT mpa_codigo, mpa_codigo, mpa_nome, mpa_sigla, mpa_ativo, mpa_observacao "
                + "FROM motivoparada "
                + "WHERE mpa_nome ILIKE ? "
                + "ORDER BY mpa_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-MotivoParadaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private MotivoParada cargaObjeto(ResultSet rs) throws SQLException {
        return new MotivoParada(
                rs.getInt("mpa_codigo"),
                rs.getString("mpa_nome"),
                rs.getString("mpa_sigla"),
                rs.getBoolean("mpa_ativo"),
                rs.getString("mpa_observacao")
        );
    }
}
