package lib.dao.cadastro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.cadastro.Unidade;

/**
 *
 * @author abner.jacomo
 */
public abstract class UnidadeDAO {

    public UnidadeDAO() {
    }

    protected boolean gravar(Unidade u) {
        String sql = "INSERT INTO unidade (uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao) "
                + "VALUES (default, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, u.getRazaoSocial());
                    ps.setString(2, u.getNomeFantasia());
                    ps.setString(3, u.getCnpj());
                    ps.setString(4, u.getTelefoneFixo());
                    ps.setString(5, u.getTelefoneCelular());
                    ps.setString(6, u.getEmail());
                    ps.setString(7, u.getInscricaoEstadual());
                    ps.setString(8, u.getReferencia());
                    ps.setString(9, u.getSenha());
                    ps.setBoolean(10, u.isAtivo());
                    ps.setString(11, u.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-UnidadeDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Unidade u) {
        String sql = "UPDATE unidade "
                + "SET uni_razaosocial = ?, uni_nomefantasia = ?, uni_cnpj = ?, "
                + "uni_telefonefixo = ?, uni_telefonemovel = ?, uni_email = ?, "
                + "uni_inscricaoestadual = ?, uni_referencia = ?, uni_senha = ?, uni_ativo = ?, uni_observacao = ? "
                + "WHERE uni_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, u.getRazaoSocial());
                    ps.setString(2, u.getNomeFantasia());
                    ps.setString(3, u.getCnpj());
                    ps.setString(4, u.getTelefoneFixo());
                    ps.setString(5, u.getTelefoneCelular());
                    ps.setString(6, u.getEmail());
                    ps.setString(7, u.getInscricaoEstadual());
                    ps.setString(8, u.getReferencia());
                    ps.setString(9, u.getSenha());
                    ps.setBoolean(10, u.isAtivo());
                    ps.setString(11, u.getObservacao());
                    ps.setInt(12, u.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-UnidadeDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM unidade "
                + "WHERE uni_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-UnidadeDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Unidade ultimoRegistro() {
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "ORDER BY uni_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            return getRegistroNavegacao(sql);
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-UnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Unidade primeiroRegistro() {
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "ORDER BY uni_codigo ASC "
                + "LIMIT 1;";

        try {
            return getRegistroNavegacao(sql);
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-UnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Unidade proximoRegistro(int codigo) {
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_codigo > ? "
                + "ORDER BY uni_codigo ASC "
                + "LIMIT 1;";

        try {
            return getRegistroNavegacao(sql, codigo);
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-UnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Unidade anteriorRegistro(int codigo) {
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_codigo < ? "
                + "ORDER BY uni_codigo DESC "
                + "LIMIT 1;";

        try {
            return getRegistroNavegacao(sql, codigo);
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-UnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Unidade registroPorCodigo(int codigo) {
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_codigo = ?;";

        try {
            return getRegistroNavegacao(sql, codigo);
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-UnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com a referência informada
     *
     * @param filtro
     * @return
     */
    protected Unidade registroPorReferencia(String filtro) {
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM medida "
                + "WHERE uni_referencia ILIKE ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, filtro);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorReferencia-UnidadeDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param filtro
     * @return
     */
    protected List<Unidade> listaPorNomeFantasia(String filtro) {
        List<Unidade> lista = new ArrayList<>();
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_nomefantasia ILIKE ? "
                + "ORDER BY uni_nomefantasia ASC;";

        try {
            lista = getListaRegistro(sql, filtro);
        } catch (SQLException ex) {
            System.out.println("getListaPorNomeFantasia-UnidadeDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param filtro
     * @return
     */
    protected List<Unidade> listaPorReferencia(String filtro) {
        List<Unidade> lista = new ArrayList<>();
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_referencia ILIKE ? "
                + "ORDER BY uni_referencia ASC;";

        try {
            lista = getListaRegistro(sql, filtro);
        } catch (SQLException ex) {
            System.out.println("getListaPorReferencia-UnidadeDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param filtro
     * @return
     */
    protected List<Unidade> listaPorRazaoSocial(String filtro) {
        List<Unidade> lista = new ArrayList<>();
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_razaosocial ILIKE ? "
                + "ORDER BY uni_razaosocial ASC;";

        try {
            lista = getListaRegistro(sql, filtro);
        } catch (SQLException ex) {
            System.out.println("listaPorRazaoSocial-UnidadeDAO: " + ex.getMessage());
        }
        return lista;
    }
    
    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param filtro
     * @return
     */
    protected List<Unidade> listaPorCnpj(String filtro) {
        List<Unidade> lista = new ArrayList<>();
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_cnpj ILIKE ? "
                + "ORDER BY uni_cnpj ASC;";

        try {
            lista = getListaRegistro(sql, filtro);
        } catch (SQLException ex) {
            System.out.println("listaPorCnpj-UnidadeDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param filtro
     * @param limite
     * @return
     */
    protected List<Unidade> listaLimitada(String filtro, int limite) {
        List<Unidade> lista = new ArrayList<>();
        String sql = "SELECT uni_codigo, uni_razaosocial, uni_nomefantasia, uni_cnpj, "
                + "uni_telefonefixo, uni_telefonemovel, uni_email, uni_inscricaoestadual, uni_referencia, uni_senha, uni_ativo, uni_observacao "
                + "FROM unidade "
                + "WHERE uni_nomefantasia ILIKE ? "
                + "ORDER BY uni_nomefantasia ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, filtro);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-UnidadeDAO: " + ex.getMessage());
        }
        return lista;
    }

    /*
    Utilizado para retorno de lista de registro, pesquisado por string
     */
    private List<Unidade> getListaRegistro(String sql, String filtro) throws SQLException {
        List<Unidade> lista = new ArrayList<>();

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, filtro);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        }
        return lista;
    }

    /*
    Utilizado somente para navegação: primeiro, ultimo
     */
    private Unidade getRegistroNavegacao(String sql) throws SQLException {
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        }
        return null;
    }

    /*
    Utilizado somente para navegação: próximo, anterior, busca por código
     */
    private Unidade getRegistroNavegacao(String sql, int codigo) throws SQLException {
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        }
        return null;
    }

    /* Carregar objeto com resultado de resultset */
    private Unidade cargaObjeto(ResultSet rs) throws SQLException {
        return new Unidade(
                rs.getInt("uni_codigo"),
                rs.getString("uni_razaosocial"),
                rs.getString("uni_cnpj"),
                rs.getString("uni_inscricaoestadual"),
                rs.getString("uni_telefonefixo"),
                rs.getString("uni_telefonemovel"),
                rs.getString("uni_email"),
                rs.getString("uni_nomefantasia"),
                rs.getString("uni_referencia"),
                rs.getString("uni_senha"),
                rs.getBoolean("uni_ativo"),
                rs.getString("uni_observacao")
        );
    }
}
