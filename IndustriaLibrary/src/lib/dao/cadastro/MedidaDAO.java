package lib.dao.cadastro;

import lib.model.cadastro.Medida;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class MedidaDAO {

    public MedidaDAO() {
    }

    protected boolean gravar(Medida m) {
        String sql = "INSERT INTO medida (med_codigo, med_nome, med_sigla, med_ativo, med_observacao) "
                + "VALUES (default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, m.getNome());
                    ps.setString(2, m.getSigla());
                    ps.setBoolean(3, m.isAtivo());
                    ps.setString(4, m.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-MedidaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Medida m) {
        String sql = "UPDATE medida "
                + "SET med_nome = ?, med_sigla = ?, med_ativo = ?, med_observacao = ? "
                + "WHERE med_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, m.getNome());
                    ps.setString(2, m.getSigla());
                    ps.setBoolean(3, m.isAtivo());
                    ps.setString(4, m.getObservacao());
                    ps.setInt(5, m.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-MedidaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM medida "
                + "WHERE med_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-MedidaDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Medida ultimoRegistro() {
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "ORDER BY med_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Medida primeiroRegistro() {
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "ORDER BY med_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Medida proximoRegistro(int codigo) {
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "WHERE med_codigo > ? "
                + "ORDER BY med_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Medida anteriorRegistro(int codigo) {
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "WHERE med_codigo < ? "
                + "ORDER BY med_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

//------------------------------------------------------------------------------ TODOS
    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Medida registroPorCodigo(int codigo) {
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "WHERE med_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorCodigo-MedidaDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param sigla
     * @return
     */
    protected List<Medida> listaPorSigla(String sigla) {
        List<Medida> lista = new ArrayList<>();
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "WHERE med_sigla ILIKE ? "
                + "ORDER BY med_sigla ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorSigla-MedidaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<Medida> listaPorNome(String nome) {
        List<Medida> lista = new ArrayList<>();
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "WHERE med_nome ILIKE ? "
                + "ORDER BY med_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("Erro_getListaPorNome-MedidaDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param nome
     * @param limite
     * @return
     */
    protected List<Medida> listaLimitada(String nome, int limite) {
        List<Medida> lista = new ArrayList<>();
        String sql = "SELECT med_codigo, med_nome, med_sigla, med_ativo, med_observacao "
                + "FROM medida "
                + "WHERE med_nome ILIKE ? "
                + "ORDER BY med_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-MedidaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Medida cargaObjeto(ResultSet rs) throws SQLException {
        return new Medida(
                rs.getInt("med_codigo"),
                rs.getString("med_nome"),
                rs.getString("med_sigla"),
                rs.getBoolean("med_ativo"),
                rs.getString("med_observacao")
        );
    }
}
