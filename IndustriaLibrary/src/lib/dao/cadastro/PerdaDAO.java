package lib.dao.cadastro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lib.banco.Conexao;
import lib.model.cadastro.Perda;

/**
 *
 * @author abnerjp
 */
public abstract class PerdaDAO {

    protected boolean gravar(Perda p) {
        String sql = "INSERT INTO perda (" + todosCampos() + ") "
                + "VALUES "
                + "(default, ?, ?, ?, ?);";
        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, p.getNome());
                    ps.setString(2, p.getSigla());
                    ps.setBoolean(3, p.isAtivo());
                    ps.setString(4, p.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-PerdaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Perda p) {
        String sql = "UPDATE perda "
                + "SET per_nome = ?, per_sigla = ?, per_ativo = ?, per_observacao = ? "
                + "WHERE per_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, p.getNome());
                    ps.setString(2, p.getSigla());
                    ps.setBoolean(3, p.isAtivo());
                    ps.setString(4, p.getObservacao());
                    ps.setInt(5, p.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-PerdaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM perda "
                + "WHERE per_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("exlcuir-PerdaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected Perda ultimoRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "ORDER BY per_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("ultimoRegistro-PerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Perda primeiroRegistro() {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "ORDER BY per_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("primeiroRegistro-PerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Perda proximoRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "WHERE per_codigo > ? "
                + "ORDER BY per_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("proximoRegistro-PerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Perda anteriorRegistro(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "WHERE per_codigo < ? "
                + "ORDER BY per_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("anteriorRegistro-PerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Perda registroPorCodigo(int codigo) {
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "WHERE per_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }

                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCodigo-PerdaDAO: " + ex.getMessage());
        }
        return null;
    }

    protected List<Perda> listaPorSigla(String sigla) {
        List<Perda> lista = new ArrayList();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "WHERE per_sigla ILIKE ? "
                + "ORDER BY per_sigla ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorSigla-PerdaDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Perda> listaPorNome(String nome) {
        List<Perda> lista = new ArrayList();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "WHERE per_nome ILIKE ? "
                + "ORDER BY per_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaPorNome-PerdaDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Perda> listaLimitada(String nome, int limite) {
        List<Perda> lista = new ArrayList<>();
        String sql = "SELECT " + todosCampos() + " "
                + "FROM perda "
                + "WHERE per_nome ILIKE ? "
                + "ORDER BY per_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("listaLimitada-PerdaDAO: " + ex.getMessage());
        }
        return lista;
    }

    private String todosCampos() {
        return "per_codigo, per_nome, per_sigla, per_ativo, per_observacao";
    }

    private Perda cargaObjeto(ResultSet rs) throws SQLException {
        return new Perda(
                rs.getInt("per_codigo"),
                rs.getString("per_nome"),
                rs.getString("per_sigla"),
                rs.getBoolean("per_ativo"),
                rs.getString("per_observacao")
        );
    }
}
