package lib.dao.cadastro;

import lib.model.cadastro.Endereco;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class EnderecoDAO {

    public EnderecoDAO() {
    }

    protected boolean gravar(Endereco e) {
        String sql = "INSERT INTO endereco (end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo) "
                + "VALUES (default, ?, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, e.getLogradouro());
                    ps.setString(2, e.getCep());
                    ps.setString(3, e.getBairro());
                    ps.setString(4, e.getObservacao());
                    ps.setInt(5, e.getCodigoCidade());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-EnderecoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Endereco e) {
        String sql = "UPDATE endereco "
                + "SET end_logradouro = ?, end_cep = ?, end_bairro = ?, end_observacao = ?, cid_codigo = ? "
                + "WHERE end_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, e.getLogradouro());
                    ps.setString(2, e.getCep());
                    ps.setString(3, e.getBairro());
                    ps.setString(4, e.getObservacao());
                    ps.setInt(5, e.getCodigoCidade());
                    ps.setInt(6, e.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-EnderecoDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM endereco "
                + "WHERE end_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("excluir-EnderecoDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Endereco ultimoRegistro() {
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "ORDER BY end_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-EnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Endereco primeiroRegistro() {
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "ORDER BY end_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-EnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Endereco proximoRegistro(int codigo) {
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_codigo > ? "
                + "ORDER BY end_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-EnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Endereco anteriorRegistro(int codigo) {
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_codigo < ? "
                + "ORDER BY end_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-EnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Endereco registroPorCodigo(int codigo) {
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_codigo = ? "
                + "ORDER BY end_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-EnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    protected Endereco registroPorCep(String cep) {
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_cep ILIKE ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, cep);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("registroPorCep-EnderecoDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param endereco
     * @return
     */
    protected List<Endereco> listaPorEndereco(String endereco) {
        List<Endereco> lista = new ArrayList<>();
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_logradouro ILIKE ? "
                + "ORDER BY end_logradouro ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, endereco);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-EnderecoDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param cep
     * @return
     */
    protected List<Endereco> listaPorCep(String cep) {
        List<Endereco> lista = new ArrayList<>();
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_cep ILIKE ? "
                + "ORDER BY end_cep ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, cep);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorCep-EnderecoDAO: " + ex.getMessage());
        }
        return lista;
    }

    protected List<Endereco> listaLimitada(String logradouro, int limite) {
        List<Endereco> lista = new ArrayList<>();
        String sql = "SELECT end_codigo, end_logradouro, end_cep, end_bairro, end_observacao, cid_codigo "
                + "FROM endereco "
                + "WHERE end_logradouro ILIKE ? "
                + "ORDER BY end_cep ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, logradouro);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-EnderecoDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nomeCidade
     * @return
     */
    protected List<Endereco> listaPorCidade(String nomeCidade) {
        List<Endereco> lista = new ArrayList<>();
        String sql = "SELECT e.end_codigo, e.end_logradouro, e.end_cep, e.end_bairro, e.end_observacao, e.cid_codigo "
                + "FROM endereco as e "
                + "INNER JOIN cidade as cid on cid.cid_codigo = e.cid_codigo "
                + "WHERE cid.cid_nome ILIKE ? "
                + "ORDER BY cid.cid_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nomeCidade);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorCidade-CidadeDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Endereco cargaObjeto(ResultSet rs) throws SQLException {
        return new Endereco(
                rs.getInt("end_codigo"),
                rs.getString("end_logradouro"),
                rs.getString("end_cep"),
                rs.getString("end_bairro"),
                rs.getString("end_observacao"),
                rs.getInt("cid_codigo")
        );
    }
}
