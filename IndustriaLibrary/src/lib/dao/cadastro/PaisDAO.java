package lib.dao.cadastro;

import lib.model.cadastro.Pais;
import lib.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abnerjp
 */
public abstract class PaisDAO {

    public PaisDAO() {
    }

    protected boolean gravar(Pais p) {
        String sql = "INSERT INTO pais (pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao) "
                + "VALUES "
                + "(default, ?, ?, ?, ?);";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, p.getNome());
                    ps.setString(2, p.getSigla());
                    ps.setBoolean(3, p.isAtivo());
                    ps.setString(4, p.getObservacao());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("gravar-PaisDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean alterar(Pais p) {
        String sql = "UPDATE pais "
                + "SET pai_nome = ?, pai_sigla = ?, pai_ativo = ?, pai_observacao = ? "
                + "WHERE pai_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, p.getNome());
                    ps.setString(2, p.getSigla());
                    ps.setBoolean(3, p.isAtivo());
                    ps.setString(4, p.getObservacao());
                    ps.setInt(5, p.getCodigo());
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("alterar-MedidaDAO: " + ex.getMessage());
        }
        return false;
    }

    protected boolean excluir(int codigo) {
        String sql = "DELETE FROM pais "
                + "WHERE pai_codigo = ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    return ps.executeUpdate() == 1;
                }
            }
        } catch (SQLException ex) {
            System.out.println("exlcuir-MedidaDAO: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Retorna o último registro da tabela
     *
     * @return
     */
    protected Pais ultimoRegistro() {
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "ORDER BY pai_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getUltimoRegistro-PaisDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o primeiro registro da tabela
     *
     * @return
     */
    protected Pais primeiroRegistro() {
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "ORDER BY pai_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (Statement st = conn.createStatement()) {
                    try (ResultSet rs = st.executeQuery(sql)) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getPrimeiroRegistro-PaisDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o próximo registro da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Pais proximoRegistro(int codigo) {
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "WHERE pai_codigo > ? "
                + "ORDER BY pai_codigo ASC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getProximoRegistro-PaisDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna o registro anterior da tabela, a partir do código informado
     *
     * @param codigo
     * @return
     */
    protected Pais anteriorRegistro(int codigo) {
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "WHERE pai_codigo < ? "
                + "ORDER BY pai_codigo DESC "
                + "LIMIT 1;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getAnteriorRegistro-PaisDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna um registro de acordo com o codigo informado
     *
     * @param codigo
     * @return
     */
    protected Pais registroPorCodigo(int codigo) {
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "WHERE pai_codigo = ? "
                + "ORDER BY pai_codigo ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, codigo);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return cargaObjeto(rs);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getRegistroPorCodigo-PaisDAO: " + ex.getMessage());
        }
        return null;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param sigla
     * @return
     */
    protected List<Pais> listaPorSigla(String sigla) {
        List<Pais> lista = new ArrayList<>();
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "WHERE pai_sigla ILIKE ? "
                + "ORDER BY pai_sigla ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, sigla);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorSigla-PaisDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado
     *
     * @param nome
     * @return
     */
    protected List<Pais> listaPorNome(String nome) {
        List<Pais> lista = new ArrayList<>();
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "WHERE pai_nome ILIKE ? "
                + "ORDER BY pai_nome ASC;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaPorNome-PaisDAO: " + ex.getMessage());
        }
        return lista;
    }

    /**
     * Retorna uma lista de registro de acordo com o filtro informado, e com
     * resultado limitado a quantidade informada
     *
     * @param nome
     * @param limite
     * @return
     */
    protected List<Pais> listaLimitada(String nome, int limite) {
        List<Pais> lista = new ArrayList<>();
        String sql = "SELECT pai_codigo, pai_nome, pai_sigla, pai_ativo, pai_observacao "
                + "FROM pais "
                + "WHERE pai_nome ILIKE ? "
                + "ORDER BY pai_nome ASC "
                + "LIMIT ?;";

        try (Connection conn = Conexao.abre()) {
            if (conn != null) {
                try (PreparedStatement ps = conn.prepareStatement(sql)) {
                    ps.setString(1, nome);
                    ps.setInt(2, limite);
                    try (ResultSet rs = ps.executeQuery()) {
                        while (rs.next()) {
                            lista.add(cargaObjeto(rs));
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("getListaLimitada-PaisDAO: " + ex.getMessage());
        }
        return lista;
    }

    private Pais cargaObjeto(ResultSet rs) throws SQLException {
        return new Pais(
                rs.getInt("pai_codigo"),
                rs.getString("pai_nome"),
                rs.getString("pai_sigla"),
                rs.getBoolean("pai_ativo"),
                rs.getString("pai_observacao")
        );
    }
}
