package lib.model.associacao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lib.dao.associacao.UnidadeMotivoComTipoParadaDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeMotivoComTipoParada extends UnidadeMotivoComTipoParadaDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidadeTipoParada;
    private int codUnidadeMotivoParada;

    public UnidadeMotivoComTipoParada(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidadeTipoParada, int codUnidadeMotivoParada) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidadeTipoParada = codUnidadeTipoParada;
        this.codUnidadeMotivoParada = codUnidadeMotivoParada;
    }

    public UnidadeMotivoComTipoParada(boolean ativo, LocalDate data, String observacao, int codUnidadeTipoParada, int codUnidadeMotivoParada) {
        this(0, ativo, data, observacao, codUnidadeTipoParada, codUnidadeMotivoParada);
    }

    public UnidadeMotivoComTipoParada(int codUnidadeTipoParada) {
        this(false, LocalDate.now(), "", codUnidadeTipoParada, 0);
    }

    public UnidadeMotivoComTipoParada() {
        this(false, LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidadeTipoParada() {
        return codUnidadeTipoParada;
    }

    public void setCodUnidadeTipoParada(int codUnidadeTipoParada) {
        this.codUnidadeTipoParada = codUnidadeTipoParada;
    }

    public int getCodUnidadeMotivoParada() {
        return codUnidadeMotivoParada;
    }

    public void setCodUnidadeMotivoParada(int codUnidadeMotivoParada) {
        this.codUnidadeMotivoParada = codUnidadeMotivoParada;
    }

    /* acesso ao banco de dados */
    private int validarAtributosGravar(UnidadeMotivoComTipoParada obj) {
        if (obj.getCodUnidadeTipoParada() <= 0) {
            return 1603;
        }

        if (obj.getCodUnidadeMotivoParada() <= 0) {
            return 1604;
        }

        int result = validarAtributos(obj);
        if (result > 0) {
            return result;
        }

        if (isDuplicado(obj)) {
            return 1608;
        }

        return 0;
    }

    private int validarAtributos(UnidadeMotivoComTipoParada obj) {
        if (obj.getData() == null || obj.getData().compareTo(LocalDate.now()) > 0) {
            return 1505;
        }

        if (obj.getObservacao() != null && obj.getObservacao().length() > 200) {
            return 1506;
        }

        return 0;
    }

    public int gravar() {
        int result = validarAtributosGravar(this);
        if (result > 0) {
            return result;
        }

        if (!super.gravar(this)) {
            result = 1;
        }

        return result;
    }

    public int gravar(List<UnidadeMotivoComTipoParada> listaVinculo) {
        int result = 0;
        List<UnidadeMotivoComTipoParada> lista = new ArrayList<>();
        for (UnidadeMotivoComTipoParada obj : listaVinculo) {
            result = validarAtributosGravar(obj);
            if (result == 0) {
                lista.add(obj);
            }
        }

        if (!super.gravarLista(lista)) {
            result = 1;
        }

        return result;
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos(this);
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (codigo > 0) {
            if (super.excluir(codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeMotivoComTipoParada getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public UnidadeMotivoComTipoParada getRegPorTipoParadaEMotivoParada(int idUnidadeTipoParada, int idUnidadeMotivoParada) {
        if (codUnidadeTipoParada > 0 && codUnidadeMotivoParada > 0) {
            return super.regPorTipoParadaEMotivoParada(idUnidadeTipoParada, idUnidadeTipoParada);
        }
        return null;
    }

    public boolean isDuplicado(UnidadeMotivoComTipoParada obj) {
        try {
            return getRegPorTipoParadaEMotivoParada(obj.getCodUnidadeTipoParada(), obj.getCodUnidadeMotivoParada()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public List<UnidadeMotivoComTipoParada> getMotivosParadaPorTipoParada() {
        return super.getMotivosParadasPorTipoParada(codUnidadeTipoParada);
    }

    public List<UnidadeMotivoComTipoParada> getMotivosETiposPorUnidade(int codUnidade) {
        return super.getMotivosTiposPorUnidade(codUnidade);
    }
}
