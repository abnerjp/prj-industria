package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadeAreaDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeArea extends UnidadeAreaDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidade;
    private int codArea;

    public UnidadeArea(int codigo, boolean ativo, LocalDate data, String observação, int codUnidade, int codArea) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observação;
        this.codArea = codArea;
        this.codUnidade = codUnidade;
    }

    public UnidadeArea(boolean ativo, LocalDate data, String observação, int codUnidade, int codArea) {
        this(0, ativo, data, observação, codUnidade, codArea);
    }

    public UnidadeArea(int codUnidade) {
        this(false, LocalDate.now(), "", codUnidade, 0);
    }

    public UnidadeArea() {
        this(false, LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUndiade(int codUndiade) {
        this.codUnidade = codUndiade;
    }

    public int getCodArea() {
        return codArea;
    }

    public void setCodArea(int codArea) {
        this.codArea = codArea;
    }

    private int validarAtributos() {
        if (this.data == null || this.data.compareTo(LocalDate.now()) > 0) {
            return 2305;
        }
        if (this.observacao != null && this.observacao.length() > 200) {
            return 2306;
        }
        return 0;
    }

    public int gravar() {
        if (this.codUnidade <= 0) {
            return 2303;
        }

        if (this.codArea <= 0) {
            return 2304;
        }

        if (isDuplicado(this)) {
            return 2308;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        }
        return 1;
    }

    public int alterar() {
        if (this.codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        }
        return 1;
    }

    public int excluir() {
        if (this.codigo > 0) {
            if (super.excluir(this.codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeArea getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public boolean isDuplicado(UnidadeArea obj) {
        try {
            return getPorUnidadeEArea(obj.getCodUnidade(), obj.getCodArea()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public UnidadeArea getPorUnidadeEArea(int idUnidade, int idArea) {
        if (idUnidade > 0 && idArea > 0) {
            return super.regPorunidadeEArea(idUnidade, idArea);
        }
        return null;
    }

    public List<UnidadeArea> getAreasPorUnidade() {
        return super.areasPorUnidade(this.codUnidade);
    }
}
