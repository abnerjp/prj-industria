package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UsuarioEnderecoDAO;

/**
 *
 * @author abnerjp
 */
public class UsuarioEndereco {

    private int numero;
    private LocalDate data;
    private String complemento;
    private String observacao;
    private int codUsuario;
    private int codEndereco;

    public UsuarioEndereco(int numero, LocalDate data, String complemento, String observacao, int codUsuario, int codEndereco) {
        this.numero = numero;
        this.data = data;
        this.complemento = complemento;
        this.observacao = observacao;
        this.codUsuario = codUsuario;
        this.codEndereco = codEndereco;
    }

    public UsuarioEndereco() {
        this(0, LocalDate.now(), "", "", 0, 0);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }

    public int getCodEndereco() {
        return codEndereco;
    }

    public void setCodEndereco(int codEndereco) {
        this.codEndereco = codEndereco;
    }

    /* acesso ao banco de dados */
    public int gravar() {
        if (codUsuario <= 0) {
            return 2103;
        }
        if (codEndereco <= 0) {
            return 2104;
        }
        if (numero < 0) {
            return 2105;
        }
        if (complemento != null && complemento.length() > 30) {
            return 2106;
        }
        if (observacao != null && observacao.length() > 200) {
            return 2107;
        }
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 2108;
        }
        if (new UsuarioEnderecoDAO().gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (codUsuario > 0 && codEndereco > 0 && data != null) {
            if (new UsuarioEnderecoDAO().excluir(codUsuario, codEndereco, data)) {
                return 0;
            }
        }
        return 1;
    }

    public UsuarioEndereco getEnderecoAtual(int idUsuario) {
        return new UsuarioEnderecoDAO().regAtual(idUsuario);
    }

    public List<UsuarioEndereco> getTodosEnderecos(int idUsuario) {
        return new UsuarioEnderecoDAO().enderecosUsuario(idUsuario);
    }
}
