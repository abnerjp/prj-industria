package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadeEquipamentoDAO;

/**
 *
 * @author abner.jacomo
 */
public class UnidadeEquipamento extends UnidadeEquipamentoDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidade;
    private int codEquipamento;

    public UnidadeEquipamento(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidade, int codEquipamento) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidade = codUnidade;
        this.codEquipamento = codEquipamento;
    }

    public UnidadeEquipamento(boolean ativo, LocalDate data, String observacao, int codUnidade, int codEquipamento) {
        this(0, ativo, data, observacao, codUnidade, codEquipamento);
    }

    public UnidadeEquipamento(int codUnidade) {
        this(false, LocalDate.now(), "", codUnidade, 0);
    }

    public UnidadeEquipamento() {
        this(0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUnidade(int codUnidade) {
        this.codUnidade = codUnidade;
    }

    public int getCodEquipamento() {
        return codEquipamento;
    }

    public void setCodEquipamento(int codEquipamento) {
        this.codEquipamento = codEquipamento;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar data
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 2205;
        }

        if (observacao != null && observacao.length() > 200) {
            return 2206;
        }
        return 0;
    }

    public int gravar() {
        //validar unidade
        if (codUnidade <= 0) {
            return 2203;
        }

        //validar equipamento
        if (codEquipamento <= 0) {
            return 2204;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //verificar se vínculo ja existe
        if (isDuplicado(this)) {
            return 2208;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (this.codigo > 0) {
            if (super.excluir(this.codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeEquipamento getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public UnidadeEquipamento getRegPorUnidadeEEquipamento(int idUnidade, int idEquipamento) {
        if (idUnidade > 0 && idEquipamento > 0) {
            return super.regPorUnidadeEEquipamento(idUnidade, idEquipamento);
        }
        return null;
    }

    public boolean isDuplicado(UnidadeEquipamento obj) {
        try {
            return getRegPorUnidadeEEquipamento(obj.getCodUnidade(), obj.getCodEquipamento()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public List<UnidadeEquipamento> getEquipamentosPorUnidade() {
        return super.equipamentosPorUnidade(this.codUnidade);
    }

}
