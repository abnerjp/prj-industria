package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadeTipoParadaDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeTipoParada extends UnidadeTipoParadaDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidade;
    private int codTipoParada;

    public UnidadeTipoParada(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidade, int codTipoParada) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidade = codUnidade;
        this.codTipoParada = codTipoParada;
    }

    public UnidadeTipoParada(boolean ativo, LocalDate data, String observacao, int codUnidade, int codTipoParada) {
        this(0, ativo, data, observacao, codUnidade, codTipoParada);
    }

    public UnidadeTipoParada(int codUnidade) {
        this(false, LocalDate.now(), "", codUnidade, 0);
    }

    public UnidadeTipoParada() {
        this(false, LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUnidade(int codUnidade) {
        this.codUnidade = codUnidade;
    }

    public int getCodTipoParada() {
        return codTipoParada;
    }

    public void setCodTipoParada(int codTipoParada) {
        this.codTipoParada = codTipoParada;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar data
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 1205;
        }

        //validar observacao
        if (observacao != null && observacao.length() > 200) {
            return 1206;
        }

        //não houve erros
        return 0;
    }

    public int gravar() {
        if (codUnidade <= 0) {
            return 1203;
        }

        //validar tipo parada
        if (codTipoParada <= 0) {
            return 1204;
        }

        if (isDuplicado()) {
            return 1208;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (codigo > 0) {
            if (super.excluir(codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeTipoParada getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public UnidadeTipoParada getPorUnidadeETipoParada(int idUnidade, int idTipoParada) {
        if (idUnidade > 0 && idTipoParada > 0) {
            return super.regPorUnidadeETipoParada(idUnidade, idTipoParada);
        }
        return null;
    }

    public boolean isDuplicado() {
        return getPorUnidadeETipoParada(codUnidade, codTipoParada) != null;
    }

    public List<UnidadeTipoParada> getTiposParadaPorUnidade() {
        return super.tiposParadaPorUnidade(codUnidade);
    }
}
