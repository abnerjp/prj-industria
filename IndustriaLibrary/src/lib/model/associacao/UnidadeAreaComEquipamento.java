package lib.model.associacao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lib.dao.associacao.UnidadeAreaComEquipamentoDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeAreaComEquipamento extends UnidadeAreaComEquipamentoDAO {

    private int codigo;
    private boolean ativo;
    private String referencia;
    private LocalDate data;
    private String observacao;
    private int codUnidadeArea;
    private int codUnidadeEquipamento;

    public UnidadeAreaComEquipamento(int codigo, boolean ativo, String referencia, LocalDate data, String observacao, int codUnidadeArea, int codUnidadeEquipamento) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.referencia = referencia;
        this.data = data;
        this.observacao = observacao;
        this.codUnidadeArea = codUnidadeArea;
        this.codUnidadeEquipamento = codUnidadeEquipamento;
    }

    public UnidadeAreaComEquipamento(boolean ativo, String referencia, LocalDate data, String observacao, int codUnidadeArea, int codUnidadeEquipamento) {
        this(0, ativo, referencia, data, observacao, codUnidadeArea, codUnidadeEquipamento);
    }

    public UnidadeAreaComEquipamento(int codUnidadeArea) {
        this(false, "", LocalDate.now(), "", codUnidadeArea, 0);
    }

    public UnidadeAreaComEquipamento() {
        this(false, "", LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidadeArea() {
        return codUnidadeArea;
    }

    public void setCodUnidadeArea(int codUnidadeArea) {
        this.codUnidadeArea = codUnidadeArea;
    }

    public int getCodUnidadeEquipamento() {
        return codUnidadeEquipamento;
    }

    public void setCodUnidadeEquipamento(int codUnidadeEquipamento) {
        this.codUnidadeEquipamento = codUnidadeEquipamento;
    }

    /* acesso ao banco de dados */
    private int validarAtributos(UnidadeAreaComEquipamento obj) {
        if (obj.getData() == null || obj.getData().compareTo(LocalDate.now()) > 0) {
            return 2406;
        }

        if (obj.getObservacao() != null && obj.getObservacao().length() > 200) {
            return 2407;
        }
        return 0;
    }

    private int validarAtributosGravar(UnidadeAreaComEquipamento obj) {
        if (obj.getCodUnidadeArea() <= 0) {
            return 2403;
        }

        if (obj.getCodUnidadeEquipamento() <= 0) {
            return 2404;
        }

        if (obj.getReferencia() == null || obj.getReferencia().length() > 6) {
            return 2405;
        }

        int result = validarAtributos(obj);
        if (result > 0) {
            return result;
        }

        if (isDuplicado(obj)) {
            return 2409;
        }

        return 0;
    }

    public int gravar() {
        int result = validarAtributosGravar(this);
        if (result > 0) {
            return result;
        }

        if (!super.gravar(this)) {
            result = 1;
        }

        return result;
    }

    public int gravar(List<UnidadeAreaComEquipamento> listaVinculo) {
        int result = 0;
        boolean houvePeloMenosUmErro = false;

        List<UnidadeAreaComEquipamento> lista = new ArrayList<>();
        for (UnidadeAreaComEquipamento obj : listaVinculo) {
            result = validarAtributosGravar(obj);
            if (result == 0) {
                lista.add(obj);
            } else {
                houvePeloMenosUmErro = true;
            }
        }

        if (!super.gravarLista(lista)) {
            result = 1;
        }
        
        if (houvePeloMenosUmErro) {
            result = 2410;
        }

        return result;
    }

    public int alterar() {
        if (this.codigo <= 0) {
            return 2;
        }

        int result = validarAtributos(this);
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (this.codigo > 0) {
            if (super.excluir(this.codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeAreaComEquipamento getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public UnidadeAreaComEquipamento getRegistro(int idUnidadeArea, int idUnidadeEquipamento, String referencia) {
        if (idUnidadeArea > 0 && idUnidadeEquipamento > 0) {
            return super.getRegistroUnico(idUnidadeArea, idUnidadeEquipamento, referencia);
        }
        return null;
    }

    public boolean isDuplicado(UnidadeAreaComEquipamento obj) {
        try {
            return getRegistro(obj.getCodUnidadeArea(), obj.getCodUnidadeEquipamento(), obj.getReferencia()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public List<UnidadeAreaComEquipamento> getEquipamentosPorArea() {
        return super.getEquipamentosPorArea(this.codUnidadeArea);
    }

    public List<UnidadeAreaComEquipamento> getEquipamentosEAreasPorUnidade(int codUnidade) {
        return super.getEquipamentosPorUnidade(codUnidade);
    }
}
