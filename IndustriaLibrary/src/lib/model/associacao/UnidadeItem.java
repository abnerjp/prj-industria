package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadeItemDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeItem extends UnidadeItemDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidade;
    private int codItem;

    public UnidadeItem(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidade, int codItem) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidade = codUnidade;
        this.codItem = codItem;
    }

    public UnidadeItem(boolean ativo, LocalDate data, String observacao, int codUnidade, int codItem) {
        this(0, ativo, data, observacao, codUnidade, codItem);
    }

    public UnidadeItem(int codUnidade) {
        this(false, LocalDate.now(), "", codUnidade, 0);
    }

    public UnidadeItem() {
        this(0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUnidade(int codUnidade) {
        this.codUnidade = codUnidade;
    }

    public int getCodItem() {
        return codItem;
    }

    public void setCodItem(int codItem) {
        this.codItem = codItem;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar data
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 1305;
        }

        //validar observacao
        if (observacao != null && observacao.length() > 200) {
            return 1306;
        }

        //não houve erros
        return 0;
    }

    public int gravar() {
        //validar unidade
        if (codUnidade <= 0) {
            return 1303;
        }

        //validar item
        if (codItem <= 0) {
            return 1304;
        }

        //validar se vinculo já existe
        if (isDuplicado(this)) {
            return 1308;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (codigo > 0) {
            if (super.excluir(codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeItem getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public UnidadeItem getRegPorUnidadeEItem(int idUnidade, int idItem) {
        if (idUnidade > 0 && idItem > 0) {
            return super.regPorUnidadeEItem(idUnidade, idItem);
        }
        return null;
    }

    public boolean isDuplicado(UnidadeItem obj) {
        try {
            return getRegPorUnidadeEItem(obj.getCodUnidade(), obj.getCodItem()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public List<UnidadeItem> getItensPorUnidade() {
        return super.itensPorUnidade(this.codUnidade);
    }
}
