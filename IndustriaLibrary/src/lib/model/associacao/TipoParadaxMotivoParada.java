package lib.model.associacao;

import java.time.LocalDate;

/**
 *
 * @author abnerjp
 */
public class TipoParadaxMotivoParada {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidadeTipoParada;
    private int codUnidadeMotivoParada;

    public TipoParadaxMotivoParada(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidadeTipoParada, int codUnidadeMotivoParada) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidadeTipoParada = codUnidadeTipoParada;
        this.codUnidadeMotivoParada = codUnidadeMotivoParada;
    }

    public TipoParadaxMotivoParada(boolean ativo, LocalDate data, String observacao, int codUnidadeTipoParada, int codUnidadeMotivoParada) {
        this(0, ativo, data, observacao, codUnidadeTipoParada, codUnidadeMotivoParada);
    }

    public TipoParadaxMotivoParada() {
        this(false, LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidadeTipoParada() {
        return codUnidadeTipoParada;
    }

    public void setCodUnidadeTipoParada(int codUnidadeTipoParada) {
        this.codUnidadeTipoParada = codUnidadeTipoParada;
    }

    public int getCodUnidadeMotivoParada() {
        return codUnidadeMotivoParada;
    }

    public void setCodUnidadeMotivoParada(int codUnidadeMotivoParada) {
        this.codUnidadeMotivoParada = codUnidadeMotivoParada;
    }
    
    
    private int validarAtributos() {
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 1605;
        }
        
        if (observacao != null && observacao.length() > 200) {
            return 1606;
        }
        
        return 0;
    }
    
    public int gravar() {
        if (codUnidadeTipoParada <= 0) {
            return 1603;
        }
        
        if (codUnidadeMotivoParada <= 0) {
            return 1604;
        }
        return 0;
    }
    
    public boolean isDuplicado(TipoParadaxMotivoParada obj) {
       return true; 
    }
    
}
