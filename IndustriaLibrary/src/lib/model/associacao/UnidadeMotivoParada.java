package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadeMotivoParadaDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeMotivoParada extends UnidadeMotivoParadaDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidade;
    private int codMotivoParada;

    public UnidadeMotivoParada(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidade, int codMotivoParada) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidade = codUnidade;
        this.codMotivoParada = codMotivoParada;
    }

    public UnidadeMotivoParada(boolean ativo, LocalDate data, String observacao, int codUnidade, int codMotivoParada) {
        this(0, ativo, data, observacao, codUnidade, codMotivoParada);
    }

    public UnidadeMotivoParada(int codUnidade) {
        this(false, LocalDate.now(), "", codUnidade, 0);
    }

    public UnidadeMotivoParada() {
        this(false, LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUnidade(int codUnidade) {
        this.codUnidade = codUnidade;
    }

    public int getCodMotivoParada() {
        return codMotivoParada;
    }

    public void setCodMotivoParada(int codMotivoParada) {
        this.codMotivoParada = codMotivoParada;
    }

    private int validarAtributos() {
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 1505;
        }
        if (observacao != null && observacao.length() > 200) {
            return 1506;
        }
        return 0;
    }

    public int gravar() {
        if (codUnidade <= 0) {
            return 1503;
        }

        if (codMotivoParada <= 0) {
            return 1504;
        }

        if (isDuplicado(this)) {
            return 1508;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (codigo > 0) {
            if (super.excluir(codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeMotivoParada getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public boolean isDuplicado(UnidadeMotivoParada obj) {
        try {
            return getPorUnidadeEMotivoParada(obj.getCodUnidade(), obj.getCodMotivoParada()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public UnidadeMotivoParada getPorUnidadeEMotivoParada(int idUnidade, int idMotivoParada) {
        if (idUnidade > 0 && idMotivoParada > 0) {
            return super.regPorUnidadeEMotivoParada(idUnidade, idMotivoParada);
        }
        return null;
    }

    public List<UnidadeMotivoParada> getMotivosParadaPorUnidade() {
        return super.motivosParadaPorUnidade(codUnidade);
    }
}
