package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadeEnderecoDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadeEndereco extends UnidadeEnderecoDAO {

    private int numero;
    private LocalDate data;
    private String complemento;
    private String observacao;
    private int codUnidade;
    private int codEndereco;

    public UnidadeEndereco(int numero, LocalDate data, String complemento, String observacao, int codUnidade, int codEndereco) {
        this.numero = numero;
        this.data = data;
        this.complemento = complemento;
        this.observacao = observacao;
        this.codUnidade = codUnidade;
        this.codEndereco = codEndereco;
    }

    public UnidadeEndereco() {
        this(0, LocalDate.now(), "", "", 0, 0);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getObservacao() {
        return observacao == null ? "" : observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUnidade(int codUnidade) {
        this.codUnidade = codUnidade;
    }

    public int getCodEndereco() {
        return codEndereco;
    }

    public void setCodEndereco(int codEndereco) {
        this.codEndereco = codEndereco;
    }

    /* acesso ao banco de dados */
    public int gravar() {
        // validar unidade
        if (codUnidade <= 0) {
            return 1003;
        }

        // validar endereço
        if (codEndereco <= 0) {
            return 1004;
        }

        //validar número do endereço
        if (numero < 0) {
            return 1005;
        }

        // validar complemento
        if (complemento != null && complemento.length() > 30) {
            return 1006;
        }

        // validar observação
        if (observacao != null && observacao.length() > 200) {
            return 1007;
        }

        // validar data
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 1008;
        }

        // validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (codUnidade > 0 && codEndereco > 0 && data != null) {
            if (super.excluir(codUnidade, codEndereco, data)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadeEndereco getEnderecoAtual(int idUnidade) {
        return super.regAtual(idUnidade);
    }

    public List<UnidadeEndereco> getTodosEnderecos(int idUnidade) {
        return super.enderecosUnidade(idUnidade);
    }
}
