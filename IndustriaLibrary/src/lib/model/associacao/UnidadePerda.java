package lib.model.associacao;

import java.time.LocalDate;
import java.util.List;
import lib.dao.associacao.UnidadePerdaDAO;

/**
 *
 * @author abnerjp
 */
public class UnidadePerda extends UnidadePerdaDAO {

    private int codigo;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private int codUnidade;
    private int codPerda;

    public UnidadePerda(int codigo, boolean ativo, LocalDate data, String observacao, int codUnidade, int codPerda) {
        this.codigo = codigo;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.codUnidade = codUnidade;
        this.codPerda = codPerda;
    }

    public UnidadePerda(boolean ativo, LocalDate data, String observacao, int codUnidade, int codPerda) {
        this(0, ativo, data, observacao, codUnidade, codPerda);
    }

    public UnidadePerda(int codUnidade) {
        this(false, LocalDate.now(), "", codUnidade, 0);
    }

    public UnidadePerda() {
        this(false, LocalDate.now(), "", 0, 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodUnidade() {
        return codUnidade;
    }

    public void setCodUnidade(int codUnidade) {
        this.codUnidade = codUnidade;
    }

    public int getCodPerda() {
        return codPerda;
    }

    public void setCodPerda(int codPerda) {
        this.codPerda = codPerda;
    }

    private int validarAtributos() {
        if (data == null || data.compareTo(LocalDate.now()) > 0) {
            return 1905;
        }
        if (observacao != null && observacao.length() > 200) {
            return 1906;
        }
        return 0;
    }

    public int gravar() {
        if (codUnidade <= 0) {
            return 1903;
        }

        if (codPerda <= 0) {
            return 1904;
        }

        if (isDuplicado(this)) {
            return 1908;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        }
        return 1;
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        }
        return 1;
    }

    public int excluir() {
        if (codigo > 0) {
            if (super.excluir(codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public UnidadePerda getRegPorCodigo(int id) {
        if (id > 0) {
            return super.regPorCodigo(id);
        }
        return null;
    }

    public boolean isDuplicado(UnidadePerda obj) {
        try {
            return getPorUnidadeEPerda(obj.getCodUnidade(), obj.getCodPerda()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public UnidadePerda getPorUnidadeEPerda(int idUnidade, int idPerda) {
        if (idUnidade > 0 && idPerda > 0) {
            return super.regPorUnidadeEPerda(idUnidade, idPerda);
        }
        return null;
    }

    public List<UnidadePerda> getPerdasPorUnidade() {
        return super.perdasPorUnidade(codUnidade);
    }
}
