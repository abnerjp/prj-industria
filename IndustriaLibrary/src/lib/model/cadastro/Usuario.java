package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.UsuarioDAO;
import lib.util.Seguranca;

/**
 *
 * @author abnerjp
 */
public class Usuario extends UsuarioDAO {

    private int codigo;
    private String nome;
    private String sexo;
    private String login;
    private String senha;
    private String email;
    private String imagem;
    private boolean ativo;
    private String observacao;

    public Usuario(int codigo, String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sexo = sexo;
        this.login = login;
        this.senha = senha;
        this.email = email;
        this.imagem = imagem;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Usuario(String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao) {
        this(0, nome, sexo, login, senha, email, imagem, ativo, observacao);
    }

    public Usuario(int codigo) {
        this(codigo, "", "", "", "", "", "", false, "");
    }

    public Usuario() {
        this("", "", "", "", "", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    private int validarAtributos() {
        if (nome == null || nome.length() < 2) {
            return 2003;
        }
        if (nome.length() > 50) {
            return 2004;
        }
        if (sexo == null || sexo.isEmpty()) {
            return 2005;
        }
        if (sexo.length() > 1) {
            return 2006;
        }
        if (email != null && email.length() > 0 && (!email.contains("@") || email.length() < 3 || email.length() > 70)) {
            return 2007;
        }
        if (login == null || login.length() < 2) {
            return 2008;
        }
        if (login.length() > 20) {
            return 2009;
        }
        if (senha == null || senha.length() < 4) {
            return 2011;
        }
        if (senha.length() > 20) {
            return 2012;
        }
        if (observacao == null || observacao.length() > 200) {
            return 2013;
        }
        return 0;
    }

    public int gravar() {
        if (codigo != 0) {
            return 2;
        }
        int result = validarAtributos();
        if (result > 0) {
            return result;
        }
        if (isDuplicado(this)) {
            return 2010;
        }
        senha = criptografarStr(senha);
        
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }
        int result = validarAtributos();
        if (result > 0) {
            return result;
        }
        senha = criptografarStr(senha);
        
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterarDados() {
        if (codigo <= 0) {
            return 2;
        }
        int result = validarAtributos();
        if (result > 0) {
            return result;
        }
        senha = criptografarStr(senha);
        
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    private String criptografarStr(String str) {
        try {
            return Seguranca.criptografar(str);
        } catch (Exception ex) {
            return "";
        }

    }

    public int excluir() {
        if (codigo > 0) {
            if (super.excluir(codigo)) {
                return 0;
            }
        }
        return 1;
    }

    public Usuario getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Usuario getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Usuario getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Usuario getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Usuario getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Usuario> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Usuario> getListaPorLogin(String filtro) {
        return super.listaPorLogin(filtro);
    }
    
    public List<Usuario> getListaPorEmail(String filtro) {
        return super.listaPorEmail(filtro);
    }

    public List<Usuario> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }

    public boolean isDuplicado(Usuario obj) {
        try {
            return getPorLogin(obj.getLogin()) != null;
        } catch (NullPointerException ex) {
            return false;
        }
    }

    public Usuario getPorLogin(String filtro) {
        return super.registroPorLogin(filtro);
    }
}
