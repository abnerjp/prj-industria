package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.SubmotivoDAO;

/**
 *
 * @author abnerjp
 */
public class Submotivo extends SubmotivoDAO {

    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public Submotivo(int codigo, String nome, String sigla, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Submotivo(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);

    }

    public Submotivo(int codigo) {
        this(codigo, "", "", false, "");
    }

    public Submotivo() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        if (nome == null || nome.length() < 2) {
            return 403;
        }
        if (nome.length() > 50) {
            return 404;
        }
        if (sigla == null || sigla.isEmpty()) {
            return 405;
        }
        if (sigla.length() > 6) {
            return 406;
        }
        if (observacao == null || observacao.length() > 200) {
            return 407;
        }
        return 0;
    }

    public int gravar() {
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Submotivo getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Submotivo getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Submotivo getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Submotivo getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Submotivo getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Submotivo> getListaPorSigla(String filtro) {
        return super.listaPorSigla(filtro);
    }

    public List<Submotivo> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Submotivo> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
