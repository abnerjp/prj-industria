package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.AreaDAO;

/**
 *
 * @author abnerjp
 */
public class Area extends AreaDAO {

    private int codigo;
    private String nome;
    private String descricao;
    private boolean ativo;
    private String observacao;

    public Area(int codigo, String nome, String descricao, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Area(String nome, String descricao, boolean ativo, String observacao) {
        this(0, nome, descricao, ativo, observacao);
    }

    public Area(int codigo) {
        this(codigo, "", "", false, "");
    }
    
    public Area() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 1103;
        }
        if (nome.length() > 50) {
            return 1104;
        }

        //validar descricao
        if (descricao != null && descricao.length() > 100) {
            return 1105;
        }

        //validar observação
        if (observacao == null || observacao.length() > 200) {
            return 1106;
        }

        //não houve erro
        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Area getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Area getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Area getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Area getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }
    
    public Area getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Area> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Area> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
