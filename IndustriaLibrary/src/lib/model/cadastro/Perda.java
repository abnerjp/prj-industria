package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.PerdaDAO;

/**
 *
 * @author abnerjp
 */
public class Perda extends PerdaDAO {

    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public Perda(int codigo, String nome, String sigla, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Perda(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);

    }

    public Perda(int codigo) {
        this(codigo, "", "", false, "");
    }

    public Perda() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    private int validarAtributos() {
        if (nome == null || nome.length() < 2) {
            return 1703;
        }
        if (nome.length() > 50) {
            return 1704;
        }
        if (sigla == null || sigla.isEmpty()) {
            return 1705;
        }
        if (sigla.length() > 6) {
            return 1706;
        }
        if (observacao == null || observacao.length() > 200) {
            return 1707;
        }
        return 0;
    }

    public int gravar() {
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Perda getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Perda getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Perda getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Perda getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Perda getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Perda> getListaPorSigla(String filtro) {
        return super.listaPorSigla(filtro);
    }

    public List<Perda> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Perda> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
