package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.MedidaDAO;

/**
 *
 * @author abnerjp
 */
public class Medida extends MedidaDAO{

    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public Medida(int codigo, String nome, String sigla, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.observacao = observacao;
        this.ativo = ativo;
    }

    public Medida(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);
    }

    public Medida(int codigo) {
        this(codigo, "", "", false, "");
    }

    public Medida() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public String toString() {
        return nome;
    }

    public Object getDataparada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 3;
        }
        if (nome.length() > 30) {
            return 4;
        }

        //validar sigla
        if (sigla == null || sigla.isEmpty()) {
            return 5;
        }
        if (sigla.length() > 6) {
            return 6;
        }

        //observação
        if (observacao == null || observacao.length() > 200) {
            return 7;
        }

        //não houve erro
        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Medida getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Medida getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Medida getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Medida getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Medida getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Medida> getListaPorSigla(String filtro) {
        return super.listaPorSigla(filtro);
    }

    public List<Medida> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Medida> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
