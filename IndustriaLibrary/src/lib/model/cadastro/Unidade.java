package lib.model.cadastro;

import java.util.ArrayList;
import java.util.List;
import lib.dao.cadastro.UnidadeDAO;

/**
 *
 * @author abnerjp
 */
public class Unidade extends UnidadeDAO {

    private int codigo;
    private String razaoSocial;
    private String cnpj;
    private String inscricaoEstadual;
    private String telefoneFixo;
    private String telefoneCelular;
    private String email;
    private String nomeFantasia;
    private String referencia;
    private String senha;
    private boolean ativo;
    private String observacao;

    public Unidade(int codigo, String razaoSocial, String cnpj, String inscricaoEstadual, String telefoneFixo, String telefoneCelular, String email, String nomeFantasia, String referencia, String senha, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.razaoSocial = razaoSocial;
        this.cnpj = somenteNumeros(cnpj);
        this.inscricaoEstadual = somenteNumeros(inscricaoEstadual);
        this.telefoneFixo = somenteNumeros(telefoneFixo);
        this.telefoneCelular = somenteNumeros(telefoneCelular);
        this.email = email;
        this.nomeFantasia = nomeFantasia;
        this.referencia = referencia;
        this.senha = senha;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Unidade(String razaoSocial, String cnpj, String inscricaoEstadual, String telefoneFixo, String telefoneCelular, String email, String nomeFantasia, String referencia, String senha, boolean ativo, String observacao) {
        this(0, razaoSocial, cnpj, inscricaoEstadual, telefoneFixo, telefoneCelular, email, nomeFantasia, referencia, senha, ativo, observacao);
    }

    public Unidade(int codigo) {
        this(codigo, "", "", "", "", "", "", "", "", "", false, "");
    }

    public Unidade() {
        this(0, "", "", "", "", "", "", "", "", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = somenteNumeros(cnpj);
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = somenteNumeros(inscricaoEstadual);
    }

    public String getTelefoneFixo() {
        return telefoneFixo;
    }

    public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = somenteNumeros(telefoneFixo);
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = somenteNumeros(telefoneCelular);
    }

    private String somenteNumeros(String info) {
        return info != null ? info.replaceAll("[^0-9]", "") : info;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    private String verificaNulo(String valor) {
        return valor != null ? valor : "";
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {

        //1 validar referência
        if (referencia != null && referencia.length() > 10) {
            return 910;
        }

        //2 validar razao social
        if (razaoSocial == null || razaoSocial.length() < 2) {
            return 903;
        }
        if (razaoSocial.length() > 150) {
            return 904;
        }

        //4 validar cnpj
        if (cnpj == null || cnpj.length() != 14) {
            return 905;
        }

        //5 validar inscrição estadual
        if (inscricaoEstadual != null && inscricaoEstadual.length() > 20) {
            return 909;
        }

        //6 validar nome fantasia
        if (nomeFantasia == null || nomeFantasia.length() < 2) {
            return 913;
        }
        if (nomeFantasia != null && nomeFantasia.length() > 50) {
            return 914;
        }

        //7 validar e-mail
        if (email != null && email.length() > 0 && (!email.contains("@") || email.length() < 3 || email.length() > 70)) {
            return 908;
        }

        //8 validar telefone fixo
        if (telefoneFixo != null && telefoneFixo.length() > 10) {
            return 906;
        }

        //9 validar celular
        if (telefoneCelular != null && telefoneCelular.length() > 11) {
            return 907;
        }

        //10 validar senha
        if (senha != null && ((senha.length() > 0 && senha.length() < 4) || senha.length() > 100)) {
            return 911;
        }

        //11 validar observação
        if (observacao != null && observacao.length() > 200) {
            return 912;
        }

        // não houve erros
        return 0;
    }

    public int gravar() {
        // validar código
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //valida gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        // validar código
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //valida alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Unidade getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Unidade getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Unidade getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Unidade getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Unidade getRegPorCodigo(int id) {
        if (id > 0) {
            return super.registroPorCodigo(id);
        }
        return null;
    }

    public Unidade getRegPorReferencia(String filtro) {
        return super.registroPorReferencia(filtro);
    }

    public List<Unidade> getListaPorReferencia(String filtro) {
        return super.listaPorReferencia(filtro);
    }

    public List<Unidade> getListaPorNomeFantasia(String filtro) {
        return super.listaPorNomeFantasia(filtro);
    }

    public List<Unidade> getListaPorRazaoSocial(String filtro) {
        return super.listaPorRazaoSocial(filtro);
    }

    public List<Unidade> getListaPorCnpj(String filtro) {
        return super.listaPorCnpj(filtro);
    }

    public List<Unidade> getListaLimitada(String nomeFantasia, int qtdeRegistro) {
        if (qtdeRegistro > 0) {
            return super.listaLimitada(nomeFantasia, qtdeRegistro);
        }
        return new ArrayList<>();
    }
}
