package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.PaisDAO;

/**
 *
 * @author abnerjp
 */
public class Pais extends PaisDAO{

    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public Pais(int codigo, String nome, String sigla, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Pais(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);
    }

    public Pais(int codigo) {
        this(codigo, "", "", false, "");
    }
    
    public Pais() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 203;
        }
        if (nome.length() > 100) {
            return 204;
        }

        //validar sigla
        if (sigla == null || sigla.isEmpty()) {
            return 205;
        }
        if (sigla.length() > 10) {
            return 206;
        }

        // observação
        if (observacao == null || observacao.length() > 200) {
            return 207;
        }

        //não houve erro
        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Pais getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Pais getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Pais getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Pais getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Pais getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Pais> getListaPorSigla(String filtro) {
        return super.listaPorSigla(filtro);
    }
    
    public List<Pais> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }
    
    public List<Pais> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
