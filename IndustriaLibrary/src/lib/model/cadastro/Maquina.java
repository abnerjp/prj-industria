package lib.model.cadastro;

/**
 *
 * @author abnerjp
 */
public class Maquina {
    
    private int codigo;
    private String modelo;
    private boolean ativo;
    private String observacao;

    public Maquina(int codigo, String modelo, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Maquina(String modelo, boolean ativo, String observacao) {
        this(0, modelo, ativo, observacao);
    }

    public Maquina() {
        this("", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    @Override
    public String toString() {
        return modelo;
    }
    
}
