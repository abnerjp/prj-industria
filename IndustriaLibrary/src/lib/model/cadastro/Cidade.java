package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.CidadeDAO;

/**
 *
 * @author abner.jacomo
 */
public class Cidade extends CidadeDAO{

    private int codigo;
    private String nome;
    private boolean ativo;
    private String observacao;
    private int codigoEstado;

    public Cidade(int codigo, String nome, boolean ativo, String observacao, int codigoEstado) {
        this.codigo = codigo;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
        this.codigoEstado = codigoEstado;
    }

    public Cidade(String nome, boolean ativo, String observacao, int codigoEstado) {
        this(0, nome, ativo, observacao, codigoEstado);
    }

    public Cidade(int codigo) {
        this(codigo, "", false, "", 0);
    }

    public Cidade() {
        this("", false, "", 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 703;
        }
        if (nome.length() > 70) {
            return 704;
        }

        //validar medida
        if (codigoEstado == 0) {
            return 705;
        }

        //observação
        if (observacao == null || observacao.length() > 200) {
            return 706;
        }
        
        //não houve erro
        return 0;
    }
    
    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }
    
    public Cidade getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }
    
    public Cidade getUltimoRegistro() {
        return super.ultimoRegistro();
    }
    
    public Cidade getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }
    
    public Cidade getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }
    
    public Cidade getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }
    
    public List<Cidade> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }
    
    public List<Cidade> getListaPorEstado(String filtro) {
        return super.listaPorEstado(filtro);
    }
    
    public List<Cidade> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
