package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.EnderecoDAO;

/**
 *
 * @author abner.jacomo
 */
public class Endereco extends EnderecoDAO {

    private int codigo;
    private String logradouro;
    private String cep;
    private String bairro;
    private String observacao;
    private int codigoCidade;

    public Endereco(int codigo, String logradouro, String cep, String bairro, String observacao, int codigoCidade) {
        this.codigo = codigo;
        this.logradouro = logradouro;
        this.cep = limpaCep(cep);
        this.bairro = bairro;
        this.observacao = observacao;
        this.codigoCidade = codigoCidade;
    }

    public Endereco(String logradouro, String cep, String bairro, String observacao, int codigoCidade) {
        this(0, logradouro, cep, bairro, observacao, codigoCidade);
    }

    public Endereco(int codigo) {
        this(codigo, "", "", "", "", 0);
    }

    public Endereco() {
        this("", "", "", "", 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = limpaCep(cep);
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodigoCidade() {
        return codigoCidade;
    }

    public void setCodigoCidade(int codigoCidade) {
        this.codigoCidade = codigoCidade;
    }

    /* internos */
    private String limpaCep(String cep) {
        return cep != null ? cep.replace("-", "") : cep;
    }
    
    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar logradouro
        if (logradouro == null || logradouro.length() < 2) {
            return 803;
        }
        if (logradouro.length() > 100) {
            return 804;
        }

        //validar cep
        if (cep == null || (cep != null && cep.length() > 0 && cep.length() != 8)) {
            return 805; 
        }

        //validar bairro
        if (bairro == null || bairro.length() < 2) {
            return 806;
        }
        if (bairro.length() > 50) {
            return 807;
        }

        //validar cidade
        if (codigoCidade == 0) {
            return 808;
        }

        //observação
        if (observacao == null || observacao.length() > 200) {
            return 809;
        }

        //não houve erro
        return 0;
    }

    public int gravar() {
        // validar código
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //valida gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        // validar código
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //valida alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Endereco getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Endereco getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Endereco getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Endereco getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Endereco getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public Endereco getRegPorCep(String filtro) {
        return super.registroPorCep(limpaCep(filtro));
    }
    
    public List<Endereco> getListaPorEndereco(String filtro) {
        return super.listaPorEndereco(filtro);
    }

    public List<Endereco> getListaPorCep(String filtro) {
        return super.listaPorCep(filtro);
    }

    public List<Endereco> getListaPorCidade(String filtro) {
        return super.listaPorCidade(filtro);
    }

    public List<Endereco> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
