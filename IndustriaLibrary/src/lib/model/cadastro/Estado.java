package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.EstadoDAO;

/**
 *
 * @author abnerjp
 */
public class Estado extends EstadoDAO {

    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;
    private int codigoPais;

    public Estado(int codigo, String nome, String sigla, boolean ativo, String observacao, int codigoPais) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
        this.codigoPais = codigoPais;
    }

    public Estado(String nome, String sigla, boolean ativo, String observacao, int codigoPais) {
        this(0, nome, sigla, ativo, observacao, codigoPais);
    }

    public Estado() {
        this("", "", false, "", 0);
    }

    public Estado(int codigo) {
        this(codigo, "", "", false, "", 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodigoPais() {
        return codigoPais;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 303;
        }
        if (nome.length() > 100) {
            return 304;
        }

        //validar sigla
        if (sigla == null || sigla.isEmpty()) {
            return 305;
        }
        if (sigla.length() > 10) {
            return 306;
        }

        //validar medida
        if (codigoPais == 0) {
            return 307;
        }

        //observação
        if (observacao == null || observacao.length() > 200) {
            return 308;
        }

        //não houve erro
        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para gravar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Estado getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Estado getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Estado getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Estado getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Estado getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }
    
    public Estado getRegPorSigla(String filtro) {
        return super.registroPorSigla(filtro);
    }

    public List<Estado> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Estado> getListaPorSigla(String filtro) {
        return super.listaPorSigla(filtro);
    }

    public List<Estado> getListaPorPais(String filtro) {
        return super.listaPorPais(filtro);
    }

    public List<Estado> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
