package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.ItemDAO;

/**
 *
 * @author abnerjp
 */
public class Item extends ItemDAO{

    private int codigo;
    private String nome;
    private boolean ativo;
    private String observacao;
    private int codigoMedida;

    public Item(int codigo, String nome, boolean ativo, String observacao, int codigoMedida) {
        this.codigo = codigo;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
        this.codigoMedida = codigoMedida;
    }

    public Item(String nome, boolean ativo, String observacao, int codigoMedida) {
        this(0, nome, ativo, observacao, codigoMedida);
    }

    public Item(int codigo) {
        this(codigo, "", false, "", 0);
    }
    
    public Item() {
        this("", false, "", 0);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getCodigoMedida() {
        return codigoMedida;
    }

    public void setCodigoMedida(int codigoMedida) {
        this.codigoMedida = codigoMedida;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 103;
        }
        if (nome.length() > 50) {
            return 104;
        }

        //validar medida
        if (codigoMedida == 0) {
            return 105;
        }

        //observação
        if (observacao == null || observacao.length() > 200) {
            return 106;
        }
        
        //não houve erro
        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Item getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Item getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Item getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Item getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Item getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Item> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Item> getListaPorMedida(String filtro) {
        return super.listaPorMedida(filtro);
    }

    public List<Item> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
