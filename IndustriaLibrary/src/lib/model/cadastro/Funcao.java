package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.FuncaoDAO;

/**
 *
 * @author abnerjp
 */
public class Funcao extends FuncaoDAO {

    private int codigo;
    private String nome;
    private boolean ativo;
    private String observacao;

    public Funcao(int codigo, String nome, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Funcao(String nome, boolean ativo, String observacao) {
        this(0, nome, ativo, observacao);
    }

    public Funcao(int codigo) {
        this(codigo, "", false, "");
    }

    public Funcao() {
        this("", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int id) {
        this.codigo = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    private int validarAtributos() {
        if (nome == null || nome.length() < 2) {
            return 1803;
        }
        if (nome.length() > 30) {
            return 1804;
        }
        if (observacao == null || observacao.length() > 200) {
            return 1805;
        }
        return 0;
    }

    public int gravar() {
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Funcao getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Funcao getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Funcao getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Funcao getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Funcao getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Funcao> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Funcao> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
