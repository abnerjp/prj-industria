package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.TipoParadaDAO;

/**
 *
 * @author abnerjp
 */
public class TipoParada extends TipoParadaDAO {

    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public TipoParada(int codigo, String nome, String sigla, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public TipoParada(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);
    }

    public TipoParada(int codigo) {
        this(codigo, "", "", false, "");
    }

    public TipoParada() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 503;
        }
        if (nome.length() > 30) {
            return 504;
        }

        //validar sigla
        if (sigla == null || sigla.isEmpty()) {
            return 505;
        }
        if (sigla.length() > 6) {
            return 506;
        }

        // observação
        if (observacao == null || observacao.length() > 200) {
            return 507;
        }

        //não houve erro
        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar gravação
        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public TipoParada getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public TipoParada getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public TipoParada getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public TipoParada getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public TipoParada getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<TipoParada> getListaPorSigla(String filtro) {
        return super.listaPorSigla(filtro);
    }

    public List<TipoParada> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<TipoParada> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }

}
