package lib.model.cadastro;

import java.util.List;
import lib.dao.cadastro.EquipamentoDAO;

/**
 *
 * @author abner.jacomo
 */
public class Equipamento extends EquipamentoDAO {

    private int codigo;
    private String nome;
    private String modelo;
    private boolean ativo;
    private String observacao;

    public Equipamento(int codigo, String nome, String modelo, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.modelo = modelo;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Equipamento(String nome, String modelo, boolean ativo, String observacao) {
        this(0, nome, modelo, ativo, observacao);
    }

    public Equipamento(int codigo) {
        this(codigo, "", "", false, "");
    }

    public Equipamento() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /* acesso ao banco de dados */
    private int validarAtributos() {
        //validar nome
        if (nome == null || nome.length() < 2) {
            return 1403;
        }
        if (nome.length() > 50) {
            return 1404;
        }

        //validar modelo
        if (modelo == null || modelo.length() > 30) {
            return 1405;
        }

        //validar observação
        if (observacao == null || observacao.length() > 200) {
            return 1406;
        }

        return 0;
    }

    public int gravar() {
        //validar código para gravar
        if (codigo != 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        if (super.gravar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int alterar() {
        //validar código para alterar
        if (codigo <= 0) {
            return 2;
        }

        int result = validarAtributos();
        if (result > 0) {
            return result;
        }

        //validar alteração
        if (super.alterar(this)) {
            return 0;
        } else {
            return 1;
        }
    }

    public int excluir() {
        if (super.excluir(codigo)) {
            return 0;
        } else {
            return 1;
        }
    }

    public Equipamento getPrimeiroRegistro() {
        return super.primeiroRegistro();
    }

    public Equipamento getUltimoRegistro() {
        return super.ultimoRegistro();
    }

    public Equipamento getProximoRegistro() {
        return super.proximoRegistro(codigo);
    }

    public Equipamento getAnteriorRegistro() {
        return super.anteriorRegistro(codigo);
    }

    public Equipamento getRegPorCodigo(int id) {
        return super.registroPorCodigo(id);
    }

    public List<Equipamento> getListaPorNome(String filtro) {
        return super.listaPorNome(filtro);
    }

    public List<Equipamento> getListaPorModelo(String filtro) {
        return super.listaPorModelo(filtro);
    }

    public List<Equipamento> getListaLimitada(String filtro, int qtdeRegistro) {
        return super.listaLimitada(filtro, qtdeRegistro);
    }
}
