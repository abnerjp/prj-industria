package lib.banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abnerjp
 */
public class Conexao {
    private static final String URL = 
            "jdbc:postgresql://localhost/industria";
    private static final String usuario = "appuser";
    private static final String senha = "20160x";

    public static Connection abre() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(URL,
                    usuario, senha);

        } catch (ClassNotFoundException | SQLException ex) {
            //Logger.getLogger(Conexao.class.getName())
            //        .log(Level.SEVERE, null, ex);
        }
        return null;
    }    
}
