package lib.viewmodel.associacao;

import java.time.LocalDate;
import lib.controller.cadastro.EnderecoController;
import lib.util.Data;
import lib.viewmodel.cadastro.EnderecoViewModel;

/**
 *
 * @author abner.jacomo
 */
public class UnidadeEnderecoViewModel {

    private int numero;
    private LocalDate data;
    private String complemento;
    private String observacao;
    private EnderecoViewModel endereco;

    public UnidadeEnderecoViewModel(int numero, LocalDate data, String complemento, String observacao, int codEndereco) {
        this.numero = numero;
        this.data = data;
        this.complemento = complemento;
        this.observacao = observacao;
        this.endereco = codEndereco > 0 ? getEnderecoPorCodigo(codEndereco) : null;
    }

    //public UnidadeEnderecoViewModel() {
    //    this(0, LocalDate.now(), "", "", 0);
    //}
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public EnderecoViewModel getEndereco() {
        return endereco;
    }

    public void setEndereco(int codEndereco) {
        this.endereco = codEndereco > 0 ? getEnderecoPorCodigo(codEndereco) : null;
    }

    private EnderecoViewModel getEnderecoPorCodigo(int idEndereco) {
        return new EnderecoController().regPorCodigo(idEndereco);
    }

    public String getEnderecoFormatado() {
        String info = "";
        if (endereco != null) {
            info += endereco.getLogradouro();
            info += info.length() > 0 ? ", " : "";
            info += getNumeroFormatado();
        }
        return info;
    }

    private String getNumeroFormatado() {
        if (numero <= 0) {
            return "S/N";
        }
        return String.valueOf(numero);
    }

}
