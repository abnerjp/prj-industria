package lib.viewmodel.associacao;

import java.time.LocalDate;
import java.util.Objects;
import lib.controller.associacao.UnidadeMotivoParadaController;
import lib.controller.associacao.UnidadeTipoParadaController;
import lib.util.Data;

/**
 *
 * @author abnerjp
 */
public class UnidadeMotivoComTipoParadaViewModel {

    private int id;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private UnidadeTipoParadaViewModel unidadeTipoParada;
    private UnidadeMotivoParadaViewModel unidadeMotivoParada;

    public UnidadeMotivoComTipoParadaViewModel(int id, boolean ativo, LocalDate data, String observacao, int idUnidadeTipoParada, int idUnidadeMotivoParada) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.unidadeTipoParada = idUnidadeTipoParada > 0 ? getUnidadeTipoParadaViewModel(idUnidadeTipoParada) : null;
        this.unidadeMotivoParada = idUnidadeMotivoParada > 0 ? getUnidadeMotivoParadaViewModel(idUnidadeMotivoParada) : null;
    }

    public UnidadeMotivoComTipoParadaViewModel(boolean ativo, LocalDate data, String observacao, int idUnidadeTipoParada, int idUnidadeMotivoParada) {
        this(0, ativo, data, observacao, idUnidadeTipoParada, idUnidadeMotivoParada);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public UnidadeTipoParadaViewModel getUnidadeTipoParada() {
        return unidadeTipoParada;
    }

    public void setUnidadeTipoParada(UnidadeTipoParadaViewModel unidadeTipoParada) {
        this.unidadeTipoParada = unidadeTipoParada;
    }

    public UnidadeMotivoParadaViewModel getUnidadeMotivoParada() {
        return unidadeMotivoParada;
    }

    public void setUnidadeMotivoParada(UnidadeMotivoParadaViewModel unidadeMotivoParada) {
        this.unidadeMotivoParada = unidadeMotivoParada;
    }

    public String getNomeVinculoConcatenado() {
        try {
            return this.getUnidadeTipoParada().getTipoParada().getNome() + " x " + this.getUnidadeMotivoParada().getMotivoParada().getNome();
        } catch (NullPointerException ex) {
            return "";
        }
    }

    public String getSiglaNomeVinculoConcatenado() {
        try {
            return this.getUnidadeTipoParada().getNomeComSigla() + " x " + this.getUnidadeMotivoParada().getNomeComSigla();
        } catch (NullPointerException ex) {
            return "";
        }
    }

    public String getSiglaVinculoConcatenado() {
        try {
            return this.getUnidadeTipoParada().getTipoParada().getSigla() + " x " + this.getUnidadeMotivoParada().getMotivoParada().getSigla();
        } catch (NullPointerException ex) {
            return "";
        }
    }

    private UnidadeTipoParadaViewModel getUnidadeTipoParadaViewModel(int id) {
        return new UnidadeTipoParadaController().getRegPorCodigo(id);
    }

    private UnidadeMotivoParadaViewModel getUnidadeMotivoParadaViewModel(int id) {
        return new UnidadeMotivoParadaController().getRegPorCodigo(id);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.unidadeTipoParada);
        hash = 71 * hash + Objects.hashCode(this.unidadeMotivoParada);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeMotivoComTipoParadaViewModel other = (UnidadeMotivoComTipoParadaViewModel) obj;
        if (!Objects.equals(this.unidadeTipoParada, other.unidadeTipoParada)) {
            return false;
        }
        return Objects.equals(this.unidadeMotivoParada, other.unidadeMotivoParada);
    }

}
