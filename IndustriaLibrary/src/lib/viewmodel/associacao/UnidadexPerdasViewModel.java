package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UnidadePerdaController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abnerjp
 */
public final class UnidadexPerdasViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadePerdaViewModel> listaPerda;

    public UnidadexPerdasViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexPerdasViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizarPerdas();
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizarPerdas();
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }

    private void atualizarPerdas() {
        if (unidade != null) {
            if (unidade.getId() > 0) {
                listaPerda = new UnidadePerdaController().getPerdasUnidade(unidade.getId());
            } else {
                initListaPerda();
            }
        }
    }

    public void addPerdaNaLista(UnidadePerdaViewModel perdaDaUnidade) {
        if (unidade != null && unidade.getId() > 0 && perdaDaUnidade != null) {
            if (listaPerda == null) {
                initListaPerda();
            }
            
            listaPerda.add(perdaDaUnidade);
            ordenarLista();
        }
    }

    private void ordenarLista() {
        Collections.sort(listaPerda,
                (UnidadePerdaViewModel obj1, UnidadePerdaViewModel obj2)
                -> obj1.getPerda().getNome().compareToIgnoreCase(obj2.getPerda().getNome())
        );
    }

    public void removerPerdaDaLista(UnidadePerdaViewModel perdaDaUnidade) {
        if (unidade != null) {
            try {
                listaPerda.remove(getPosicaoPerdaNaLista(perdaDaUnidade));
            } catch (IndexOutOfBoundsException ex) {
                // não excluído
            }
        }
    }

    public void atualizarPerdaNaLista(UnidadePerdaViewModel perdaDaUnidade) {
        if (unidade != null) {
            int posicao = getPosicaoPerdaNaLista(perdaDaUnidade);
            try {
                listaPerda.set(posicao, perdaDaUnidade);
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // item não atualizado
            }
        }
    }

    public int getPosicaoPerdaNaLista(UnidadePerdaViewModel perdaDaUnidade) {
        try {
            return listaPerda.indexOf(perdaDaUnidade);
        } catch (NullPointerException | ClassCastException ex) {
            return -1;
        }
    }

    private void initListaPerda() {
        listaPerda = new ArrayList<>();
    }

    public List<UnidadePerdaViewModel> getListaPerdas() {
        return listaPerda;
    }

}
