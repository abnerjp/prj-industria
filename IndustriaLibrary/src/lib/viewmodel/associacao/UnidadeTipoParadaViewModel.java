package lib.viewmodel.associacao;

import java.time.LocalDate;
import lib.controller.cadastro.TipoParadaController;
import lib.util.Data;
import lib.viewmodel.cadastro.TipoParadaViewModel;

/**
 * classe para ser utilizada como uma lista de tipos de paradas de uma unidade
 *
 * @author abner.jacomo
 */
public final class UnidadeTipoParadaViewModel {

    private int id;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private TipoParadaViewModel tipoParada;

    public UnidadeTipoParadaViewModel(int id, boolean ativo, LocalDate data, String observacao, int idTipoParada) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.tipoParada = idTipoParada > 0 ? getTipoParadaViewModel(idTipoParada) : null;
    }

    public UnidadeTipoParadaViewModel(boolean ativo, LocalDate data, String observacao, int idTipoParada) {
        this(0, ativo, data, observacao, idTipoParada);
    }

    public int getId() {
        return id;
    }

    public void setId(int codigo) {
        this.id = codigo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativa" : "Desativada";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public TipoParadaViewModel getTipoParada() {
        return tipoParada;
    }

    public void setTipoParada(TipoParadaViewModel tipoParada) {
        this.tipoParada = tipoParada;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(data);
    }

    public String getNomeComSigla() {
        return tipoParada != null ? tipoParada.getTipoParadaFormatado() : "";
    }

    @Override
    public String toString() {
        return getNomeComSigla();
    }

    private TipoParadaViewModel getTipoParadaViewModel(int id) {
        return new TipoParadaController().regPorCodigo(id);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeTipoParadaViewModel other = (UnidadeTipoParadaViewModel) obj;
        return this.id == other.id;
    }

}
