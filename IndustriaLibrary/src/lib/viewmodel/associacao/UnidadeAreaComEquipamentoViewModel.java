package lib.viewmodel.associacao;

import java.time.LocalDate;
import java.util.Objects;
import lib.controller.associacao.UnidadeAreaController;
import lib.controller.associacao.UnidadeEquipamentoController;
import lib.util.Data;

/**
 *
 * @author abnerjp
 */
public class UnidadeAreaComEquipamentoViewModel {

    private int id;
    private boolean ativo;
    private String referencia;
    private LocalDate data;
    private String observacao;
    private UnidadeAreaViewModel unidadeArea;
    private UnidadeEquipamentoViewModel unidadeEquipamento;

    public UnidadeAreaComEquipamentoViewModel(int id, boolean ativo, String referencia, LocalDate data, String observacao, int idUnidadeArea, int idUnidadeEquipamento) {
        this.id = id;
        this.ativo = ativo;
        this.referencia = referencia;
        this.data = data;
        this.observacao = observacao;
        this.unidadeArea = idUnidadeArea > 0 ? getUnidadeAreaViewModel(idUnidadeArea) : null;
        this.unidadeEquipamento = idUnidadeEquipamento > 0 ? getUnidadeEquipamentoViewModel(idUnidadeEquipamento) : null;
    }

    public UnidadeAreaComEquipamentoViewModel(boolean ativo, String referencia, LocalDate data, String observacao, int idUnidadeArea, int idUnidadeEquipamento) {
        this(0, ativo, referencia, data, observacao, idUnidadeArea, idUnidadeEquipamento);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public UnidadeAreaViewModel getUnidadeArea() {
        return unidadeArea;
    }

    public void setUnidadeArea(UnidadeAreaViewModel unidadeArea) {
        this.unidadeArea = unidadeArea;
    }

    public UnidadeEquipamentoViewModel getUnidadeEquipamento() {
        return unidadeEquipamento;
    }

    public void setUnidadeEquipamento(UnidadeEquipamentoViewModel unidadeEquipamento) {
        this.unidadeEquipamento = unidadeEquipamento;
    }

    public String getNomeVinculoConcatenado() {
        String retorno = "";
        try {
            retorno = this.getUnidadeArea().getArea().getNome() + " x " + this.getUnidadeEquipamento().toString();
        } catch (NullPointerException ex) {
            retorno += "";
        }

        try {
            if (this.getReferencia().length() > 0) {
                retorno += " - " + this.referencia;
            }
        } catch (NullPointerException ex) {
            retorno += "";
        }
        
        return retorno;
    }

    public UnidadeAreaComEquipamentoViewModel getCopiaObj() {
        return new UnidadeAreaComEquipamentoViewModel(
                this.id,
                this.ativo,
                this.referencia,
                this.data,
                this.observacao,
                this.unidadeArea.getId(),
                this.unidadeEquipamento.getId()
        );
    }

    private UnidadeAreaViewModel getUnidadeAreaViewModel(int id) {
        return new UnidadeAreaController().getRegPorCodigo(id);
    }

    private UnidadeEquipamentoViewModel getUnidadeEquipamentoViewModel(int id) {
        return new UnidadeEquipamentoController().getRegPorCodigo(id);
    }

    @Override
    public String toString() {
        return getNomeVinculoConcatenado();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.referencia);
        hash = 71 * hash + Objects.hashCode(this.unidadeArea);
        hash = 71 * hash + Objects.hashCode(this.unidadeEquipamento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeAreaComEquipamentoViewModel other = (UnidadeAreaComEquipamentoViewModel) obj;
        if (!Objects.equals(this.referencia, other.referencia)) {
            return false;
        }
        if (!Objects.equals(this.unidadeArea, other.unidadeArea)) {
            return false;
        }
        if (!Objects.equals(this.unidadeEquipamento, other.unidadeEquipamento)) {
            return false;
        }
        return true;
    }

}
