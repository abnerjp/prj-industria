package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UnidadeEquipamentoController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abnerjp
 */
public final class UnidadexEquipamentosViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeEquipamentoViewModel> listaEquipamento;

    public UnidadexEquipamentosViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexEquipamentosViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizaEquipamentos();
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizaEquipamentos();
    }

    public List<UnidadeEquipamentoViewModel> getEquipamentos() {
        return this.listaEquipamento;
    }

    public void addEquipamentoNaLista(UnidadeEquipamentoViewModel equipamentoDaUnidade) {
        if (this.unidade != null && this.unidade.getId() > 0 && equipamentoDaUnidade != null) {
            if (this.listaEquipamento == null) {
                initListaEquipamentos();
            }
            this.listaEquipamento.add(equipamentoDaUnidade);
            ordenarListaEquipamentos();
        }
    }

    public void removerItemDaLista(UnidadeEquipamentoViewModel equipamentoDaUnidade) {
        if (this.unidade != null) {
            this.listaEquipamento.remove(getPosicaoEquipamentoNaLista(equipamentoDaUnidade));
        }
    }

    public void atualizarEquipamentoNaLista(UnidadeEquipamentoViewModel equipamentoDaUnidade) {
        if (this.unidade != null) {
            int posicao = getPosicaoEquipamentoNaLista(equipamentoDaUnidade);
            if (posicao > -1) {
                this.listaEquipamento.set(posicao, equipamentoDaUnidade);
            }
        }
    }

    public int getPosicaoEquipamentoNaLista(UnidadeEquipamentoViewModel equipamento) {
        int indexEquipamento = 0;
        if (this.listaEquipamento != null && equipamento != null) {
            while (indexEquipamento < this.listaEquipamento.size() && equipamento.getId() != this.listaEquipamento.get(indexEquipamento).getId()) {
                indexEquipamento++;
            }
            return indexEquipamento < this.listaEquipamento.size() ? indexEquipamento-- : -1;
        }
        return -1;
    }

    private void ordenarListaEquipamentos() {
        Collections.sort(
                this.listaEquipamento,
                (UnidadeEquipamentoViewModel obj1, UnidadeEquipamentoViewModel obj2)
                -> obj1.toString().compareTo(obj2.toString()));
    }

    private void initListaEquipamentos() {
        listaEquipamento = new ArrayList<>();
    }

    private void atualizaEquipamentos() {
        if (unidade != null) {
            if (unidade.getId() > 0) {
                this.listaEquipamento = new UnidadeEquipamentoController().getEquipamentosUnidade(this.unidade.getId());
            } else {
                initListaEquipamentos();
            }
        }
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }
}
