package lib.viewmodel.associacao;

import java.time.LocalDate;
import lib.controller.cadastro.AreaController;
import lib.util.Data;
import lib.viewmodel.cadastro.AreaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeAreaViewModel {

    private int id;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private AreaViewModel area;

    public UnidadeAreaViewModel(int id, boolean ativo, LocalDate data, String observacao, int idArea) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.area = idArea > 0 ? getAreaViewModel(idArea) : null;
    }

    public UnidadeAreaViewModel(boolean ativo, LocalDate data, String observacao, int idArea) {
        this(0, ativo, data, observacao, idArea);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public AreaViewModel getArea() {
        return area;
    }

    public void setArea(AreaViewModel area) {
        this.area = area;
    }

    private AreaViewModel getAreaViewModel(int id) {
        return new AreaController().getRegPorCodigo(id);
    }

    @Override
    public String toString() {
        try {
            return getArea().getNome();
        } catch (NullPointerException ex) {
            return "";
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeAreaViewModel other = (UnidadeAreaViewModel) obj;
        return this.id == other.id;
    }

}
