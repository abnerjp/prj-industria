package lib.viewmodel.associacao;

import java.time.LocalDate;
import lib.controller.cadastro.MotivoParadaController;
import lib.util.Data;
import lib.viewmodel.cadastro.MotivoParadaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadeMotivoParadaViewModel {

    private int id;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private MotivoParadaViewModel motivoParada;

    public UnidadeMotivoParadaViewModel(int id, boolean ativo, LocalDate data, String observacao, int idMotivoParada) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.motivoParada = idMotivoParada > 0 ? getMotivoParadaViewModel(idMotivoParada) : null;
    }

    public UnidadeMotivoParadaViewModel(boolean ativo, LocalDate data, String observacao, int idMotivoParada) {
        this(0, ativo, data, observacao, idMotivoParada);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public MotivoParadaViewModel getMotivoParada() {
        return motivoParada;
    }

    public void setMotivoParada(MotivoParadaViewModel motivoParada) {
        this.motivoParada = motivoParada;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public String getNomeComSigla() {
        return motivoParada != null ? motivoParada.getMotivoParadaFormatado() : "";
    }

    @Override
    public String toString() {
        return getNomeComSigla();
    }

    private MotivoParadaViewModel getMotivoParadaViewModel(int id) {
        return new MotivoParadaController().regPorCodigo(id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeMotivoParadaViewModel other = (UnidadeMotivoParadaViewModel) obj;
        return this.id == other.id;
    }
}
