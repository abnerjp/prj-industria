package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UnidadeMotivoComTipoParadaController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abnerjp
 */
public final class UnidadexMotivosComTiposParadaViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeMotivoComTipoParadaViewModel> listaVinculos;

    public UnidadexMotivosComTiposParadaViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexMotivosComTiposParadaViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizarVinculos();
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizarVinculos();
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public int getIdUnidade() {
        try {
            return unidade.getId();
        } catch (NullPointerException ex) {
            return 0;
        }
    }

    public List<UnidadeMotivoComTipoParadaViewModel> getListaVinculos() {
        return listaVinculos;
    }

    public UnidadeMotivoComTipoParadaViewModel getVinculoPorPosicao(int pos) {
        try {
            return listaVinculos.get(pos);
        } catch (NullPointerException | IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public String getSiglaComNomeConcatenadoDaLista(int index) {
        try {
            return getVinculoPorPosicao(index).getSiglaNomeVinculoConcatenado();
        } catch (NullPointerException ex) {
            return "";
        }
    }

    public void addVinculoNaLista(UnidadeMotivoComTipoParadaViewModel unidadeMotivoComTipo) {
        if (unidade != null && unidade.getId() > 0 && unidadeMotivoComTipo != null) {
            if (listaVinculos == null) {
                initListaVinculo();
            }
            listaVinculos.add(unidadeMotivoComTipo);
            ordenarListaVinculos();
        }
    }

    private void ordenarListaVinculos() {
        Collections.sort(
                listaVinculos,
                (UnidadeMotivoComTipoParadaViewModel obj1, UnidadeMotivoComTipoParadaViewModel obj2)
                -> obj1.getNomeVinculoConcatenado().compareTo(obj2.getNomeVinculoConcatenado())
        );
    }

    public void recarregarVinculos() {
        atualizarVinculos();
    }

    private void atualizarVinculos() {
        if (unidade != null) {
            if (unidade.getId() > 0) {
                listaVinculos = new UnidadeMotivoComTipoParadaController().getMotivosETiposPorUnidade(unidade.getId());
            } else {
                initListaVinculo();
            }
        }
    }

    private void initListaVinculo() {
        listaVinculos = new ArrayList<>();
    }

    public int getPosicaoVinculoNaLista(UnidadeMotivoComTipoParadaViewModel unidadeMotivoComTipo) {
        int pos;
        try {
            pos = listaVinculos.indexOf(unidadeMotivoComTipo);
        } catch (NullPointerException | ClassCastException ex) {
            pos = -1;
        }
        return pos;
    }

    public void atualizarVinculoNaLista(UnidadeMotivoComTipoParadaViewModel unidadeMotivoComTipo) {
        if (unidade != null) {
            int posicao = getPosicaoVinculoNaLista(unidadeMotivoComTipo);
            try {
                listaVinculos.set(posicao, unidadeMotivoComTipo);
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // item não atualizado
            }
        }
    }

    public void removeVinculoDaLista(UnidadeMotivoComTipoParadaViewModel unidadeMotivoComTipo) {
        if (unidade != null) {
            try {
                listaVinculos.remove(getPosicaoVinculoNaLista(unidadeMotivoComTipo));
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // não excluído
            }
        }
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }
}
