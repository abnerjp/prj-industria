package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UnidadeAreaComEquipamentoController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abnerjp
 */
public final class UnidadexAreasComEquipamentosViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeAreaComEquipamentoViewModel> listaVinculos;

    public UnidadexAreasComEquipamentosViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexAreasComEquipamentosViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizarVinculos();
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizarVinculos();
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public int getIdUnidade() {
        try {
            return unidade.getId();
        } catch (NullPointerException ex) {
            return 0;
        }
    }

    public List<UnidadeAreaComEquipamentoViewModel> getListaVinculos() {
        return listaVinculos;
    }

    public UnidadeAreaComEquipamentoViewModel getVinculoPorPosicao(int pos) {
        try {
            return listaVinculos.get(pos);
        } catch (NullPointerException | IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public String getNomeVinculoConcatenado(int index) {
        try {
            return getVinculoPorPosicao(index).getNomeVinculoConcatenado();
        } catch (NullPointerException ex) {
            return "";
        }
    }

    public void addVinculoNaLista(UnidadeAreaComEquipamentoViewModel unidadeAreaComEquipamento) {
        if (this.unidade != null && this.unidade.getId() > 0 && unidadeAreaComEquipamento != null) {
            if (this.listaVinculos == null) {
                initListaVinculo();
            }
            this.listaVinculos.add(unidadeAreaComEquipamento);
            ordenarListaVinculos();
        }
    }

    private void ordenarListaVinculos() {
        Collections.sort(
                listaVinculos,
                (UnidadeAreaComEquipamentoViewModel obj1, UnidadeAreaComEquipamentoViewModel obj2)
                -> obj1.getNomeVinculoConcatenado().compareToIgnoreCase(obj2.getNomeVinculoConcatenado())
        );
    }

    public void recarregarVinculos() {
        atualizarVinculos();
    }

    private void atualizarVinculos() {
        if (this.unidade != null) {
            if (this.unidade.getId() > 0) {
                listaVinculos = new UnidadeAreaComEquipamentoController().getEquipamentosEAreasPorUnidade(unidade.getId());
            } else {
                initListaVinculo();
            }
        }
    }

    private void initListaVinculo() {
        this.listaVinculos = new ArrayList<>();
    }

    public int getPosicaoVinculoNaLista(UnidadeAreaComEquipamentoViewModel unidadeAreaComEquipamento) {
        int pos;
        try {
            pos = this.listaVinculos.indexOf(unidadeAreaComEquipamento);
        } catch (NullPointerException | ClassCastException ex) {
            pos = -1;
        }
        return pos;
    }

    public void atualizarVinculoNaLista(UnidadeAreaComEquipamentoViewModel unidadeAreaComEquipamento) {
        if (this.unidade != null) {
            int posicao = getPosicaoVinculoNaLista(unidadeAreaComEquipamento);
            try {
                listaVinculos.set(posicao, unidadeAreaComEquipamento);
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // item não atualizado
            }
        }
    }

    public void removeVinculoDaLista(UnidadeAreaComEquipamentoViewModel unidadeAreaComEquipamento) {
        if (this.unidade != null) {
            try {
                listaVinculos.remove(getPosicaoVinculoNaLista(unidadeAreaComEquipamento));
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // não excluído
            }
        }
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }
}
