package lib.viewmodel.associacao;

import java.time.LocalDate;
import lib.controller.cadastro.PerdaController;
import lib.util.Data;
import lib.viewmodel.cadastro.PerdaViewModel;

/**
 *
 * @author abnerjp
 */
public class UnidadePerdaViewModel {

    private int id;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private PerdaViewModel perda;

    public UnidadePerdaViewModel(int id, boolean ativo, LocalDate data, String observacao, int idPerda) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.perda = idPerda > 0 ? getPerdaViewModel(idPerda) : null;
    }

    public UnidadePerdaViewModel(boolean ativo, LocalDate data, String observacao, int idPerda) {
        this(0, ativo, data, observacao, idPerda);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public PerdaViewModel getPerda() {
        return perda;
    }

    public void setPerda(PerdaViewModel perda) {
        this.perda = perda;
    }

    public String getNomeComSigla() {
        return perda != null ? perda.getPerdaFormatada() : "";
    }

    private PerdaViewModel getPerdaViewModel(int id) {
        return new PerdaController().regPorCodigo(id);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadePerdaViewModel other = (UnidadePerdaViewModel) obj;
        return this.id == other.id;
    }

}
