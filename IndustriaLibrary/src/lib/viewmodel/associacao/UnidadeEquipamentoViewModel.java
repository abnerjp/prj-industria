package lib.viewmodel.associacao;

import java.time.LocalDate;
import lib.controller.cadastro.EquipamentoController;
import lib.util.Data;
import lib.viewmodel.cadastro.EquipamentoViewModel;

/**
 *
 * @author abner.jacomo
 */
public class UnidadeEquipamentoViewModel {

    private int id;
    private String referencia;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private EquipamentoViewModel equipamento;

    public UnidadeEquipamentoViewModel(int id, boolean ativo, LocalDate data, String observacao, int idEquipamento) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.equipamento = idEquipamento > 0 ? getEquipamentoViewModel(idEquipamento) : null;
    }

    public UnidadeEquipamentoViewModel(boolean ativo, LocalDate data, String observacao, int idEquipamento) {
        this(0, ativo, data, observacao, idEquipamento);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public String getDataFormatada() {
        return Data.dataFormatada(this.getData());
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public EquipamentoViewModel getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(EquipamentoViewModel equipamento) {
        this.equipamento = equipamento;
    }

    public String getNomeComModelo() {
        return equipamento != null ? equipamento.getEquipamentoFormatado() : "";
    }

    @Override
    public String toString() {
        return getNomeComModelo();
    }

    private EquipamentoViewModel getEquipamentoViewModel(int id) {
        return new EquipamentoController().getRegPorCodigo(id);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeEquipamentoViewModel other = (UnidadeEquipamentoViewModel) obj;
        return this.id == other.id;
    }

}
