package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.cadastro.UnidadeController;
import lib.controller.associacao.UnidadeTipoParadaController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * @author abnerjp
 */
public final class UnidadexTiposParadaViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeTipoParadaViewModel> listaTipoParada;

    public UnidadexTiposParadaViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexTiposParadaViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizarTiposParadas();
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizarTiposParadas();
    }

    public List<UnidadeTipoParadaViewModel> getTiposParada() {
        return listaTipoParada;
    }

    public void addTipoParadaNaLista(UnidadeTipoParadaViewModel tipoParadaDaUnidade) {
        if (unidade != null && unidade.getId() > 0 && tipoParadaDaUnidade != null) {
            if (listaTipoParada == null) {
                initListaTipoParada();
            }
            listaTipoParada.add(tipoParadaDaUnidade);
            ordenarLista();
        }
    }

    public void removerTipoParadaDaLista(UnidadeTipoParadaViewModel tipoParadaDaUnidade) {
        if (unidade != null) {
            listaTipoParada.remove(getPosicaoTipoParadaNaLista(tipoParadaDaUnidade));
        }
    }

    public void atualizarTipoParadaNaLista(UnidadeTipoParadaViewModel tipoParadaDaUnidade) {
        if (unidade != null) {
            int posicao = getPosicaoTipoParadaNaLista(tipoParadaDaUnidade);
            if (posicao > -1) {
                listaTipoParada.set(posicao, tipoParadaDaUnidade);
            }
        }
    }

    public int getPosicaoTipoParadaNaLista(UnidadeTipoParadaViewModel tipoParadaDaUnidade) {
        if (listaTipoParada != null) {
            return listaTipoParada.indexOf(tipoParadaDaUnidade);
        }
        return -1;
    }

    private void ordenarLista() {
        Collections.sort(listaTipoParada, 
                (UnidadeTipoParadaViewModel obj1, UnidadeTipoParadaViewModel obj2) 
                        -> obj1.getTipoParada().getNome().toLowerCase().compareTo(obj2.getTipoParada().getNome().toLowerCase())
        );
    }

    private void initListaTipoParada() {
        listaTipoParada = new ArrayList<>();
    }

    private void atualizarTiposParadas() {
        if (unidade != null) {
            if (unidade.getId() > 0) {
                listaTipoParada = new UnidadeTipoParadaController().getTiposParadaUnidade(unidade.getId());
            } else {
                initListaTipoParada();
            }
        }
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }
}
