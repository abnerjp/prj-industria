/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.viewmodel.associacao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import lib.controller.cadastro.ItemController;
import lib.viewmodel.cadastro.ItemViewModel;

/**
 *
 * @author abner.jacomo
 */
public class UnidadeItemViewModel {

    private int id;
    private boolean ativo;
    private LocalDate data;
    private String observacao;
    private ItemViewModel item;

    public UnidadeItemViewModel(int id, boolean ativo, LocalDate data, String observacao, int idItem) {
        this.id = id;
        this.ativo = ativo;
        this.data = data;
        this.observacao = observacao;
        this.item = idItem > 0 ? getItemViewModel(idItem) : null;
    }

    public UnidadeItemViewModel(boolean ativo, LocalDate data, String observacao, int idItem) {
        this(0, ativo, data, observacao, idItem);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public ItemViewModel getItem() {
        return item;
    }

    public void setItem(ItemViewModel item) {
        this.item = item;
    }

    public String getDataFormatada() {
        String dataFormatada = "";
        if (data != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            dataFormatada = data.format(formatter);
        }
        return dataFormatada;
    }

    private ItemViewModel getItemViewModel(int id) {
        return new ItemController().regPorCodigo(id);
    }

}
