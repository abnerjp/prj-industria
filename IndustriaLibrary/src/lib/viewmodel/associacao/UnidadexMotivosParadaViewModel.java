package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UnidadeMotivoParadaController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * @author abnerjp
 */
public final class UnidadexMotivosParadaViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeMotivoParadaViewModel> listaMotivoParada;

    public UnidadexMotivosParadaViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexMotivosParadaViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizarMotivosParadas();
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizarMotivosParadas();
    }

    public List<UnidadeMotivoParadaViewModel> getMotivosParada() {
        return listaMotivoParada;
    }

    public void addMotivoParadaNaLista(UnidadeMotivoParadaViewModel motivoParadaDaUnidade) {
        if (unidade != null && unidade.getId() > 0 && motivoParadaDaUnidade != null) {
            if (listaMotivoParada == null) {
                initListaMotivoParada();
            }
            listaMotivoParada.add(motivoParadaDaUnidade);
            ordenarLista();
        }
    }

    public void removerMotivoParadaDaLista(UnidadeMotivoParadaViewModel motivoParadaDaUnidade) {
        if (unidade != null) {
            try {
                listaMotivoParada.remove(getPosicaoMotivoParadaNaLista(motivoParadaDaUnidade));
            } catch (IndexOutOfBoundsException ex) {
                // não excluído
            }
        }
    }

    public void atualizarMotivoParadaNaLista(UnidadeMotivoParadaViewModel motivoParadaDaUnidade) {
        if (unidade != null) {
            int posicao = getPosicaoMotivoParadaNaLista(motivoParadaDaUnidade);
            try {
                listaMotivoParada.set(posicao, motivoParadaDaUnidade);
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // item não atualizado
            }
        }
    }

    public int getPosicaoMotivoParadaNaLista(UnidadeMotivoParadaViewModel motivoParadaDaUnidade) {
        try {
            return listaMotivoParada.indexOf(motivoParadaDaUnidade);
        } catch (NullPointerException | ClassCastException ex) {
            return -1;
        }
    }

    private void ordenarLista() {
        Collections.sort(
                listaMotivoParada,
                (UnidadeMotivoParadaViewModel obj1, UnidadeMotivoParadaViewModel obj2) -> 
                        obj1.getMotivoParada().getNome().compareTo(obj2.getMotivoParada().getNome())
        );
    }

    private void atualizarMotivosParadas() {
        if (unidade != null) {
            if (unidade.getId() > 0) {
                listaMotivoParada = new UnidadeMotivoParadaController().getMotivosParadaUnidade(unidade.getId());
            } else {
                initListaMotivoParada();
            }
        }
    }

    private void initListaMotivoParada() {
        listaMotivoParada = new ArrayList<>();
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }
}
