package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UnidadeAreaController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abnerjp
 */
public final class UnidadexAreasViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeAreaViewModel> listaArea;

    public UnidadexAreasViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexAreasViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizarAreas();
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizarAreas();
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }

    private void atualizarAreas() {
        if (this.unidade != null) {
            if (this.unidade.getId() > 0) {
                listaArea = new UnidadeAreaController().getAreasUnidade(this.unidade.getId());
            } else {
                initListaArea();
            }
        }
    }

    public void addAreaNaLista(UnidadeAreaViewModel areaDaUnidade) {
        if (this.unidade != null && this.unidade.getId() > 0 && areaDaUnidade != null) {
            if (listaArea == null) {
                initListaArea();
            }

            listaArea.add(areaDaUnidade);
            ordenarLista();
        }
    }

    private void ordenarLista() {
        Collections.sort(this.listaArea,
                (UnidadeAreaViewModel obj1, UnidadeAreaViewModel obj2)
                -> obj1.getArea().getNome().compareToIgnoreCase(obj2.getArea().getNome())
        );
    }

    public void removerAreaDaLista(UnidadeAreaViewModel areaDaUnidade) {
        if (this.unidade != null) {
            try {
                listaArea.remove(getPosicaoAreaNaLista(areaDaUnidade));
            } catch (IndexOutOfBoundsException ex) {
                //não excluído
            }
        }
    }

    public void atualizarAreaNaLista(UnidadeAreaViewModel areaDaUnidade) {
        if (this.unidade != null) {
            int posicao = getPosicaoAreaNaLista(areaDaUnidade);
            try {
                this.listaArea.set(posicao, areaDaUnidade);
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                // item não atualizado
            }
        }
    }

    public int getPosicaoAreaNaLista(UnidadeAreaViewModel areaDaUnidade) {
        try {
            return this.listaArea.indexOf(areaDaUnidade);
        } catch (NullPointerException | ClassCastException ex) {
            return -1;
        }
    }

    private void initListaArea() {
        listaArea = new ArrayList<>();
    }
    
    public List<UnidadeAreaViewModel> getListaAreas() {
        return this.listaArea;
    }

}
