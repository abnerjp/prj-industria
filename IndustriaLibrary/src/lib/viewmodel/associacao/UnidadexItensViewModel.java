package lib.viewmodel.associacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.cadastro.UnidadeController;
import lib.controller.associacao.UnidadeItemController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 *
 * @author abnerjp
 */
public final class UnidadexItensViewModel {

    private UnidadeViewModel unidade;
    private List<UnidadeItemViewModel> listaItem;

    public UnidadexItensViewModel(int idUnidade) {
        setUnidade(idUnidade);
    }

    public UnidadexItensViewModel(UnidadeViewModel unidade) {
        setUnidade(unidade);
    }

    public UnidadeViewModel getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeViewModel unidade) {
        this.unidade = unidade;
        atualizaItens();
    }

    public void setUnidade(int idUnidade) {
        this.unidade = idUnidade > 0 ? getUnidadeViewModel(idUnidade) : null;
        atualizaItens();
    }

    public List<UnidadeItemViewModel> getItens() {
        return listaItem;
    }
    
    public void addItemNaLista(UnidadeItemViewModel itemDaUnidade) {
        if (unidade != null && unidade.getId() > 0 && itemDaUnidade != null) {
            if (listaItem == null) {
                initListaItens();
            }
            listaItem.add(itemDaUnidade);
            ordenarListaItens();
        }
    }

    public void removeItemDaLista(UnidadeItemViewModel itemDaUnidade) {
        if (unidade != null) {
            listaItem.remove(getPosicaoItemNaLista(itemDaUnidade));
        }
    }

    public void atualizarItemNaLista(UnidadeItemViewModel itemDaUnidade) {
        if (unidade != null) {
            int posicao = getPosicaoItemNaLista(itemDaUnidade);
            if (posicao > -1) {
                listaItem.set(posicao, itemDaUnidade);
            }
        }
    }

    public int getPosicaoItemNaLista(UnidadeItemViewModel item) {
        int indexItem = 0;
        if (listaItem != null && item != null) {
            while (indexItem < listaItem.size() && item.getId() != listaItem.get(indexItem).getId()) {
                indexItem++;
            }
            return indexItem < listaItem.size() ? indexItem-- : -1;
        }
        return -1;
    }

    private void ordenarListaItens() {
        Collections.sort(
                listaItem, 
                (UnidadeItemViewModel obj1, UnidadeItemViewModel obj2) -> 
                        obj1.getItem().getNome().compareTo(obj2.getItem().getNome())
        );
    }

    private void initListaItens() {
        listaItem = new ArrayList<>();
    }

    private void atualizaItens() {
        if (unidade != null) {
            if (unidade.getId() > 0) {
                listaItem = new UnidadeItemController().getItensUnidade(unidade.getId());
            } else {
                initListaItens();
            }
        }
    }

    private UnidadeViewModel getUnidadeViewModel(int id) {
        return new UnidadeController().regPorCodigo(id, false);
    }
}
