/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.viewmodel.cadastro;

import lib.controller.cadastro.CidadeController;

/**
 *
 * @author abner.jacomo
 */
public class EnderecoViewModel {

    private int id;
    private String logradouro;
    private String cep;
    private String bairro;
    private String observacao;
    private CidadeViewModel cidade;

    public EnderecoViewModel(int id, String logradouro, String cep, String bairro, String observacao, int idCidade) {
        this.id = id;
        this.logradouro = logradouro;
        this.cep = cep;
        this.bairro = bairro;
        this.observacao = observacao;
        this.cidade = idCidade > 0 ? getCidadeViewModel(idCidade) : null;
    }

    public EnderecoViewModel(String logradouro, String cep, String bairro, String observacao, int idCidade) {
        this(0, logradouro, cep, bairro, observacao, idCidade);
    }

    public EnderecoViewModel() {
        this("", "", "", "", 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public String getCepFormatado() {
        if (cep != null && cep.length() == 8) {
            return cep.substring(0, 5) + "-" + cep.substring(5);
        }
        return "00000-000";
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public CidadeViewModel getCidade() {
        return cidade;
    }

    public void setCidade(CidadeViewModel cidade) {
        this.cidade = cidade;
    }

    public String getEnderecoFormatado() {
        String str = logradouro;
        str += str != null && str.length() > 0 ? ", " : "";
        str += bairro;
        return str;
    }

    private CidadeViewModel getCidadeViewModel(int id) {
        return new CidadeController().regPorCodigo(id);
    }

    @Override
    public String toString() {
        return getEnderecoFormatado();
    }
}
