package lib.viewmodel.cadastro;

import lib.controller.cadastro.MedidaController;

/**
 *
 * @author abner.jacomo
 */
public class ItemViewModel {

    private int id;
    private String nome;
    private boolean ativo;
    private String observacao;
    private MedidaViewModel medida;

    public ItemViewModel(int id, String nome, boolean ativo, String observacao, int idMedida) {
        this.id = id;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
        this.medida = idMedida > 0 ? getMedidaViewModel(idMedida) : null;
    }

    public ItemViewModel(String nome, boolean ativo, String observacao, int codMedida) {
        this(0, nome, ativo, observacao, codMedida);
    }

    public ItemViewModel() {
        this("", false, "", 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public MedidaViewModel getMedida() {
        return medida;
    }

    public void setMedida(MedidaViewModel medida) {
        this.medida = medida;
    }

    private MedidaViewModel getMedidaViewModel(int id) {
        return new MedidaController().regPorCodigo(id);
    }

    @Override
    public String toString() {
        return nome;
    }
}
