package lib.viewmodel.cadastro;

/**
 *
 * @author abnerjp
 */
public class PerdaViewModel {

    private int id;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public PerdaViewModel(int id, String nome, String sigla, boolean ativo, String observacao) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public PerdaViewModel(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);
    }

    public PerdaViewModel() {
        this("", "", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getPerdaFormatada() {
        return getSigla() + " - " + getNome();
    }

    @Override
    public String toString() {
        return getPerdaFormatada();
    }

}
