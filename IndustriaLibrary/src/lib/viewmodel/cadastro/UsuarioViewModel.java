package lib.viewmodel.cadastro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lib.controller.associacao.UsuarioEnderecoController;
import lib.viewmodel.associacao.UsuarioEnderecoViewModel;

/**
 *
 * @author abnerjp
 */
public class UsuarioViewModel {

    private int id;
    private String nome;
    private String sexo;
    private String login;
    private String senha;
    private String email;
    private String imagem;
    private boolean ativo;
    private String observacao;
    private UsuarioEnderecoViewModel enderecoAtual;
    private List<UsuarioEnderecoViewModel> historicoEndereco;

    public UsuarioViewModel(int id, String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao, UsuarioEnderecoViewModel enderecoAtual, List<UsuarioEnderecoViewModel> historicoEndereco) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.login = login;
        this.senha = senha;
        this.email = email;
        this.imagem = imagem;
        this.ativo = ativo;
        this.observacao = observacao;
        this.enderecoAtual = enderecoAtual;
        this.historicoEndereco = historicoEndereco;
    }

    public UsuarioViewModel(String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao, UsuarioEnderecoViewModel enderecoAtual, List<UsuarioEnderecoViewModel> historicoEndereco) {
        this(0, nome, sexo, login, senha, email, imagem, ativo, observacao, enderecoAtual, historicoEndereco);
    }

    public UsuarioViewModel(String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao, UsuarioEnderecoViewModel enderecoAtual) {
        this(nome, sexo, login, senha, email, imagem, ativo, observacao, enderecoAtual, null);
    }

    public UsuarioViewModel(String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao) {
        this(nome, sexo, login, senha, email, imagem, ativo, observacao, null);
    }

    public UsuarioViewModel(int id, String nome, String sexo, String login, String senha, String email, String imagem, boolean ativo, String observacao) {
        this(id, nome, sexo, login, senha, email, imagem, ativo, observacao, null, null);
    }

    public UsuarioViewModel() {
        this("", "", "", "", "", "", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public UsuarioEnderecoViewModel getEnderecoAtual() {
        if (enderecoAtual == null) {
            atualizarEnderecoAtual();
        }
        return enderecoAtual;
    }

    private void atualizarEnderecoAtual() {
        enderecoAtual = new UsuarioEnderecoController().getEnderecoAtual(id);
    }

    public void setEnderecoAtual(UsuarioEnderecoViewModel enderecoAtual) {
        this.enderecoAtual = enderecoAtual;
    }

    public List<UsuarioEnderecoViewModel> getHistoricoEndereco() {
        return historicoEndereco;
    }

    public void setHistoricoEndereco(List<UsuarioEnderecoViewModel> historicoEndereco) {
        this.historicoEndereco = historicoEndereco;
        if (historicoEndereco != null && !historicoEndereco.isEmpty()) {
            enderecoAtual = historicoEndereco.get(historicoEndereco.size() - 1);
        }
    }

    public void addEndereco(UsuarioEnderecoViewModel end) {
        if (historicoEndereco == null) {
            initHistoricoEndereco();
        }
        historicoEndereco.add(end);
        ordenarHistoricoEndereco();
        enderecoAtual = historicoEndereco.get(historicoEndereco.size() - 1);
    }

    //inicia a lista de histórico de endereços
    private void initHistoricoEndereco() {
        historicoEndereco = new ArrayList<>();
    }

    public void removeEndereco(UsuarioEnderecoViewModel end) {
        if (historicoEndereco != null && historicoEndereco.size() > 0) {
            historicoEndereco.remove(end);
            try {
                enderecoAtual = historicoEndereco.get(historicoEndereco.size() - 1);
            } catch (IndexOutOfBoundsException ex) {
                enderecoAtual = null;
            }
        }
    }

    private void ordenarHistoricoEndereco() {
        Collections.sort(historicoEndereco, (UsuarioEnderecoViewModel data1, UsuarioEnderecoViewModel data2) -> data1.getData().compareTo(data2.getData()));
    }

    public String getNomeAbreviado() {
        String str = getNome();
        try {
            return str.substring(0, 10) + "...";
        } catch (IndexOutOfBoundsException ex) {
            return str.substring(0, str.length());
        }
    }

    @Override
    public String toString() {
        return getNome();
    }

}
