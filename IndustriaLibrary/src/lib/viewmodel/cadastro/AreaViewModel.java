package lib.viewmodel.cadastro;

/**
 *
 * @author abnerjp
 */
public class AreaViewModel {

    private int id;
    private String nome;
    private String descricao;
    private boolean ativo;
    private String observacao;

    public AreaViewModel(int id, String nome, String descricao, boolean ativo, String observacao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public AreaViewModel(String nome, String descricao, boolean ativo, String observacao) {
        this(0, nome, descricao, ativo, observacao);
    }

    public AreaViewModel() {
        this("", "", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public String toString() {
        return nome;
    }
}
