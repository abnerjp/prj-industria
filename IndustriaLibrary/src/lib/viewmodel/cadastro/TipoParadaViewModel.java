package lib.viewmodel.cadastro;

/**
 *
 * @author abner.jacomo
 */
public class TipoParadaViewModel {

    private int id;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public TipoParadaViewModel(int id, String nome, String sigla, boolean ativo, String observacao) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public TipoParadaViewModel(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);
    }

    public TipoParadaViewModel() {
        this("", "", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getTipoParadaFormatado() {
        return sigla + " - " + nome;
    }

    @Override
    public String toString() {
        return getTipoParadaFormatado();
    }

}
