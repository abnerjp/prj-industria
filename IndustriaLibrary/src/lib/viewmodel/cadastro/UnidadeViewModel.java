package lib.viewmodel.cadastro;

import lib.viewmodel.associacao.UnidadeEnderecoViewModel;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author abner.jacomo
 */
public class UnidadeViewModel {

    private int id;
    private String razaoSocial;
    private String cnpj;
    private String inscricaoEstadual;
    private String telefoneFixo;
    private String telefoneCelular;
    private String email;
    private String nomeFantasia;
    private String referencia;
    private String senha;
    private boolean ativo;
    private String observacao;
    private UnidadeEnderecoViewModel enderecoAtual;
    private List<UnidadeEnderecoViewModel> historicoEndereco;

    public UnidadeViewModel(int id, String razaoSocial, String cnpj, String inscricaoEstadual,
            String telefoneFixo, String telefoneCelular, String email, String nomeFantasia,
            String referencia, String senha, boolean ativo, String observacao, UnidadeEnderecoViewModel enderecoAtual, List<UnidadeEnderecoViewModel> historicoEndereco) {
        this.id = id;
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.telefoneFixo = telefoneFixo;
        this.telefoneCelular = telefoneCelular;
        this.email = email;
        this.nomeFantasia = nomeFantasia;
        this.referencia = referencia;
        this.senha = senha;
        this.ativo = ativo;
        this.observacao = observacao;
        this.enderecoAtual = enderecoAtual;
        this.historicoEndereco = historicoEndereco;
    }

    public UnidadeViewModel(String razaoSocial, String cnpj, String inscricaoEstadual,
            String telefoneFixo, String telefoneCelular, String email, String nomeFantasia,
            String referencia, String senha, boolean ativo, String observacao, UnidadeEnderecoViewModel enderecoAtual, List<UnidadeEnderecoViewModel> historicoEndereco) {
        this(0, razaoSocial, cnpj, inscricaoEstadual, telefoneFixo, telefoneCelular, email, nomeFantasia, referencia, senha, ativo, observacao, enderecoAtual, historicoEndereco);
    }

    public UnidadeViewModel(String razaoSocial, String cnpj, String inscricaoEstadual,
            String telefoneFixo, String telefoneCelular, String email, String nomeFantasia,
            String referencia, String senha, boolean ativo, String observacao, UnidadeEnderecoViewModel enderecoAtual) {
        this(razaoSocial, cnpj, inscricaoEstadual, telefoneFixo, telefoneCelular, email, nomeFantasia, referencia, senha, ativo, observacao, enderecoAtual, null);
    }

    public UnidadeViewModel(String razaoSocial, String cnpj, String inscricaoEstadual,
            String telefoneFixo, String telefoneCelular, String email, String nomeFantasia,
            String referencia, String senha, boolean ativo, String observacao) {
        this(razaoSocial, cnpj, inscricaoEstadual, telefoneFixo, telefoneCelular, email, nomeFantasia, referencia, senha, ativo, observacao, null);
    }

    public UnidadeViewModel(int id, String razaoSocial, String cnpj, String inscricaoEstadual,
            String telefoneFixo, String telefoneCelular, String email, String nomeFantasia,
            String referencia, String senha, boolean ativo, String observacao) {
        this(id, razaoSocial, cnpj, inscricaoEstadual, telefoneFixo, telefoneCelular, email, nomeFantasia, referencia, senha, ativo, observacao, null, null);
    }

    public UnidadeViewModel() {
        this("", "", "", "", "", "", "", "", "", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        if (cnpj != null) {
            return cnpj;
        } else {
            return "";
        }
    }

    public String getCnpjFormatado() {
        if (cnpj != null) {
            try {
                MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
                mask.setValueContainsLiteralCharacters(false);
                return mask.valueToString(cnpj);
            } catch (ParseException ex) {
            }
        }
        return getCnpj();
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getTelefoneFixo() {
        return telefoneFixo;
    }

    public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = telefoneFixo;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public UnidadeEnderecoViewModel getEnderecoAtual() {
        return enderecoAtual;
    }

    public void setEnderecoAtual(UnidadeEnderecoViewModel enderecoAtual) {
        this.enderecoAtual = enderecoAtual;
    }

    public List<UnidadeEnderecoViewModel> getHistoricoEndereco() {
        return historicoEndereco;
    }

    public void setHistoricoEndereco(List<UnidadeEnderecoViewModel> historicoEndereco) {
        this.historicoEndereco = historicoEndereco;
        if (historicoEndereco != null && !historicoEndereco.isEmpty()) {
            ordenarHistoricoEndereco();
            enderecoAtual = historicoEndereco.get(historicoEndereco.size() - 1);
        }
    }

    public void addEndereco(UnidadeEnderecoViewModel end) {
        if (historicoEndereco == null) {
            initHistoricoEndereco();
        }
        historicoEndereco.add(end);
        ordenarHistoricoEndereco();
        enderecoAtual = historicoEndereco.get(historicoEndereco.size() - 1);
    }

    public void removeEndereco(UnidadeEnderecoViewModel end) {
        if (historicoEndereco != null && historicoEndereco.size() > 0) {
            historicoEndereco.remove(end);
            try {
                enderecoAtual = historicoEndereco.get(historicoEndereco.size() - 1);
            } catch (IndexOutOfBoundsException ex) {
                enderecoAtual = null;
            }
        }
    }

    private void ordenarHistoricoEndereco() {
        Collections.sort(historicoEndereco, (UnidadeEnderecoViewModel data1, UnidadeEnderecoViewModel data2) -> data1.getData().compareTo(data2.getData()));
    }

    //inicia a lista de histórico de endereços
    private void initHistoricoEndereco() {
        historicoEndereco = new ArrayList<>();
    }

    public String getNomeFormatado() {
        String descricao = "";

        if (referencia != null && referencia.length() > 0) {
            descricao += descricao.length() > 0 ? " - " + referencia : referencia;
        }
        if (nomeFantasia != null && nomeFantasia.length() > 0) {
            descricao += descricao.length() > 0 ? " - " + nomeFantasia : nomeFantasia;
        }
        return descricao;
    }

    @Override
    public String toString() {
        return nomeFantasia;
    }
}
