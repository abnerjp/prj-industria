package lib.viewmodel.cadastro;

import lib.controller.cadastro.EstadoController;

/**
 *
 * @author abner.jacomo
 */
public class CidadeViewModel {

    private int id;
    private String nome;
    private boolean ativo;
    private String observacao;
    private EstadoViewModel estado;

    public CidadeViewModel(int id, String nome, boolean ativo, String observacao, int idEstado) {
        this.id = id;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
        this.estado = idEstado > 0 ? getEstadoViewModel(idEstado) : null;
    }

    public CidadeViewModel(String nome, boolean ativo, String observacao, int idEstado) {
        this(0, nome, ativo, observacao, idEstado);
    }

    public CidadeViewModel() {
        this("", false, "", 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public EstadoViewModel getEstado() {
        return estado;
    }

    public void setEstado(EstadoViewModel estado) {
        this.estado = estado;
    }

    private EstadoViewModel getEstadoViewModel(int id) {
        return new EstadoController().regPorCodigo(id);
    }

    public String getCidadeUf() {
        if (estado != null && nome.length() > 0) {
            return nome + "/" + estado.getSigla();
        }
        return "";
    }

    @Override
    public String toString() {
        return nome;
    }
}
