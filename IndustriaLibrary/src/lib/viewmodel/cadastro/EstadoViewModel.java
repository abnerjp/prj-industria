package lib.viewmodel.cadastro;

import lib.controller.cadastro.PaisController;

/**
 *
 * @author abner.jacomo
 */
public class EstadoViewModel {

    private int id;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;
    private PaisViewModel pais;

    public EstadoViewModel(int id, String nome, String sigla, boolean ativo, String observacao, int codPais) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
        this.pais = codPais > 0 ? getPaisPorCodigo(codPais) : null;
    }

    public EstadoViewModel(String nome, String sigla, boolean ativo, String observacao, int codPais) {
        this(0, nome, sigla, ativo, observacao, codPais);
    }

    public EstadoViewModel() {
        this("", "", false, "", 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public PaisViewModel getPais() {
        return pais;
    }

    public void setPais(PaisViewModel pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        return nome + "/" + sigla;
    }

    private PaisViewModel getPaisPorCodigo(int idPais) {
        return new PaisController().regPorCodigo(idPais);
    }
}
