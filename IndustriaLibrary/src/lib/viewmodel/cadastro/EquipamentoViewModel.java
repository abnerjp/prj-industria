package lib.viewmodel.cadastro;

/**
 *
 * @author abnerjp
 */
public class EquipamentoViewModel {

    private int id;
    private String nome;
    private String modelo;
    private boolean ativo;
    private String observacao;

    public EquipamentoViewModel(int id, String nome, String modelo, boolean ativo, String observacao) {
        this.id = id;
        this.nome = nome;
        this.modelo = modelo;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public EquipamentoViewModel(String nome, String modelo, boolean ativo, String observacao) {
        this(0, nome, modelo, ativo, observacao);
    }

    public EquipamentoViewModel() {
        this("", "", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getEquipamentoFormatado() {
        return getNome() + ": " + getModelo();
    }
    
    @Override
    public String toString() {
        return getEquipamentoFormatado();
    }
}
