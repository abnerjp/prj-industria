package lib.viewmodel.cadastro;

/**
 *
 * @author abnerjp
 */
public class FuncaoViewModel {

    private int id;
    private String nome;
    private boolean ativo;
    private String observacao;

    public FuncaoViewModel(int id, String nome, boolean ativo, String observacao) {
        this.id = id;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public FuncaoViewModel(String nome, boolean ativo, String observacao) {
        this(0, nome, ativo, observacao);
    }

    public FuncaoViewModel() {
        this("", false, "");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public String isAtivoToString() {
        return ativo ? "Ativo" : "Desativado";
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public String toString() {
        return nome;
    }

}
