package lib.util;

/**
 *
 * @author abner.jacomo
 */
public class Texto {

    /**
     * Remover todos espaços de uma string
     * @param texto
     * @return 
     */
    public static String trimAll(String texto) {
        try {
            return texto.replaceAll(" ", "");
        } catch (NullPointerException ex) {
            // argumento nulo
        }
        return "";
    }
}
