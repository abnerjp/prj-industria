
package lib.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author abnerjp
 */
public class Data {
    
    public static String dataFormatada(LocalDate data) {
        String dataFormatada = "";
        if (data != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            dataFormatada = data.format(formatter);
        }
        return dataFormatada;
    }
}
