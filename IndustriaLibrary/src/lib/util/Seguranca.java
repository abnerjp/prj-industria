package lib.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author abnerjp
 */
public class Seguranca {

    public static boolean Iguais(String strNova, String strCriptografada) {
        try {
            return strCriptografada.equals(criptografar(strNova));
        } catch (Exception ex) {
            return false;
        }
    }

    public static String criptografar(String str) throws Exception {
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
            byte messageDigest[] = algorithm.digest(str.getBytes("UTF-8"));

            StringBuilder hexString = new StringBuilder();
            for (byte b : messageDigest) {
                hexString.append(String.format("%02X", 0xFF & b));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new Exception("str não criptografada, erro: " + ex.getMessage());
        }
    }
}
