package industria.entidade;

/**
 *
 * @author abnerjp
 */
public class Funcao {
    private int codigo;
    private String nome;
    private boolean ativo;
    private String observacao;

    public Funcao(int codigo, String nome, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public Funcao(String nome, boolean ativo, String observacao) {
        this(0, nome, ativo, observacao);
    }

    public Funcao() {
        this("", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int id) {
        this.codigo = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    @Override
    public String toString() {
        return nome;
    }
    
}
