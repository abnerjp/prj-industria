package industria.entidade;

/**
 *
 * @author abnerjp
 */
public class SubMotivo {
    
    private int codigo;
    private String nome;
    private String sigla;
    private boolean ativo;
    private String observacao;

    public SubMotivo(int codigo, String nome, String sigla, boolean ativo, String observacao) {
        this.codigo = codigo;
        this.nome = nome;
        this.sigla = sigla;
        this.ativo = ativo;
        this.observacao = observacao;
    }

    public SubMotivo(String nome, String sigla, boolean ativo, String observacao) {
        this(0, nome, sigla, ativo, observacao);

    }

    public SubMotivo() {
        this("", "", false, "");
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    @Override
    public String toString() {
        return nome;
    }
    
}
