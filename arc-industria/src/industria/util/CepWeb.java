package industria.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author abnerjp
 */
public class CepWeb {
    public static String consultaCep(String cep, String formato)
    {
        StringBuilder dados = new StringBuilder();
        try {
            URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=" + cep + "&formato="+formato);
            URLConnection con = url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setAllowUserInteraction(false);
            InputStream in = con.getInputStream();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
                String s = "";
                while (null != (s = br.readLine()))
                    dados.append(s);
            }
        } catch (IOException ex) {
            dados.append("");
            System.out.println(ex);
        }
        return dados.toString();
    }
}
