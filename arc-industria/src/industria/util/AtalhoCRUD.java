package industria.util;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * classe para auxiliar nos atalhos de teclado para CRUD
 *
 * @author abnerjp
 */
public class AtalhoCRUD {
    
    // NOVO........CTRL+N
    // DELETE......CTRL+DEL
    // DUPLICAR....CTRL+V
    // CANCELAR....ESC
    // ALTERAR.....F2
    // BUSCAR......CTRL+F
    // BUSCAR AUX..F5
    // SALVAR......CTRL+S

    public AtalhoCRUD() {
    }

    /* CTRL N*/
    public boolean isNovo(KeyEvent e) {
        return e.isControlDown() && e.getCode() == KeyCode.N;
    }

    /* CTRL DELETE */
    public boolean isExcluir(KeyEvent e) {
        //if (e.isControlDown()) {
        //    return e.getCode() == KeyCode.DELETE;
        //}
        return e.isControlDown() && e.getCode() == KeyCode.DELETE;
    }

    /* CTRL V */
    public boolean isDuplicar(KeyEvent e) {
        //if (e.isControlDown()) {
        //    return e.getCode() == KeyCode.V;
        //}
        return e.isControlDown() && e.getCode() == KeyCode.V;
    }

    /* Esc */
    public boolean isCancelar(KeyEvent e) {
        return e.getCode() == KeyCode.ESCAPE;
    }

    /* F2 */
    public boolean isAlterar(KeyEvent e) {
        return e.getCode() == KeyCode.F2;
    }

    /* CTRL F */
    public boolean isBusca(KeyEvent e) {
        //if (e.isControlDown()) {
        //    return e.getCode() == KeyCode.F;
        //}
        return e.isControlDown() && e.getCode() == KeyCode.F;
    }

    /* F5 */
    public boolean isBuscaAuxiliar(KeyEvent e) {
        return e.getCode() == KeyCode.F5;
    }
    
    /* CTRL S */
    public boolean isSalvar(KeyEvent e) {
        //if (e.isControlDown()) {
        //    return e.getCode() == KeyCode.S;
        //}
        return e.isControlDown() && e.getCode() == KeyCode.S;
    }

}
