/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package industria.util;

import industria.controller.util.MessageBox;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author abner.jacomo
 */
public class MsgBox {

    public static void exibeLista(List<String> listaParaExibir) {
        try {
            Tela tl = new Tela("/industria/view/util/MessageBox.fxml", "", false, false, false);
            MessageBox controller = tl.getLoader().getController();
            controller.setInformacoes(listaParaExibir);
            tl.exibirTelaFilha();
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela de pesquisa " + ex.getMessage());
        }
    }
}
