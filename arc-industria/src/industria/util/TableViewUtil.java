/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package industria.util;

import javafx.scene.control.TableView;

/**
 *
 * @author abnerjp
 */
public class TableViewUtil {

    public static void selecionarLinhaTabela(TableView table, int index) {
        try {
            if (index < table.getItems().size()) {
                table.getSelectionModel().clearAndSelect(index);
                table.scrollTo(index);
            }
        } catch (Exception e) {
            // sem ação
        }
    }

}
