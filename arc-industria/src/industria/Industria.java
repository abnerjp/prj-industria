package industria;

import industria.util.Tela;
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author abnerjp
 */
public class Industria extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException {
        Tela tl = new Tela("/industria/view/associacao/CadUnidadeEquipamentoComArea.fxml", "", false, true, true);
        try {
            tl.exibirPrimeiraTela();
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
