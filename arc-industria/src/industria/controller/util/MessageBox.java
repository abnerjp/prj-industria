package industria.controller.util;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author abner.jacomo
 */
public class MessageBox implements Initializable {

    private double cx, cy;
    private List<String> listaInformacoes;

    @FXML
    private JFXTabPane tpInformacao;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<String> tbvInformacao;
    @FXML
    private TableColumn<String, String> tcInformacao;
    @FXML
    private JFXButton btnSair;

    public MessageBox() {
        listaInformacoes = new ArrayList();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    private void carregaTabela(List<String> lista) {
        ObservableList<String> obsLista;
        tbvInformacao.getItems().clear();

        tcInformacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvInformacao.setItems(obsLista);
        selecionarLinhaTabela(tbvInformacao, -1);
    }

    private void selecionarLinhaTabela(TableView table, int index) {
        if (index < table.getItems().size()) {
            table.getSelectionModel().select(index);
            table.scrollTo(index);
        }
    }

    @FXML
    private void clkFechar(ActionEvent event) {
        sair();
    }

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    public void setInformacoes(List<String> listaInformacoes) {
        try {
            this.listaInformacoes.addAll(listaInformacoes);
        } catch (NullPointerException ex) {
            this.listaInformacoes = new ArrayList();
        }
        carregaTabela(listaInformacoes);
    }

}
