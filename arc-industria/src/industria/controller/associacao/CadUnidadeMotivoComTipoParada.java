package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadeMotivoComTipoParadaController;
import lib.controller.cadastro.UnidadeController;
import lib.util.Texto;
import lib.viewmodel.associacao.UnidadeMotivoComTipoParadaViewModel;
import lib.viewmodel.associacao.UnidadexMotivosComTiposParadaViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadeMotivoComTipoParada implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeMotivoComTipoParadaController controllerUnidadeMotivoComTipoParada = new UnidadeMotivoComTipoParadaController();

    private UnidadexMotivosComTiposParadaViewModel vmUnidadeVinculosTipoComMotivo = null;
    private int idUnidadeMotivoComTipoParada = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeMotivoComTipoParadaViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeMotivoComTipoParadaViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeMotivoComTipoParadaViewModel, String> tcTipoParada;
    @FXML
    private TableColumn<UnidadeMotivoComTipoParadaViewModel, String> tcMotivoParada;
    @FXML
    private TableColumn<UnidadeMotivoComTipoParadaViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtNomeVinculoObs;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidadeVinculosTipoComMotivo = new UnidadexMotivosComTiposParadaViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizaInterface(vmUnidadeVinculosTipoComMotivo);
        botoes(true);
    }

    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadeMotivoComTipoParadaViewModel>() {
                @Override
                public void updateItem(UnidadeMotivoComTipoParadaViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidadeVinculosTipoComMotivo = new UnidadexMotivosComTiposParadaViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizaInterface(vmUnidadeVinculosTipoComMotivo);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidadeVinculosTipoComMotivo.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidadeVinculosTipoComMotivo.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeVinculosTipoComMotivo = new UnidadexMotivosComTiposParadaViewModel(unAux);
            }
            atualizaInterface(vmUnidadeVinculosTipoComMotivo);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidadeVinculosTipoComMotivo.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidadeVinculosTipoComMotivo.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeVinculosTipoComMotivo = new UnidadexMotivosComTiposParadaViewModel(unAux);
            }
            atualizaInterface(vmUnidadeVinculosTipoComMotivo);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUnidadeVinculosTipoComMotivo = new UnidadexMotivosComTiposParadaViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizaInterface(vmUnidadeVinculosTipoComMotivo);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        atualizaUnidade(unidadeAux);
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        atualizaUnidade(codigoAux);
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadeMotivoComTipoParadaViewModel vmVinculoAux = tbvDados.getSelectionModel().getSelectedItem();
            if (vmVinculoAux != null) {
                Alerta dialog = new Alerta("Vínculo de parada", "Dados do vínculo",
                        "Data do vínculo: " + vmVinculoAux.getDataFormatada()
                        + "\nStatus do vínculo: " + vmVinculoAux.isAtivoToString()
                        + "\n\nObservação: \n" + vmVinculoAux.getObservacao()
                );
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        List<UnidadeMotivoComTipoParadaViewModel> listaVinculoAux;

        if (vmUnidadeVinculosTipoComMotivo != null && vmUnidadeVinculosTipoComMotivo.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/associacao/CadVinculoTipoParadaMotivoParada.fxml", "", false, true, false);
                CadVinculoTipoParadaMotivoParada controller = tl.getLoader().getController();
                controller.setParametrosIniciais(vmUnidadeVinculosTipoComMotivo.getIdUnidade(), vmUnidadeVinculosTipoComMotivo.getListaVinculos());
                tl.exibirTelaFilha();// exibe a tela
                listaVinculoAux = controller.getNovosVinculos();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                listaVinculoAux = new ArrayList<>();
                System.out.println("erro ao exibir janela de pesquisa " + ex.getMessage());
            }

            if (listaVinculoAux.size() > 0) {
                statusGravacao = controllerUnidadeMotivoComTipoParada.gravar(listaVinculoAux);

                if (statusGravacao > 0) {
                    //não gravado
                    exibeAlerta(statusGravacao);
                } else {
                    //gravado com sucesso
                    vmUnidadeVinculosTipoComMotivo.recarregarVinculos();

                    carregaTabela(vmUnidadeVinculosTipoComMotivo.getListaVinculos());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
                }
            }
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeMotivoComTipoParadaViewModel vmVinculoParadaAux = tbvDados.getSelectionModel().getSelectedItem();
        if (vmVinculoParadaAux != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Vínculo de parada da unidade", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidadeMotivoComTipoParada.excluir(vmVinculoParadaAux) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    vmUnidadeVinculosTipoComMotivo.removeVinculoDaLista(vmVinculoParadaAux);
                    carregaTabela(vmUnidadeVinculosTipoComMotivo.getListaVinculos());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadeMotivoComTipoParadaViewModel vmVinculoParadaAux = tbvDados.getSelectionModel().getSelectedItem();
        if (vmVinculoParadaAux != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Vínculo de parada da unidade", "", "");
            if (vmVinculoParadaAux.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar este vínculo de parada inativo para a unidade?");
            } else {
                dialog.setInformacao("Ativar registro", "tornar este vínculo de parada ativo para a unidade?");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                vmVinculoParadaAux.setAtivo(!vmVinculoParadaAux.isAtivo());
                int status = controllerUnidadeMotivoComTipoParada.alterar(vmVinculoParadaAux);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidadeVinculosTipoComMotivo.atualizarVinculoNaLista(vmVinculoParadaAux);
                    carregaTabela(vmUnidadeVinculosTipoComMotivo.getListaVinculos());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeVinculosTipoComMotivo.getPosicaoVinculoNaLista(vmVinculoParadaAux));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadeMotivoComTipoParadaViewModel vmVinculoParadaAux = tbvDados.getSelectionModel().getSelectedItem();
        if (vmVinculoParadaAux != null) {
            atualizaInterfaceAlteracao(vmVinculoParadaAux);
            botoes(false);
        }
    }

    @FXML// ok
    private void clkBuscarNaLista(ActionEvent event) {
        Alerta dialog = new Alerta("Vínculo de parada", "", "");
        String retorno;

        retorno = dialog.caixaTexto("nome tipo/motivo");
        retorno = Texto.trimAll(retorno);
        if (retorno.length() > 0 && vmUnidadeVinculosTipoComMotivo != null && vmUnidadeVinculosTipoComMotivo.getListaVinculos() != null) {
            int pos = 0;
            while (pos < vmUnidadeVinculosTipoComMotivo.getListaVinculos().size()
                    && !Texto.trimAll(vmUnidadeVinculosTipoComMotivo.getSiglaComNomeConcatenadoDaLista(pos).toLowerCase()).contains(retorno.toLowerCase())) {
                pos++;
            }
            if (pos < vmUnidadeVinculosTipoComMotivo.getListaVinculos().size()) {
                TableViewUtil.selecionarLinhaTabela(tbvDados, pos);
            }
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadeMotivoComTipoParadaViewModel vmVinculoUnidadeMotivoComTipoParada = controllerUnidadeMotivoComTipoParada.getRegPorCodigoDoVinculo(idUnidadeMotivoComTipoParada);
        int statusGravacao;
        if (vmUnidadeVinculosTipoComMotivo != null && vmVinculoUnidadeMotivoComTipoParada != null) {
            if (vmVinculoUnidadeMotivoComTipoParada.isAtivo() != rbAtivo.isSelected() || !vmVinculoUnidadeMotivoComTipoParada.getObservacao().equals(txtObservacao.getText())) {
                vmVinculoUnidadeMotivoComTipoParada.setAtivo(rbAtivo.isSelected());
                vmVinculoUnidadeMotivoComTipoParada.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadeMotivoComTipoParada.alterar(vmVinculoUnidadeMotivoComTipoParada);
                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    vmUnidadeVinculosTipoComMotivo.atualizarVinculoNaLista(vmVinculoUnidadeMotivoComTipoParada);
                    atualizaInterface(vmUnidadeVinculosTipoComMotivo);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeVinculosTipoComMotivo.getPosicaoVinculoNaLista(vmVinculoUnidadeMotivoComTipoParada));
                    limparCampos();
                    botoes(true);
                }
            } else {
                limparCampos();
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Vínculo de Motivo parada com Tipo de parada - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1603:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Tipo de parada", "tipo de parada da unidade não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1604:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Motivo de parada", "motivo de parada da unidade não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1605:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1606:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1607:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1508:
                dialog.setInformacao("Duplicidade", "vínculo ja registrado para esta unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaUnidade(UnidadeViewModel unidade) {
        UnidadexMotivosComTiposParadaViewModel unAux = new UnidadexMotivosComTiposParadaViewModel(unidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeVinculosTipoComMotivo = unAux;
            atualizaInterface(vmUnidadeVinculosTipoComMotivo);
            botoes(true);
        }
    }

    private void atualizaUnidade(int codUnidade) {
        UnidadexMotivosComTiposParadaViewModel unAux = new UnidadexMotivosComTiposParadaViewModel(codUnidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeVinculosTipoComMotivo = unAux;
            atualizaInterface(vmUnidadeVinculosTipoComMotivo);
            botoes(true);
        }
    }

    private void atualizaInterface(UnidadexMotivosComTiposParadaViewModel unidadeComVinculos) {
        if (unidadeComVinculos != null && unidadeComVinculos.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComVinculos.getUnidade().getId()));
            txtDescricao.setText(unidadeComVinculos.getUnidade().getNomeFormatado());
            if (unidadeComVinculos.getListaVinculos() != null) {
                carregaTabela(unidadeComVinculos.getListaVinculos());
            }
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizaInterfaceAlteracao(UnidadeMotivoComTipoParadaViewModel vmVinculoUnidadeMotivoComParada) {
        try {
            idUnidadeMotivoComTipoParada = vmVinculoUnidadeMotivoComParada.getId();
            txtNomeVinculoObs.setText(vmVinculoUnidadeMotivoComParada.getSiglaVinculoConcatenado());
            txtDataObs.setText(vmVinculoUnidadeMotivoComParada.getDataFormatada());
            if (vmVinculoUnidadeMotivoComParada.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmVinculoUnidadeMotivoComParada.getObservacao());
        } catch (NullPointerException ex) {
            limparCampos();
        }
    }

    private void limparCampos() {
        this.idUnidadeMotivoComTipoParada = 0;
        txtNomeVinculoObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void carregaTabela(List<UnidadeMotivoComTipoParadaViewModel> lista) {
        ObservableList<UnidadeMotivoComTipoParadaViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcTipoParada.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeTipoParada().getNomeComSigla()));
        tcMotivoParada.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeMotivoParada().getNomeComSigla()));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    //true - ativa: novo e excluir, 
    //false - ativa: gravar e cancelar
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
    }

}
