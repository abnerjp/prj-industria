package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesItem;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.UnidadeController;
import lib.controller.associacao.UnidadeItemController;
import lib.viewmodel.cadastro.ItemViewModel;
import lib.viewmodel.associacao.UnidadeItemViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;
import lib.viewmodel.associacao.UnidadexItensViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadeItem implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeItemController controllerUnidadeItem = new UnidadeItemController();

    private UnidadexItensViewModel vmUnidadeComItens = null;
    private int idItemUnidade = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeItemViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeItemViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeItemViewModel, String> tcItem;
    @FXML
    private TableColumn<UnidadeItemViewModel, String> tcUndMedida;
    @FXML
    private TableColumn<UnidadeItemViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtItemObs;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidadeComItens = new UnidadexItensViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizaInterface(vmUnidadeComItens);
        botoes(true);
    }

    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadeItemViewModel>() {
                @Override
                public void updateItem(UnidadeItemViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidadeComItens = new UnidadexItensViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizaInterface(vmUnidadeComItens);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidadeComItens.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidadeComItens.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComItens = new UnidadexItensViewModel(unAux);
            }
            atualizaInterface(vmUnidadeComItens);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidadeComItens.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidadeComItens.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComItens = new UnidadexItensViewModel(unAux);
            }
            atualizaInterface(vmUnidadeComItens);
            botoes(true);
        }
    }

    @FXML
    private void clkUltimo(ActionEvent event) {
        vmUnidadeComItens = new UnidadexItensViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizaInterface(vmUnidadeComItens);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        UnidadexItensViewModel unAux = new UnidadexItensViewModel(unidadeAux);
        if (unAux.getUnidade() != null) {
            vmUnidadeComItens = unAux;
            atualizaInterface(vmUnidadeComItens);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        UnidadexItensViewModel unAux = new UnidadexItensViewModel(codigoAux);
        if (unAux.getUnidade() != null) {
            vmUnidadeComItens = unAux;
            atualizaInterface(vmUnidadeComItens);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadeItemViewModel vmItemDaUnidade = tbvDados.getSelectionModel().getSelectedItem();
            if (vmItemDaUnidade != null) {
                Alerta dialog = new Alerta("Item", "Dados do vínculo",
                        "Data do vínculo: " + vmItemDaUnidade.getDataFormatada()
                        + "\nStatus do vínculo: " + vmItemDaUnidade.isAtivoToString()
                        + "\n\nObservação: \n" + vmItemDaUnidade.getObservacao());
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        ItemViewModel itemAux;
        UnidadeItemViewModel vmItemDaUnidade;

        if (vmUnidadeComItens != null && vmUnidadeComItens.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/pesquisa/PesItem.fxml", "Buscar item", false, true, false);
                PesItem controller = tl.getLoader().getController();
                controller.setAcessoTerceiro(true);
                tl.exibirTelaFilha();// exibe a tela
                itemAux = controller.getItem();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                itemAux = null;
                System.out.println("erro ao exibir janela de pesquisa");
            }

            if (itemAux != null) {
                vmItemDaUnidade = new UnidadeItemViewModel(
                        true,
                        LocalDate.now(),
                        "",
                        itemAux.getId()
                );

                statusGravacao = controllerUnidadeItem.gravar(vmUnidadeComItens.getUnidade().getId(), vmItemDaUnidade);
                if (statusGravacao > 0) {
                    //não gravado
                    exibeAlerta(statusGravacao);
                } else {
                    //gravado com sucesso
                    vmItemDaUnidade = controllerUnidadeItem.getRegPorUnidadeEItem(
                            vmUnidadeComItens.getUnidade().getId(),
                            vmItemDaUnidade.getItem().getId()
                    );
                    vmUnidadeComItens.addItemNaLista(vmItemDaUnidade);
                    atualizaInterface(vmUnidadeComItens);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComItens.getPosicaoItemNaLista(vmItemDaUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeItemViewModel itemUnidadeAux = tbvDados.getSelectionModel().getSelectedItem();
        if (itemUnidadeAux != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Unidade tipo de parada", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidadeItem.excluir(vmUnidadeComItens.getUnidade().getId(), itemUnidadeAux) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    vmUnidadeComItens.removeItemDaLista(itemUnidadeAux);
                    carregaTabela(vmUnidadeComItens.getItens());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadeItemViewModel itemUnidadeAux = tbvDados.getSelectionModel().getSelectedItem();
        if (itemUnidadeAux != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Item da unidade", "", "");
            if (itemUnidadeAux.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar item inativo para a unidade?");
            } else {
                dialog.setInformacao("Ativar registro", "tornar item ativo para a unidade?");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                itemUnidadeAux.setAtivo(!itemUnidadeAux.isAtivo());
                int status = controllerUnidadeItem.alterar(vmUnidadeComItens.getUnidade().getId(), itemUnidadeAux);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidadeComItens.atualizarItemNaLista(itemUnidadeAux);
                    carregaTabela(vmUnidadeComItens.getItens());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComItens.getPosicaoItemNaLista(itemUnidadeAux));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadeItemViewModel vmItemUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmItemUnidade != null) {
            atualizaInterfaceAlteracao(vmItemUnidade);
            botoes(false);
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadeItemViewModel vmItemUnidade = controllerUnidadeItem.getRegPorCodigo(idItemUnidade);
        int statusGravacao;
        if (vmUnidadeComItens != null && vmItemUnidade != null) {
            if (vmItemUnidade.isAtivo() != rbAtivo.isSelected() || !vmItemUnidade.getObservacao().equals(txtObservacao.getText())) {
                vmItemUnidade.setAtivo(rbAtivo.isSelected());
                vmItemUnidade.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadeItem.alterar(vmUnidadeComItens.getUnidade().getId(), vmItemUnidade);
                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    vmUnidadeComItens.atualizarItemNaLista(vmItemUnidade);
                    atualizaInterface(vmUnidadeComItens);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComItens.getPosicaoItemNaLista(vmItemUnidade));
                    limparCampos();
                    botoes(true);
                }
            } else {
                limparCampos();
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Itens da unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1303:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Unidade", "unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1304:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Item", "item não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1305:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1306:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1307:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1308:
                dialog.setInformacao("Duplicidade", "item já vinculado à unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(UnidadexItensViewModel unidadeComItens) {
        if (unidadeComItens != null && unidadeComItens.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComItens.getUnidade().getId()));
            txtDescricao.setText(unidadeComItens.getUnidade().getNomeFormatado());
            if (unidadeComItens.getItens() != null) {
                carregaTabela(unidadeComItens.getItens());
            }
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizaInterfaceAlteracao(UnidadeItemViewModel vmItemDaUnidade) {
        if (vmItemDaUnidade != null) {
            idItemUnidade = vmItemDaUnidade.getId();
            txtItemObs.setText(vmItemDaUnidade.getItem().getNome());
            txtDataObs.setText(vmItemDaUnidade.getDataFormatada());
            if (vmItemDaUnidade.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmItemDaUnidade.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idItemUnidade = 0;
        txtItemObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    private void carregaTabela(List<UnidadeItemViewModel> lista) {
        ObservableList<UnidadeItemViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcItem.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getItem().getNome()));
        tcUndMedida.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getItem().getMedida().getSigla()));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    /*
     * true - ativa: novo e excluir, false - ativa: gravar e cancelar
     *
     * @param controle
     */
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
    }
}
