package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesEndereco;
import industria.controller.pesquisa.PesUsuario;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UsuarioEnderecoController;
import lib.controller.cadastro.EnderecoController;
import lib.controller.cadastro.UsuarioController;
import lib.viewmodel.associacao.UsuarioEnderecoViewModel;
import lib.viewmodel.cadastro.EnderecoViewModel;
import lib.viewmodel.cadastro.UsuarioViewModel;

/**
 *
 * @author abnerjp
 */
public class CadUsuarioEndereco implements Initializable {

    private final UsuarioController controllerUsuario = new UsuarioController();
    private final UsuarioEnderecoController controllerUsuarioEndereco = new UsuarioEnderecoController();
    private UsuarioViewModel vmUsuario = null;
    private int idEndereco = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    // recebera parâmetro de quem chamou
    // false  - a chamada não veio da tela de cadastro de unidade, portanto não será permitido selecionar os desativados
    // true - a chamada veio da tela de cadastro de unidade, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;

    @FXML
    private JFXButton btnPrimeiro;
    @FXML
    private JFXButton btnAnterior;
    @FXML
    private JFXButton btnProximo;
    @FXML
    private JFXButton btnUltimo;
    @FXML
    private JFXButton btnBusca;
    @FXML
    private JFXButton btnIrPara;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UsuarioEnderecoViewModel> tbvDados;
    @FXML
    private TableColumn<UsuarioEnderecoViewModel, String> tcData;
    @FXML
    private TableColumn<UsuarioEnderecoViewModel, String> tcCep;
    @FXML
    private TableColumn<UsuarioEnderecoViewModel, String> tcEndereco;
    @FXML
    private TableColumn<UsuarioEnderecoViewModel, String> tcCidade;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextField txtCep;
    @FXML
    private JFXDatePicker dtpDataEndereco;
    @FXML
    private JFXTextField txtLogradouro;
    @FXML
    private JFXTextField txtNumero;
    @FXML
    private JFXTextField txtComplemento;
    @FXML
    private JFXTextField txtBairro;
    @FXML
    private JFXTextField txtCidade;
    @FXML
    private JFXButton btnBuscarEndereco;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        focusTxtCep(txtCep);
        if (!acessoTerceiro) {
            vmUsuario = controllerUsuario.getUltimoRegistro();
        }
        atualizaInterface(vmUsuario);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUsuario = controllerUsuario.getPrimeiroRegistro();
        atualizaInterface(vmUsuario);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUsuario != null) {
            UsuarioViewModel unAux = controllerUsuario.getAnteriorRegistro(vmUsuario.getId());
            if (unAux != null) {
                vmUsuario = unAux;
            }
            atualizaInterface(vmUsuario);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUsuario != null) {
            UsuarioViewModel userAux = controllerUsuario.getProximoRegistro(vmUsuario.getId());
            if (userAux != null) {
                vmUsuario = userAux;
            }
            atualizaInterface(vmUsuario);
            botoes(true);
        }
    }

    @FXML
    private void clkUltimo(ActionEvent event) {
        vmUsuario = controllerUsuario.getUltimoRegistro();
        atualizaInterface(vmUsuario);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UsuarioViewModel usuarioAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUsuario.fxml", "Buscar usuário", false, true, false);
            PesUsuario controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            usuarioAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            usuarioAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (usuarioAux != null) {
            vmUsuario = usuarioAux;
            botoes(true);
            atualizaInterface(vmUsuario);
        }
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        if (!acessoTerceiro) {
            Alerta dialog = new Alerta("Código usuário", "", "");
            int codigoAux;
            try {
                codigoAux = Integer.parseInt(dialog.caixaTexto("código do usuário"), 10);
            } catch (NumberFormatException ex) {
                codigoAux = 0;
            }

            UsuarioViewModel userAux = controllerUsuario.regPorCodigo(codigoAux);
            if (userAux != null) {
                vmUsuario = userAux;
                atualizaInterface(vmUsuario);
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        if (vmUsuario != null) {
            botoes(false);
            atualizaInterface(vmUsuario);
        } else {
            new Alerta("Endereço usuário", "Usuário não selecionado", "para cadastro de endereço, deve-se selecionar um usuário").exibeInformacao(Alert.AlertType.INFORMATION);
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UsuarioEnderecoViewModel endSelecionado = tbvDados.getSelectionModel().getSelectedItem();
        int status;
        if (endSelecionado != null) {
            Alerta alert = new Alerta("Endereço usuário", "Excluir endereço", "confirma a exclusão definitiva do endereço selecinado?");
            if (alert.decisaoYesNo() == ButtonType.YES) {
                status = new UsuarioEnderecoController().excluir(vmUsuario.getId(), endSelecionado);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUsuario.removeEndereco(endSelecionado);
                    atualizaInterface(vmUsuario);
                }
            }
        }
    }

    @FXML// ok
    private void clkTxtBuscarEndereco(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBuscarEndereco(null);
        }
    }

    @FXML// ok
    private void clkBuscarEndereco(ActionEvent event) {
        EnderecoViewModel enderecoAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesEndereco.fxml", "Buscar endereço", false, true, false);
            PesEndereco controller = tl.getLoader().getController();
            controller.setAcessoTerceiro(true);

            tl.exibirTelaFilha();// exibe a tela
            enderecoAux = controller.getEndereco();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            enderecoAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (enderecoAux != null) {
            atualizaInterfaceCadastroEndereco(enderecoAux);
        } else {
            txtCep.requestFocus();
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int numero;
        String complemento = txtComplemento.getText();
        UsuarioEnderecoViewModel usuarioEnderecoAux;
        LocalDate dataEndereco = dtpDataEndereco.getValue();
        int status;

        try {
            numero = Integer.parseInt(txtNumero.getText());
        } catch (NumberFormatException ex) {
            numero = 0;
        }

        usuarioEnderecoAux = new UsuarioEnderecoViewModel(
                numero,
                dataEndereco,
                complemento,
                "",
                idEndereco
        );

        status = new UsuarioEnderecoController().gravar(vmUsuario != null ? vmUsuario.getId() : 0, usuarioEnderecoAux);

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmUsuario.addEndereco(usuarioEnderecoAux);
            botoes(true);
            atualizaInterface(vmUsuario);
        }
    }

    @FXML
    private void clkCancelar(ActionEvent event) {
        botoes(true);
        atualizaInterface(vmUsuario);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        sair();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Endereço usuário - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2103:
                dialog.setInformacao("Usuário", "usuário não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2104:
                dialog.setInformacao("Endereço", "endereço não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCep.requestFocus();
                break;
            case 2105:
                dialog.setInformacao("Número", "número para o endereço não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNumero.requestFocus();
                break;
            case 2106:
                dialog.setInformacao("Complemento", "complemento não pode conter mais que 30 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtComplemento.requestFocus();
                break;
            case 2107:
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2108:
                dialog.setInformacao("Data", "data informada não pode ser maior que a data atual");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                dtpDataEndereco.requestFocus();
                break;
            case 2109:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void atualizaInterface(UsuarioViewModel vmUsuario) {
        if (btnNovo.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
            txtCep.requestFocus();
            limparCampos();
            dtpDataEndereco.valueProperty().setValue(LocalDate.now());
        } else {
            tabPane.getSelectionModel().select(tab1);
            txtCodigo.requestFocus();
            if (vmUsuario != null) {
                txtCodigo.setText(String.valueOf(vmUsuario.getId()));
                txtDescricao.setText(vmUsuario.getNome());
                if (vmUsuario.getHistoricoEndereco() == null) {
                    vmUsuario.setHistoricoEndereco(controllerUsuario.getHistoricoEndereco(vmUsuario.getId()));
                }
                if (vmUsuario.getHistoricoEndereco() != null) {
                    carregaTabela(vmUsuario.getHistoricoEndereco());
                }
            } else {
                limparCampos();
                limparTabela();
            }
        }
    }

    //método para controlar a quando o foco "deixar" o textbox
    private void focusTxtCep(JFXTextField campo) {
        campo.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean velhoValor, Boolean novoValor) -> {

            // condição para quando deixar o foco
            if (btnNovo.isDisabled() && !novoValor) {
                if (!campo.getText().isEmpty()) {
                    EnderecoViewModel vmEndereco;
                    vmEndereco = new EnderecoController().regPorCep(campo.getText());

                    if (vmEndereco != null) {
                        atualizaInterfaceCadastroEndereco(vmEndereco);
                    } else {
                        new Alerta("Endereço do usuário", "Endereço não localizado", "não foi localizado o endereço através do cep informado").exibeInformacao(Alert.AlertType.INFORMATION);
                        limparCampos();
                    }
                }
            }
        });
    }

    private void atualizaInterfaceCadastroEndereco(EnderecoViewModel vmEndereco) {
        if (vmEndereco != null) {
            idEndereco = vmEndereco.getId();
            txtCep.setText(vmEndereco.getCepFormatado());
            txtLogradouro.setText(vmEndereco.getLogradouro());
            txtBairro.setText(vmEndereco.getBairro());
            txtCidade.setText(vmEndereco.getCidade().getCidadeUf());
            txtNumero.requestFocus();
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idEndereco = 0;
        txtCep.setText("");
        txtLogradouro.setText("");
        txtNumero.setText("");
        txtComplemento.setText("");
        txtBairro.setText("");
        txtCidade.setText("");
    }

    private void carregaTabela(List<UsuarioEnderecoViewModel> lista) {
        ObservableList<UsuarioEnderecoViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcCep.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEndereco().getCepFormatado()));
        tcEndereco.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEnderecoFormatado()));
        tcCidade.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEndereco().getCidade().getCidadeUf()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        tbvDados.getSelectionModel().select(-1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);

        tab2.setDisable(controle);
        btnBuscarEndereco.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);
    }

    /**
     * True - Desabilita a navegação entre registros no formulário
     *
     * @param acesso
     */
    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        // true - oculta acessos abaixo
        btnPrimeiro.setDisable(acessoTerceiro);
        btnAnterior.setDisable(acessoTerceiro);
        btnProximo.setDisable(acessoTerceiro);
        btnUltimo.setDisable(acessoTerceiro);

        btnIrPara.setDisable(acessoTerceiro);
        btnBusca.setDisable(acessoTerceiro);
        txtCodigo.setDisable(acessoTerceiro);
        txtDescricao.setDisable(acessoTerceiro);
        botoes(acessoTerceiro);
    }

    /**
     * informar o registro a ser exibido e true para bloquear a navegação entre
     * registros na tela
     *
     * @param vmUsuario
     * @param navegar
     */
    public void setUsuario(UsuarioViewModel vmUsuario, boolean navegar) {
        this.vmUsuario = vmUsuario;
        setAcessoTerceiro(navegar);
        if (acessoTerceiro) {
            atualizaInterface(vmUsuario);
        }
    }

    /**
     * retorna o endereço atual do usuario apresentado na tela
     *
     * @return
     */
    public UsuarioEnderecoViewModel getEnderecoAtual() {
        return vmUsuario != null ? vmUsuario.getEnderecoAtual() : null;
    }

}
