package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesTipoParada;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.UnidadeController;
import lib.controller.associacao.UnidadeTipoParadaController;
import lib.viewmodel.cadastro.TipoParadaViewModel;
import lib.viewmodel.associacao.UnidadeTipoParadaViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;
import lib.viewmodel.associacao.UnidadexTiposParadaViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadeTipoParada implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeTipoParadaController controllerUnidadeTipoParada = new UnidadeTipoParadaController();

    private UnidadexTiposParadaViewModel vmUnidadeComTiposParada = null;
    private int idTipoParadaUnidade = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeTipoParadaViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeTipoParadaViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeTipoParadaViewModel, String> tcTipoParada;
    @FXML
    private TableColumn<UnidadeTipoParadaViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextField txtTipoParadaObs;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextArea txtObservacao;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidadeComTiposParada = new UnidadexTiposParadaViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizaInterface(vmUnidadeComTiposParada);
        botoes(true);
    }
    
    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadeTipoParadaViewModel>() {
                @Override
                public void updateItem(UnidadeTipoParadaViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidadeComTiposParada = new UnidadexTiposParadaViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizaInterface(vmUnidadeComTiposParada);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidadeComTiposParada.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidadeComTiposParada.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComTiposParada = new UnidadexTiposParadaViewModel(unAux);
            }
            atualizaInterface(vmUnidadeComTiposParada);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidadeComTiposParada.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidadeComTiposParada.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComTiposParada = new UnidadexTiposParadaViewModel(unAux);
            }
            atualizaInterface(vmUnidadeComTiposParada);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUnidadeComTiposParada = new UnidadexTiposParadaViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizaInterface(vmUnidadeComTiposParada);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        atualizaUnidade(unidadeAux);
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        atualizaUnidade(codigoAux);
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadeTipoParadaViewModel vmTipoParadaUnidade = tbvDados.getSelectionModel().getSelectedItem();
            if (vmTipoParadaUnidade != null) {
                Alerta dialog = new Alerta("Tipo de parada", "Dados do vínculo",
                        "Data do vínculo: " + vmTipoParadaUnidade.getDataFormatada()
                        + "\nStatus do vínculo: " + vmTipoParadaUnidade.isAtivoToString()
                        + "\n\nObservação: \n" + vmTipoParadaUnidade.getObservacao());
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        TipoParadaViewModel tipoParadaAux;
        UnidadeTipoParadaViewModel vmTipoParadaUnidade;

        if (vmUnidadeComTiposParada != null && vmUnidadeComTiposParada.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/pesquisa/PesTipoParada.fxml", "Buscar tipo parada", false, true, false);
                PesTipoParada controller = tl.getLoader().getController();
                controller.setAcessoTerceiro(true);
                tl.exibirTelaFilha();// exibe a tela
                tipoParadaAux = controller.getTipoParada();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                tipoParadaAux = null;
                System.out.println("erro ao exibir janela de pesquisa");
            }

            if (tipoParadaAux != null) {
                vmTipoParadaUnidade = new UnidadeTipoParadaViewModel(
                        true,
                        LocalDate.now(),
                        "",
                        tipoParadaAux.getId()
                );

                statusGravacao = controllerUnidadeTipoParada.gravar(vmUnidadeComTiposParada.getUnidade().getId(), vmTipoParadaUnidade);
                if (statusGravacao > 0) {
                    //não gravado
                    exibeAlerta(statusGravacao);
                } else {
                    //gravado com sucesso
                    vmTipoParadaUnidade = controllerUnidadeTipoParada.getRegPorUnidadeETipoParada(
                            vmUnidadeComTiposParada.getUnidade().getId(),
                            vmTipoParadaUnidade.getTipoParada().getId()
                    );
                    vmUnidadeComTiposParada.addTipoParadaNaLista(vmTipoParadaUnidade);
                    atualizaInterface(vmUnidadeComTiposParada);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComTiposParada.getPosicaoTipoParadaNaLista(vmTipoParadaUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeTipoParadaViewModel tipoParadaAux = tbvDados.getSelectionModel().getSelectedItem();
        if (tipoParadaAux != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Tipo de parada da unidade", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidadeTipoParada.excluir(vmUnidadeComTiposParada.getUnidade().getId(), tipoParadaAux) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    vmUnidadeComTiposParada.removerTipoParadaDaLista(tipoParadaAux);
                    carregaTabela(vmUnidadeComTiposParada.getTiposParada());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadeTipoParadaViewModel tipoParadaAux = tbvDados.getSelectionModel().getSelectedItem();
        if (tipoParadaAux != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Tipo de parada da unidade", "", "");
            if (tipoParadaAux.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar tipo de parada inativo para a unidade?");
            } else {
                dialog.setInformacao("Ativar registro", "tornar tipo de parada ativo para a unidade?");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                tipoParadaAux.setAtivo(!tipoParadaAux.isAtivo());
                int status = controllerUnidadeTipoParada.alterar(vmUnidadeComTiposParada.getUnidade().getId(), tipoParadaAux);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidadeComTiposParada.atualizarTipoParadaNaLista(tipoParadaAux);
                    carregaTabela(vmUnidadeComTiposParada.getTiposParada());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComTiposParada.getPosicaoTipoParadaNaLista(tipoParadaAux));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadeTipoParadaViewModel vmTipoParadaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmTipoParadaUnidade != null) {
            atualizaInterfaceAlteracao(vmTipoParadaUnidade);
            botoes(false);
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadeTipoParadaViewModel vmTipoParadaUnidade = controllerUnidadeTipoParada.getRegPorCodigo(idTipoParadaUnidade);
        int statusGravacao;
        if (vmUnidadeComTiposParada != null && vmTipoParadaUnidade != null) {
            if (vmTipoParadaUnidade.isAtivo() != rbAtivo.isSelected() || !vmTipoParadaUnidade.getObservacao().equals(txtObservacao.getText())) {
                vmTipoParadaUnidade.setAtivo(rbAtivo.isSelected());
                vmTipoParadaUnidade.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadeTipoParada.alterar(vmUnidadeComTiposParada.getUnidade().getId(), vmTipoParadaUnidade);
                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    vmUnidadeComTiposParada.atualizarTipoParadaNaLista(vmTipoParadaUnidade);
                    atualizaInterface(vmUnidadeComTiposParada);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComTiposParada.getPosicaoTipoParadaNaLista(vmTipoParadaUnidade));
                    limparCampos();
                    botoes(true);
                }
            } else {
                limparCampos();
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }

    @FXML// ok
    private void clkBuscarNaLista(ActionEvent event) {
        Alerta dialog = new Alerta("Tipo de Parada", "", "");
        String retorno;

        retorno = dialog.caixaTexto("nome");
        retorno = retorno.trim();
        if (retorno.length() > 0 && vmUnidadeComTiposParada != null && vmUnidadeComTiposParada.getTiposParada() != null) {
            int i = 0;
            while (i < vmUnidadeComTiposParada.getTiposParada().size()
                    && !vmUnidadeComTiposParada.getTiposParada().get(i).getTipoParada().getNome().toLowerCase().contains(retorno.toLowerCase())) {
                i++;
            }
            if (i < vmUnidadeComTiposParada.getTiposParada().size()) {
                TableViewUtil.selecionarLinhaTabela(tbvDados, i);
            }
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Tipo parada da unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1203:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Unidade", "unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1204:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Tipo de Parada", "tipo de parada não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1205:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1206:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1207:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1208:
                dialog.setInformacao("Duplicidade", "tipo de parada já vinculado à unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaUnidade(UnidadeViewModel unidade) {
        UnidadexTiposParadaViewModel unAux = new UnidadexTiposParadaViewModel(unidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeComTiposParada = unAux;
            atualizaInterface(vmUnidadeComTiposParada);
            botoes(true);
        }
    }

    private void atualizaUnidade(int codUnidade) {
        UnidadexTiposParadaViewModel unAux = new UnidadexTiposParadaViewModel(codUnidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeComTiposParada = unAux;
            atualizaInterface(vmUnidadeComTiposParada);
            botoes(true);
        }
    }

    private void atualizaInterface(UnidadexTiposParadaViewModel unidadeComTiposParada) {
        if (unidadeComTiposParada != null && unidadeComTiposParada.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComTiposParada.getUnidade().getId()));
            txtDescricao.setText(unidadeComTiposParada.getUnidade().getNomeFormatado());
            if (unidadeComTiposParada.getTiposParada() != null) {
                carregaTabela(unidadeComTiposParada.getTiposParada());
            }
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizaInterfaceAlteracao(UnidadeTipoParadaViewModel vmTipoParadaUnidade) {
        if (vmTipoParadaUnidade != null) {
            idTipoParadaUnidade = vmTipoParadaUnidade.getId();
            txtTipoParadaObs.setText(vmTipoParadaUnidade.getTipoParada().getTipoParadaFormatado());
            txtDataObs.setText(vmTipoParadaUnidade.getDataFormatada());
            if (vmTipoParadaUnidade.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmTipoParadaUnidade.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idTipoParadaUnidade = 0;
        txtTipoParadaObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void carregaTabela(List<UnidadeTipoParadaViewModel> lista) {
        ObservableList<UnidadeTipoParadaViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcTipoParada.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getTipoParada().getTipoParadaFormatado()));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    //true - ativa: novo e excluir, 
    //false - ativa: gravar e cancelar
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
    }
}
