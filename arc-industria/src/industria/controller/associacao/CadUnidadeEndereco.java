package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesEndereco;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.EnderecoController;
import lib.controller.associacao.UnidadeEnderecoController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.EnderecoViewModel;
import lib.viewmodel.associacao.UnidadeEnderecoViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abner.jacomo
 */
public class CadUnidadeEndereco implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeEnderecoController controllerUnidadeEndereco = new UnidadeEnderecoController();
    private UnidadeViewModel vmUnidade = null;
    private int idEndereco = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    // recebera parâmetro de quem chamou
    // false  - a chamada não veio da tela de cadastro de unidade, portanto não será permitido selecionar os desativados
    // true - a chamada veio da tela de cadastro de unidade, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;

    @FXML
    private JFXButton btnBusca;
    @FXML
    private JFXButton btnIrPara;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeEnderecoViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeEnderecoViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeEnderecoViewModel, String> tcCep;
    @FXML
    private TableColumn<UnidadeEnderecoViewModel, String> tcEndereco;
    @FXML
    private TableColumn<UnidadeEnderecoViewModel, String> tcCidade;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextField txtCep;
    @FXML
    private JFXTextField txtLogradouro;
    @FXML
    private JFXTextField txtNumero;
    @FXML
    private JFXTextField txtComplemento;
    @FXML
    private JFXTextField txtBairro;
    @FXML
    private JFXTextField txtCidade;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXButton btnPrimeiro;
    @FXML
    private JFXButton btnAnterior;
    @FXML
    private JFXButton btnProximo;
    @FXML
    private JFXButton btnUltimo;
    @FXML
    private JFXButton btnBuscarEndereco;
    @FXML
    private JFXDatePicker dtpDataEndereco;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        focusTxtCep(txtCep);
        if (!acessoTerceiro) {
            vmUnidade = controllerUnidade.getUltimoRegistro(false);
        }
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidade = controllerUnidade.getPrimeiroRegistro(false);
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidade != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidade.getId(), false);
            if (unAux != null) {
                vmUnidade = unAux;
            }
            atualizaInterface(vmUnidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidade != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidade.getId(), false);
            if (unAux != null) {
                vmUnidade = unAux;
            }
            atualizaInterface(vmUnidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUnidade = controllerUnidade.getUltimoRegistro(false);
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (unidadeAux != null) {
            vmUnidade = unidadeAux;
            botoes(true);
            atualizaInterface(vmUnidade);
        }
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        if (!acessoTerceiro) {
            Alerta dialog = new Alerta("Código unidade", "", "");
            int codigoAux;
            try {
                codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
            } catch (NumberFormatException ex) {
                codigoAux = 0;
            }

            UnidadeViewModel unAux = controllerUnidade.regPorCodigo(codigoAux, false);
            if (unAux != null) {
                vmUnidade = unAux;
                atualizaInterface(vmUnidade);
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        if (vmUnidade != null) {
            botoes(false);
            atualizaInterface(vmUnidade);
        } else {
            new Alerta("Endereço de Unidade", "Unidade não selecionada", "para cadastro de endereço, deve-se selecionar uma unidade").exibeInformacao(Alert.AlertType.INFORMATION);
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeEnderecoViewModel endSelecionado = tbvDados.getSelectionModel().getSelectedItem();
        int status;
        if (endSelecionado != null) {
            Alerta alert = new Alerta("Endereço unidade", "Excluir endereço", "confirma a exclusão definitiva do endereço selecinado?");
            if (alert.decisaoYesNo() == ButtonType.YES) {
                status = new UnidadeEnderecoController().excluir(vmUnidade.getId(), endSelecionado);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidade.removeEndereco(endSelecionado);
                    atualizaInterface(vmUnidade);
                }
            }
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int numero;
        String complemento = txtComplemento.getText();
        UnidadeEnderecoViewModel unidadeEnderecoAux;
        LocalDate dataEndereco = dtpDataEndereco.getValue();
        int status;

        try {
            numero = Integer.parseInt(txtNumero.getText());
        } catch (NumberFormatException ex) {
            numero = 0;
        }

        unidadeEnderecoAux = new UnidadeEnderecoViewModel(
                numero,
                dataEndereco,
                complemento,
                "",
                idEndereco
        );

        status = new UnidadeEnderecoController().gravar(vmUnidade != null ? vmUnidade.getId() : 0, unidadeEnderecoAux);

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmUnidade.addEndereco(unidadeEnderecoAux);
            botoes(true);
            atualizaInterface(vmUnidade);
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        botoes(true);
        atualizaInterface(vmUnidade);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        sair();
    }

    @FXML// ok
    private void clkBuscarEndereco(ActionEvent event) {
        EnderecoViewModel enderecoAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesEndereco.fxml", "Buscar endereço", false, true, false);
            PesEndereco controller = tl.getLoader().getController();
            controller.setAcessoTerceiro(true);

            tl.exibirTelaFilha();// exibe a tela
            enderecoAux = controller.getEndereco();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            enderecoAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (enderecoAux != null) {
            atualizaInterfaceCadastroEndereco(enderecoAux);
        } else {
            txtCep.requestFocus();
        }
    }

    @FXML// ok
    private void clkTxtBuscarEndereco(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBuscarEndereco(null);
        }
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Endereço unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1003:
                dialog.setInformacao("Unidade", "unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1004:
                dialog.setInformacao("Endereço", "endereço não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCep.requestFocus();
                break;
            case 1005:
                dialog.setInformacao("Número", "número para o endereço não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNumero.requestFocus();
                break;
            case 1006:
                dialog.setInformacao("Complemento", "complemento não pode conter mais que 30 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtComplemento.requestFocus();
                break;
            case 1007:
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1008:
                dialog.setInformacao("Data", "data informada não pode ser maior que a data atual");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                dtpDataEndereco.requestFocus();
                break;
            case 1009:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void atualizaInterface(UnidadeViewModel vmUnidade) {
        if (btnNovo.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
            txtCep.requestFocus();
            limparCampos();
            dtpDataEndereco.valueProperty().setValue(LocalDate.now());
        } else {
            tabPane.getSelectionModel().select(tab1);
            txtCodigo.requestFocus();
            if (vmUnidade != null) {
                txtCodigo.setText(String.valueOf(vmUnidade.getId()));
                txtDescricao.setText(vmUnidade.getNomeFormatado());
                if (vmUnidade.getHistoricoEndereco() == null) {
                    vmUnidade.setHistoricoEndereco(controllerUnidade.getHistoricoEndereco(vmUnidade.getId()));
                }
                if (vmUnidade.getHistoricoEndereco() != null) {
                    carregaTabela(vmUnidade.getHistoricoEndereco());
                }
            } else {
                limparCampos();
                limparTabela();
            }
        }
    }

    //método para controlar a quando o foco "deixar" o textbox
    private void focusTxtCep(JFXTextField campo) {
        campo.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean velhoValor, Boolean novoValor) -> {

            // condição para quando deixar o foco
            if (btnNovo.isDisabled() && !novoValor) {
                if (!campo.getText().isEmpty()) {
                    EnderecoViewModel vmEndereco;
                    vmEndereco = new EnderecoController().regPorCep(campo.getText());

                    if (vmEndereco != null) {
                        atualizaInterfaceCadastroEndereco(vmEndereco);
                    } else {
                        new Alerta("Endereço de Unidade", "Endereço não localizado", "não foi localizado o endereço através do cep informado").exibeInformacao(Alert.AlertType.INFORMATION);
                        limparCampos();
                    }
                }
            }
        });
    }

    private void atualizaInterfaceCadastroEndereco(EnderecoViewModel vmEndereco) {
        if (vmEndereco != null) {
            idEndereco = vmEndereco.getId();
            txtCep.setText(vmEndereco.getCepFormatado());
            txtLogradouro.setText(vmEndereco.getLogradouro());
            txtBairro.setText(vmEndereco.getBairro());
            txtCidade.setText(vmEndereco.getCidade().getCidadeUf());
            txtNumero.requestFocus();
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idEndereco = 0;
        txtCep.setText("");
        txtLogradouro.setText("");
        txtNumero.setText("");
        txtComplemento.setText("");
        txtBairro.setText("");
        txtCidade.setText("");
    }

    private void carregaTabela(List<UnidadeEnderecoViewModel> lista) {
        ObservableList<UnidadeEnderecoViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcCep.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEndereco().getCepFormatado()));
        tcEndereco.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEnderecoFormatado()));
        tcCidade.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEndereco().getCidade().getCidadeUf()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        tbvDados.getSelectionModel().select(-1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    /**
     * true - ativa: novo e excluir, false - ativa: gravar e cancelar
     *
     * @param controle
     */
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);

        tab2.setDisable(controle);
        btnBuscarEndereco.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);
    }

    /**
     * True - Desabilita a navegação entre registros no formulário
     *
     * @param acesso
     */
    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        // true - oculta acessos abaixo
        btnPrimeiro.setDisable(acessoTerceiro);
        btnAnterior.setDisable(acessoTerceiro);
        btnProximo.setDisable(acessoTerceiro);
        btnUltimo.setDisable(acessoTerceiro);

        btnIrPara.setDisable(acessoTerceiro);
        btnBusca.setDisable(acessoTerceiro);
        txtCodigo.setDisable(acessoTerceiro);
        txtDescricao.setDisable(acessoTerceiro);
        botoes(acessoTerceiro);
    }

    /**
     * informar o registro a ser exibido e true para bloquear a navegação entre
     * registros na tela
     *
     * @param vmUnidade
     * @param navegar
     */
    public void setUnidade(UnidadeViewModel vmUnidade, boolean navegar) {
        this.vmUnidade = vmUnidade;
        setAcessoTerceiro(navegar);
        if (acessoTerceiro) {
            atualizaInterface(vmUnidade);
        }
    }

    /**
     * retorna o endereço atual da unidade apresentada na tela
     *
     * @return
     */
    public UnidadeEnderecoViewModel getEnderecoAtual() {
        return vmUnidade != null ? vmUnidade.getEnderecoAtual() : null;
    }
}
