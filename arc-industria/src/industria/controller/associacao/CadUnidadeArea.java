package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesArea;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadeAreaController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.associacao.UnidadeAreaViewModel;
import lib.viewmodel.associacao.UnidadexAreasViewModel;
import lib.viewmodel.cadastro.AreaViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadeArea implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeAreaController controllerUnidadeArea = new UnidadeAreaController();

    private UnidadexAreasViewModel vmUnidadeComAreas = null;
    private int idAreaUnidade = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeAreaViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeAreaViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeAreaViewModel, String> tcArea;
    @FXML
    private TableColumn<UnidadeAreaViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXTextField txtAreaObs;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.vmUnidadeComAreas = new UnidadexAreasViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizarInterface(this.vmUnidadeComAreas);
        botoes(true);
    }

    //ok
    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadeAreaViewModel>() {
                @Override
                public void updateItem(UnidadeAreaViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                            setStyle("-fx-background-color: yellow;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }
    
    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        this.vmUnidadeComAreas = new UnidadexAreasViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizarInterface(this.vmUnidadeComAreas);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (this.vmUnidadeComAreas.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(this.vmUnidadeComAreas.getUnidade().getId(), false);
            if (unAux != null) {
                this.vmUnidadeComAreas = new UnidadexAreasViewModel(unAux);
            }
            atualizarInterface(this.vmUnidadeComAreas);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (this.vmUnidadeComAreas.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(this.vmUnidadeComAreas.getUnidade().getId(), false);
            if (unAux != null) {
                this.vmUnidadeComAreas = new UnidadexAreasViewModel(unAux);
            }
            atualizarInterface(this.vmUnidadeComAreas);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        this.vmUnidadeComAreas = new UnidadexAreasViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizarInterface(this.vmUnidadeComAreas);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        atualizarUnidade(unidadeAux);
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        atualizarUnidade(codigoAux);
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadeAreaViewModel vmAreaUnidade = tbvDados.getSelectionModel().getSelectedItem();
            if (vmAreaUnidade != null) {
                Alerta dialog = new Alerta("Área", "Dados do vínculo",
                        "Data do vínculo: " + vmAreaUnidade.getDataFormatada()
                        + "\nStatus do vínculo: " + vmAreaUnidade.isAtivoToString()
                        + "\n\nObservação: \n" + vmAreaUnidade.getObservacao());
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        AreaViewModel areaAux;
        UnidadeAreaViewModel vmAreaUnidade;

        if (this.vmUnidadeComAreas != null && this.vmUnidadeComAreas.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/pesquisa/PesArea.fxml", "Buscar área", false, true, false);
                PesArea controller = tl.getLoader().getController();
                controller.setAcessoTerceiro(true);
                tl.exibirTelaFilha();// exibe a tela
                areaAux = controller.getArea();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                areaAux = null;
                System.out.println("erro ao exibir janela de pesquisa");
            }

            if (areaAux != null) {
                vmAreaUnidade = new UnidadeAreaViewModel(
                        true,
                        LocalDate.now(),
                        "",
                        areaAux.getId()
                );

                statusGravacao = this.controllerUnidadeArea.gravar(this.vmUnidadeComAreas.getUnidade().getId(), vmAreaUnidade);
                if (statusGravacao > 0) {
                    //não gravado
                    exibeAlerta(statusGravacao);
                } else {
                    //gravado com sucesso
                    vmAreaUnidade = this.controllerUnidadeArea.getRegPorUnidadeEArea(
                            this.vmUnidadeComAreas.getUnidade().getId(),
                            vmAreaUnidade.getArea().getId()
                    );
                    this.vmUnidadeComAreas.addAreaNaLista(vmAreaUnidade);
                    atualizarInterface(this.vmUnidadeComAreas);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, this.vmUnidadeComAreas.getPosicaoAreaNaLista(vmAreaUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeAreaViewModel vmAreaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmAreaUnidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Área da unidade", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (this.controllerUnidadeArea.excluir(this.vmUnidadeComAreas.getUnidade().getId(), vmAreaUnidade) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    this.vmUnidadeComAreas.removerAreaDaLista(vmAreaUnidade);
                    carregaTabela(this.vmUnidadeComAreas.getListaAreas());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadeAreaViewModel vmAreaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmAreaUnidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Área da unidade", "", "");
            if (vmAreaUnidade.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar área inativa para a unidade?");
            } else {
                dialog.setInformacao("Ativar registro", "tornar área ativa para a unidade?");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                vmAreaUnidade.setAtivo(!vmAreaUnidade.isAtivo());
                int status = this.controllerUnidadeArea.alterar(this.vmUnidadeComAreas.getUnidade().getId(), vmAreaUnidade);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    this.vmUnidadeComAreas.atualizarAreaNaLista(vmAreaUnidade);
                    carregaTabela(this.vmUnidadeComAreas.getListaAreas());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, this.vmUnidadeComAreas.getPosicaoAreaNaLista(vmAreaUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadeAreaViewModel vmAreaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmAreaUnidade != null) {
            atualizarInterfaceAlteracao(vmAreaUnidade);
            botoes(false);
        }
    }

    @FXML// ok
    private void clkBuscarNaLista(ActionEvent event) {
        Alerta dialog = new Alerta("Área", "", "");
        String retorno;

        retorno = dialog.caixaTexto("nome");
        retorno = retorno.trim();
        if (retorno.length() > 0 && this.vmUnidadeComAreas != null && this.vmUnidadeComAreas.getListaAreas() != null) {
            int pos = 0;
            while (pos < this.vmUnidadeComAreas.getListaAreas().size()
                    && !this.vmUnidadeComAreas.getListaAreas().get(pos).getArea().getNome().toLowerCase().contains(retorno.toLowerCase())) {
                pos++;
            }
            if (pos < this.vmUnidadeComAreas.getListaAreas().size()) {
                TableViewUtil.selecionarLinhaTabela(tbvDados, pos);
            }
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadeAreaViewModel vmAreaUnidade = controllerUnidadeArea.getRegPorCodigo(idAreaUnidade);
        int statusGravacao;
        if (this.vmUnidadeComAreas != null && vmAreaUnidade != null) {
            if (vmAreaUnidade.isAtivo() != rbAtivo.isSelected() || !vmAreaUnidade.getObservacao().equals(txtObservacao.getText())) {
                vmAreaUnidade.setAtivo(rbAtivo.isSelected());
                vmAreaUnidade.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadeArea.alterar(this.vmUnidadeComAreas.getUnidade().getId(), vmAreaUnidade);
                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    this.vmUnidadeComAreas.atualizarAreaNaLista(vmAreaUnidade);
                    atualizarInterface(this.vmUnidadeComAreas);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, this.vmUnidadeComAreas.getPosicaoAreaNaLista(vmAreaUnidade));
                    limparCampos();
                    botoes(true);
                }
            } else {
                limparCampos();
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Área da unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2303:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Unidade", "unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2304:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Área", "área não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2305:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2306:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 2307:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2308:
                dialog.setInformacao("Duplicidade", "área já vinculada à unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizarUnidade(UnidadeViewModel unidade) {
        UnidadexAreasViewModel unAux = new UnidadexAreasViewModel(unidade);
        if (unAux.getUnidade() != null) {
            this.vmUnidadeComAreas = unAux;
            atualizarInterface(this.vmUnidadeComAreas);
            botoes(true);
        }
    }

    private void atualizarUnidade(int codUnidade) {
        UnidadexAreasViewModel unAux = new UnidadexAreasViewModel(codUnidade);
        if (unAux.getUnidade() != null) {
            this.vmUnidadeComAreas = unAux;
            atualizarInterface(this.vmUnidadeComAreas);
            botoes(true);
        }
    }

    private void atualizarInterface(UnidadexAreasViewModel unidadeComAreas) {
        if (unidadeComAreas != null && unidadeComAreas.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComAreas.getUnidade().getId()));
            txtDescricao.setText(unidadeComAreas.getUnidade().getNomeFormatado());
            if (unidadeComAreas.getListaAreas() != null) {
                carregaTabela(unidadeComAreas.getListaAreas());
            }
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizarInterfaceAlteracao(UnidadeAreaViewModel vmAreaUnidade) {
        if (vmAreaUnidade != null) {
            idAreaUnidade = vmAreaUnidade.getId();
            txtAreaObs.setText(vmAreaUnidade.getArea().getNome());
            txtDataObs.setText(vmAreaUnidade.getDataFormatada());
            if (vmAreaUnidade.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmAreaUnidade.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idAreaUnidade = 0;
        txtAreaObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void carregaTabela(List<UnidadeAreaViewModel> lista) {
        ObservableList<UnidadeAreaViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcArea.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getArea().getNome()));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    //true - ativa: novo e excluir, 
    //false - ativa: gravar e cancelar
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
    }
}
