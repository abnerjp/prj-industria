package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import industria.util.MsgBox;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadeMotivoParadaController;
import lib.controller.associacao.UnidadeTipoParadaController;
import lib.viewmodel.associacao.UnidadeMotivoComTipoParadaViewModel;
import lib.viewmodel.associacao.UnidadeMotivoParadaViewModel;
import lib.viewmodel.associacao.UnidadeTipoParadaViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadVinculoTipoParadaMotivoParada implements Initializable {

    private double cx, cy;

    private int idUnidade = 0;
    private final UnidadeTipoParadaController controllerUnidadeTipoParada = new UnidadeTipoParadaController();
    private final UnidadeMotivoParadaController controllerUnidadeMotivoParada = new UnidadeMotivoParadaController();
    private List<UnidadeMotivoComTipoParadaViewModel> listaNovosVinculos;
    private List<UnidadeMotivoComTipoParadaViewModel> listaNovosVinculosConfirmados;
    private List<UnidadeMotivoComTipoParadaViewModel> listaVinculos;

    @FXML
    private Button btnSair;
    @FXML
    private JFXTabPane tpPrincipal;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtTipoParada;
    @FXML
    private JFXListView<UnidadeTipoParadaViewModel> lvTipoParada;
    @FXML
    private JFXTextField txtMotivoParada;
    @FXML
    private JFXListView<UnidadeMotivoParadaViewModel> lvMotivoParada;
    @FXML
    private JFXButton btnVincular;
    @FXML
    private TableView<UnidadeMotivoComTipoParadaViewModel> tbvVinculo;
    @FXML
    private TableColumn<UnidadeMotivoComTipoParadaViewModel, String> tcTipoParada;
    @FXML
    private TableColumn<UnidadeMotivoComTipoParadaViewModel, String> tcMotivoParada;
    @FXML
    private JFXButton btnCancelar;

    public CadVinculoTipoParadaMotivoParada() {
        listaVinculos = new ArrayList();
        listaNovosVinculosConfirmados = new ArrayList();
    }

    public void setParametrosIniciais(int idUnidade, List<UnidadeMotivoComTipoParadaViewModel> listaVinculos) {
        this.idUnidade = idUnidade;
        setListaVinculo(listaVinculos);
        this.listaNovosVinculos = new ArrayList();
        carregarTiposEMotivos(this.idUnidade);
        carregaTabela(this.listaVinculos);
    }

    private void carregarTiposEMotivos(int idUnidade) {
        lvTipoParada.getItems().clear();
        lvTipoParada.getItems().addAll(controllerUnidadeTipoParada.getTiposParadaUnidade(idUnidade));

        lvMotivoParada.getItems().clear();
        lvMotivoParada.getItems().addAll(controllerUnidadeMotivoParada.getMotivosParadaUnidade(idUnidade));
    }

    private void setListaVinculo(List<UnidadeMotivoComTipoParadaViewModel> listaVinculos) {
        try {
            this.listaVinculos.addAll(listaVinculos);
        } catch (NullPointerException ex) {
            System.out.println("Erro " + ex.getMessage());
            this.listaVinculos = new ArrayList();
        }
    }

    public List<UnidadeMotivoComTipoParadaViewModel> getNovosVinculos() {
        if (this.listaNovosVinculosConfirmados == null) {
            listaNovosVinculosConfirmados = new ArrayList();
        }
        return this.listaNovosVinculosConfirmados;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lvMotivoParada.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tbvVinculo.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        destacarLinhasVinculos();
    }

    private void destacarLinhasVinculos() {
        tbvVinculo.setRowFactory(tv -> {
            return new TableRow<UnidadeMotivoComTipoParadaViewModel>() {
                @Override
                public void updateItem(UnidadeMotivoComTipoParadaViewModel vinculo, boolean novo) {
                    super.updateItem(vinculo, novo);
                    if (listaVinculos.contains(vinculo)) {
                        setStyle("-fx-text-background-color: blue;");
                    } else {
                        setStyle("-fx-text-background-color: green;");
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML
    private void clkSair(ActionEvent event) {
        listaNovosVinculosConfirmados = null;
        sair();
    }

    @FXML
    private void clkCancelar(ActionEvent event) {
        listaNovosVinculosConfirmados = null;
        sair();
    }

    @FXML
    private void clkConfirmar(ActionEvent event) {
        try {
            listaNovosVinculosConfirmados.clear();
            listaNovosVinculosConfirmados.addAll(listaNovosVinculos);
        } catch (NullPointerException ex) {
            listaNovosVinculosConfirmados = new ArrayList();
        }
        sair();
    }

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void kpBuscaTipoParada(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            clkBuscaTipoParada(null);
        }
    }

    @FXML
    private void clkBuscaTipoParada(ActionEvent event) {
        String retorno = txtTipoParada.getText().trim().toLowerCase();

        if (retorno.length() > 0 && lvTipoParada.getItems().size() > 0) {
            int i = 0;
            while (i < lvTipoParada.getItems().size()
                    && !lvTipoParada.getItems().get(i).getNomeComSigla().toLowerCase().contains(retorno)) {
                i++;
            }
            if (i < lvTipoParada.getItems().size()) {
                selecionarLinhaListView(lvTipoParada, i);
            }
        }
    }

    @FXML
    private void kpBuscarMotivoParada(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            clkBuscarMotivoParada(null);
        }
    }

    @FXML
    private void clkBuscarMotivoParada(ActionEvent event) {
        String retorno = txtMotivoParada.getText().trim().toLowerCase();
        if (retorno.length() > 0 && lvMotivoParada.getItems().size() > 0) {
            int i = 0;
            while (i < lvMotivoParada.getItems().size()
                    && !lvMotivoParada.getItems().get(i).getNomeComSigla().toLowerCase().contains(retorno)) {
                i++;
            }
            if (i < lvMotivoParada.getItems().size()) {
                selecionarLinhaListView(lvMotivoParada, i);
            }
        }
    }

    private void selecionarLinhaListView(JFXListView listView, int index) {
        try {
            if (index < listView.getItems().size()) {
                listView.getSelectionModel().clearAndSelect(index);
                listView.scrollTo(index);
            }
        } catch (NullPointerException ex) {

        }
    }

    @FXML
    private void clkDuploCliqueMotivo(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkVincular(null);
        }
    }

    @FXML
    private void clkVincular(ActionEvent event) {
        UnidadeTipoParadaViewModel tipo = lvTipoParada.getSelectionModel().getSelectedItem();
        List<UnidadeMotivoParadaViewModel> motivo = lvMotivoParada.getSelectionModel().getSelectedItems();
        List<UnidadeMotivoComTipoParadaViewModel> listaNovosVinculosAux = criarListaInsercao(tipo, motivo);

        if (!listaNovosVinculosAux.isEmpty()) {
            List<String> erros = addListaVinculo(listaNovosVinculosAux);

            carregaTabela(listaVinculos, listaNovosVinculos);
            
            if (!erros.isEmpty()) {
                MsgBox.exibeLista(erros);
            }
        }
    }

    private List<UnidadeMotivoComTipoParadaViewModel> criarListaInsercao(UnidadeTipoParadaViewModel tipo, List<UnidadeMotivoParadaViewModel> motivo) {
        List<UnidadeMotivoComTipoParadaViewModel> listaVinculosAux = new ArrayList();
        try {
            motivo.forEach((objMotivo) -> {
                listaVinculosAux.add(new UnidadeMotivoComTipoParadaViewModel(
                        true,
                        LocalDate.now(),
                        "",
                        tipo.getId(),
                        objMotivo.getId()
                ));
            });
        } catch (NullPointerException ex) {
            listaVinculosAux.clear();
        }
        return listaVinculosAux;
    }

    private List<String> addListaVinculo(List<UnidadeMotivoComTipoParadaViewModel> novaLista) {
        List<String> listaNaoAdd = new ArrayList<>();

        novaLista.forEach((vinculo) -> {
            if (!addNovoVinculo(vinculo)) {
                listaNaoAdd.add("(Verifique duplicidade) -> " + vinculo.getSiglaNomeVinculoConcatenado());
            }
        });

        return listaNaoAdd;
    }

    private boolean addNovoVinculo(UnidadeMotivoComTipoParadaViewModel vinculo) {
        boolean sucesso = false;

        if (!existeNaLista(vinculo)) {
            try {
                sucesso = this.listaNovosVinculos.add(vinculo);
            } catch (NullPointerException ex) {
                sucesso = false;
            }
        }
        return sucesso;
    }

    public boolean existeNaLista(UnidadeMotivoComTipoParadaViewModel vinculo) {
        try {
            return this.listaVinculos.contains(vinculo) || this.listaNovosVinculos.contains(vinculo);
        } catch (NullPointerException ex) {
            return false;
        }
    }

    private void carregaTabela(List<UnidadeMotivoComTipoParadaViewModel> lista1, List<UnidadeMotivoComTipoParadaViewModel> lista2) {
        carregaTabela(concatenarListas(lista1, lista2));
    }

    private List<UnidadeMotivoComTipoParadaViewModel> concatenarListas(
            List<UnidadeMotivoComTipoParadaViewModel> lista1, List<UnidadeMotivoComTipoParadaViewModel> lista2) {
        List<UnidadeMotivoComTipoParadaViewModel> novaLista = new ArrayList();
        try {
            novaLista.addAll(lista1);
            novaLista.addAll(lista2);
        } catch (NullPointerException ex) {
            //excessao encontrada
            System.out.println("erro concatenar: " + ex.getMessage());
        }
        return novaLista;
    }

    private void carregaTabela(List<UnidadeMotivoComTipoParadaViewModel> lista) {
        ObservableList<UnidadeMotivoComTipoParadaViewModel> obsLista;
        tbvVinculo.getItems().clear();

        tcTipoParada.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeTipoParada().getNomeComSigla()));
        tcMotivoParada.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeMotivoParada().getNomeComSigla()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvVinculo.setItems(obsLista);
        tbvVinculo.getSelectionModel().select(-1);
    }

    @FXML
    private void clkDuploCliqueVinculo(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkRemover(null);
        }
    }

    @FXML
    private void clkRemover(ActionEvent event) {
        List<String> listaNaoRemovido = new ArrayList();
        List<UnidadeMotivoComTipoParadaViewModel> vinculosAux = tbvVinculo.getSelectionModel().getSelectedItems();
        try {
            vinculosAux.forEach((obj) -> {
                if (!removerVinculoDaLista(obj)) {
                    listaNaoRemovido.add("(Não pode ser removido) -> " + obj.getSiglaNomeVinculoConcatenado());
                }
            });
        } catch (NullPointerException ex) {
        }
        carregaTabela(listaVinculos, listaNovosVinculos);
        
        if (!listaNaoRemovido.isEmpty()) {
            MsgBox.exibeLista(listaNaoRemovido);
        }
    }

    private boolean removerVinculoDaLista(UnidadeMotivoComTipoParadaViewModel vinculo) {
        try {
            if (listaNovosVinculos.contains(vinculo)) {
                return listaNovosVinculos.remove(vinculo);
            }
        } catch (NullPointerException ex) {
        }
        return false;
    }

}
