package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import industria.util.MsgBox;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadeAreaController;
import lib.controller.associacao.UnidadeEquipamentoController;
import lib.viewmodel.associacao.UnidadeAreaComEquipamentoViewModel;
import lib.viewmodel.associacao.UnidadeAreaViewModel;
import lib.viewmodel.associacao.UnidadeEquipamentoViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadVinculoAreaEquipamento implements Initializable {

    private double cx, cy;

    private int idUnidade = 0;
    private final UnidadeAreaController controllerUnidadeArea = new UnidadeAreaController();
    private final UnidadeEquipamentoController controllerUnidadeEquipamento = new UnidadeEquipamentoController();
    private List<UnidadeAreaComEquipamentoViewModel> listaNovosVinculos;
    private List<UnidadeAreaComEquipamentoViewModel> listaNovosVinculosConfirmados;
    private List<UnidadeAreaComEquipamentoViewModel> listaVinculos;

    @FXML
    private Button btnSair;
    @FXML
    private JFXTabPane tpPrincipal;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtArea;
    @FXML
    private JFXListView<UnidadeAreaViewModel> lvArea;
    @FXML
    private JFXTextField txtEquipamento;
    @FXML
    private JFXListView<UnidadeEquipamentoViewModel> lvEquipamento;
    @FXML
    private JFXButton btnVincular;
    @FXML
    private TableView<UnidadeAreaComEquipamentoViewModel> tbvVinculo;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcArea;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcEquipamento;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcReferencia;

    // ok
    public CadVinculoAreaEquipamento() {
        this.listaVinculos = new ArrayList<>();
        this.listaNovosVinculosConfirmados = new ArrayList<>();
    }

    // ok
    public void setParametrosIniciais(int idUnidade, List<UnidadeAreaComEquipamentoViewModel> listaVinculos) {
        this.idUnidade = idUnidade;
        setListaVinculo(listaVinculos);
        this.listaNovosVinculos = new ArrayList<>();
        carregarAreasEEquipamentos(this.idUnidade);
        carregaTabela(this.listaVinculos);
        permiteEditar(tbvVinculo, tcReferencia);
    }

    // ok
    private void carregarAreasEEquipamentos(int idUnidade) {
        lvArea.getItems().clear();
        lvArea.getItems().addAll(controllerUnidadeArea.getAreasUnidade(idUnidade));

        lvEquipamento.getItems().clear();
        lvEquipamento.getItems().addAll(controllerUnidadeEquipamento.getEquipamentosUnidade(idUnidade));
    }

    // ok
    private void setListaVinculo(List<UnidadeAreaComEquipamentoViewModel> listaVinculos) {
        try {
            this.listaVinculos.addAll(listaVinculos);
        } catch (NullPointerException ex) {
            System.out.println("Erro" + ex.getMessage());
            this.listaVinculos = new ArrayList<>();
        }
    }

    // ok
    public List<UnidadeAreaComEquipamentoViewModel> getNovosVinculos() {
        if (this.listaNovosVinculosConfirmados == null) {
            listaNovosVinculosConfirmados = new ArrayList<>();
        }
        return this.listaNovosVinculosConfirmados;
    }

    @Override // ok
    public void initialize(URL url, ResourceBundle rb) {
        lvEquipamento.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tbvVinculo.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        destacarLinhasVinculos();
    }

    //ok
    private void destacarLinhasVinculos() {
        tbvVinculo.setRowFactory(tv -> {
            return new TableRow<UnidadeAreaComEquipamentoViewModel>() {
                @Override
                public void updateItem(UnidadeAreaComEquipamentoViewModel vinculo, boolean novo) {
                    super.updateItem(vinculo, novo);
                    try {
                        //if (listaVinculos.contains(vinculo)) {
                        if (vinculo.getId() > 0) {
                            setStyle("-fx-text-background-color: blue;");
                        } else {
                            setStyle("-fx-text-background-color: green;");
                        }
                    } catch (NullPointerException ex) {

                    }

                    //se vínculo ainda não tiver uma referência
                    try {
                        if (vinculo.getId() == 0 && vinculo.getReferencia().equals("")) {
                            setStyle("-fx-text-background-color: red;");
                        }
                    } catch (NullPointerException ex) {
                        setStyle("-fx-text-background-color: red;");
                    }
                }
            };
        });
    }

    @FXML// ok

    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        listaNovosVinculosConfirmados = null;
        sair();
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        listaNovosVinculosConfirmados = null;
        sair();
    }

    @FXML
    private void clkConfirmar(ActionEvent event) {
        try {
            listaNovosVinculosConfirmados.clear();
            listaNovosVinculosConfirmados.addAll(listaNovosVinculos);
        } catch (NullPointerException ex) {
            listaNovosVinculosConfirmados = new ArrayList();
        }
        sair();
    }

    //ok
    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpBuscarArea(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            clkBuscarArea(null);
        }
    }

    @FXML// ok
    private void clkBuscarArea(ActionEvent event) {
        String retorno = txtArea.getText().trim().toLowerCase();

        if (retorno.length() > 0 && lvArea.getItems().size() > 0) {
            int i = 0;
            while (i < lvArea.getItems().size() && !lvArea.getItems().get(i).getArea().getNome().toLowerCase().contains(retorno)) {
                i++;
            }
            if (i < lvArea.getItems().size()) {
                selecionarLinhaListView(lvArea, i);
            }
        }
    }

    @FXML// ok
    private void kpBuscarEquipamento(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            clkBuscarEquipamento(null);
        }
    }

    @FXML// ok
    private void clkBuscarEquipamento(ActionEvent event) {
        String retorno = txtEquipamento.getText().trim().toLowerCase();

        if (retorno.length() > 0 && lvEquipamento.getItems().size() > 0) {
            int i = 0;
            while (i < lvEquipamento.getItems().size() && !lvEquipamento.getItems().get(i).toString().toLowerCase().contains(retorno)) {
                i++;
            }
            if (i < lvEquipamento.getItems().size()) {

                selecionarLinhaListView(lvEquipamento, i);
            }
        }

    }

    // ok
    private void selecionarLinhaListView(JFXListView listView, int index) {
        try {
            if (index < listView.getItems().size()) {
                listView.getSelectionModel().clearAndSelect(index);
                listView.scrollTo(index);
            }
        } catch (NullPointerException ex) {

        }
    }

    @FXML// ok
    private void clkDuploCliqueEquipamento(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkVincular(null);
        }
    }

    @FXML// ok
    private void clkVincular(ActionEvent event) {
        UnidadeAreaViewModel area = lvArea.getSelectionModel().getSelectedItem();
        List<UnidadeEquipamentoViewModel> equipamentos = lvEquipamento.getSelectionModel().getSelectedItems();
        List<UnidadeAreaComEquipamentoViewModel> novosEquipamentosAux = criarListaInsercao(area, equipamentos);

        if (!novosEquipamentosAux.isEmpty()) {
            List<String> erros = addListaVinculo(novosEquipamentosAux);

            carregaTabela(listaVinculos, listaNovosVinculos);

            if (!erros.isEmpty()) {
                MsgBox.exibeLista(erros);
            }
        }
    }

    // ok
    private List<UnidadeAreaComEquipamentoViewModel> criarListaInsercao(UnidadeAreaViewModel area, List<UnidadeEquipamentoViewModel> equipamentos) {
        List<UnidadeAreaComEquipamentoViewModel> vinculosAux = new ArrayList<>();
        try {
            equipamentos.forEach((objEquipamento) -> {
                vinculosAux.add(new UnidadeAreaComEquipamentoViewModel(
                        true,
                        "",
                        LocalDate.now(),
                        "",
                        area.getId(),
                        objEquipamento.getId()
                ));
            });
        } catch (NullPointerException ex) {
            vinculosAux.clear();
        }
        return vinculosAux;
    }

    // ok
    private List<String> addListaVinculo(List<UnidadeAreaComEquipamentoViewModel> novaLista) {
        List<String> listaNaoAdd = new ArrayList<>();

        novaLista.forEach((vinculo) -> {
            if (!addNovoVinculo(vinculo)) {
                listaNaoAdd.add("(Vericique duplicidade) -> " + vinculo.getNomeVinculoConcatenado());
            }
        });

        return listaNaoAdd;
    }

    // ok
    private boolean addNovoVinculo(UnidadeAreaComEquipamentoViewModel vinculo) {
        boolean sucesso = false;
        if (!existeNaLista(vinculo)) {
            try {
                sucesso = this.listaNovosVinculos.add(vinculo);
            } catch (NullPointerException ex) {
                sucesso = false;
            }
        }
        return sucesso;
    }

    // ok
    public boolean existeNaLista(UnidadeAreaComEquipamentoViewModel vinculo) {
        try {
            return this.listaNovosVinculos.contains(vinculo);
            //return this.listaVinculos.contains(vinculo) || this.listaNovosVinculos.contains(vinculo);
        } catch (NullPointerException ex) {
            return false;
        }
    }

    // ok
    private void carregaTabela(List<UnidadeAreaComEquipamentoViewModel> lista1, List<UnidadeAreaComEquipamentoViewModel> lista2) {
        carregaTabela(concatenarListas(lista1, lista2));
    }

    // ok
    private List<UnidadeAreaComEquipamentoViewModel> concatenarListas(
            List<UnidadeAreaComEquipamentoViewModel> lista1, List<UnidadeAreaComEquipamentoViewModel> lista2) {
        List<UnidadeAreaComEquipamentoViewModel> novaLista = new ArrayList<>();
        try {
            novaLista.addAll(lista1);
            novaLista.addAll(lista2);
        } catch (NullPointerException ex) {
            System.out.println("erro concatenar: " + ex.getMessage());
        }
        return novaLista;
    }

    // ok
    private void carregaTabela(List<UnidadeAreaComEquipamentoViewModel> lista) {
        ObservableList<UnidadeAreaComEquipamentoViewModel> obsLista;
        tbvVinculo.getItems().clear();

        tcArea.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeArea().getArea().getNome()));
        tcEquipamento.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeEquipamento().toString()));
        tcReferencia.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getReferencia()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvVinculo.setItems(obsLista);
        tbvVinculo.getSelectionModel().select(-1);
    }

    @FXML// ok
    private void clkDuploCliqueVinculo(MouseEvent event) {
        if (event.getClickCount() == 2) {
            //clkRemover(null);
        }
    }

    @FXML// ok
    private void clkRemover(ActionEvent event) {
        List<String> listaNaoRemovido = new ArrayList<>();
        List<UnidadeAreaComEquipamentoViewModel> vinculosAux = tbvVinculo.getSelectionModel().getSelectedItems();

        try {
            vinculosAux.forEach((obj) -> {
                if (!removerVinculoDaLista(obj)) {
                    listaNaoRemovido.add("(Não pode ser removido) -> " + obj.getNomeVinculoConcatenado());
                }
            });
        } catch (NullPointerException ex) {
        }
        carregaTabela(listaVinculos, listaNovosVinculos);

        if (!listaNaoRemovido.isEmpty()) {
            MsgBox.exibeLista(listaNaoRemovido);
        }
    }

    // ok
    private boolean removerVinculoDaLista(UnidadeAreaComEquipamentoViewModel vinculo) {
        try {
            if (listaNovosVinculos.contains(vinculo) && vinculo.getId() == 0) {
                return listaNovosVinculos.remove(vinculo);
            }
        } catch (NullPointerException ex) {
        }
        return false;
    }

    private void permiteEditar(TableView tabela, TableColumn coluna) {
        tabela.setEditable(true);
        coluna.setCellFactory(TextFieldTableCell.forTableColumn());
    }

    @FXML
    private void editTcReferencia(TableColumn.CellEditEvent<UnidadeAreaComEquipamentoViewModel, String> event) {

        try {
            UnidadeAreaComEquipamentoViewModel vinculo = tbvVinculo.getSelectionModel().getSelectedItem();
            UnidadeAreaComEquipamentoViewModel vincAux = vinculo.getCopiaObj();
            String ref = event.getNewValue();
            vincAux.setReferencia(ref);
            if (vinculo.getId() <= 0) {
                if (listaNovosVinculos.contains(vinculo)) {
                    if (!listaNovosVinculos.contains(vincAux)) {
                        vinculo.setReferencia(ref);
                        listaNovosVinculos.get(listaNovosVinculos.indexOf(vinculo)).setReferencia(ref);

                    } else {
                        exibeAlerta(2409);
                    }

                } else {
                    exibeAlerta(2409);
                }
            }
        } catch (NullPointerException ex) {

        }
        carregaTabela(listaVinculos, listaNovosVinculos);
    }

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Vínculo Equipamento com Área - " + String.valueOf(status), "", "");
        switch (status) {
            case 2409:
                dialog.setInformacao("Duplicidade", "vínculo ja registrado para esta unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

}
