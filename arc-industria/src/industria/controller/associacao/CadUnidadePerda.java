package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesPerda;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadePerdaController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.associacao.UnidadePerdaViewModel;
import lib.viewmodel.associacao.UnidadexPerdasViewModel;
import lib.viewmodel.cadastro.PerdaViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadePerda implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadePerdaController controllerUnidadePerda = new UnidadePerdaController();

    private UnidadexPerdasViewModel vmUnidadeComPerdas = null;
    private int idPerdaUnidade = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadePerdaViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadePerdaViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadePerdaViewModel, String> tcPerda;
    @FXML
    private TableColumn<UnidadePerdaViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXTextField txtPerdaObs;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidadeComPerdas = new UnidadexPerdasViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizarInterface(vmUnidadeComPerdas);
        botoes(true);
    }

    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadePerdaViewModel>() {
                @Override
                public void updateItem(UnidadePerdaViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidadeComPerdas = new UnidadexPerdasViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizarInterface(vmUnidadeComPerdas);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidadeComPerdas.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidadeComPerdas.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComPerdas = new UnidadexPerdasViewModel(unAux);
            }
            atualizarInterface(vmUnidadeComPerdas);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidadeComPerdas.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidadeComPerdas.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComPerdas = new UnidadexPerdasViewModel(unAux);
            }
            atualizarInterface(vmUnidadeComPerdas);
            botoes(true);
        }
    }

    @FXML//ok 
    private void clkUltimo(ActionEvent event) {
        vmUnidadeComPerdas = new UnidadexPerdasViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizarInterface(vmUnidadeComPerdas);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        atualizarUnidade(unidadeAux);
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        atualizarUnidade(codigoAux);
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadePerdaViewModel vmPerdaUnidade = tbvDados.getSelectionModel().getSelectedItem();
            if (vmPerdaUnidade != null) {
                Alerta dialog = new Alerta("Perda", "Dados do vínculo",
                        "Data do vínculo: " + vmPerdaUnidade.getDataFormatada()
                        + "\nStatus do vínculo: " + vmPerdaUnidade.isAtivoToString()
                        + "\n\nObservação: \n" + vmPerdaUnidade.getObservacao());
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML//ok 
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        PerdaViewModel perdaAux;
        UnidadePerdaViewModel vmPerdaUnidade;

        if (vmUnidadeComPerdas != null && vmUnidadeComPerdas.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/pesquisa/PesPerda.fxml", "Buscar perda", false, true, false);
                PesPerda controller = tl.getLoader().getController();
                controller.setAcessoTerceiro(true);
                tl.exibirTelaFilha();// exibe a tela
                perdaAux = controller.getPerda();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                perdaAux = null;
                System.out.println("erro ao exibir janela de pesquisa");
            }

            if (perdaAux != null) {
                vmPerdaUnidade = new UnidadePerdaViewModel(
                        true,
                        LocalDate.now(),
                        "",
                        perdaAux.getId()
                );

                statusGravacao = controllerUnidadePerda.gravar(vmUnidadeComPerdas.getUnidade().getId(), vmPerdaUnidade);
                if (statusGravacao > 0) {
                    //não gravado
                    exibeAlerta(statusGravacao);
                } else {
                    //gravado com sucesso
                    vmPerdaUnidade = controllerUnidadePerda.getRegPorUnidadeEPerda(
                            vmUnidadeComPerdas.getUnidade().getId(),
                            vmPerdaUnidade.getPerda().getId()
                    );
                    vmUnidadeComPerdas.addPerdaNaLista(vmPerdaUnidade);
                    atualizarInterface(vmUnidadeComPerdas);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComPerdas.getPosicaoPerdaNaLista(vmPerdaUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadePerdaViewModel vmPerdaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmPerdaUnidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Perda da unidade", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidadePerda.excluir(vmUnidadeComPerdas.getUnidade().getId(), vmPerdaUnidade) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    vmUnidadeComPerdas.removerPerdaDaLista(vmPerdaUnidade);
                    carregaTabela(vmUnidadeComPerdas.getListaPerdas());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadePerdaViewModel vmPerdaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmPerdaUnidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Perda da unidade", "", "");
            if (vmPerdaUnidade.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar perda inativa para a unidade?");
            } else {
                dialog.setInformacao("Ativar registro", "tornar perda ativa para a unidade?");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                vmPerdaUnidade.setAtivo(!vmPerdaUnidade.isAtivo());
                int status = controllerUnidadePerda.alterar(vmUnidadeComPerdas.getUnidade().getId(), vmPerdaUnidade);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidadeComPerdas.atualizarPerdaNaLista(vmPerdaUnidade);
                    carregaTabela(vmUnidadeComPerdas.getListaPerdas());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComPerdas.getPosicaoPerdaNaLista(vmPerdaUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadePerdaViewModel vmPerdaUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmPerdaUnidade != null) {
            atualizarInterfaceAlteracao(vmPerdaUnidade);
            botoes(false);
        }
    }

    @FXML
    private void clkBuscarNaLista(ActionEvent event) {
        Alerta dialog = new Alerta("Perda", "", "");
        String retorno;

        retorno = dialog.caixaTexto("nome");
        retorno = retorno.trim();
        if (retorno.length() > 0 && vmUnidadeComPerdas != null && vmUnidadeComPerdas.getListaPerdas() != null) {
            int pos = 0;
            while (pos < vmUnidadeComPerdas.getListaPerdas().size()
                    && !vmUnidadeComPerdas.getListaPerdas().get(pos).getPerda().getNome().toLowerCase().contains(retorno.toLowerCase())) {
                pos++;
            }
            if (pos < vmUnidadeComPerdas.getListaPerdas().size()) {
                TableViewUtil.selecionarLinhaTabela(tbvDados, pos);
            }
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadePerdaViewModel vmPerdaUnidade = controllerUnidadePerda.getRegPorCodigo(idPerdaUnidade);
        int statusGravacao;
        if (vmUnidadeComPerdas != null && vmPerdaUnidade != null) {
            if (vmPerdaUnidade.isAtivo() != rbAtivo.isSelected() || !vmPerdaUnidade.getObservacao().equals(txtObservacao.getText())) {
                vmPerdaUnidade.setAtivo(rbAtivo.isSelected());
                vmPerdaUnidade.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadePerda.alterar(vmUnidadeComPerdas.getUnidade().getId(), vmPerdaUnidade);
                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    vmUnidadeComPerdas.atualizarPerdaNaLista(vmPerdaUnidade);
                    atualizarInterface(vmUnidadeComPerdas);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComPerdas.getPosicaoPerdaNaLista(vmPerdaUnidade));
                    limparCampos();
                    botoes(true);
                }
            } else {
                limparCampos();
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Perda da unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1903:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Unidade", "unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1904:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Perda", "perda não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1905:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1906:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1907:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1908:
                dialog.setInformacao("Duplicidade", "perda já vinculada à unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizarUnidade(UnidadeViewModel unidade) {
        UnidadexPerdasViewModel unAux = new UnidadexPerdasViewModel(unidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeComPerdas = unAux;
            atualizarInterface(vmUnidadeComPerdas);
            botoes(true);
        }
    }

    private void atualizarUnidade(int codUnidade) {
        UnidadexPerdasViewModel unAux = new UnidadexPerdasViewModel(codUnidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeComPerdas = unAux;
            atualizarInterface(vmUnidadeComPerdas);
            botoes(true);
        }
    }

    private void atualizarInterface(UnidadexPerdasViewModel unidadeComPerdas) {
        if (unidadeComPerdas != null && unidadeComPerdas.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComPerdas.getUnidade().getId()));
            txtDescricao.setText(unidadeComPerdas.getUnidade().getNomeFormatado());
            if (unidadeComPerdas.getListaPerdas() != null) {
                carregaTabela(unidadeComPerdas.getListaPerdas());
            }
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizarInterfaceAlteracao(UnidadePerdaViewModel vmPerdaUndiade) {
        if (vmPerdaUndiade != null) {
            idPerdaUnidade = vmPerdaUndiade.getId();
            txtPerdaObs.setText(vmPerdaUndiade.getPerda().getPerdaFormatada());
            txtDataObs.setText(vmPerdaUndiade.getDataFormatada());
            if (vmPerdaUndiade.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmPerdaUndiade.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idPerdaUnidade = 0;
        txtPerdaObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void carregaTabela(List<UnidadePerdaViewModel> lista) {
        ObservableList<UnidadePerdaViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcPerda.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getPerda().getPerdaFormatada()));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    //true - ativa: novo e excluir, 
    //false - ativa: gravar e cancelar
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
    }

}
