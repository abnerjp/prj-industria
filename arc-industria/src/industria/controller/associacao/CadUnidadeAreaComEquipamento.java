package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadeAreaComEquipamentoController;
import lib.controller.cadastro.UnidadeController;
import lib.util.Texto;
import lib.viewmodel.associacao.UnidadeAreaComEquipamentoViewModel;
import lib.viewmodel.associacao.UnidadexAreasComEquipamentosViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadeAreaComEquipamento implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeAreaComEquipamentoController controllerUnidadeEquipamentoComArea = new UnidadeAreaComEquipamentoController();

    private UnidadexAreasComEquipamentosViewModel vmUnidadeVinculosAreaComEquipamento = null;
    private int idUnidadeEquipamentoComArea = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeAreaComEquipamentoViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcArea;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcEquipamento;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcReferencia;
    @FXML
    private TableColumn<UnidadeAreaComEquipamentoViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXTextField txtNomeVinculoObs;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtReferenciaObs;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidadeVinculosAreaComEquipamento = new UnidadexAreasComEquipamentosViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizaInterface(vmUnidadeVinculosAreaComEquipamento);
        bloqueiaCampos();
        botoes(true);
    }

    // ok
    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadeAreaComEquipamentoViewModel>() {
                @Override
                public void updateItem(UnidadeAreaComEquipamentoViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidadeVinculosAreaComEquipamento = new UnidadexAreasComEquipamentosViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizaInterface(vmUnidadeVinculosAreaComEquipamento);
        botoes(true);

    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidadeVinculosAreaComEquipamento.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidadeVinculosAreaComEquipamento.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeVinculosAreaComEquipamento = new UnidadexAreasComEquipamentosViewModel(unAux);
            }
            atualizaInterface(vmUnidadeVinculosAreaComEquipamento);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidadeVinculosAreaComEquipamento.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidadeVinculosAreaComEquipamento.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeVinculosAreaComEquipamento = new UnidadexAreasComEquipamentosViewModel(unAux);
            }
            atualizaInterface(vmUnidadeVinculosAreaComEquipamento);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUnidadeVinculosAreaComEquipamento = new UnidadexAreasComEquipamentosViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizaInterface(vmUnidadeVinculosAreaComEquipamento);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        atualizaUnidade(unidadeAux);
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        atualizaUnidade(codigoAux);
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadeAreaComEquipamentoViewModel vmVinculoAux = tbvDados.getSelectionModel().getSelectedItem();
            if (vmVinculoAux != null) {
                Alerta dialog = new Alerta("Vínculo de equipamento", "Dados do vínculo",
                        "Data do vínculo: " + vmVinculoAux.getDataFormatada()
                        + "\nStatus do vínculo: " + vmVinculoAux.isAtivoToString()
                        + "\n\nObservação: \n" + vmVinculoAux.getObservacao()
                );
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        List<UnidadeAreaComEquipamentoViewModel> listaVinculoAux;

        if (vmUnidadeVinculosAreaComEquipamento != null && vmUnidadeVinculosAreaComEquipamento.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/associacao/CadVinculoAreaEquipamento.fxml", "", false, true, false);
                CadVinculoAreaEquipamento controller = tl.getLoader().getController();
                controller.setParametrosIniciais(vmUnidadeVinculosAreaComEquipamento.getIdUnidade(), vmUnidadeVinculosAreaComEquipamento.getListaVinculos());
                tl.exibirTelaFilha();
                listaVinculoAux = controller.getNovosVinculos();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                listaVinculoAux = new ArrayList<>();
                System.out.println("erro ao exibir janela de pesquisa " + ex.getMessage());
            }

            if (listaVinculoAux.size() > 0) {
                statusGravacao = controllerUnidadeEquipamentoComArea.gravar(listaVinculoAux);

                if (statusGravacao > 0) {
                    // não gravado no bd
                    exibeAlerta(statusGravacao);

                    if (!(statusGravacao != 2410)) {
                        atualizarAposNovosVinculos();
                    }
                } else {
                    // gravado com sucesso no bd
                    atualizarAposNovosVinculos();
                }
            }
        }
    }

    private void atualizarAposNovosVinculos() {
        vmUnidadeVinculosAreaComEquipamento.recarregarVinculos();
        carregaTabela(vmUnidadeVinculosAreaComEquipamento.getListaVinculos());
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeAreaComEquipamentoViewModel vmVinculoEquipamentoAux = tbvDados.getSelectionModel().getSelectedItem();
        if (vmVinculoEquipamentoAux != null && !btnExcluir.isDisabled()) {
            Alerta dialog = new Alerta("Vínculo de equipamento da unidade", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidadeEquipamentoComArea.excluir(vmVinculoEquipamentoAux) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    vmUnidadeVinculosAreaComEquipamento.removeVinculoDaLista(vmVinculoEquipamentoAux);
                    carregaTabela(vmUnidadeVinculosAreaComEquipamento.getListaVinculos());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadeAreaComEquipamentoViewModel vmVinculoEquipamentoAux = tbvDados.getSelectionModel().getSelectedItem();
        if (vmVinculoEquipamentoAux != null && !btnExcluir.isDisabled()) {
            Alerta dialog = new Alerta("Vínculo de equipamento da unidade", "", "");
            if (vmVinculoEquipamentoAux.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar este vínculo de equipamentos inativo para a unidade");
            } else {
                dialog.setInformacao("Ativar registro", "tornar este vínculo de equipamentos ativo para a unidade");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                vmVinculoEquipamentoAux.setAtivo(!vmVinculoEquipamentoAux.isAtivo());
                int status = controllerUnidadeEquipamentoComArea.alterar(vmVinculoEquipamentoAux);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidadeVinculosAreaComEquipamento.atualizarVinculoNaLista(vmVinculoEquipamentoAux);
                    carregaTabela(vmUnidadeVinculosAreaComEquipamento.getListaVinculos());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeVinculosAreaComEquipamento.getPosicaoVinculoNaLista(vmVinculoEquipamentoAux));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadeAreaComEquipamentoViewModel vmVinculoEquipamentoAux = tbvDados.getSelectionModel().getSelectedItem();
        if (vmVinculoEquipamentoAux != null) {
            atualizaInterfaceAlteracao(vmVinculoEquipamentoAux);
            botoes(false);
        }
    }

    @FXML// ok
    private void clkBuscarNaLista(ActionEvent event) {
        Alerta dialog = new Alerta("Vínculo de equipamento", "", "");
        String retorno;

        retorno = dialog.caixaTexto("nome área/equipamento");
        retorno = Texto.trimAll(retorno);
        if (retorno.length() > 0 && vmUnidadeVinculosAreaComEquipamento != null && vmUnidadeVinculosAreaComEquipamento.getListaVinculos() != null) {
            int pos = 0;
            while (pos < vmUnidadeVinculosAreaComEquipamento.getListaVinculos().size()
                    && !Texto.trimAll(vmUnidadeVinculosAreaComEquipamento.getNomeVinculoConcatenado(pos).toLowerCase()).contains(retorno.toLowerCase())) {
                pos++;
            }
            if (pos < vmUnidadeVinculosAreaComEquipamento.getListaVinculos().size()) {
                TableViewUtil.selecionarLinhaTabela(tbvDados, pos);
            }
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadeAreaComEquipamentoViewModel vmVinculoUnidadeAreaComEquipamento = controllerUnidadeEquipamentoComArea.getRegPorCodigoDoVinculo(idUnidadeEquipamentoComArea);
        int statusGravacao;
        if (this.vmUnidadeVinculosAreaComEquipamento != null && vmVinculoUnidadeAreaComEquipamento != null) {
            if (vmVinculoUnidadeAreaComEquipamento.isAtivo() != rbAtivo.isSelected() || !vmVinculoUnidadeAreaComEquipamento.getObservacao().equals(txtObservacao.getText())) {
                vmVinculoUnidadeAreaComEquipamento.setAtivo(rbAtivo.isSelected());
                vmVinculoUnidadeAreaComEquipamento.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadeEquipamentoComArea.alterar(vmVinculoUnidadeAreaComEquipamento);
                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    this.vmUnidadeVinculosAreaComEquipamento.atualizarVinculoNaLista(vmVinculoUnidadeAreaComEquipamento);
                    atualizaInterface(this.vmUnidadeVinculosAreaComEquipamento);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, this.vmUnidadeVinculosAreaComEquipamento.getPosicaoVinculoNaLista(vmVinculoUnidadeAreaComEquipamento));
                    limparCampos();
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }
//------------------------------------------------------------------------------    

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Vínculo Equipamento com Área - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2403:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Área", "área da unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2404:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Equipamento", "equipamento da unidade não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2405:
                dialog.setInformacao("Referência", "referência não pode conter mais que 06 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2406:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2407:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 2408:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2409:
                dialog.setInformacao("Duplicidade", "vínculo ja registrado para esta unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2410:
                dialog.setInformacao("Lista", "um ou mais itens da lista não atende as regras de negócio, portanto esses itens não serão gravados.");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaUnidade(UnidadeViewModel unidade) {
        UnidadexAreasComEquipamentosViewModel unAux = new UnidadexAreasComEquipamentosViewModel(unidade);
        if (unAux.getUnidade() != null) {
            this.vmUnidadeVinculosAreaComEquipamento = unAux;
            atualizaInterface(this.vmUnidadeVinculosAreaComEquipamento);
            botoes(true);
        }
    }

    private void atualizaUnidade(int codUnidade) {
        UnidadexAreasComEquipamentosViewModel unAux = new UnidadexAreasComEquipamentosViewModel(codUnidade);
        if (unAux.getUnidade() != null) {
            this.vmUnidadeVinculosAreaComEquipamento = unAux;
            atualizaInterface(this.vmUnidadeVinculosAreaComEquipamento);
            botoes(true);
        }
    }

    private void atualizaInterface(UnidadexAreasComEquipamentosViewModel unidadeComVinculos) {
        if (unidadeComVinculos != null && unidadeComVinculos.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComVinculos.getUnidade().getId()));
            txtDescricao.setText(unidadeComVinculos.getUnidade().getNomeFormatado());
            if (unidadeComVinculos.getListaVinculos() != null) {
                carregaTabela(unidadeComVinculos.getListaVinculos());
            }
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizaInterfaceAlteracao(UnidadeAreaComEquipamentoViewModel vmVinculoUnidadeEquipamentoComArea) {
        try {
            this.idUnidadeEquipamentoComArea = vmVinculoUnidadeEquipamentoComArea.getId();
            txtNomeVinculoObs.setText(vmVinculoUnidadeEquipamentoComArea.getNomeVinculoConcatenado());
            txtReferenciaObs.setText(vmVinculoUnidadeEquipamentoComArea.getReferencia());
            txtDataObs.setText(vmVinculoUnidadeEquipamentoComArea.getDataFormatada());
            if (vmVinculoUnidadeEquipamentoComArea.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmVinculoUnidadeEquipamentoComArea.getObservacao());
        } catch (NullPointerException ex) {
            limparCampos();
        }
    }

    private void limparCampos() {
        this.idUnidadeEquipamentoComArea = 0;
        txtNomeVinculoObs.setText("");
        txtReferenciaObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void carregaTabela(List<UnidadeAreaComEquipamentoViewModel> lista) {
        ObservableList<UnidadeAreaComEquipamentoViewModel> obsLista;
        limparTabela();

        tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
        tcArea.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeArea().toString()));
        tcEquipamento.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getUnidadeEquipamento().toString()));
        tcReferencia.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getReferencia()));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvDados.setItems(obsLista);
        TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    //true - ativa: novo e excluir, 
    //false - ativa: gravar e cancelar
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
    }

    private void bloqueiaCampos() {
        txtCodigo.setEditable(false);
        txtDescricao.setEditable(false);
        txtNomeVinculoObs.setEditable(false);
        txtReferenciaObs.setEditable(false);
        txtDataObs.setEditable(false);
    }
}
