package industria.controller.associacao;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesEquipamento;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.TableViewUtil;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UnidadeEquipamentoController;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.associacao.UnidadeEquipamentoViewModel;
import lib.viewmodel.associacao.UnidadexEquipamentosViewModel;
import lib.viewmodel.cadastro.EquipamentoViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidadeEquipamento implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();
    private final UnidadeEquipamentoController controllerUnidadeEquipamento = new UnidadeEquipamentoController();

    private UnidadexEquipamentosViewModel vmUnidadeComEquipamentos = null;
    private int idEquipamentoUnidade = 0;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private TableView<UnidadeEquipamentoViewModel> tbvDados;
    @FXML
    private TableColumn<UnidadeEquipamentoViewModel, String> tcData;
    @FXML
    private TableColumn<UnidadeEquipamentoViewModel, String> tcEquipamento;
    @FXML
    private TableColumn<UnidadeEquipamentoViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnAtivar;
    @FXML
    private JFXButton btnObservacao;
    @FXML
    private Tab tab2;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXTextField txtEquipamentoObs;
    @FXML
    private JFXTextField txtDataObs;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidadeComEquipamentos = new UnidadexEquipamentosViewModel(controllerUnidade.getUltimoRegistro(false));
        destacarDesativados();
        atualizarInterface(vmUnidadeComEquipamentos);
        botoes(true);
        bloqueiaCampos();
    }

    //ok
    private void destacarDesativados() {
        tbvDados.setRowFactory(tv -> {
            return new TableRow<UnidadeEquipamentoViewModel>() {
                @Override
                public void updateItem(UnidadeEquipamentoViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidadeComEquipamentos = new UnidadexEquipamentosViewModel(controllerUnidade.getPrimeiroRegistro(false));
        atualizarInterface(vmUnidadeComEquipamentos);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidadeComEquipamentos.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidadeComEquipamentos.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComEquipamentos = new UnidadexEquipamentosViewModel(unAux);
            }
            atualizarInterface(vmUnidadeComEquipamentos);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidadeComEquipamentos.getUnidade() != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidadeComEquipamentos.getUnidade().getId(), false);
            if (unAux != null) {
                vmUnidadeComEquipamentos = new UnidadexEquipamentosViewModel(unAux);
            }
            atualizarInterface(vmUnidadeComEquipamentos);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUnidadeComEquipamentos = new UnidadexEquipamentosViewModel(controllerUnidade.getUltimoRegistro(false));
        atualizarInterface(vmUnidadeComEquipamentos);
        botoes(true);
    }

    @FXML//ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        atualizarUnidade(unidadeAux);
    }

    @FXML// ok
    private void clkIrPara(ActionEvent event) {
        Alerta dialog = new Alerta("Código unidade", "", "");
        int codigoAux;
        try {
            codigoAux = Integer.parseInt(dialog.caixaTexto("código da unidade"), 10);
        } catch (NumberFormatException ex) {
            codigoAux = 0;
        }

        atualizarUnidade(codigoAux);
    }

    @FXML// ok
    private void clkDuploCliqueBuscaUnidade(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkBusca(null);
        }
    }

    @FXML// ok
    private void clkTbvDados(MouseEvent event) {
        if (event.getClickCount() == 2) {
            UnidadeEquipamentoViewModel vmEquipamentoUnidade = tbvDados.getSelectionModel().getSelectedItem();
            if (vmEquipamentoUnidade != null) {
                Alerta dialog = new Alerta("Equipamento", "Dados do vínculo",
                        "Data do vínculo: " + vmEquipamentoUnidade.getDataFormatada()
                        + "\nStatus do vínculo: " + vmEquipamentoUnidade.isAtivoToString()
                        + "\n\nObservação: \n" + vmEquipamentoUnidade.getObservacao());
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            }
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        int statusGravacao;
        EquipamentoViewModel equipamentoAux;
        UnidadeEquipamentoViewModel vmEquipamentoUnidade;

        if (vmUnidadeComEquipamentos != null && vmUnidadeComEquipamentos.getUnidade() != null) {
            try {
                Tela tl = new Tela("/industria/view/pesquisa/PesEquipamento.fxml", "Buscar equipamento", false, true, false);
                PesEquipamento controller = tl.getLoader().getController();
                controller.setAcessoTerceiro(true);
                tl.exibirTelaFilha();// exibe a tela
                equipamentoAux = controller.getEquipamento();// salva retorno da janela de pesquisa
            } catch (IOException ex) {
                equipamentoAux = null;
                System.out.println("erro ao exibir janela de pesquisa");
            }

            if (equipamentoAux != null) {
                vmEquipamentoUnidade = new UnidadeEquipamentoViewModel(
                        true,
                        LocalDate.now(),
                        "",
                        equipamentoAux.getId()
                );

                statusGravacao = controllerUnidadeEquipamento.gravar(vmUnidadeComEquipamentos.getUnidade().getId(), vmEquipamentoUnidade);
                if (statusGravacao > 0) {
                    //não gravado
                    exibeAlerta(statusGravacao);
                } else {
                    //gravado com sucesso
                    vmEquipamentoUnidade = controllerUnidadeEquipamento.getRegPorUnidadeEEquipamento(
                            vmUnidadeComEquipamentos.getUnidade().getId(),
                            vmEquipamentoUnidade.getEquipamento().getId()
                    );
                    vmUnidadeComEquipamentos.addEquipamentoNaLista(vmEquipamentoUnidade);
                    atualizarInterface(vmUnidadeComEquipamentos);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComEquipamentos.getPosicaoEquipamentoNaLista(vmEquipamentoUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        UnidadeEquipamentoViewModel vmEquipamentoUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmEquipamentoUnidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Equipamento unidade", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidadeEquipamento.excluir(vmUnidadeComEquipamentos.getUnidade().getId(), vmEquipamentoUnidade) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    vmUnidadeComEquipamentos.removerItemDaLista(vmEquipamentoUnidade);
                    carregarTabela(vmUnidadeComEquipamentos.getEquipamentos());
                }
            }
        }
    }

    @FXML// ok
    private void clkAtivar(ActionEvent event) {
        UnidadeEquipamentoViewModel vmEquipamentoUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmEquipamentoUnidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Equipamento da unidade", "", "");
            if (vmEquipamentoUnidade.isAtivo()) {
                dialog.setInformacao("Desativar registro", "tornar equipamento inativo para a unidade?");
            } else {
                dialog.setInformacao("Ativar registro", "tornar equipamento ativo para a unidade?");
            }
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                vmEquipamentoUnidade.setAtivo(!vmEquipamentoUnidade.isAtivo());
                int status = controllerUnidadeEquipamento.alterar(vmUnidadeComEquipamentos.getUnidade().getId(), vmEquipamentoUnidade);
                if (status > 0) {
                    exibeAlerta(status);
                } else {
                    vmUnidadeComEquipamentos.atualizarEquipamentoNaLista(vmEquipamentoUnidade);
                    carregarTabela(vmUnidadeComEquipamentos.getEquipamentos());
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComEquipamentos.getPosicaoEquipamentoNaLista(vmEquipamentoUnidade));
                }
            }
        }
    }

    @FXML// ok
    private void clkObservacao(ActionEvent event) {
        UnidadeEquipamentoViewModel vmEquipamentoUnidade = tbvDados.getSelectionModel().getSelectedItem();
        if (vmEquipamentoUnidade != null) {
            atualizarInterfaceAlteracao(vmEquipamentoUnidade);
            botoes(false);
        }
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        UnidadeEquipamentoViewModel vmEquipamentoUnidade = controllerUnidadeEquipamento.getRegPorCodigo(idEquipamentoUnidade);
        int statusGravacao;
        if (vmUnidadeComEquipamentos != null && vmEquipamentoUnidade != null) {
            if (vmEquipamentoUnidade.isAtivo() != rbAtivo.isSelected() || !vmEquipamentoUnidade.getObservacao().equalsIgnoreCase(txtObservacao.getText())) {
                vmEquipamentoUnidade.setAtivo(rbAtivo.isSelected());
                vmEquipamentoUnidade.setObservacao(txtObservacao.getText());

                statusGravacao = controllerUnidadeEquipamento.alterar(vmUnidadeComEquipamentos.getUnidade().getId(), vmEquipamentoUnidade);

                if (statusGravacao > 0) {
                    //não alterado
                    exibeAlerta(statusGravacao);
                } else {
                    //alterado com sucesso
                    vmUnidadeComEquipamentos.atualizarEquipamentoNaLista(vmEquipamentoUnidade);
                    atualizarInterface(vmUnidadeComEquipamentos);
                    TableViewUtil.selecionarLinhaTabela(tbvDados, vmUnidadeComEquipamentos.getPosicaoEquipamentoNaLista(vmEquipamentoUnidade));
                    limparCampos();
                    botoes(true);
                }
            } else {
                limparCampos();
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        limparCampos();
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML //ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (event.isControlDown() && event.getCode() == KeyCode.T) {
            clkIrPara(null);
        }
    }

//------------------------------------------------------------------------------   
    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Equipamentos da unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2203:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Unidade", "unidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2204:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Equipamento", "equipamento não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2205:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Data", "data não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2206:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 2207:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2208:
                dialog.setInformacao("Duplicidade", "equipamento já vinculado à unidade");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizarUnidade(UnidadeViewModel unidade) {
        UnidadexEquipamentosViewModel unAux = new UnidadexEquipamentosViewModel(unidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeComEquipamentos = unAux;
            atualizarInterface(vmUnidadeComEquipamentos);
            botoes(true);
        }
    }

    private void atualizarUnidade(int codUnidade) {
        UnidadexEquipamentosViewModel unAux = new UnidadexEquipamentosViewModel(codUnidade);
        if (unAux.getUnidade() != null) {
            vmUnidadeComEquipamentos = unAux;
            atualizarInterface(vmUnidadeComEquipamentos);
            botoes(true);
        }
    }

    private void atualizarInterface(UnidadexEquipamentosViewModel unidadeComEquipamentos) {
        if (unidadeComEquipamentos != null && unidadeComEquipamentos.getUnidade() != null) {
            txtCodigo.setText(String.valueOf(unidadeComEquipamentos.getUnidade().getId()));
            txtDescricao.setText(unidadeComEquipamentos.getUnidade().getNomeFormatado());
            carregarTabela(unidadeComEquipamentos.getEquipamentos());
        } else {
            txtCodigo.setText("");
            txtDescricao.setText("");
            limparTabela();
        }
    }

    private void atualizarInterfaceAlteracao(UnidadeEquipamentoViewModel vmEquipamentoUnidade) {
        if (vmEquipamentoUnidade != null) {
            idEquipamentoUnidade = vmEquipamentoUnidade.getId();
            txtEquipamentoObs.setText(vmEquipamentoUnidade.getEquipamento().getEquipamentoFormatado());
            txtDataObs.setText(vmEquipamentoUnidade.getDataFormatada());
            if (vmEquipamentoUnidade.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmEquipamentoUnidade.getObservacao());

        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        idEquipamentoUnidade = 0;
        txtEquipamentoObs.setText("");
        txtDataObs.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

    private void carregarTabela(List<UnidadeEquipamentoViewModel> lista) {
        if (lista != null) {
            ObservableList<UnidadeEquipamentoViewModel> obsLista;
            limparTabela();

            tcData.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getDataFormatada()));
            tcEquipamento.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().toString()));
            tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

            obsLista = FXCollections.observableArrayList(lista);

            tbvDados.setItems(obsLista);
            TableViewUtil.selecionarLinhaTabela(tbvDados, -1);
        }
    }

    private void limparTabela() {
        tbvDados.getItems().clear();
    }

    //true - ativa: novo e excluir, 
    //false - ativa: gravar e cancelar
    private void botoes(boolean controle) {
        tab1.setDisable(!controle);
        btnNovo.setDisable(!controle);
        btnExcluir.setDisable(!controle);
        btnAtivar.setDisable(!controle);
        btnObservacao.setDisable(!controle);

        tab2.setDisable(controle);
        btnGravar.setDisable(controle);
        btnCancelar.setDisable(controle);

        selecionaAba(controle);
    }

    private void selecionaAba(boolean controle) {
        //testar se esta funciona
        if (!controle) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }

        /* codigo copiado da classe CadUnidadeMotivoParada
        if (tab1.isDisable()) {
            tabPane.getSelectionModel().select(tab2);
        } else {
            tabPane.getSelectionModel().select(tab1);
        }
         */
    }

    private void bloqueiaCampos() {
        txtCodigo.setEditable(false);
        txtDescricao.setEditable(false);
        txtEquipamentoObs.setEditable(false);
        txtDataObs.setEditable(false);
    }
}
