package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesCidade;
import industria.controller.pesquisa.PesEstado;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.CidadeController;
import lib.controller.cadastro.EstadoController;
import lib.viewmodel.cadastro.CidadeViewModel;
import lib.viewmodel.cadastro.EstadoViewModel;

/**
 * FXML Controller class
 *
 * @author abner.jacomo
 */
public class CadCidade implements Initializable {

    private final CidadeController controllerCidade = new CidadeController();
    private final EstadoController controllerEstado = new EstadoController();
    private CidadeViewModel vmCidade = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtCodigoEstado;
    @FXML
    private JFXTextField txtNomeEstado;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        focusTxtCodigoEstado(txtCodigoEstado, txtNomeEstado);
        vmCidade = controllerCidade.getUltimoRegistro();
        atualizaInterface(vmCidade);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmCidade = controllerCidade.getPrimeiroRegistro();
        atualizaInterface(vmCidade);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmCidade != null) {
            CidadeViewModel cidadeAux = controllerCidade.getAnteriorRegistro(vmCidade.getId());
            if (cidadeAux != null) {
                vmCidade = cidadeAux;
            }
            atualizaInterface(vmCidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmCidade != null) {
            CidadeViewModel cidadeAux = controllerCidade.getProximoRegistro(vmCidade.getId());
            if (cidadeAux != null) {
                vmCidade = cidadeAux;
            }
            atualizaInterface(vmCidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmCidade = controllerCidade.getUltimoRegistro();
        atualizaInterface(vmCidade);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        CidadeViewModel cidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesCidade.fxml", "Buscar cidade", false, true, false);
            PesCidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            cidadeAux = controller.getCidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            cidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (cidadeAux != null) {
            vmCidade = cidadeAux;
            atualizaInterface(vmCidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmCidade != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(
                    new CidadeViewModel(
                            vmCidade.getNome(),
                            vmCidade.isAtivo(),
                            vmCidade.getObservacao(),
                            vmCidade.getEstado().getId()
                    )
            );
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        int codigoEstado;
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        CidadeViewModel cidadeAux;
        int status;

        //codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        //medida
        try {
            codigoEstado = Integer.parseInt(txtCodigoEstado.getText(), 10);
        } catch (NumberFormatException ex) {
            codigoEstado = 0;
        }

        cidadeAux = new CidadeViewModel(
                codigo,
                nome,
                ativo,
                observacao,
                codigoEstado
        );

        if (cidadeAux.getId() == 0) {
            //gravar
            status = controllerCidade.gravar(cidadeAux);
        } else {
            //alterar
            status = controllerCidade.alterar(cidadeAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmCidade = cidadeAux;
            if (vmCidade.getId() == 0) {
                vmCidade = controllerCidade.getUltimoRegistro();
            }
            atualizaInterface(vmCidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmCidade != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmCidade);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmCidade != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro cidade", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerCidade.excluir(vmCidade.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    // seleciona o próximo registro ou anterior, para exibir na tela
                    CidadeViewModel cidadeAux = controllerCidade.getProximoRegistro(vmCidade.getId());
                    if (cidadeAux == null) {
                        cidadeAux = controllerCidade.getAnteriorRegistro(vmCidade.getId());
                    }
                    vmCidade = cidadeAux;
                    atualizaInterface(vmCidade);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmCidade);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        // capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (btnNovo.isDisabled() && txtCodigoEstado.isFocused() && atalhoKey.isBuscaAuxiliar(event)) {
            atualizaEstado(buscaEstado());
        }
    }

    @FXML// ok
    private void clkBuscarEstado(MouseEvent event) {
        if (event.getClickCount() == 2 && btnNovo.isDisable()) {
            atualizaEstado(buscaEstado());
        }
    }

//------------------------------------------------------------------------------
    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro cidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração dever ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 703:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 704:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 705:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Estado", "estado(UF) não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoEstado.requestFocus();
                break;
            case 706:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 707:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Estado", "estado(UF) está desativado");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoEstado.requestFocus();
                break;
            case 708:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(CidadeViewModel vmCidade) {
        if (vmCidade != null) {
            txtCodigo.setText(String.valueOf(vmCidade.getId()));
            txtNome.setText(vmCidade.getNome());
            txtCodigoEstado.setText(String.valueOf(vmCidade.getEstado().getId()));
            String nomeEstado = "";
            if (vmCidade.getEstado() != null) {
                if (!vmCidade.getEstado().isAtivo()) {
                    nomeEstado = "[Desativado] ";
                }
                nomeEstado += vmCidade.getEstado().getNome();
            }
            txtNomeEstado.setText(nomeEstado);
            if (vmCidade.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(vmCidade.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtCodigoEstado.setText("");
        txtNomeEstado.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmCidade != null && vmCidade.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtCodigoEstado.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

// PARA CONTROLE DO ESTADO -----------------------------------------------------
    private EstadoViewModel buscaEstado() {
        EstadoViewModel ufAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesEstado.fxml", "Buscar estado", false, true, false);
            PesEstado controller = tl.getLoader().getController();
            controller.setAcessoTerceiro(true);//informa a classe que será acessada por terceiros

            tl.exibirTelaFilha();// exibe a tela
            ufAux = controller.getEstado();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            ufAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }
        return ufAux;
    }

    private void atualizaEstado(EstadoViewModel vmEstado) {
        if (vmEstado != null) {
            txtCodigoEstado.setText(String.valueOf(vmEstado.getId()));
            txtNomeEstado.setText(vmEstado.getNome());
        }
    }

    //método para controlar a quando o foco "deixar" o textbox código da medida
    private void focusTxtCodigoEstado(JFXTextField codigo, JFXTextField nome) {
        codigo.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean velhoValor, Boolean novoValor) -> {

            // condição para quando deixar o foco
            if (btnNovo.isDisabled() && !novoValor) {
                if (!codigo.getText().isEmpty()) {
                    EstadoViewModel estado;
                    try {
                        int codigoEstado = Integer.parseInt(codigo.getText(), 10);
                        estado = controllerEstado.regPorCodigo(codigoEstado);
                    } catch (NumberFormatException ex) {
                        estado = null;
                    }

                    if (estado == null) {
                        nome.setText("");
                    } else {
                        if (!estado.isAtivo()) {
                            estado.setNome("[Desativado] " + estado.getNome());
                        }
                        nome.setText(estado.getNome());
                    }
                } else {
                    nome.setText("");
                }
            }
        });
    }

}
