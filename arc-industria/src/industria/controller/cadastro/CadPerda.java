package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesPerda;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.PerdaController;
import lib.viewmodel.cadastro.PerdaViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadPerda implements Initializable {

    private final PerdaController controllerPerda = new PerdaController();
    private PerdaViewModel vmPerda = null;
    AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtSigla;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmPerda = controllerPerda.getUltimoRegistro();
        atualizaInterface(vmPerda);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmPerda = controllerPerda.getPrimeiroRegistro();
        atualizaInterface(vmPerda);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmPerda != null) {
            PerdaViewModel perdaAux = controllerPerda.getAnteriorRegistro(vmPerda.getId());
            if (perdaAux != null) {
                vmPerda = perdaAux;
            }
            atualizaInterface(vmPerda);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmPerda != null) {
            PerdaViewModel perdaAux = controllerPerda.getProximoRegistro(vmPerda.getId());
            if (perdaAux != null) {
                vmPerda = perdaAux;
            }
            atualizaInterface(vmPerda);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmPerda = controllerPerda.getUltimoRegistro();
        atualizaInterface(vmPerda);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        PerdaViewModel perdaAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesPerda.fxml", "Buscar perda", false, true, false);
            PesPerda controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            perdaAux = controller.getPerda();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            perdaAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (perdaAux != null) {
            vmPerda = perdaAux;
            atualizaInterface(vmPerda);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmPerda != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new PerdaViewModel(
                    vmPerda.getNome(),
                    vmPerda.getSigla(),
                    vmPerda.isAtivo(),
                    vmPerda.getObservacao())
            );
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        String sigla = txtSigla.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        int status;

        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        PerdaViewModel perdaAux = new PerdaViewModel(codigo, nome, sigla, ativo, observacao);

        if (perdaAux.getId() == 0) {
            // gravar
            status = controllerPerda.gravar(perdaAux);
        } else {
            // alterar
            status = controllerPerda.alterar(perdaAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmPerda = perdaAux;
            if (vmPerda.getId() == 0) {
                vmPerda = controllerPerda.getUltimoRegistro();
            }
            atualizaInterface(vmPerda);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmPerda != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmPerda);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmPerda != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro perda", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerPerda.excluir(vmPerda.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    PerdaViewModel perdaAux = controllerPerda.getProximoRegistro(vmPerda.getId());
                    if (perdaAux == null) {
                        perdaAux = controllerPerda.getAnteriorRegistro(vmPerda.getId());
                    }
                    vmPerda = perdaAux;
                    atualizaInterface(vmPerda);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmPerda);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro perda - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "Erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1703:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1704:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1705:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode ser vazia");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 1706:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode conter mais que 6 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 1707:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1708:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(PerdaViewModel perda) {
        if (perda != null) {
            txtCodigo.setText(String.valueOf(perda.getId()));
            txtNome.setText(perda.getNome());
            txtSigla.setText(perda.getSigla());
            if (perda.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(perda.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmPerda != null && vmPerda.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtSigla.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtSigla.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }
}
