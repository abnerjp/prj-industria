package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesMaquina;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lib.controller.cadastro.MaquinaController;
import lib.model.cadastro.Maquina;

/**
 * FXML Controller class
 *
 * Cadastro de máquina, para ser utilizada nas Unidades
 *
 * @author abnerjp
 */
public class CadMaquina implements Initializable {

    MaquinaController modeloMaq = new MaquinaController();
    Maquina maq = null;
    AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private HBox hbJanela;
    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private FlowPane pnAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        maq = modeloMaq.getUltimoRegistro();
        atualizaInterface(maq);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        maq = modeloMaq.getPrimeiroRegistro();
        atualizaInterface(maq);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (maq != null) {
            Maquina maqAux = modeloMaq.getAnteriorRegistro(maq.getCodigo());
            if (maqAux != null) {
                maq = maqAux;
            }
            atualizaInterface(maq);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (maq != null) {
            Maquina maqAux = modeloMaq.getProximoRegistro(maq.getCodigo());
            if (maqAux != null) {
                maq = maqAux;
            }
            atualizaInterface(maq);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        maq = modeloMaq.getUltimoRegistro();
        atualizaInterface(maq);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {

        Maquina maqAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesMaquina.fxml", "Buscar máquina", false, true, false);
            PesMaquina controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            maqAux = controller.getMaquina();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            maqAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (maqAux != null) {
            maq = maqAux;
            atualizaInterface(maq);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (maq != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new Maquina(maq.getModelo(), maq.isAtivo(), maq.getObservacao()));
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        Maquina maqAux = validaCampos();
        if (maqAux != null) {
            boolean status;

            if (maqAux.getCodigo() == 0) {// gravar
                status = modeloMaq.gravar(maqAux);
            } else {// alterar
                status = modeloMaq.alterar(maqAux);
            }

            if (!status) {
                Alerta dialog = new Alerta("Cadastro máquina", "Erro ao gravar", "Erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            } else {
                maq = maqAux;
                if (maq.getCodigo() == 0) {
                    maq = modeloMaq.getUltimoRegistro();
                }
                atualizaInterface(maq);
                botoes(true);
            }
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (maq != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(maq);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (maq != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro máquina", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (!modeloMaq.excluir(maq.getCodigo())) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    Maquina maqAux = modeloMaq.getProximoRegistro(maq.getCodigo());
                    if (maqAux == null) {
                        maqAux = modeloMaq.getAnteriorRegistro(maq.getCodigo());
                    }
                    maq = maqAux;
                    atualizaInterface(maq);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(maq);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private Maquina validaCampos() {
        int codigo;        // ok
        String modelo;     // ok
        boolean ativo;     // ok
        String observacao; // ok
        Alerta dialog = new Alerta("Cadastro de máquina", "", "");

        // código
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        // nome
        modelo = txtNome.getText();
        if (modelo == null || modelo.isEmpty() || modelo.length() < 2) {
            tabPane.getSelectionModel().select(tab1);
            dialog.setInformacao("Modelo", "Modelo não pode ser vazio ou menor que 2 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtNome.requestFocus();
            return null;
        } else if (modelo.length() > 30) {
            tabPane.getSelectionModel().select(tab1);
            dialog.setInformacao("Modelo", "Modelo não pode conter mais que 30 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtNome.requestFocus();
            return null;
        }

        // ativo
        ativo = rbAtivo.selectedProperty().get();

        // observação
        observacao = txtObservacao.getText();
        if (observacao.length() > 200) {
            tabPane.getSelectionModel().select(tab2);
            dialog.setInformacao("Observação", "Observação não pode conter mais que 200 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtObservacao.requestFocus();
            return null;
        }

        return new Maquina(codigo, modelo, ativo, observacao);
    }

    private void atualizaInterface(Maquina m) {
        if (m != null) {
            txtCodigo.setText(String.valueOf(m.getCodigo()));
            txtNome.setText(m.getModelo());
            if (m.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(m.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (maq != null && maq.getCodigo() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }
}
