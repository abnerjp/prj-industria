package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesEquipamento;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.EquipamentoController;
import lib.viewmodel.cadastro.EquipamentoViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadEquipamento implements Initializable {

    private final EquipamentoController controllerEquipamento = new EquipamentoController();
    private EquipamentoViewModel vmEquipamento = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtModelo;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmEquipamento = controllerEquipamento.getUltimoRegistro();
        atualizaInterface(vmEquipamento);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmEquipamento = controllerEquipamento.getPrimeiroRegistro();
        atualizaInterface(vmEquipamento);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmEquipamento != null) {
            EquipamentoViewModel equipamentoAux = controllerEquipamento.getAnteriorRegistro(vmEquipamento.getId());
            if (equipamentoAux != null) {
                vmEquipamento = equipamentoAux;
            }
            atualizaInterface(vmEquipamento);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmEquipamento != null) {
            EquipamentoViewModel equipamentoAux = controllerEquipamento.getProximoRegistro(vmEquipamento.getId());
            if (equipamentoAux != null) {
                vmEquipamento = equipamentoAux;
            }
            atualizaInterface(vmEquipamento);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmEquipamento = controllerEquipamento.getUltimoRegistro();
        atualizaInterface(vmEquipamento);
        botoes(true);
    }

    @FXML
    private void clkBusca(ActionEvent event) {
        EquipamentoViewModel equipamentoAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesEquipamento.fxml", "Buscar equipamento", false, true, false);
            PesEquipamento controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            equipamentoAux = controller.getEquipamento();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            equipamentoAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (equipamentoAux != null) {
            vmEquipamento = equipamentoAux;
            atualizaInterface(vmEquipamento);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmEquipamento != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new EquipamentoViewModel(vmEquipamento.getNome(), vmEquipamento.getModelo(), vmEquipamento.isAtivo(), vmEquipamento.getObservacao()));
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo, status;
        String nome = txtNome.getText();
        String modelo = txtModelo.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        EquipamentoViewModel equipamentoAux;

        //código
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        equipamentoAux = new EquipamentoViewModel(
                codigo,
                nome,
                modelo,
                ativo,
                observacao
        );

        if (equipamentoAux.getId() == 0) {
            //gravar
            status = controllerEquipamento.gravar(equipamentoAux);
        } else {
            status = controllerEquipamento.alterar(equipamentoAux);
        }

        if (status > 0) {
            //houve exceção na gravação
            exibeAlerta(status);
        } else {
            vmEquipamento = equipamentoAux;
            if (vmEquipamento.getId() == 0) {
                vmEquipamento = controllerEquipamento.getUltimoRegistro();
            }
            atualizaInterface(vmEquipamento);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmEquipamento != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmEquipamento);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmEquipamento != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro medida", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerEquipamento.excluir(vmEquipamento.getId()) > 0) {
                    dialog.setMensagem("não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    EquipamentoViewModel equipamentoAux = controllerEquipamento.getProximoRegistro(vmEquipamento.getId());
                    if (equipamentoAux == null) {
                        equipamentoAux = controllerEquipamento.getAnteriorRegistro(vmEquipamento.getId());
                    }
                    vmEquipamento = equipamentoAux;
                    atualizaInterface(vmEquipamento);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmEquipamento);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro equipamento - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1403:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1404:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1405:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Modelo", "descrição não pode conter mais que 30 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtModelo.requestFocus();
                break;
            case 1406:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1407:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(EquipamentoViewModel vmEquipamentoAux) {
        if (vmEquipamentoAux != null) {
            txtCodigo.setText(String.valueOf(vmEquipamentoAux.getId()));
            txtNome.setText(vmEquipamentoAux.getNome());
            txtModelo.setText(vmEquipamentoAux.getModelo());
            if (vmEquipamentoAux.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(vmEquipamentoAux.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmEquipamento != null && vmEquipamento.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtModelo.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtModelo.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }

}
