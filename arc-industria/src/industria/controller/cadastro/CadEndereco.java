package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesCidade;
import industria.controller.pesquisa.PesEndereco;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import static industria.util.CepWeb.consultaCep;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.CidadeController;
import lib.controller.cadastro.EnderecoController;
import lib.controller.cadastro.EstadoController;
import lib.viewmodel.cadastro.CidadeViewModel;
import lib.viewmodel.cadastro.EnderecoViewModel;
import lib.viewmodel.cadastro.EstadoViewModel;
import org.json.JSONObject;

/**
 * FXML Controller class
 *
 * @author abner.jacomo
 */
public class CadEndereco implements Initializable {

    private final EnderecoController controllerEndereco = new EnderecoController();
    private final CidadeController controllerCidade = new CidadeController();
    private EnderecoViewModel vmEndereco = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtLogradouro;
    @FXML
    private JFXTextField txtCep;
    @FXML
    private JFXTextField txtCodigoCidade;
    @FXML
    private JFXTextField txtNomeCidade;
    @FXML
    private JFXTextField txtBairro;
    @FXML
    private JFXButton btnBuscarNaWeb;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        focusTxtCodigoEstado(txtCodigoCidade, txtNomeCidade);
        vmEndereco = controllerEndereco.getUltimoRegistro();
        atualizaInterface(vmEndereco);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmEndereco = controllerEndereco.getPrimeiroRegistro();
        atualizaInterface(vmEndereco);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmEndereco != null) {
            EnderecoViewModel enderecoAux = controllerEndereco.getAnteriorRegistro(vmEndereco.getId());
            if (enderecoAux != null) {
                vmEndereco = enderecoAux;
            }
            atualizaInterface(vmEndereco);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmEndereco != null) {
            EnderecoViewModel enderecoAux = controllerEndereco.getProximoRegistro(vmEndereco.getId());
            if (enderecoAux != null) {
                vmEndereco = enderecoAux;
            }
            atualizaInterface(vmEndereco);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmEndereco = controllerEndereco.getUltimoRegistro();
        atualizaInterface(vmEndereco);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        EnderecoViewModel enderecoAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesEndereco.fxml", "Buscar endereço", false, true, false);
            PesEndereco controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            enderecoAux = controller.getEndereco();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            enderecoAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (enderecoAux != null) {
            vmEndereco = enderecoAux;
            atualizaInterface(vmEndereco);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        new Alerta("Cadastro endereco", "Copiar Registro", "a cópia de endereço não está habilitada").exibeInformacao(Alert.AlertType.INFORMATION);
    }

    @FXML
    private void clkBuscaCepWeb(ActionEvent event) {
        String json_str = consultaCep(txtCep.getText(), "json");

        //System.out.println(json_str);
        JSONObject my_obj = new JSONObject(json_str);
        String cidade = my_obj.getString("cidade");
        String logradouro = my_obj.getString("tipo_logradouro") + " " + my_obj.getString("logradouro");
        String bairro = my_obj.getString("bairro");
        String siglaUF = my_obj.getString("uf");
        txtLogradouro.setText(logradouro);
        txtBairro.setText(bairro);
        txtNomeCidade.setText(cidade);

        CidadeViewModel vmCidade = controllerCidade.cidadePorNome(txtNomeCidade.getText());

        if (vmCidade != null) {
            txtCodigoCidade.setText(String.valueOf(vmCidade.getId()));
        } else if (cidade != null && cidade.length() > 0) {
            Alerta dialog = new Alerta("Cadastro endereço", "Cidade: " + cidade, "a cidade pesquisada não foi encontrada na base de dados. Deseja cadastrar esta cidade?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                EstadoViewModel vmEstado = new EstadoController().regPorSigla(siglaUF);
                if (vmEstado != null) {
                    if (vmEstado.isAtivo()) {
                        vmCidade = new CidadeViewModel(cidade, true, "cadastrado via pesquisa Web", vmEstado.getId());
                        if (controllerCidade.gravar(vmCidade) != 0) {
                            new Alerta("Cadastro endereço", "Cidade", "não foi possível cadastrar a cidade: " + cidade).exibeInformacao(Alert.AlertType.INFORMATION);
                        } else {
                            new Alerta("Cadastro endereço", "Cidade: " + cidade, "cidade cadastrada com sucesso").exibeInformacao(Alert.AlertType.INFORMATION);
                            vmCidade = controllerCidade.getUltimoRegistro();
                            txtCodigoCidade.setText(String.valueOf(vmCidade.getId()));
                        }
                    } else {
                        new Alerta("Cadastro endereço", "Cidade: " + cidade, "não é possivel cadastrar a cidade \no estado: " + vmEstado.getNome() + " está desativado").exibeInformacao(Alert.AlertType.INFORMATION);
                    }
                } else {
                    new Alerta("Cadastro endereço", "Cidade: " + cidade, "cidade não cadastrada, não há estado(UF) cadastrado com a sigla: " + siglaUF).exibeInformacao(Alert.AlertType.INFORMATION);
                }
            }
        } else {
            new Alerta("Cadastro endereço", "CEP", "nenhuma ocorrência encontrada para o cep informado").exibeInformacao(Alert.AlertType.INFORMATION);
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtCep.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event
    ) {
        int codigo;
        String cep = txtCep.getText();
        int codigoCidade;
        String logradouro = txtLogradouro.getText();
        String bairro = txtBairro.getText();
        String observacao = txtObservacao.getText();
        EnderecoViewModel enderecoAux;
        int status;

        //codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        //Cidade
        try {
            codigoCidade = Integer.parseInt(txtCodigoCidade.getText());
        } catch (NumberFormatException ex) {
            codigoCidade = 0;
        }

        enderecoAux = new EnderecoViewModel(
                codigo,
                logradouro,
                cep,
                bairro,
                observacao,
                codigoCidade
        );

        if (enderecoAux.getId() == 0) {
            //gravar
            status = controllerEndereco.gravar(enderecoAux);
        } else {
            //alterar
            status = controllerEndereco.alterar(enderecoAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmEndereco = enderecoAux;
            if (vmEndereco.getId() == 0) {
                vmEndereco = controllerEndereco.getUltimoRegistro();
            }
            atualizaInterface(vmEndereco);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event
    ) {
        if (vmEndereco != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmEndereco);
            botoes(false);
            txtCep.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event
    ) {
        if (vmEndereco != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro endereço", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerCidade.excluir(vmEndereco.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    // seleciona o próximo registro ou anterior, para exibir na tela
                    EnderecoViewModel enderecoAux = controllerEndereco.getProximoRegistro(vmEndereco.getId());
                    if (enderecoAux == null) {
                        enderecoAux = controllerEndereco.getAnteriorRegistro(vmEndereco.getId());
                    }
                    vmEndereco = enderecoAux;
                    atualizaInterface(vmEndereco);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event
    ) {
        atualizaInterface(vmEndereco);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event
    ) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event
    ) {
        // capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (btnNovo.isDisabled() && txtCodigoCidade.isFocused() && atalhoKey.isBuscaAuxiliar(event)) {
            atualizaCidade(buscaCidade());
        }
    }

    @FXML// ok
    private void clkBuscarCidade(MouseEvent event
    ) {
        if (event.getClickCount() == 2 && btnNovo.isDisable()) {
            atualizaCidade(buscaCidade());
        }
    }

    //------------------------------------------------------------------------------
    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro endereco - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração dever ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 803:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Logradouro", "logradouro não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtLogradouro.requestFocus();
                break;
            case 804:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Logradouro", "logradouro não pode conter mais que 100 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtLogradouro.requestFocus();
                break;
            case 805:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("CEP", "cep não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCep.requestFocus();
                break;
            case 806:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Bairro", "bairro não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtBairro.requestFocus();
                break;
            case 807:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Bairro", "bairro não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtBairro.requestFocus();
                break;
            case 808:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Cidade", "cidade não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoCidade.requestFocus();
                break;
            case 809:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 810:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Cidade", "cidade está desativado");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoCidade.requestFocus();
                break;
            case 811:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(EnderecoViewModel vmEndereco) {
        if (vmEndereco != null) {
            txtCodigo.setText(String.valueOf(vmEndereco.getId()));
            txtCep.setText(vmEndereco.getCepFormatado());
            txtLogradouro.setText(vmEndereco.getLogradouro());
            txtBairro.setText(vmEndereco.getBairro());
            txtCodigoCidade.setText(String.valueOf(vmEndereco.getCidade().getId()));
            String nomeCidade = "";
            if (vmEndereco.getCidade() != null) {
                if (!vmEndereco.getCidade().isAtivo()) {
                    nomeCidade = "[Desativado] ";
                }
                nomeCidade += vmEndereco.getCidade().getNome();
            }
            txtNomeCidade.setText(nomeCidade);
            txtObservacao.setText(vmEndereco.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtCep.setText("");
        txtLogradouro.setText("");
        txtBairro.setText("");
        txtCodigoCidade.setText("");
        txtNomeCidade.setText("");
        txtObservacao.setText("");
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmEndereco != null && vmEndereco.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtCep.setEditable(!controle);
        txtLogradouro.setEditable(!controle);
        txtBairro.setEditable(!controle);
        txtCodigoCidade.setEditable(!controle);
        txtObservacao.setEditable(!controle);
        btnBuscarNaWeb.setDisable(controle);
    }

// PARA CONTROLE DA CIDADE -----------------------------------------------------
    private CidadeViewModel buscaCidade() {
        CidadeViewModel cidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesCidade.fxml", "Buscar cidade", false, true, false);
            PesCidade controller = tl.getLoader().getController();
            controller.setAcessoTerceiro(true);//informa a classe que será acessada por terceiros

            tl.exibirTelaFilha();// exibe a tela
            cidadeAux = controller.getCidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            cidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }
        return cidadeAux;
    }

    private void atualizaCidade(CidadeViewModel vmCidade) {
        if (vmCidade != null) {
            txtCodigoCidade.setText(String.valueOf(vmCidade.getId()));
            txtNomeCidade.setText(vmCidade.getNome());
        }
    }

    //método para controlar a quando o foco "deixar" o textbox código da medida
    private void focusTxtCodigoEstado(JFXTextField codigo, JFXTextField nome) {
        codigo.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean velhoValor, Boolean novoValor) -> {

            // condição para quando deixar o foco
            if (btnNovo.isDisabled() && !novoValor) {
                if (!codigo.getText().isEmpty()) {
                    CidadeViewModel cidade;
                    try {
                        int codigoCidade = Integer.parseInt(codigo.getText(), 10);
                        cidade = controllerCidade.regPorCodigo(codigoCidade);
                    } catch (NumberFormatException ex) {
                        cidade = null;
                    }

                    if (cidade == null) {
                        nome.setText("");
                    } else {
                        if (!cidade.isAtivo()) {
                            cidade.setNome("[Desativado] " + cidade.getNome());
                        }
                        nome.setText(cidade.getNome());
                    }
                } else {
                    nome.setText("");
                }
            }
        });
    }

}
