package industria.controller.cadastro;

import industria.controller.associacao.CadUnidadeEndereco;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesUnidade;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.UnidadeController;
import lib.controller.associacao.UnidadeEnderecoController;
import lib.viewmodel.cadastro.EnderecoViewModel;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUnidade implements Initializable {

    private final UnidadeController controllerUnidade = new UnidadeController();

    private UnidadeViewModel vmUnidade = null;
    private EnderecoViewModel vmEndereco = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtReferencia;
    @FXML
    private JFXTextField txtRazaoSocial;
    @FXML
    private JFXTextField txtCnpj;
    @FXML
    private JFXTextField txtInscricaoEstadual;
    @FXML
    private JFXTextField txtNomeFantasia;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextField txtEmail;
    @FXML
    private JFXTextField txtTelefoneFixo;
    @FXML
    private JFXTextField txtTelefoneCelular;
    @FXML
    private Tab tab3;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXPasswordField txtSenha;
    @FXML
    private JFXPasswordField txtSenhaConfirmar;
    @FXML
    private Tab tab4;
    @FXML
    private JFXTextField txtCep;
    @FXML
    private JFXTextField txtLogradouro;
    @FXML
    private JFXTextField txtNumero;
    @FXML
    private JFXTextField txtBairro;
    @FXML
    private JFXTextField txtCidade;
    @FXML
    private Tab tab5;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtComplemento;
    @FXML
    private JFXButton btnEndereco;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUnidade = controllerUnidade.getUltimoRegistro(true);
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUnidade = controllerUnidade.getPrimeiroRegistro(true);
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUnidade != null) {
            UnidadeViewModel unAux = controllerUnidade.getAnteriorRegistro(vmUnidade.getId(), true);
            if (unAux != null) {
                vmUnidade = unAux;
            }
            atualizaInterface(vmUnidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUnidade != null) {
            UnidadeViewModel unAux = controllerUnidade.getProximoRegistro(vmUnidade.getId(), true);
            if (unAux != null) {
                vmUnidade = unAux;
            }
            atualizaInterface(vmUnidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUnidade = controllerUnidade.getUltimoRegistro(true);
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UnidadeViewModel unidadeAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUnidade.fxml", "Buscar unidade", false, true, false);
            PesUnidade controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            unidadeAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            unidadeAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (unidadeAux != null) {
            vmUnidade = unidadeAux;
            atualizaInterface(vmUnidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmUnidade != null && !btnNovo.isDisable()) {
            atualizaInterface(
                    new UnidadeViewModel(
                            vmUnidade.getRazaoSocial(),
                            vmUnidade.getCnpj(),
                            vmUnidade.getInscricaoEstadual(),
                            vmUnidade.getTelefoneFixo(),
                            vmUnidade.getTelefoneCelular(),
                            vmUnidade.getEmail(),
                            vmUnidade.getNomeFantasia(),
                            vmUnidade.getReferencia(),
                            "",
                            vmUnidade.isAtivo(),
                            vmUnidade.getObservacao(),
                            vmUnidade.getEnderecoAtual()
                    )
            );
            botoes(false);
            tabPane.getSelectionModel().select(tab1);
            txtReferencia.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtReferencia.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;

        /* aba 1 */
        String referencia = txtReferencia.getText();
        String razaoSocial = txtRazaoSocial.getText();
        String cnpj = txtCnpj.getText();
        String inscricaoEstadual = txtInscricaoEstadual.getText();
        String nomeFantasia = txtNomeFantasia.getText();

        /* aba 2 */
        String email = txtEmail.getText();
        String telefoneFixo = txtTelefoneFixo.getText();
        String telefoneCelular = txtTelefoneCelular.getText();

        /* aba 3 */
        boolean ativo = rbAtivo.isSelected();
        String senha1 = txtSenha.getText();
        String senha2 = txtSenhaConfirmar.getText();

        /* aba 4 */
        //Endereco

        /* aba 5 */
        String observacao = txtObservacao.getText();

        /* geral */
        int status;

        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        UnidadeViewModel unidadeAux = new UnidadeViewModel(
                codigo,
                razaoSocial,
                cnpj,
                inscricaoEstadual,
                telefoneFixo,
                telefoneCelular,
                email,
                nomeFantasia,
                referencia,
                senha1,
                ativo,
                observacao
        );

        if (unidadeAux.getId() == 0) {
            status = controllerUnidade.gravar(unidadeAux);
        } else {
            status = controllerUnidade.alterar(unidadeAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            boolean novaUnidade = false;
            vmUnidade = unidadeAux;
            if (vmUnidade.getId() == 0) {
                novaUnidade = true;
                vmUnidade = controllerUnidade.getUltimoRegistro(true);
            }

            atualizaInterface(vmUnidade);
            botoes(true);

            if (novaUnidade) {
                Alerta dialog = new Alerta("Cadastro unidade", "Endereço da nova Unidade", "deseja adicionar o endereço atual da unidade cadastrada?");
                if (dialog.decisaoYesNo() == ButtonType.YES) {
                    clkEndereco(null);
                }
            }
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmUnidade != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmUnidade);
            botoes(false);
            txtReferencia.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmUnidade != null && !btnExcluir.isDisable()) {
            Alerta dialog = new Alerta("Cadastro unidade", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUnidade.excluir(vmUnidade.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    UnidadeViewModel unidadeAux = controllerUnidade.getProximoRegistro(vmUnidade.getId(), true);
                    if (unidadeAux == null) {
                        unidadeAux = controllerUnidade.getAnteriorRegistro(vmUnidade.getId(), true);
                    }
                    vmUnidade = unidadeAux;
                    atualizaInterface(vmUnidade);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmUnidade);
        botoes(true);
    }

    @FXML// ok
    private void clkEndereco(ActionEvent event) {
        if (vmUnidade != null) {
            try {
                Tela tl = new Tela("/industria/view/associacao/CadUnidadeEndereco.fxml", "Buscar unidade", false, true, true);
                CadUnidadeEndereco controller = tl.getLoader().getController();
                controller.setUnidade(vmUnidade, true);

                tl.exibirTelaFilha();// exibe a tela
                vmUnidade.setEnderecoAtual(new UnidadeEnderecoController().getEnderecoAtual(vmUnidade.getId()));
            } catch (IOException ex) {
                System.out.println("erro ao exibir janela de pesquisa");
            }
            atualizaInterface(vmUnidade);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro unidade - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 903:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Razão social", "razão social não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtRazaoSocial.requestFocus();
                break;
            case 904:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Razão social", "razão social não pode conter mais que 150 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtRazaoSocial.requestFocus();
                break;
            case 905:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("CNPJ", "cnpj não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCnpj.requestFocus();
                break;
            case 906:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Telefone fixo", "telefone fixo não pode conter mais que 10 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtTelefoneFixo.requestFocus();
                break;
            case 907:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Telefone celular", "telefone celular não pode conter mais que 11 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtTelefoneCelular.requestFocus();
                break;
            case 908:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("E-mail", "e-mail não é válido \nnão está correto ou contém mais que 70 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtEmail.requestFocus();
                break;
            case 909:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Inscrição estadual", "inscrição estadual não pode conter mais que 20 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtInscricaoEstadual.requestFocus();
                break;
            case 910:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Referência", "referência para a unidade não pode conter mais que 10 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtReferencia.requestFocus();
                break;
            case 911:
                tabPane.getSelectionModel().select(tab3);
                dialog.setInformacao("Senha", "senha não pode ser conter menos que 4, ou mais que 100 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSenhaConfirmar.setText("");
                txtSenha.requestFocus();
                break;
            case 912:
                tabPane.getSelectionModel().select(tab5);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 913:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome fantasia", "nome fantasia não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNomeFantasia.requestFocus();
                break;
            case 914:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome fantasia", "nome fantasia não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNomeFantasia.requestFocus();
                break;
            case 915:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(UnidadeViewModel vmUnidade) {
        if (vmUnidade != null) {
            /* aba 1 */
            txtCodigo.setText(String.valueOf(vmUnidade.getId()));
            txtReferencia.setText(vmUnidade.getReferencia());
            txtRazaoSocial.setText(vmUnidade.getRazaoSocial());
            txtCnpj.setText(vmUnidade.getCnpjFormatado());
            txtInscricaoEstadual.setText(vmUnidade.getInscricaoEstadual());
            txtNomeFantasia.setText(vmUnidade.getNomeFantasia());

            /* aba 2 */
            txtEmail.setText(vmUnidade.getEmail());
            txtTelefoneFixo.setText(vmUnidade.getTelefoneFixo());
            txtTelefoneCelular.setText(vmUnidade.getTelefoneCelular());

            /* aba 3 */
            if (vmUnidade.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtSenha.setText("");

            /* aba 4 */
            if (vmUnidade.getEnderecoAtual() != null) {
                txtCep.setText(vmUnidade.getEnderecoAtual().getEndereco().getCepFormatado());
                txtLogradouro.setText(vmUnidade.getEnderecoAtual().getEndereco().getLogradouro());
                txtNumero.setText(String.valueOf(vmUnidade.getEnderecoAtual().getNumero()));
                txtComplemento.setText(vmUnidade.getEnderecoAtual().getComplemento());
                txtBairro.setText(vmUnidade.getEnderecoAtual().getEndereco().getBairro());
                txtCidade.setText(vmUnidade.getEnderecoAtual().getEndereco().getCidade().getNome());
            } else {
                limparCamposEndereco();
            }

            /* aba 5 */
            txtObservacao.setText(vmUnidade.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        /* aba 1 */
        txtCodigo.setText("");
        txtReferencia.setText("");
        txtRazaoSocial.setText("");
        txtCnpj.setText("");
        txtInscricaoEstadual.setText("");
        txtNomeFantasia.setText("");

        /* aba 2 */
        txtEmail.setText("");
        txtTelefoneFixo.setText("");
        txtTelefoneCelular.setText("");

        /* aba 3 */
        txtSenha.setText("");
        txtSenhaConfirmar.setText("");

        /* aba 4 */
        limparCamposEndereco();

        /* aba 5 */
        txtObservacao.setText("");
    }

    private void limparCamposEndereco() {
        /* aba 4 */
        btnEndereco.setDisable(true);
        txtCep.setText("");
        txtLogradouro.setText("");
        txtNumero.setText("");
        txtComplemento.setText("");
        txtBairro.setText("");
        txtCidade.setText("");
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmUnidade != null && vmUnidade.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        /* aba 1 */
        txtCodigo.setEditable(false);
        txtReferencia.setEditable(!controle);
        txtRazaoSocial.setEditable(!controle);
        txtCnpj.setEditable(!controle);
        txtInscricaoEstadual.setEditable(!controle);
        txtNomeFantasia.setEditable(!controle);

        /* aba 2 */
        txtEmail.setEditable(!controle);
        txtTelefoneFixo.setEditable(!controle);
        txtTelefoneCelular.setEditable(!controle);

        /* aba 3 */
        pnAtivo.setDisable(controle);
        txtSenha.setEditable(!controle);
        txtSenhaConfirmar.setEditable(!controle);

        /* aba 4 */
        tab4.setDisable(!controle);         //aba desativada na gravação
        txtCep.setEditable(false);          //sempre bloqueado
        btnEndereco.setDisable(false);      //sempre bloqueado
        txtLogradouro.setEditable(false);   //sempre bloqueado
        txtNumero.setEditable(false);       //sempre bloqueado
        txtComplemento.setEditable(false);  //sempre bloqueado
        txtBairro.setEditable(false);       //sempre bloqueado
        txtCidade.setEditable(false);       //sempre bloqueado

        /* aba 5 */
        txtObservacao.setEditable(!controle);
    }

}
