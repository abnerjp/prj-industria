package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesFuncao;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.FuncaoController;
import lib.viewmodel.cadastro.FuncaoViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadFuncao implements Initializable {

    private final FuncaoController controllerFuncao = new FuncaoController();
    private FuncaoViewModel vmFuncao = null;
    AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmFuncao = controllerFuncao.getUltimoRegistro();
        atualizaInterface(vmFuncao);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmFuncao = controllerFuncao.getPrimeiroRegistro();
        atualizaInterface(vmFuncao);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmFuncao != null) {
            FuncaoViewModel funcaoAux = controllerFuncao.getAnteriorRegistro(vmFuncao.getId());
            if (funcaoAux != null) {
                vmFuncao = funcaoAux;
            }
            atualizaInterface(vmFuncao);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmFuncao != null) {
            FuncaoViewModel funcaoAux = controllerFuncao.getProximoRegistro(vmFuncao.getId());
            if (funcaoAux != null) {
                vmFuncao = funcaoAux;
            }
            atualizaInterface(vmFuncao);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmFuncao = controllerFuncao.getUltimoRegistro();
        atualizaInterface(vmFuncao);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        FuncaoViewModel funcaoAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesFuncao.fxml", "Buscar perda", false, true, false);
            PesFuncao controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            funcaoAux = controller.getFuncao();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            funcaoAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (funcaoAux != null) {
            vmFuncao = funcaoAux;
            atualizaInterface(vmFuncao);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmFuncao != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new FuncaoViewModel(
                    vmFuncao.getNome(),
                    vmFuncao.isAtivo(),
                    vmFuncao.getObservacao())
            );
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        int status;

        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        FuncaoViewModel funcaoAux = new FuncaoViewModel(codigo, nome, ativo, observacao);

        if (funcaoAux.getId() == 0) {
            // gravar
            status = controllerFuncao.gravar(funcaoAux);
        } else {
            // alterar
            status = controllerFuncao.alterar(funcaoAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmFuncao = funcaoAux;
            if (vmFuncao.getId() == 0) {
                vmFuncao = controllerFuncao.getUltimoRegistro();
            }
            atualizaInterface(vmFuncao);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmFuncao != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmFuncao);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmFuncao != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro função", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerFuncao.excluir(vmFuncao.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    FuncaoViewModel funcaoAux = controllerFuncao.getProximoRegistro(vmFuncao.getId());
                    if (funcaoAux == null) {
                        funcaoAux = controllerFuncao.getAnteriorRegistro(vmFuncao.getId());
                    }
                    vmFuncao = funcaoAux;
                    atualizaInterface(vmFuncao);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmFuncao);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
    //------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro função - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "Erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1803:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1804:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1805:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1806:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(FuncaoViewModel funcao) {
        if (funcao != null) {
            txtCodigo.setText(String.valueOf(funcao.getId()));
            txtNome.setText(funcao.getNome());
            if (funcao.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(funcao.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmFuncao != null && vmFuncao.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }

}
