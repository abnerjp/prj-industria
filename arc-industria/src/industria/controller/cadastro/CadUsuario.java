package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.associacao.CadUsuarioEndereco;
import industria.controller.pesquisa.PesUsuario;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.associacao.UsuarioEnderecoController;
import lib.controller.cadastro.UsuarioController;
import lib.viewmodel.associacao.UsuarioEnderecoViewModel;
import lib.viewmodel.cadastro.EnderecoViewModel;
import lib.viewmodel.cadastro.UsuarioViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadUsuario implements Initializable {

    private final UsuarioController controllerUsuario = new UsuarioController();

    private UsuarioViewModel vmUsuario = null;
    private EnderecoViewModel vmEndereco = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private AnchorPane pnSexo;
    @FXML
    private JFXRadioButton rbFeminino;
    @FXML
    private JFXRadioButton rbMasculino;
    @FXML
    private AnchorPane pnImagem;
    @FXML
    private ImageView imgPerfil;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextField txtLogin;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab3;
    @FXML
    private JFXTextField txtCep;
    @FXML
    private JFXButton btnEndereco;
    @FXML
    private JFXTextField txtLogradouro;
    @FXML
    private JFXTextField txtNumero;
    @FXML
    private JFXTextField txtComplemento;
    @FXML
    private JFXTextField txtBairro;
    @FXML
    private JFXTextField txtCidade;
    @FXML
    private Tab tab4;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private ToggleGroup tgSexo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXTextField txtEmail;
    @FXML
    private JFXPasswordField txtSenha1;
    @FXML
    private JFXPasswordField txtSenha2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmUsuario = controllerUsuario.getUltimoRegistro();
        atualizaInterface(vmUsuario);
        botoes(true);
        carregarImagem();
    }
    
    private void carregarImagem() {
        Image img = new Image(new String("/../../../../img/user-perfil/no-image.png"));
        
        imgPerfil.setImage(img);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmUsuario = controllerUsuario.getPrimeiroRegistro();
        atualizaInterface(vmUsuario);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmUsuario != null) {
            UsuarioViewModel unAux = controllerUsuario.getAnteriorRegistro(vmUsuario.getId());
            if (unAux != null) {
                vmUsuario = unAux;
            }
            atualizaInterface(vmUsuario);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmUsuario != null) {
            UsuarioViewModel unAux = controllerUsuario.getProximoRegistro(vmUsuario.getId());
            if (unAux != null) {
                vmUsuario = unAux;
            }
            atualizaInterface(vmUsuario);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmUsuario = controllerUsuario.getUltimoRegistro();
        atualizaInterface(vmUsuario);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        UsuarioViewModel usuarioAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesUsuario.fxml", "Buscar usuário", false, true, false);
            PesUsuario controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            usuarioAux = controller.getUnidade();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            usuarioAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (usuarioAux != null) {
            vmUsuario = usuarioAux;
            atualizaInterface(vmUsuario);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmUsuario != null && !btnNovo.isDisable()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(
                    new UsuarioViewModel(
                            vmUsuario.getNome(),
                            vmUsuario.getSexo(),
                            vmUsuario.getLogin(),
                            vmUsuario.getSenha(),
                            vmUsuario.getEmail(),
                            vmUsuario.getEmail(),
                            vmUsuario.isAtivo(),
                            vmUsuario.getObservacao()
                    )
            );
            botoes(false);
            tabPane.getSelectionModel().select(tab1);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkEndereco(ActionEvent event) {
        if (vmUsuario != null) {
            try {
                Tela tl = new Tela("/industria/view/associacao/CadUsuarioEndereco.fxml", "Buscar usuário", false, true, true);
                CadUsuarioEndereco controller = tl.getLoader().getController();
                controller.setUsuario(vmUsuario, true);

                tl.exibirTelaFilha();// exibe a tela
                vmUsuario.setEnderecoAtual(new UsuarioEnderecoController().getEnderecoAtual(vmUsuario.getId()));
            } catch (IOException ex) {
                System.out.println("erro ao exibir janela de pesquisa");
            }
            atualizaInterface(vmUsuario);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        atualizaInterface(null);
        botoes(false);
        tabPane.getSelectionModel().select(tab1);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo, statusGravacao;

        /* aba 1 */
        String nome = txtNome.getText();
        String sexo = rbMasculino.isSelected() ? "m" : "f";

        /* aba 2 */
        String email = txtEmail.getText();
        String login = txtLogin.getText();
        String senha1 = txtSenha1.getText();
        String senha2 = txtSenha2.getText();
        boolean ativo = rbAtivo.isSelected();

        /* aba 3 */
        //Endereco
        
        /* aba 4 */
        String observacao = txtObservacao.getText();

        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        statusGravacao = senhasIguais(senha1, senha2);
        if (statusGravacao == 0) {
            UsuarioViewModel usuarioAux = new UsuarioViewModel(
                    codigo,
                    nome,
                    sexo,
                    login,
                    senha1,
                    email,
                    "",
                    ativo,
                    observacao
            );

            if (usuarioAux.getId() == 0) {
                statusGravacao = controllerUsuario.gravar(usuarioAux);
            } else {
                statusGravacao = controllerUsuario.alterar(usuarioAux);
            }

            if (statusGravacao > 0) {
                exibeAlerta(statusGravacao);
            } else {
                boolean novoUsuario = false;
                vmUsuario = usuarioAux;
                if (vmUsuario.getId() == 0) {
                    novoUsuario = true;
                    vmUsuario = controllerUsuario.getUltimoRegistro();
                }

                atualizaInterface(vmUsuario);
                botoes(true);

                if (novoUsuario) {
                    Alerta dialog = new Alerta("Cadastro usuário", "Endereço do novo Usuário", "deseja adicionar o endereço atual do usuário cadastrado?");
                    if (dialog.decisaoYesNo() == ButtonType.YES) {
                        clkEndereco(null);
                    }
                }
            }
        } else {
            exibeAlerta(statusGravacao);
        }
    }

    private int senhasIguais(String str1, String str2) {
        return str1.equals(str2) ? 0 : 2015;
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmUsuario != null) {
            atualizaInterface(vmUsuario);
            botoes(false);
            tabPane.getSelectionModel().select(tab1);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmUsuario != null && !btnExcluir.isDisable()) {
            Alerta dialog = new Alerta("Cadastro usuário", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerUsuario.excluir(vmUsuario.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    UsuarioViewModel usuarioAux = controllerUsuario.getProximoRegistro(vmUsuario.getId());
                    if (usuarioAux == null) {
                        usuarioAux = controllerUsuario.getAnteriorRegistro(vmUsuario.getId());
                    }
                    vmUsuario = usuarioAux;
                    atualizaInterface(vmUsuario);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmUsuario);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro usuário - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2003:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 2004:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 2005:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sexo", "sexo não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                pnSexo.requestFocus();
                break;
            case 2006:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sexo", "sexo não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                pnSexo.requestFocus();
                break;
            case 2007:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("E-mail", "e-mail não é válido \nnão está correto ou contém mais que 70 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtEmail.requestFocus();
                break;
            case 2008:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Login", "login não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtLogin.requestFocus();
                break;
            case 2009:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Login", "login não pode conter mais que 20 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtLogin.requestFocus();
                break;
            case 2010:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Login", "login informado já existe, informe outro");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtLogin.requestFocus();
                break;
            case 2011:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Senha", "senha não pode ser vazia ou menor que 4 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSenha1.requestFocus();
                break;
            case 2012:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Senha", "senha não pode conter mais que 20 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSenha1.requestFocus();
                break;
            case 2013:
                tabPane.getSelectionModel().select(tab4);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 2014:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2015:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Senha", "senhas não são iguas");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSenha1.requestFocus();
                break;
        }
    }

    private void atualizaInterface(UsuarioViewModel vmUsuario) {
        if (vmUsuario != null) {
            /* aba 1 */
            txtCodigo.setText(String.valueOf(vmUsuario.getId()));
            txtNome.setText(vmUsuario.getNome());
            //atualizar imagem - TODO
            if (vmUsuario.getSexo().equals("m")) {
                rbMasculino.setSelected(true);
            } else if (vmUsuario.getSexo().equals("f")) {
                rbFeminino.setSelected(true);
            }

            /* aba 2 */
            txtEmail.setText(vmUsuario.getEmail());
            txtLogin.setText(vmUsuario.getLogin());
            if (vmUsuario.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }

            /* aba 3 */
            UsuarioEnderecoViewModel endereco = vmUsuario.getEnderecoAtual();
            try {
                txtCep.setText(endereco.getEndereco().getCepFormatado());
                txtLogradouro.setText(endereco.getEndereco().getLogradouro());
                txtNumero.setText(String.valueOf(endereco.getNumero()));
                txtComplemento.setText(endereco.getComplemento());
                txtBairro.setText(endereco.getEndereco().getBairro());
                txtCidade.setText(endereco.getEndereco().getCidade().getNome());
            } catch (NullPointerException ex) {
                limparCamposEndereco();
            }
            
            /* aba 4 */
            txtObservacao.setText(vmUsuario.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void limparCampos() {
        /* aba 1*/
        txtCodigo.setText("");
        txtNome.setText("");
        rbFeminino.setSelected(true);
        //limpar imagem

        /* aba 2*/
        txtEmail.setText("");
        txtLogin.setText("");
        txtSenha1.setText("");
        txtSenha2.setText("");
        rbInativo.setSelected(true);

        /* aba 3*/
        limparCamposEndereco();

        /* aba 4*/
        txtObservacao.setText("");
    }

    private void limparCamposEndereco() {
        /* aba 3 */
        btnEndereco.setDisable(true);
        txtCep.setText("");
        txtLogradouro.setText("");
        txtNumero.setText("");
        txtComplemento.setText("");
        txtBairro.setText("");
        txtCidade.setText("");
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmUsuario != null && vmUsuario.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        /* aba 1 */
        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        pnSexo.setDisable(controle);

        /* aba 2 */
        txtEmail.setEditable(!controle);
        txtLogin.setEditable(!controle);
        txtSenha1.setEditable(!controle);
        txtSenha2.setEditable(!controle);
        pnAtivo.setDisable(controle);

        /* aba 3 */
        tab3.setDisable(!controle);         //aba desativada na gravação
        txtCep.setEditable(false);          //sempre bloqueado
        btnEndereco.setDisable(false);      //sempre bloqueado
        txtLogradouro.setEditable(false);   //sempre bloqueado
        txtNumero.setEditable(false);       //sempre bloqueado
        txtComplemento.setEditable(false);  //sempre bloqueado
        txtBairro.setEditable(false);       //sempre bloqueado
        txtCidade.setEditable(false);       //sempre bloqueado

        /* aba 4 */
        txtObservacao.setEditable(!controle);
    }

}
