package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesEstado;
import industria.controller.pesquisa.PesPais;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.EstadoController;
import lib.controller.cadastro.PaisController;
import lib.viewmodel.cadastro.EstadoViewModel;
import lib.viewmodel.cadastro.PaisViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadEstado implements Initializable {

    EstadoController controllerEstado = new EstadoController();
    PaisController controllerPais = new PaisController();
    EstadoViewModel vmEstado = null;
    AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtCodigoPais;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextField txtNomePais;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtSigla;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        focusTxtCodigoMedida(txtCodigoPais, txtNomePais);
        vmEstado = controllerEstado.getUltimoRegistro();
        atualizaInterface(vmEstado);
        botoes(true);
    }

    @FXML
    private void clkPrimeiro(ActionEvent event) {
        vmEstado = controllerEstado.getPrimeiroRegistro();
        atualizaInterface(vmEstado);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmEstado != null) {
            EstadoViewModel ufAux = controllerEstado.getAnteriorRegistro(vmEstado.getId());
            if (ufAux != null) {
                vmEstado = ufAux;
            }
            atualizaInterface(vmEstado);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmEstado != null) {
            EstadoViewModel ufAux = controllerEstado.getProximoRegistro(vmEstado.getId());
            if (ufAux != null) {
                vmEstado = ufAux;
            }
            atualizaInterface(vmEstado);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmEstado = controllerEstado.getUltimoRegistro();
        atualizaInterface(vmEstado);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        EstadoViewModel ufAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesEstado.fxml", "Buscar item", false, true, false);
            PesEstado controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            ufAux = controller.getEstado();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            ufAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (ufAux != null) {
            vmEstado = ufAux;
            atualizaInterface(vmEstado);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmEstado != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(
                    new EstadoViewModel(
                            vmEstado.getNome(),
                            vmEstado.getSigla(),
                            vmEstado.isAtivo(),
                            vmEstado.getObservacao(),
                            vmEstado.getPais().getId()
                    )
            );
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        String sigla = txtSigla.getText();
        int codigoPais;
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        int status;

        //codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        //codigo país
        try {
            codigoPais = Integer.parseInt(txtCodigoPais.getText(), 10);
        } catch (NumberFormatException ex) {
            codigoPais = 0;
        }

        EstadoViewModel ufAux = new EstadoViewModel(
                codigo,
                nome,
                sigla,
                ativo,
                observacao,
                codigoPais
        );

        if (ufAux.getId() == 0) {// gravar
            status = controllerEstado.gravar(ufAux);
        } else {// alterar
            status = controllerEstado.alterar(ufAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmEstado = ufAux;
            if (vmEstado.getId() == 0) {
                vmEstado = controllerEstado.getUltimoRegistro();
            }
            atualizaInterface(vmEstado);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmEstado != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmEstado);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmEstado != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastrar estado", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerEstado.excluir(vmEstado.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    // seleciona o próximo registro ou anterior, para exibir na tela
                    EstadoViewModel ufAux = controllerEstado.getProximoRegistro(vmEstado.getId());
                    if (ufAux == null) {
                        ufAux = controllerEstado.getAnteriorRegistro(vmEstado.getId());
                    }
                    vmEstado = ufAux;
                    atualizaInterface(vmEstado);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmEstado);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        // capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (btnNovo.isDisabled() && txtCodigoPais.isFocused() && atalhoKey.isBuscaAuxiliar(event)) {
            atualizaMedida(buscaPais());
        }
    }

    @FXML// ok
    private void clkBuscarPais(MouseEvent event) {
        if (event.getClickCount() == 2 && btnNovo.isDisable()) {
            atualizaMedida(buscaPais());
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro estado - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração dever ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 303:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 304:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 100 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 305:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode ser vazia");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 306:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode conter mais que 10 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 307:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("País", "código do país não é válido");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoPais.requestFocus();
                break;
            case 308:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 309:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("País", "país está desativado");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoPais.requestFocus();
                break;
            case 310:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(EstadoViewModel uf) {
        if (uf != null) {
            txtCodigo.setText(String.valueOf(uf.getId()));
            txtNome.setText(uf.getNome());
            txtSigla.setText(uf.getSigla());
            txtCodigoPais.setText(String.valueOf(uf.getPais().getId()));
            String nomePais = "";
            if (uf.getPais() != null) {
                if (!uf.getPais().isAtivo()) {
                    nomePais = "[Desativado] ";
                }
                nomePais += uf.getPais().getNome();
            }
            txtNomePais.setText(nomePais);
            if (uf.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(uf.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmEstado != null && vmEstado.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtSigla.setEditable(!controle);
        txtCodigoPais.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtSigla.setText("");
        txtCodigoPais.setText("");
        txtNomePais.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }

// PARA CONTROLE DA MEDIDA -----------------------------------------------------
    private PaisViewModel buscaPais() {
        //Pais paisAux;
        PaisViewModel paisAux = null;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesPais.fxml", "Buscar medida", false, true, false);
            PesPais controller = tl.getLoader().getController();
            controller.setAcessoTerceiro(true);//informa a classe que será acessada por terceiros

            tl.exibirTelaFilha();// exibe a tela
            paisAux = controller.getPais();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            paisAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }
        return paisAux;
    }

    private void atualizaMedida(PaisViewModel pais) {
        if (pais != null) {
            txtCodigoPais.setText(String.valueOf(pais.getId()));
            txtNomePais.setText(pais.getNome());
        }
    }

    // método para controlar a quando o foco "deixar" o textbox código da medida
    private void focusTxtCodigoMedida(JFXTextField codigo, JFXTextField nome) {
        codigo.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean velhoValor, Boolean novoValor) -> {

            // condição para quando deixar o foco
            if (btnNovo.isDisabled() && !novoValor) {
                if (!codigo.getText().isEmpty()) {
                    PaisViewModel pais;
                    try {
                        int codPaisAux = Integer.parseInt(codigo.getText(), 10);
                        pais = controllerPais.regPorCodigo(codPaisAux);
                    } catch (NumberFormatException ex) {
                        pais = null;
                    }

                    if (pais == null) {
                        nome.setText("");
                    } else {
                        if (!pais.isAtivo()) {
                            pais.setNome("[Desativado] " + pais.getNome());
                        }
                        nome.setText(pais.getNome());
                    }
                } else {
                    nome.setText("");
                }

            }
            // condição para quando focar
            //else{            
            //System.out.println("Focou");
            //}
        });
    }
}
