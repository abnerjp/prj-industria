package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesItem;
import industria.controller.pesquisa.PesMedida;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.ItemController;
import lib.controller.cadastro.MedidaController;
import lib.viewmodel.cadastro.ItemViewModel;
import lib.viewmodel.cadastro.MedidaViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadItem implements Initializable {

    private final ItemController controllerItem = new ItemController();
    private final MedidaController controllerMedida = new MedidaController();
    private ItemViewModel vmItem = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private JFXTextField txtNomeMedida;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtCodigoMedida;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        focusTxtCodigoMedida(txtCodigoMedida, txtNomeMedida);
        vmItem = controllerItem.getUltimoRegistro();
        atualizaInterface(vmItem);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmItem = controllerItem.getPrimeiroRegistro();
        atualizaInterface(vmItem);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmItem != null) {
            ItemViewModel iteAux = controllerItem.getAnteriorRegistro(vmItem.getId());
            if (iteAux != null) {
                vmItem = iteAux;
            }
            atualizaInterface(vmItem);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmItem != null) {
            ItemViewModel iteAux = controllerItem.getProximoRegistro(vmItem.getId());
            if (iteAux != null) {
                vmItem = iteAux;
            }
            atualizaInterface(vmItem);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmItem = controllerItem.getUltimoRegistro();
        atualizaInterface(vmItem);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        ItemViewModel iteAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesItem.fxml", "Buscar item", false, true, false);
            PesItem controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            iteAux = controller.getItem();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            iteAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (iteAux != null) {
            vmItem = iteAux;
            atualizaInterface(vmItem);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmItem != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(
                    new ItemViewModel(
                            vmItem.getNome(),
                            vmItem.isAtivo(),
                            vmItem.getObservacao(),
                            vmItem.getMedida().getId()
                    )
            );
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        int codigoMedida;
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        int status;

        //codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        //medida
        try {
            codigoMedida = Integer.parseInt(txtCodigoMedida.getText(), 10);
        } catch (NumberFormatException ex) {
            codigoMedida = 0;
        }

        ItemViewModel iteAux = new ItemViewModel(
                codigo,
                nome,
                ativo,
                observacao,
                codigoMedida
        );

        if (iteAux.getId() == 0) {// gravar
            status = controllerItem.gravar(iteAux);
        } else {// alterar
            status = controllerItem.alterar(iteAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmItem = iteAux;
            if (vmItem.getId() == 0) {
                vmItem = controllerItem.getUltimoRegistro();
            }
            atualizaInterface(vmItem);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmItem != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmItem);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmItem != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro item", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerItem.excluir(vmItem.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    // seleciona o próximo registro ou anterior, para exibir na tela
                    ItemViewModel iteAux = controllerItem.getProximoRegistro(vmItem.getId());
                    if (iteAux == null) {
                        iteAux = controllerItem.getAnteriorRegistro(vmItem.getId());
                    }
                    vmItem = iteAux;
                    atualizaInterface(vmItem);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmItem);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        // capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        } else if (btnNovo.isDisabled() && txtCodigoMedida.isFocused() && atalhoKey.isBuscaAuxiliar(event)) {
            atualizaMedida(buscaMedida());
        }
    }

    @FXML// ok
    private void clkBuscarMedida(MouseEvent event) {
        if (event.getClickCount() == 2 && btnNovo.isDisable()) {
            atualizaMedida(buscaMedida());
        }
    }

//------------------------------------------------------------------------------
    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro item - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração dever ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 103:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 104:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 105:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Medida", "unidade de medida não é válida");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoMedida.requestFocus();
                break;
            case 106:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 107:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Medida", "unidade de medida está desativada");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtCodigoMedida.requestFocus();
                break;
            case 108:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(ItemViewModel ite) {
        if (ite != null) {
            txtCodigo.setText(String.valueOf(ite.getId()));
            txtNome.setText(ite.getNome());
            txtCodigoMedida.setText(String.valueOf(ite.getMedida().getId()));
            String nomeMedida = "";
            if (ite.getMedida() != null) {
                if (!ite.getMedida().isAtivo()) {
                    nomeMedida = "[Desativado] ";
                }
                nomeMedida += ite.getMedida().getNome();
            }
            txtNomeMedida.setText(nomeMedida);
            if (ite.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(ite.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmItem != null && vmItem.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtCodigoMedida.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtCodigoMedida.setText("");
        txtNomeMedida.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }

// PARA CONTROLE DA MEDIDA -----------------------------------------------------
    private MedidaViewModel buscaMedida() {
        MedidaViewModel medAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesMedida.fxml", "Buscar medida", false, true, false);
            PesMedida controller = tl.getLoader().getController();
            controller.setAcessoTerceiro(true);//informa a classe que será acessada por terceiros

            tl.exibirTelaFilha();// exibe a tela
            medAux = controller.getMedida();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            medAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }
        return medAux;
    }

    private void atualizaMedida(MedidaViewModel med) {
        if (med != null) {
            txtCodigoMedida.setText(String.valueOf(med.getId()));
            txtNomeMedida.setText(med.getNome());
        }
    }

    //método para controlar a quando o foco "deixar" o textbox código da medida
    private void focusTxtCodigoMedida(JFXTextField codigo, JFXTextField nome) {
        codigo.focusedProperty().addListener((ObservableValue<? extends Boolean> arg0, Boolean velhoValor, Boolean novoValor) -> {

            // condição para quando deixar o foco
            if (btnNovo.isDisabled() && !novoValor) {
                if (!codigo.getText().isEmpty()) {
                    MedidaViewModel medida;
                    try {
                        int codigoMedida = Integer.parseInt(codigo.getText(), 10);
                        medida = controllerMedida.regPorCodigo(codigoMedida);
                    } catch (NumberFormatException ex) {
                        medida = null;
                    }

                    if (medida == null) {
                        nome.setText("");
                    } else {
                        if (!medida.isAtivo()) {
                            medida.setNome("[Desativado] " + medida.getNome());
                        }
                        nome.setText(medida.getNome());
                    }
                } else {
                    nome.setText("");
                }
            }
        });
    }
}
