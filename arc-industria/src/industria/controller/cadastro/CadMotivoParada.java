package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesMotivoParada;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.MotivoParadaController;
import lib.viewmodel.cadastro.MotivoParadaViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class CadMotivoParada implements Initializable {

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtSigla;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    private final MotivoParadaController controllerMotivoParada = new MotivoParadaController();
    private MotivoParadaViewModel vmMotivoParada = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmMotivoParada = controllerMotivoParada.getUltimoRegistro();
        atualizaInterface(vmMotivoParada);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmMotivoParada = controllerMotivoParada.getPrimeiroRegistro();
        atualizaInterface(vmMotivoParada);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmMotivoParada != null) {
            MotivoParadaViewModel mpaAux = controllerMotivoParada.getAnteriorRegistro(vmMotivoParada.getId());
            if (mpaAux != null) {
                vmMotivoParada = mpaAux;
            }
            atualizaInterface(vmMotivoParada);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmMotivoParada != null) {
            MotivoParadaViewModel mpaAux = controllerMotivoParada.getProximoRegistro(vmMotivoParada.getId());
            if (mpaAux != null) {
                vmMotivoParada = mpaAux;
            }
            atualizaInterface(vmMotivoParada);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmMotivoParada = controllerMotivoParada.getUltimoRegistro();
        atualizaInterface(vmMotivoParada);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        MotivoParadaViewModel mpaAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesMotivoParada.fxml", "Buscar submotivo", false, true, false);
            PesMotivoParada controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            mpaAux = controller.getMotivoParada();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            mpaAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (mpaAux != null) {
            vmMotivoParada = mpaAux;
            atualizaInterface(vmMotivoParada);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmMotivoParada != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new MotivoParadaViewModel(vmMotivoParada.getNome(), vmMotivoParada.getSigla(), vmMotivoParada.isAtivo(), vmMotivoParada.getObservacao()));
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        String sigla = txtSigla.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        MotivoParadaViewModel mpaAux;
        int status;

        // codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        mpaAux = new MotivoParadaViewModel(
                codigo,
                nome,
                sigla,
                ativo,
                observacao
        );

        if (mpaAux.getId() == 0) {// gravar
            status = controllerMotivoParada.gravar(mpaAux);
        } else {// alterar
            status = controllerMotivoParada.alterar(mpaAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmMotivoParada = mpaAux;
            if (vmMotivoParada.getId() == 0) {
                vmMotivoParada = controllerMotivoParada.getUltimoRegistro();
            }
            atualizaInterface(vmMotivoParada);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmMotivoParada != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmMotivoParada);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmMotivoParada != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro motivo de parada", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerMotivoParada.excluir(vmMotivoParada.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    MotivoParadaViewModel mpaAux = controllerMotivoParada.getProximoRegistro(vmMotivoParada.getId());
                    if (mpaAux == null) {
                        mpaAux = controllerMotivoParada.getAnteriorRegistro(vmMotivoParada.getId());
                    }
                    vmMotivoParada = mpaAux;
                    atualizaInterface(vmMotivoParada);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmMotivoParada);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro motivo de parada - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 603:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 604:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 50 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 605:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode ser vazia");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 606:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode conter mais que 6 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 607:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 608:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(MotivoParadaViewModel mp) {
        if (mp != null) {
            txtCodigo.setText(String.valueOf(mp.getId()));
            txtNome.setText(mp.getNome());
            txtSigla.setText(mp.getSigla());
            if (mp.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(mp.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmMotivoParada != null && vmMotivoParada.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtSigla.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtSigla.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }
}
