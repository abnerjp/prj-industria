package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesPais;
import industria.entidade.Pais;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.PaisController;
import lib.viewmodel.cadastro.PaisViewModel;

/**
 * FXML Controller class
 *
 * Cadastro de país, para ser utilizado pelos ESTADOS(UF)
 *
 * @author abnerjp
 */
public class CadPais implements Initializable {

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtSigla;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    private final PaisController controllerPais = new PaisController();
    private PaisViewModel vmPais = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmPais = controllerPais.getUltimoRegistro();
        atualizaInterface(vmPais);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmPais = controllerPais.getPrimeiroRegistro();
        atualizaInterface(vmPais);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmPais != null) {
            PaisViewModel medAux = controllerPais.getAnteriorRegistro(vmPais.getId());
            if (medAux != null) {
                vmPais = medAux;
            }
            atualizaInterface(vmPais);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmPais != null) {
            PaisViewModel paisAux = controllerPais.getProximoRegistro(vmPais.getId());
            if (paisAux != null) {
                vmPais = paisAux;
            }
            atualizaInterface(vmPais);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmPais = controllerPais.getUltimoRegistro();
        atualizaInterface(vmPais);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        PaisViewModel paisAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesPais.fxml", "Buscar país", false, true, false);
            PesPais controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            paisAux = controller.getPais();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            paisAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (paisAux != null) {
            vmPais = paisAux;
            atualizaInterface(vmPais);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmPais != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new PaisViewModel(vmPais.getNome(), vmPais.getSigla(), vmPais.isAtivo(), vmPais.getObservacao()));
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo;
        String nome = txtNome.getText();
        String sigla = txtSigla.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        int status;

        // codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        PaisViewModel paisAux = new PaisViewModel(codigo, nome, sigla, ativo, observacao);

        if (paisAux.getId() == 0) {
            //gravar
            status = controllerPais.gravar(paisAux);
        } else {
            //alterar
            status = controllerPais.alterar(paisAux);
        }

        if (status > 0) {
            exibeAlerta(status);
        } else {
            vmPais = paisAux;
            if (vmPais.getId() == 0) {
                vmPais = controllerPais.getUltimoRegistro();
            }
            atualizaInterface(vmPais);
            botoes(true);
        }

    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmPais != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmPais);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmPais != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro país", "Apagar registro", "Confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerPais.excluir(vmPais.getId()) > 0) {
                    dialog.setMensagem("Não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    PaisViewModel paisAux = controllerPais.getProximoRegistro(vmPais.getId());
                    if (paisAux == null) {
                        paisAux = controllerPais.getAnteriorRegistro(vmPais.getId());
                    }
                    vmPais = paisAux;
                    atualizaInterface(vmPais);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmPais);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro país - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "Erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 203:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 204:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 100 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 205:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode ser vazia");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 206:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "sigla não pode conter mais que 10 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 207:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 208:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private Pais validaCampos() {
        int codigo;        // ok
        String nome;       // ok
        String sigla;      // ok
        boolean ativo;     // ok
        String observacao; // ok
        Alerta dialog = new Alerta("Cadastro país", "", "");

        // codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        // nome
        nome = txtNome.getText();
        if (nome == null || nome.length() < 2) {
            tabPane.getSelectionModel().select(tab1);
            dialog.setInformacao("Nome", "Nome não pode ser vazio ou menor que 2 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtNome.requestFocus();
            return null;
        } else if (nome.length() > 100) {
            tabPane.getSelectionModel().select(tab1);
            dialog.setInformacao("Nome", "Nome não pode conter mais que 100 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtNome.requestFocus();
            return null;
        }

        // sigla
        sigla = txtSigla.getText();
        if (sigla == null || sigla.isEmpty()) {
            tabPane.getSelectionModel().select(tab1);
            dialog.setInformacao("Sigla", "Sigla não pode ser vazia");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtSigla.requestFocus();
            return null;
        } else if (sigla.length() > 10) {
            tabPane.getSelectionModel().select(tab1);
            dialog.setInformacao("Sigla", "Sigla não pode conter mais que 10 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtSigla.requestFocus();
            return null;
        }

        // ativo
        ativo = rbAtivo.selectedProperty().get();

        // observação
        observacao = txtObservacao.getText();
        if (observacao.length() > 200) {
            tabPane.getSelectionModel().select(tab2);
            dialog.setInformacao("Observação", "Observação não pode conter mais que 200 caracteres");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
            txtObservacao.requestFocus();
            return null;
        }

        return new Pais(codigo, nome, sigla, ativo, observacao);
    }

    private void atualizaInterface(PaisViewModel p) {
        if (p != null) {
            txtCodigo.setText(String.valueOf(p.getId()));
            txtNome.setText(p.getNome());
            txtSigla.setText(p.getSigla());
            if (p.isAtivo()) {
                rbAtivo.selectedProperty().set(true);
            } else {
                rbInativo.selectedProperty().set(true);
            }
            txtObservacao.setText(p.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmPais != null && vmPais.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtSigla.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtSigla.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }
}
