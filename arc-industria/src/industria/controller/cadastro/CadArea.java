package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesArea;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.AreaController;
import lib.viewmodel.cadastro.AreaViewModel;

/**
 * FXML Controller class
 *
 * Cadastro de area, para ser utilizado pelas UNIDADES
 *
 * @author abnerjp
 */
public class CadArea implements Initializable {

    private final AreaController controllerArea = new AreaController();
    private AreaViewModel vmArea = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtDescricao;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmArea = controllerArea.getUltimoRegistro();
        atualizaInterface(vmArea);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmArea = controllerArea.getPrimeiroRegistro();
        atualizaInterface(vmArea);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmArea != null) {
            AreaViewModel areaAux = controllerArea.getAnteriorRegistro(vmArea.getId());
            if (areaAux != null) {
                vmArea = areaAux;
            }
            atualizaInterface(vmArea);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmArea != null) {
            AreaViewModel areaAux = controllerArea.getProximoRegistro(vmArea.getId());
            if (areaAux != null) {
                vmArea = areaAux;
            }
            atualizaInterface(vmArea);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmArea = controllerArea.getUltimoRegistro();
        atualizaInterface(vmArea);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        AreaViewModel areaAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesArea.fxml", "Buscar área", false, true, false);
            PesArea controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            areaAux = controller.getArea();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            areaAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (areaAux != null) {
            vmArea = areaAux;
            atualizaInterface(vmArea);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmArea != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new AreaViewModel(vmArea.getNome(), vmArea.getDescricao(), vmArea.isAtivo(), vmArea.getObservacao()));
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo, status;
        String nome = txtNome.getText();
        String descricao = txtDescricao.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        AreaViewModel areaAux;

        //código
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        areaAux = new AreaViewModel(
                codigo,
                nome,
                descricao,
                ativo,
                observacao
        );

        if (areaAux.getId() == 0) {
            //gravar
            status = controllerArea.gravar(areaAux);
        } else {
            status = controllerArea.alterar(areaAux);
        }

        if (status > 0) {
            //houve exceção na gravação
            exibeAlerta(status);
        } else {
            vmArea = areaAux;
            if (vmArea.getId() == 0) {
                vmArea = controllerArea.getUltimoRegistro();
            }
            atualizaInterface(vmArea);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmArea != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmArea);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmArea != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro medida", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerArea.excluir(vmArea.getId()) > 0) {
                    dialog.setMensagem("não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    AreaViewModel areaAux = controllerArea.getProximoRegistro(vmArea.getId());
                    if (areaAux == null) {
                        areaAux = controllerArea.getAnteriorRegistro(vmArea.getId());
                    }
                    vmArea = areaAux;
                    atualizaInterface(vmArea);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmArea);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro área - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 1103:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1104:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "nome não pode conter mais que 30 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 1105:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Descrição", "descrição não pode conter mais que 100 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtDescricao.requestFocus();
                break;
            case 1106:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 1107:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(AreaViewModel a) {
        if (a != null) {
            txtCodigo.setText(String.valueOf(a.getId()));
            txtNome.setText(a.getNome());
            txtDescricao.setText(a.getDescricao());
            if (a.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(a.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmArea != null && vmArea.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtDescricao.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtDescricao.setText("");
        rbInativo.setSelected(true);
        txtObservacao.setText("");
    }
}
