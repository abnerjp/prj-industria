package industria.controller.cadastro;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import industria.controller.pesquisa.PesMedida;
import industria.util.Alerta;
import industria.util.AtalhoCRUD;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lib.controller.cadastro.MedidaController;
import lib.viewmodel.cadastro.MedidaViewModel;

/**
 * FXML Controller class
 *
 * Cadastro de unidade de medida, para ser utilizado pelos ITENS
 *
 * @author abnerjp
 */
public class CadMedida implements Initializable {

    private final MedidaController controllerMedida = new MedidaController();
    private MedidaViewModel vmMedida = null;
    private final AtalhoCRUD atalhoKey = new AtalhoCRUD();

    @FXML
    private JFXButton btnBusca;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnGravar;
    @FXML
    private JFXButton btnAlterar;
    @FXML
    private JFXButton btnExcluir;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXTextField txtCodigo;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtNome;
    @FXML
    private JFXTextField txtSigla;
    @FXML
    private JFXRadioButton rbInativo;
    @FXML
    private JFXRadioButton rbAtivo;
    @FXML
    private Tab tab2;
    @FXML
    private JFXTextArea txtObservacao;
    @FXML
    private ToggleGroup tgAtivo;
    @FXML
    private AnchorPane pnAtivo;
    @FXML
    private JFXTabPane tabPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vmMedida = controllerMedida.getUltimoRegistro();
        atualizaInterface(vmMedida);
        botoes(true);
    }

    @FXML// ok
    private void clkPrimeiro(ActionEvent event) {
        vmMedida = controllerMedida.getPrimeiroRegistro();
        atualizaInterface(vmMedida);
        botoes(true);
    }

    @FXML// ok
    private void clkAnterior(ActionEvent event) {
        if (vmMedida != null) {
            MedidaViewModel medAux = controllerMedida.getAnteriorRegistro(vmMedida.getId());
            if (medAux != null) {
                vmMedida = medAux;
            }
            atualizaInterface(vmMedida);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkProximo(ActionEvent event) {
        if (vmMedida != null) {
            MedidaViewModel medAux = controllerMedida.getProximoRegistro(vmMedida.getId());
            if (medAux != null) {
                vmMedida = medAux;
            }
            atualizaInterface(vmMedida);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkUltimo(ActionEvent event) {
        vmMedida = controllerMedida.getUltimoRegistro();
        atualizaInterface(vmMedida);
        botoes(true);
    }

    @FXML// ok
    private void clkBusca(ActionEvent event) {
        MedidaViewModel medAux;
        try {
            Tela tl = new Tela("/industria/view/pesquisa/PesMedida.fxml", "Buscar medida", false, true, false);
            PesMedida controller = tl.getLoader().getController();

            tl.exibirTelaFilha();// exibe a tela
            medAux = controller.getMedida();// salva retorno da janela de pesquisa
        } catch (IOException ex) {
            medAux = null;
            System.out.println("erro ao exibir janela de pesquisa");
        }

        if (medAux != null) {
            vmMedida = medAux;
            atualizaInterface(vmMedida);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkCopia(ActionEvent event) {
        if (vmMedida != null && !btnNovo.disableProperty().get()) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(new MedidaViewModel(vmMedida.getNome(), vmMedida.getSigla(), vmMedida.isAtivo(), vmMedida.getObservacao()));
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        tabPane.getSelectionModel().select(tab1);
        atualizaInterface(null);
        botoes(false);
        txtNome.requestFocus();
    }

    @FXML// ok
    private void clkGravar(ActionEvent event) {
        int codigo, status;
        String nome = txtNome.getText();
        String sigla = txtSigla.getText();
        boolean ativo = rbAtivo.isSelected();
        String observacao = txtObservacao.getText();
        MedidaViewModel medAux;

        // codigo
        try {
            codigo = Integer.parseInt(txtCodigo.getText(), 10);
        } catch (NumberFormatException ex) {
            codigo = 0;
        }

        medAux = new MedidaViewModel(
                codigo,
                nome,
                sigla,
                ativo,
                observacao
        );
        if (medAux.getId() == 0) {
            //gravar
            status = controllerMedida.gravar(medAux);
        } else {
            //alterar
            status = controllerMedida.alterar(medAux);
        }

        if (status > 0) {
            //houve exceção na gravação
            exibeAlerta(status);
        } else {
            //gravação realizada com sucesso
            vmMedida = medAux;
            if (vmMedida.getId() == 0) {
                vmMedida = controllerMedida.getUltimoRegistro();
            }
            atualizaInterface(vmMedida);
            botoes(true);
        }
    }

    @FXML// ok
    private void clkAlterar(ActionEvent event) {
        if (vmMedida != null) {
            tabPane.getSelectionModel().select(tab1);
            atualizaInterface(vmMedida);
            botoes(false);
            txtNome.requestFocus();
        }
    }

    @FXML// ok
    private void clkExcluir(ActionEvent event) {
        if (vmMedida != null && !btnExcluir.disableProperty().get()) {
            Alerta dialog = new Alerta("Cadastro medida", "Apagar registro", "confirma a exclusão do registro?");
            if (dialog.decisaoYesNo() == ButtonType.YES) {
                if (controllerMedida.excluir(vmMedida.getId()) > 0) {
                    dialog.setMensagem("não é possível apagar o registro, contate o administrador");
                    dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                } else {
                    //seleciona o próximo registro ou anterior, para exibir na tela
                    MedidaViewModel medAux = controllerMedida.getProximoRegistro(vmMedida.getId());
                    if (medAux == null) {
                        medAux = controllerMedida.getAnteriorRegistro(vmMedida.getId());
                    }
                    vmMedida = medAux;
                    atualizaInterface(vmMedida);
                    botoes(true);
                }
            }
        }
    }

    @FXML// ok
    private void clkCancelar(ActionEvent event) {
        atualizaInterface(vmMedida);
        botoes(true);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    @FXML// ok - pode ser adicionado mais atalhos
    private void kpbusca(KeyEvent event) {
        //capturar atalhos do teclado
        if (atalhoKey.isBusca(event)) {
            clkBusca(null);
        } else if (atalhoKey.isCancelar(event) && !btnCancelar.disableProperty().get()) {
            clkCancelar(null);
        } else if (atalhoKey.isNovo(event) && !btnNovo.disableProperty().get()) {
            clkNovo(null);
        } else if (atalhoKey.isAlterar(event) && !btnAlterar.disableProperty().get()) {
            clkAlterar(null);
        } else if (atalhoKey.isDuplicar(event)) {
            clkCopia(null);
        } else if (atalhoKey.isSalvar(event) && btnNovo.disableProperty().get()) {
            clkGravar(null);
        } else if (atalhoKey.isExcluir(event) && !btnExcluir.disableProperty().get()) {
            clkExcluir(null);
        }
    }
//------------------------------------------------------------------------------

    private void exibeAlerta(int status) {
        Alerta dialog = new Alerta("Cadastro medida - " + String.valueOf(status), "", "");
        switch (status) {
            case 1:
                dialog.setInformacao("Erro ao gravar", "erro ao gravar no banco de dados");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 2:
                dialog.setInformacao("Código", "para gravação, o código deve estar definido com valor 0 (zero) e para alteração deve ser maior que 0 (zero). Contate o administrador");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
            case 3:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "Nome não pode ser vazio ou menor que 2 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 4:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Nome", "Nome não pode conter mais que 30 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtNome.requestFocus();
                break;
            case 5:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "Sigla não pode ser vazia");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 6:
                tabPane.getSelectionModel().select(tab1);
                dialog.setInformacao("Sigla", "Sigla não pode conter mais que 6 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtSigla.requestFocus();
                break;
            case 7:
                tabPane.getSelectionModel().select(tab2);
                dialog.setInformacao("Observação", "Observação não pode conter mais que 200 caracteres");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                txtObservacao.requestFocus();
                break;
            case 8:
                dialog.setInformacao("Erro ao gravar", "não é possível gravar um objeto nulo");
                dialog.exibeInformacao(Alert.AlertType.INFORMATION);
                break;
        }
    }

    private void atualizaInterface(MedidaViewModel m) {
        if (m != null) {
            txtCodigo.setText(String.valueOf(m.getId()));
            txtNome.setText(m.getNome());
            txtSigla.setText(m.getSigla());
            if (m.isAtivo()) {
                rbAtivo.setSelected(true);
            } else {
                rbInativo.setSelected(true);
            }
            txtObservacao.setText(m.getObservacao());
        } else {
            limparCampos();
        }
    }

    private void botoes(boolean controle) {
        btnNovo.setDisable(!controle);
        btnGravar.setDisable(controle);
        if (vmMedida != null && vmMedida.getId() > 0 && !btnNovo.disableProperty().get()) {
            btnAlterar.setDisable(false);
            btnExcluir.setDisable(false);
        } else {
            btnAlterar.setDisable(true);
            btnExcluir.setDisable(true);
        }
        btnCancelar.setDisable(controle);

        txtCodigo.setEditable(false);
        txtNome.setEditable(!controle);
        txtSigla.setEditable(!controle);
        pnAtivo.setDisable(controle);
        txtObservacao.setEditable(!controle);
    }

    private void limparCampos() {
        txtCodigo.setText("");
        txtNome.setText("");
        txtSigla.setText("");
        rbInativo.selectedProperty().set(true);
        txtObservacao.setText("");
    }
}
