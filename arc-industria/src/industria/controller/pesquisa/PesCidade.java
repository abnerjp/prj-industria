package industria.controller.pesquisa;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import industria.util.Constante;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lib.controller.cadastro.CidadeController;
import lib.viewmodel.cadastro.CidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class PesCidade implements Initializable, Constante {

    private double cx, cy;
    private CidadeViewModel vmCidade = null;
    private final CidadeController controllerCidade = new CidadeController();

    // recebera parâmetro de quem chamou
    // true  - a chamada não veio da tela de cadastro de medida, portanto não será permitido selecionar os desativados
    // false - a chamada veio da tela de cadastro de medida, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;

    @FXML
    private Button btnFechar;
    @FXML
    private JFXTabPane tpItem;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtBusca;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXRadioButton rbCodigo;
    @FXML
    private ToggleGroup tgPesquisa;
    @FXML
    private JFXRadioButton rbNome;
    @FXML
    private JFXRadioButton rbUf;
    @FXML
    private TableView<CidadeViewModel> tbvBusca;
    @FXML
    private TableColumn<CidadeViewModel, Integer> tcCodigo;
    @FXML
    private TableColumn<CidadeViewModel, String> tcNome;
    @FXML
    private TableColumn<CidadeViewModel, String> tcEstado;
    @FXML
    private TableColumn<CidadeViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnSelecionar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXButton btnNovo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //bloqueia botão de adicionar
        btnNovo.setVisible(acessoTerceiro);

        List<CidadeViewModel> lista = new ArrayList<>();
        lista.addAll(controllerCidade.listaLimitada("%%", QTDE_RESULTADO));
        destacarDesativados();
        carregaTabela(lista);
    }

    private void destacarDesativados() {
        tbvBusca.setRowFactory(tv -> {
            return new TableRow<CidadeViewModel>() {
                @Override
                public void updateItem(CidadeViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        vmCidade = null;
        sair();
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML// ok
    private void clkBuscar(ActionEvent event) {
        List<CidadeViewModel> lista = new ArrayList<>();
        if (!txtBusca.getText().isEmpty()) {
            if (rbNome.isSelected()) {
                lista.addAll(controllerCidade.listaPorNome(txtBusca.getText() + "%"));
            } else if (rbUf.isSelected()) {
                lista.addAll(controllerCidade.listaPorEstado(txtBusca.getText() + "%"));
            } else {
                try {
                    int codigo = Integer.parseInt(txtBusca.getText(), 10);
                    CidadeViewModel vmAux = controllerCidade.regPorCodigo(codigo);
                    if (vmAux != null) {
                        lista.add(vmAux);
                    }
                } catch (NumberFormatException ex) {

                }
            }

        } else {
            lista.addAll(controllerCidade.listaLimitada("%%", QTDE_RESULTADO));
        }
        carregaTabela(lista);
        txtBusca.requestFocus();
    }

    @FXML// ok
    private void clkDuploClique(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkSelecionar(null);
        }
    }

    @FXML// ok
    private void clkSelecionar(ActionEvent event) {
        vmCidade = tbvBusca.getSelectionModel().getSelectedItem();
        if (acessoTerceiro && vmCidade != null && !vmCidade.isAtivo()) {
            Alerta dialog = new Alerta("Buscar cidade", "Cidade desativada", "não é possível selecionar um registro desativado");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
        } else {
            sair();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        try {
            Tela tl = new Tela("/industria/view/cadastro/CadCidade.fxml", "Cadastrar Cidade", false, true, true);

            tl.exibirTelaFilha();// exibe a tela
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela de cadastro");
        }
        clkBuscar(null);
    }
//------------------------------------------------------------------------------

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void carregaTabela(List<CidadeViewModel> lista) {
        ObservableList<CidadeViewModel> obsLista;
        tbvBusca.getItems().clear();

        tcCodigo.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        tcEstado.setCellValueFactory(new PropertyValueFactory<>("estado"));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvBusca.setItems(obsLista);
        tbvBusca.getSelectionModel().select(-1);
    }

    /**
     * retorna objeto da tela atual
     *
     * @return
     */
    public CidadeViewModel getCidade() {
        return vmCidade;
    }

    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        //oculta ou não o botão de adicionar se acesso for false
        btnNovo.setVisible(acessoTerceiro);
    }
}
