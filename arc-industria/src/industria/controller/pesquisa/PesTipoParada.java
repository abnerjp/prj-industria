package industria.controller.pesquisa;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import industria.util.Constante;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lib.controller.cadastro.TipoParadaController;
import lib.viewmodel.cadastro.TipoParadaViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class PesTipoParada implements Initializable, Constante {

    private double cx, cy;
    private TipoParadaViewModel vmTipoParada = null;
    private final TipoParadaController controllerTipoParada = new TipoParadaController();

    // recebera parâmetro de quem chamou
    // true  - a chamada não veio da tela de cadastro de medida, portanto não será permitido selecionar os desativados
    // false - a chamada veio da tela de cadastro de medida, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;

    @FXML
    private Button btnFechar;
    @FXML
    private JFXTabPane tpItem;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtBusca;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXRadioButton rbCodigo;
    @FXML
    private ToggleGroup tgPesquisa;
    @FXML
    private JFXRadioButton rbSigla;
    @FXML
    private JFXRadioButton rbNome;
    @FXML
    private TableView<TipoParadaViewModel> tbvBusca;
    @FXML
    private TableColumn<TipoParadaViewModel, Integer> tcCodigo;
    @FXML
    private TableColumn<TipoParadaViewModel, String> tcSigla;
    @FXML
    private TableColumn<TipoParadaViewModel, String> tcNome;
    @FXML
    private TableColumn<TipoParadaViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnSelecionar;
    @FXML
    private JFXButton btnSair;
    @FXML
    private JFXButton btnNovo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //bloqueia botão de adicionar
        btnNovo.setVisible(false);

        List<TipoParadaViewModel> lista = new ArrayList<>();
        lista.addAll(controllerTipoParada.listaLimitada("%%", QTDE_RESULTADO));
        destacarDesativados();
        carregaTabela(lista);
    }

    private void destacarDesativados() {
        tbvBusca.setRowFactory(tv -> {
            return new TableRow<TipoParadaViewModel>() {
                @Override
                public void updateItem(TipoParadaViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        vmTipoParada = null;
        sair();
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML// ok
    private void clkBuscar(ActionEvent event) {
        List<TipoParadaViewModel> lista = new ArrayList<>();
        if (!txtBusca.getText().isEmpty()) {
            if (rbNome.isSelected()) {
                lista.addAll(controllerTipoParada.listaPorNome(txtBusca.getText() + "%"));
            } else if (rbSigla.isSelected()) {
                lista.addAll(controllerTipoParada.listaPorSigla(txtBusca.getText() + "%"));
            } else {
                try {
                    int codigo = Integer.parseInt(txtBusca.getText(), 10);
                    TipoParadaViewModel vmAux = controllerTipoParada.regPorCodigo(codigo);
                    if (vmAux != null) {
                        lista.add(vmAux);
                    }
                } catch (NumberFormatException ex) {

                }
            }

        } else {
            lista.addAll(controllerTipoParada.listaLimitada("%%", QTDE_RESULTADO));
        }
        carregaTabela(lista);
        txtBusca.requestFocus();
    }

    @FXML// ok
    private void clkDuploClique(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkSelecionar(null);
        }
    }

    @FXML// ok
    private void clkSelecionar(ActionEvent event) {
        vmTipoParada = tbvBusca.getSelectionModel().getSelectedItem();
        if (acessoTerceiro && vmTipoParada != null && !vmTipoParada.isAtivo()) {
            Alerta dialog = new Alerta("Buscar tipo de parada", "Tipo desativado", "não é possível selecionar um registro desativado");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
        } else {
            sair();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        try {
            Tela tl = new Tela("/industria/view/cadastro/CadTipoParada.fxml", "Cadastrar Tipo de Parada", false, true, true);

            tl.exibirTelaFilha();// exibe a tela
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela de cadastro");
        }
        clkBuscar(null);
    }
//------------------------------------------------------------------------------

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void carregaTabela(List<TipoParadaViewModel> lista) {
        ObservableList<TipoParadaViewModel> obsLista;
        tbvBusca.getItems().clear();

        tcCodigo.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcSigla.setCellValueFactory(new PropertyValueFactory<>("sigla"));
        tcNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvBusca.setItems(obsLista);
        tbvBusca.getSelectionModel().select(-1);
    }

    /**
     * retorna objeto da tela atual
     *
     * @return
     */
    public TipoParadaViewModel getTipoParada() {
        return vmTipoParada;
    }

    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        //oculta ou não o botão de adicionar se acesso for false
        btnNovo.setVisible(acessoTerceiro);
    }
}
