package industria.controller.pesquisa;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import industria.util.Constante;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lib.controller.cadastro.EnderecoController;
import lib.viewmodel.cadastro.EnderecoViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class PesEndereco implements Initializable, Constante {

    private double cx, cy;
    private EnderecoViewModel vmEndereco = null;
    private final EnderecoController controllerEndereco = new EnderecoController();

    // recebera parâmetro de quem chamou
    // true  - a chamada não veio da tela de cadastro de medida, portanto não será permitido selecionar os desativados
    // false - a chamada veio da tela de cadastro de medida, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;

    @FXML
    private Button btnFechar;
    @FXML
    private JFXTabPane tpItem;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtBusca;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXRadioButton rbCodigo;
    @FXML
    private ToggleGroup tgPesquisa;
    @FXML
    private JFXRadioButton rbEndereco;
    @FXML
    private JFXRadioButton rbCep;
    @FXML
    private JFXRadioButton rbCidade;
    @FXML
    private TableView<EnderecoViewModel> tbvBusca;
    @FXML
    private TableColumn<EnderecoViewModel, Integer> tcCodigo;
    @FXML
    private TableColumn<EnderecoViewModel, String> tcCep;
    @FXML
    private TableColumn<EnderecoViewModel, String> tcEndereco;
    @FXML
    private TableColumn<EnderecoViewModel, String> tcCidade;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnSelecionar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //bloqueia botão de adicionar
        btnNovo.setVisible(false);

        List<EnderecoViewModel> lista = new ArrayList<>();
        lista.addAll(controllerEndereco.listaLimitada("%%", QTDE_RESULTADO));
        carregaTabela(lista);
    }

    @FXML// ok
    private void clkSair(ActionEvent event) {
        vmEndereco = null;
        sair();
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML// ok
    private void clkBuscar(ActionEvent event) {
        List<EnderecoViewModel> lista = new ArrayList<>();
        if (!txtBusca.getText().isEmpty()) {
            if (rbCodigo.isSelected()) {
                try {
                    int codigo = Integer.parseInt(txtBusca.getText(), 10);
                    EnderecoViewModel vmAux = controllerEndereco.regPorCodigo(codigo);
                    if (vmAux != null) {
                        lista.add(vmAux);
                    }
                } catch (NumberFormatException ex) {
                }
            } else if (rbEndereco.isSelected()) {
                lista.addAll(controllerEndereco.listaPorEndereco(txtBusca.getText() + "%"));
            } else if (rbCep.isSelected()) {
                lista.addAll(controllerEndereco.listaPorCep(txtBusca.getText().trim().replace("-", "") + "%"));
            } else {
                lista.addAll(controllerEndereco.listaPorCidade(txtBusca.getText() + "%"));
            }
        } else {
            lista.addAll(controllerEndereco.listaLimitada("%%", QTDE_RESULTADO));
        }
        carregaTabela(lista);
        txtBusca.requestFocus();
    }

    @FXML// ok
    private void clkDuploClique(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkSelecionar(null);
        }
    }

    @FXML// ok
    private void clkSelecionar(ActionEvent event) {
        vmEndereco = tbvBusca.getSelectionModel().getSelectedItem();
        if (acessoTerceiro && vmEndereco == null) {
            Alerta dialog = new Alerta("Buscar endereço", "Endereço não válido", "não é possível selecionar este endereço");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
        } else {
            sair();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        try {
            Tela tl = new Tela("/industria/view/cadastro/CadEndereco.fxml", "Cadastrar Endereço", false, true, true);

            tl.exibirTelaFilha();// exibe a tela
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela de cadastro");
        }
        clkBuscar(null);
    }
//------------------------------------------------------------------------------    

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void carregaTabela(List<EnderecoViewModel> lista) {
        ObservableList<EnderecoViewModel> obsLista;
        tbvBusca.getItems().clear();

        tcCodigo.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcCep.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getCepFormatado()));
        tcEndereco.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getEnderecoFormatado()));
        tcCidade.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getCidade().getCidadeUf()));
        //tcCidade.setCellValueFactory(new PropertyValueFactory<>("cidade"));

        obsLista = FXCollections.observableArrayList(lista);

        tbvBusca.setItems(obsLista);
        tbvBusca.getSelectionModel().select(-1);
    }

    /**
     * retorna objeto da tela atual
     *
     * @return
     */
    public EnderecoViewModel getEndereco() {
        return vmEndereco;
    }

    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        //oculta ou não o botão de adicionar se acesso for false
        btnNovo.setVisible(acessoTerceiro);
    }
}
