package industria.controller.pesquisa;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import industria.util.Constante;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lib.controller.cadastro.UnidadeController;
import lib.viewmodel.cadastro.UnidadeViewModel;

/**
 * FXML Controller class
 *
 * @author abner.jacomo
 */
public class PesUnidade implements Initializable, Constante {

    private double cx, cy;
    private UnidadeViewModel vmUnidade = null;
    private final UnidadeController controllerUnidade = new UnidadeController();

    // recebera parâmetro de quem chamou
    // true  - a chamada não veio da tela de cadastro de medida, portanto não será permitido selecionar os desativados
    // false - a chamada veio da tela de cadastro de medida, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;

    @FXML
    private Button btnFechar;
    @FXML
    private JFXTabPane tpItem;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtBusca;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXRadioButton rbCodigo;
    @FXML
    private ToggleGroup tgPesquisa;
    @FXML
    private JFXRadioButton rbReferencia;
    @FXML
    private JFXRadioButton rbRazaoSocial;
    @FXML
    private JFXRadioButton rbCnpj;
    @FXML
    private TableView<UnidadeViewModel> tbvBusca;
    @FXML
    private TableColumn<UnidadeViewModel, Integer> tcCodigo;
    @FXML
    private TableColumn<UnidadeViewModel, String> tcReferencia;
    @FXML
    private TableColumn<UnidadeViewModel, String> tcRazaoSocial;
    @FXML
    private TableColumn<UnidadeViewModel, String> tcCnpj;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnSelecionar;
    @FXML
    private JFXButton btnSair;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnNovo.setVisible(false);

        List<UnidadeViewModel> lista = new ArrayList<>();
        lista.addAll(controllerUnidade.listaLimitada("%%", QTDE_RESULTADO));
        destacarDesativados();
        carregaTabela(lista);
    }

    private void destacarDesativados() {
        tbvBusca.setRowFactory(tv -> {
            return new TableRow<UnidadeViewModel>() {
                @Override
                public void updateItem(UnidadeViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }

    @FXML
    private void clkSair(ActionEvent event) {
        vmUnidade = null;
        sair();
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML// ok
    private void clkBuscar(ActionEvent event) {
        List<UnidadeViewModel> lista = new ArrayList<>();
        if (!txtBusca.getText().isEmpty()) {
            String textoBusca = txtBusca.getText().trim().replace("%", "");
            if (rbCodigo.isSelected()) {
                try {
                    int codigo = Integer.parseInt(textoBusca, 10);
                    UnidadeViewModel vmAux = controllerUnidade.regPorCodigo(codigo, false);
                    if (vmAux != null) {
                        lista.add(vmAux);
                    }
                } catch (NumberFormatException ex) {
                }
            } else if (rbReferencia.isSelected()) {
                lista.addAll(controllerUnidade.listaPorReferencia(textoBusca + "%"));
            } else if (rbRazaoSocial.isSelected()) {
                lista.addAll(controllerUnidade.listaPorRazaoSocial(textoBusca + "%"));
            } else {
                lista.addAll(controllerUnidade.listaPorCnpj(textoBusca.replace(".", "").replace("-", "").replace("/", "") + "%"));
            }
        } else {
            lista.addAll(controllerUnidade.listaLimitada("%%", QTDE_RESULTADO));
        }
        carregaTabela(lista);
        txtBusca.requestFocus();
    }

    @FXML// ok
    private void clkDuploClique(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkSelecionar(null);
        }
    }

    @FXML// ok
    private void clkSelecionar(ActionEvent event) {
        vmUnidade = tbvBusca.getSelectionModel().getSelectedItem();
        if (acessoTerceiro && vmUnidade != null && !vmUnidade.isAtivo()) {
            Alerta dialog = new Alerta("Buscar unidade", "Unidade desativada", "não é possível selecionar um registro desativado");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
        } else {
            sair();
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        try {
            Tela tl = new Tela("/industria/view/cadastro/CadUnidade.fxml", "Cadastrar Unidade", false, true, true);

            tl.exibirTelaFilha();// exibe a tela
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela de cadastro");
        }
        clkBuscar(null);
    }
//------------------------------------------------------------------------------    

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void carregaTabela(List<UnidadeViewModel> lista) {
        ObservableList<UnidadeViewModel> obsLista;
        tbvBusca.getItems().clear();

        tcCodigo.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcReferencia.setCellValueFactory(new PropertyValueFactory<>("referencia"));
        tcRazaoSocial.setCellValueFactory(new PropertyValueFactory<>("razaoSocial"));
        tcCnpj.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().getCnpjFormatado()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvBusca.setItems(obsLista);
        tbvBusca.getSelectionModel().select(-1);
    }

    /**
     * retorna objeto da tela atual
     *
     * @return
     */
    public UnidadeViewModel getUnidade() {
        return vmUnidade;
    }

    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        //oculta ou não o botão de adicionar se acesso for false
        btnNovo.setVisible(acessoTerceiro);
    }
}
