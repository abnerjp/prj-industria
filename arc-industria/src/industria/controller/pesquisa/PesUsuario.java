/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package industria.controller.pesquisa;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import industria.util.Alerta;
import static industria.util.Constante.QTDE_RESULTADO;
import industria.util.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lib.controller.cadastro.UsuarioController;
import lib.viewmodel.cadastro.UsuarioViewModel;

/**
 * FXML Controller class
 *
 * @author abnerjp
 */
public class PesUsuario implements Initializable {

    private double cx, cy;
    private UsuarioViewModel vmUsuario = null;
    private final UsuarioController controllerUsuario = new UsuarioController();
    
    // recebera parâmetro de quem chamou
    // true  - a chamada não veio da tela de cadastro de medida, portanto não será permitido selecionar os desativados
    // false - a chamada veio da tela de cadastro de medida, portanto será permitido selecionar todos
    private boolean acessoTerceiro = false;
    
    @FXML
    private Button btnFechar;
    @FXML
    private JFXTabPane tpItem;
    @FXML
    private Tab tab1;
    @FXML
    private JFXTextField txtBusca;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXRadioButton rbCodigo;
    @FXML
    private ToggleGroup tgPesquisa;
    @FXML
    private JFXRadioButton rbNome;
    @FXML
    private JFXRadioButton rbLogin;
    @FXML
    private JFXRadioButton rbEmail;
    @FXML
    private TableView<UsuarioViewModel> tbvBusca;
    @FXML
    private TableColumn<UsuarioViewModel, Integer> tcCodigo;
    @FXML
    private TableColumn<UsuarioViewModel, String> tcNome;
    @FXML
    private TableColumn<UsuarioViewModel, String> tcLogin;
    @FXML
    private TableColumn<UsuarioViewModel, String> tcEmail;
    @FXML
    private TableColumn<UsuarioViewModel, String> tcSituacao;
    @FXML
    private JFXButton btnNovo;
    @FXML
    private JFXButton btnSelecionar;
    @FXML
    private JFXButton btnSair;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnNovo.setVisible(false);

        List<UsuarioViewModel> lista = new ArrayList<>();
        lista.addAll(controllerUsuario.listaLimitada("%%", QTDE_RESULTADO));
        destacarDesativados();
        carregaTabela(lista);
    }    

    private void destacarDesativados() {
        tbvBusca.setRowFactory(tv -> {
            return new TableRow<UsuarioViewModel>() {
                @Override
                public void updateItem(UsuarioViewModel obj, boolean novo) {
                    super.updateItem(obj, novo);
                    try {
                        if (!obj.isAtivo()) {
                            setStyle("-fx-text-background-color: red;");
                        } else {
                            setStyle("");
                        }
                    } catch (NullPointerException ex) {
                        //objeto nulo
                    }
                }
            };
        });
    }
    
    @FXML
    private void clkSair(ActionEvent event) {
        vmUsuario = null;
        sair();
    }

    @FXML// ok
    private void clkMouseDrag(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.setX(event.getScreenX() + cx);
        stage.setY(event.getScreenY() + cy);
    }

    @FXML// ok
    private void clkMousePress(MouseEvent event) {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        cx = stage.getX() - event.getScreenX();
        cy = stage.getY() - event.getScreenY();
    }

    @FXML// ok
    private void clkBuscar(ActionEvent event) {
        List<UsuarioViewModel> lista = new ArrayList<>();
        if (!txtBusca.getText().isEmpty()) {
            String textoBusca = txtBusca.getText().trim().replace("%", "");
            if (rbCodigo.isSelected()) {
                try {
                    int codigo = Integer.parseInt(textoBusca, 10);
                    UsuarioViewModel vmAux = controllerUsuario.regPorCodigo(codigo);
                    if (vmAux != null) {
                        lista.add(vmAux);
                    }
                } catch (NumberFormatException ex) {
                }
            } else if (rbNome.isSelected()) {
                lista.addAll(controllerUsuario.listaPorNome(textoBusca + "%"));
            } else if (rbLogin.isSelected()) {
                lista.addAll(controllerUsuario.listaPorLogin(textoBusca + "%"));
            } else {
                lista.addAll(controllerUsuario.listaPorEmail(textoBusca + "%"));
            }
        } else {
            lista.addAll(controllerUsuario.listaLimitada("%%", QTDE_RESULTADO));
        }
        carregaTabela(lista);
        txtBusca.requestFocus();
    }

    @FXML// ok
    private void clkDuploClique(MouseEvent event) {
        if (event.getClickCount() == 2) {
            clkSelecionar(null);
        }
    }

    @FXML// ok
    private void clkNovo(ActionEvent event) {
        try {
            Tela tl = new Tela("/industria/view/cadastro/CadUsuario.fxml", "Cadastrar Usuário", false, true, true);

            tl.exibirTelaFilha();// exibe a tela
        } catch (IOException ex) {
            System.out.println("erro ao exibir janela de cadastro");
        }
        clkBuscar(null);
    }

    @FXML// ok
    private void clkSelecionar(ActionEvent event) {
        vmUsuario = tbvBusca.getSelectionModel().getSelectedItem();
        if (acessoTerceiro && vmUsuario != null && !vmUsuario.isAtivo()) {
            Alerta dialog = new Alerta("Buscar usuário", "Usuário desativado", "não é possível selecionar um registro desativado");
            dialog.exibeInformacao(Alert.AlertType.INFORMATION);
        } else {
            sair();
        }
    }
//------------------------------------------------------------------------------    

    private void sair() {
        Stage stage = (Stage) btnSair.getScene().getWindow();
        stage.close();
    }

    private void carregaTabela(List<UsuarioViewModel> lista) {
        ObservableList<UsuarioViewModel> obsLista;
        tbvBusca.getItems().clear();

        tcCodigo.setCellValueFactory(new PropertyValueFactory<>("id"));
        tcNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        tcLogin.setCellValueFactory(new PropertyValueFactory<>("login"));
        tcEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        tcSituacao.setCellValueFactory(status -> new SimpleStringProperty(status.getValue().isAtivoToString()));

        obsLista = FXCollections.observableArrayList(lista);

        tbvBusca.setItems(obsLista);
        tbvBusca.getSelectionModel().select(-1);
    }
    /**
     * retorna objeto da tela atual
     *
     * @return
     */
    public UsuarioViewModel getUnidade() {
        return vmUsuario;
    }
    
    public void setAcessoTerceiro(boolean acesso) {
        acessoTerceiro = acesso;

        //oculta ou não o botão de adicionar se acesso for false
        btnNovo.setVisible(acessoTerceiro);
    }
    
}
